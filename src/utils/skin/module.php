<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Skin_Bootstrap_Module_Utils_Module extends Utils_Module
{
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();
		Core_Event::attach('Skin_Bootstrap.onLoadSkinConfig', array('Skin_Bootstrap_Module_Utils_Module', 'onLoadSkinConfig'));
	}

	static public function onLoadSkinConfig($object, $args)
	{
		// Load config
		$aConfig = $object->getConfig();

		if(!isset($aConfig['adminMenu']['utilites'])) {
			$aConfig['adminMenu']['utilites'] = array(
				'ico' => 'fa fa-wrench',
				'caption' => 'Утилиты',
			);
		}

		$aConfig['adminMenu']['utilites']['modules'][] = 'utils';

		// Set new config
		$object->setConfig($aConfig);
	}
}