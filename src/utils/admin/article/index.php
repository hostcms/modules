<?php
use Utils_Utl as utl;
/**
 * Online shop.
 *
 * @package HostCMS
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2014 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */

!defined('CURRENT_SITE') && define('CURRENT_SITE',2);
require_once(__DIR__.'/../../../../../../../bootstrap.php');

// Код формы
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid('FBEF86C8-9BE1-9AD8-381A-D8CC7D6FD46E');
$iAdmin_Form_Id = $oAdmin_Form->id;
$sFormAction = '/admin/utils/article';


// Меню формы
$oMenu = Admin_Form_Entity::factory('Menus');

// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->setUp()
	->path($sFormAction)
	->title('Управление статьями')
	->pageTitle('Управление статьями');
//----------------------------------------------------------------------------------------------------------------------
// Элементы меню
$oMenu->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Shop_Item.links_items'))
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Shop_Item.links_items_add'))
				->img('/admin/images/page_add.gif')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref
					(
						$oAdmin_Form_Controller->getPath(), 'addArticle', NULL, 1, 0
					)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax
					(
						$oAdmin_Form_Controller->getPath(), 'addArticle', NULL, 1, 0
					)
				)
		)
);
// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oMenu);
$oAdminFormActionArtEdit = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('addArticle');

if ($oAdminFormActionArtEdit && $oAdmin_Form_Controller->getAction() == 'addArticle')
{
	$oShopItemControllerArtEdit = Admin_Form_Action_Controller::factory(
		'Utils_Admin_Panel_Article_Form_Action_Controller', $oAdminFormActionArtEdit
	);

	$oShopItemControllerArtEdit
		->title("Управление статьями")
	;
	// Добавляем типовой контроллер редактирования контроллеру формы
	$oAdmin_Form_Controller->addAction($oShopItemControllerArtEdit);
}
//----------------------------------------------------------------------------------------------------------------------
$oDataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Shop_Item'));
$oAdmin_Form_Controller->addDataset($oDataset);

// Показ формы
$oAdmin_Form_Controller->execute();