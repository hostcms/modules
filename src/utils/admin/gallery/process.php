<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
require_once(__DIR__.'/../../../../../../../bootstrap.php');

Core_Auth::authorization($sModule = 'utils');
Core::parseUrl();

header('Content-Type: application/json');

$oSite = Core_Entity::factory('Site', CURRENT_SITE);

$shopId = 0;
$result =  array();

$operation = Core_Array::getRequest('operation', 'none');
$galleryType = Core_Array::getRequest('galleryType', 'photos');

$itemID = Core_Array::getRequest('itemID', -1);
$itemFounded = ($itemID>0) ? Core_Entity::factory('Shop_Item', $itemID) : NULL;
(!is_null($itemFounded) && isset($itemFounded->shop_id)) && $shopId=$itemFounded->shop_id;
(!is_null($itemFounded) && isset($itemFounded->shop_id)) && $itemPath = '/'.$itemFounded->getPath();
$currenttime = str_replace('-', '', microtime(true));

$itemIDs = Core_Array::getRequest('itemIDs', array());
$itemParent = $itemFounded;
$currentShop = NULL;
(!is_null($itemFounded)) && $currentShop = Core_Entity::factory('Shop')->getById($itemFounded->shop_id);

if(isset($currentShop->id) && ($currentShop->id>0) || ($operation=='deleteItem' || $operation=='deleteItems' || $operation=='reorderItems')) {
	$filesProperty = isset($currentShop->id) ? Core_Entity::factory('Property')->getByGuid($currentShop->id.".utils.gallery.files") : NULL;
	$filesPropertyID = (isset($filesProperty->id) ? $filesProperty->id : 0);
//	if($imagesPropertyID==0) {
		$aPropertyMatches = [];
		preg_match('/^prop_img_(\d+)/', $galleryType, $aPropertyMatches);
		$imagesPropertyID = Core_Array::get($aPropertyMatches, 1, 0);

	if($imagesPropertyID == 0){
		$imagesProperty = isset($currentShop->id) ? Core_Entity::factory('Property')->getByGuid($currentShop->id.".utils.gallery.images") : NULL;
		$imagesPropertyID = (isset($imagesProperty->id) ? $imagesProperty->id : 0);
	}else{
		$galleryType = 'images';
	}

	$informationSystem = isset($currentShop->id) ? $currentShop->site->informationsystems->getByName('Фотогалерея.'.$currentShop->id) : NULL;
	$informationSystemID = (isset($informationSystem->id) ? $informationSystem->id : 0);
	switch($operation) {
		case 'getList':
			switch(TRUE) {
				case $informationSystemID>0 && $galleryType=='photos':
					$srcItems = Utils_Admin_Panel_Gallery_Ajax_Controller::getGalleryListByShopItem($itemFounded->id);
					if(count($srcItems['objects'])>0) {
						foreach($srcItems['objects'] as $photo) {
							$result[] = array(
								"src" => $photo->getSmallFileHref()
							, "size" => Core_File::filesize($photo->getSmallFilePath())
							, "largeSrc" => $photo->getLargeFileHref()
							, "largeSize" => Core_File::filesize($photo->getLargeFilePath())
							, "name" => $photo->name
							, "itemID" => $photo->id
							);
						}
					}
//					$isGroup = utl::getGroupObjectByPath($itemPath, $currentShop->id, $informationSystemID);
//					if(isset($isGroup->informationsystem_items)) {
//						$photos = $isGroup->informationsystem_items->findAll();
//						foreach($photos as $photo) {
//							$result[] = array(
//							  "src" => $photo->getSmallFileHref()
//							, "size" => Core_File::filesize($photo->getSmallFilePath())
//							, "largeSrc" => $photo->getLargeFileHref()
//							, "largeSize" => Core_File::filesize($photo->getLargeFilePath())
//							, "name" => $photo->name
//							, "itemID" => $photo->id
//							);
//						}
//					}
					break;
				case $filesPropertyID> 0 && $galleryType=='files':
					$items = array();
					$oProperty = Core_Entity::factory('Property', $filesPropertyID);
					$aProperty_Values = $oProperty->getValues($itemFounded->id, FALSE);
					foreach($aProperty_Values as $item) {
						$fileHref = $itemFounded->getItemHref().$item->getLargeFilePath();
						$filePath = $itemFounded->getItemPath().$item->getLargeFilePath();
						$ext = Core_File::getExtension($item->file_name);
						$fileIcon = '';
						switch(true) {
							case $ext=='doc':
								$fileIcon = '/modules/utils/images/type/doc.png';
								break;
							case $ext=='docx':
								$fileIcon = '/modules/utils/images/type/docx.png';
								break;
							case $ext=='csv':
								$fileIcon = '/modules/utils/images/type/csv.png';
								break;
							case $ext=='xls':
								$fileIcon = '/modules/utils/images/type/xls.png';
								break;
							case $ext=='xlsx':
								$fileIcon = '/modules/utils/images/type/xlsx.png';
								break;
							case $ext=='pdf':
								$fileIcon = '/modules/utils/images/type/pdf.png';
								break;
							default:
								$fileIcon = $fileHref;
						}
						$result[] = array(
							"src" => $fileIcon
						, "size" => Core_File::filesize($filePath)
						, "largeSrc" => ""
						, "largeSize" => ""
						, "name" => $item->file_name
						, "itemID" => $item->id
						);
					}
					break;
				case $imagesPropertyID > 0 && $galleryType=='images':
					$items = array();
					$oProperty = Core_Entity::factory('Property', $imagesPropertyID);
					$aProperty_Values = $oProperty->getValues($itemFounded->id, FALSE);
					foreach($aProperty_Values as $item) {
						$fileHref = $itemFounded->getItemHref().$item->getLargeFilePath();
						$filePath = $itemFounded->getItemPath().$item->getLargeFilePath();
						$ext = Core_File::getExtension($item->file_name);
						$fileIcon = $itemFounded->getItemHref().$item->file;

						$result[] = array(
							"src" => $fileIcon
						, "size" => Core_File::filesize($filePath)
						, "largeSrc" => ""
						, "largeSize" => ""
						, "name" => $item->file_name
						, "itemID" => $item->id
						);
					}
					break;
			}
			break;
		case 'addItem':
			switch(TRUE) {
				case $galleryType=='photos':
					$isGroup = utl::getGroupObjectByPath($itemPath, $currentShop->id, $informationSystemID, true);
					$itemModelNew = Core_Entity::factory('Informationsystem_Item');
					$itemModelNew->informationsystem_id = $informationSystemID;  //-- Текущая ИС --
					$itemModelNew->informationsystem_group_id = $isGroup->id;    //-- По умолчанию - корень магазина --
					$itemModelNew->indexing = 0;
					$itemModelNew->name = "Фото ".str_replace('.', '', $currenttime);
					$itemNew = $itemModelNew->save();
					utl::setShopImage($itemModelNew, $_FILES['file']['tmp_name'], $currentShop);
					$result =  array(
						'status' => "OK",
						'message' => "",
						'itemID' => $itemNew->id,
					);
					break;
				case $filesPropertyID> 0 && $galleryType=='files':
					if(!is_null($fileType = utl::getMimeFileType($_FILES['file']['name']))) {
						$fileExt = array_keys($fileType)[0];
						switch(strtolower($fileExt)) {
							case 'webp':
							case 'doc':
							case 'docx':
							case 'xls':
							case 'xlsx':
							case 'csv':
							case 'pdf':
							case 'png':
							case 'jpg':
							case 'jpeg':
								$pvf = Core_Entity::factory('Property_Value_File');
								$pvf->entity_id = $itemID;
								$pvf->property_id = $filesPropertyID;
								$pvf->save();
								$processed = utl::setShopImage($pvf, $_FILES['file']['tmp_name'], $currentShop, $itemParent->getItemPath(), '', $fileExt, $_FILES['file']['name']);
								$result =  array(
									'status' => "OK",
									'message' => "",
									'itemID' => $pvf->id,
								);
								break;
							default:
								$result =  array(
									'status' => "ERROR",
									'message' => "Тип не поддерживается",
									'itemID' => -1,
								);
						}
					} else {
						$result =  array(
							'status' => "ERROR",
							'message' => "Тип не определен",
							'itemID' => -1,
						);
					}
					break;
				case $imagesPropertyID > 0 && $galleryType=='images':
					if(!is_null($fileType = utl::getMimeFileType($_FILES['file']['name']))) {
						$fileExt = array_keys($fileType)[0];
						switch( strtolower($fileExt) ) {
							case 'jpg':
							case 'jpeg':
							case 'png':
							case 'gif':
								$pvf = Core_Entity::factory('Property_Value_File');
								$pvf->entity_id = $itemID;
								$pvf->property_id = $imagesPropertyID;
								$pvf->save();
								$processed = utl::setShopImage($pvf, $_FILES['file']['tmp_name'], $currentShop, $itemParent->getItemPath(), '', $fileExt, $_FILES['file']['name']);
								$result =  array(
									'status' => "OK",
									'message' => "",
									'itemID' => $pvf->id,
								);
								break;
							default:
								$result =  array(
									'status' => "ERROR",
									'message' => "Тип не поддерживается",
									'itemID' => -1,
								);
						}
					} else {
						$result =  array(
							'status' => "ERROR",
							'message' => "Тип не определен",
							'itemID' => -1,
						);
					}
					break;
				default:
					$result =  array(
						'status' => "ERROR",
						'message' => "Тип не найден",
						'itemID' => -1,
					);
			}
			break;
		case 'reorderItems':
			foreach($itemIDs as $k => $itemID) {
				switch($galleryType) {
					case 'photos':
						$itemModel = Core_Entity::factory('Informationsystem_Item', $itemID);
						break;
					case 'files':
						$itemModel = Core_Entity::factory('Property_Value_File', $itemID);
						if(!is_null($itemModel) && !is_null($item = $itemModel->property->shop_item_property->shop->shop_items->getById($itemModel->entity_id))) {
							is_file($item->getItemPath().$itemModel->file) && unlink( $item->getItemPath().$itemModel->file);
							is_file($item->getItemPath().$itemModel->file_small) && unlink( $item->getItemPath().$itemModel->file_small );
						}
						break;
				}
				$ki = 1;
				if(!is_null($itemModel) && isset($itemModel->sorting)) {
					$itemModel->sorting = $k*10 + 1000*$ki;
					$itemModel->save();
				}
			}
			$result =  array(
				'status' => "OK",
				'message' => "",
				'reorderedItemIds' => $itemIDs,
			);
			break;
		case 'deleteItem':
			$itemModel = NULL;
			switch($galleryType) {
				case 'photos':
					$itemModel = Core_Entity::factory('Informationsystem_Item', $itemID);
					break;
				case 'files':
				case 'images':
					$itemModel = Core_Entity::factory('Property_Value_File', $itemID);
					if(!is_null($itemModel) && !is_null($item = $itemModel->property->shop_item_property->shop->shop_items->getById($itemModel->entity_id))) {
						is_file($item->getItemPath().$itemModel->file) && unlink( $item->getItemPath().$itemModel->file);
						is_file($item->getItemPath().$itemModel->file_small) && unlink( $item->getItemPath().$itemModel->file_small );
					}
					break;
			}
			if(!is_null($itemModel)) {
				$itemModel->delete();
			}
			$result =  array(
				'status' => "OK",
				'message' => "",
				'itemID' => $itemID,
			);
			break;
		case 'deleteItems':
			if(count($itemIDs)>0) {
				foreach($itemIDs as $itemID) {
					$itemModel = NULL;
					switch($galleryType) {
						case 'photos':
							$itemModel = Core_Entity::factory('Informationsystem_Item', $itemID);
							break;
						case 'files':
						case 'images':
							$itemModel = Core_Entity::factory('Property_Value_File', $itemID);
							if(!is_null($itemModel) && !is_null($item = $itemModel->property->shop_item_property->shop->shop_items->getById($itemModel->entity_id))) {
								is_file($item->getItemPath().$itemModel->file) && unlink( $item->getItemPath().$itemModel->file);
								is_file($item->getItemPath().$itemModel->file_small) && unlink( $item->getItemPath().$itemModel->file_small );
							}
							break;
					}
					if(!is_null($itemModel)) {
						$itemModel->delete();
					}
				}
			}
			$result =  array(
				'status' => "OK",
				'message' => "",
				'deletedItems' => $itemIDs,
			);
			break;
		case 'saveMainImage':
			if (in_array($_FILES['image']['type'], array('image/jpeg', 'image/gif', 'image/png'))) {
				// Обработка картинок
				$param = array();
				$modelName = 'shop_item';
				!defined('CHMOD_FILE') && define('CHMOD_FILE', octdec(644));
				!defined('CHMOD') && define('CHMOD', octdec(777));

				$large_image = $small_image = '';

				$aCore_Config = Core::$mainConfig;

				$create_small_image_from_large = Core_Array::getPost(
					'create_small_image_from_large_small_image');

				$bLargeImageIsCorrect =
					// Поле файла большого изображения существует
					!is_null($aFileData = Core_Array::getFiles('image', NULL))
					// и передан файл
					&& intval($aFileData['size']) > 0;

				if ($bLargeImageIsCorrect)
				{
					// Проверка на допустимый тип файла
					if (Core_File::isValidExtension($aFileData['name'], $aCore_Config['availableExtension']))
					{
						// Удаление файла большого изображения
						if ($itemFounded->image_large)
						{
							// !! дописать метод
							$itemFounded->deleteLargeImage();
						}

						$file_name = $aFileData['name'];

						// Не преобразовываем название загружаемого файла
						if (!$currentShop->change_filename)
						{
							$large_image = $file_name;
						}
						else
						{
							// Определяем расширение файла
							$ext = Core_File::getExtension($aFileData['name']);
							//$large_image = 'information_groups_' . $itemFounded->id . '.' . $ext;

							$large_image =
								($modelName == 'shop_item'
									? 'shop_items_catalog_image'
									: 'shop_group_image') . $itemFounded->id . '.' . $ext;
						}
					}
					else
					{
						$this->addMessage(Core_Message::get(Core::_('Core.extension_does_not_allow', Core_File::getExtension($aFileData['name'])), 'error'));
					}
				}

				$aSmallFileData = Core_Array::getFiles('small_image', NULL);
				$bSmallImageIsCorrect =
					// Поле файла малого изображения существует
					!is_null($aSmallFileData)
					&& $aSmallFileData['size'];

				// Задано малое изображение и при этом не задано создание малого изображения
				// из большого или задано создание малого изображения из большого и
				// при этом не задано большое изображение.

				if ($bSmallImageIsCorrect || $create_small_image_from_large && $bLargeImageIsCorrect)
				{
					// Удаление файла малого изображения
					if ($itemFounded->image_small)
					{
						// !! дописать метод
						$itemFounded->deleteSmallImage();
					}

					// Явно указано малое изображение
					if ($bSmallImageIsCorrect
						&& Core_File::isValidExtension($aSmallFileData['name'],
							$aCore_Config['availableExtension']))
					{
						// Для инфогруппы ранее задано изображение
						if ($itemFounded->image_large != '')
						{
							// Существует ли большое изображение
							$param['large_image_isset'] = true;
							$create_large_image = false;
						}
						else // Для информационной группы ранее не задано большое изображение
						{
							$create_large_image = empty($large_image);
						}

						$file_name = $aSmallFileData['name'];

						// Не преобразовываем название загружаемого файла
						if (!$currentShop->change_filename)
						{
							if ($create_large_image)
							{
								$large_image = $file_name;
								$small_image = 'small_' . $large_image;
							}
							else
							{
								$small_image = $file_name;
							}
						}
						else
						{
							// Определяем расширение файла
							$ext = Core_File::getExtension($file_name);

							$small_image =
								($modelName == 'shop_item'
									? 'small_shop_items_catalog_image'
									: 'small_shop_group_image') . $itemFounded->id . '.' . $ext;

						}
					}
					elseif ($create_small_image_from_large && $bLargeImageIsCorrect)
					{
						$small_image = 'small_' . $large_image;
					}
					// Тип загружаемого файла является недопустимым для загрузки файла
					else
					{
						$this->addMessage(Core_Message::get(Core::_('Core.extension_does_not_allow', Core_File::getExtension($aSmallFileData['name'])), 'error'));
					}
				}

				if ($bLargeImageIsCorrect || $bSmallImageIsCorrect)
				{
					if ($bLargeImageIsCorrect)
					{
						// Путь к файлу-источнику большого изображения;
						$param['large_image_source'] = $aFileData['tmp_name'];
						// Оригинальное имя файла большого изображения
						$param['large_image_name'] = $aFileData['name'];
					}

					if ($bSmallImageIsCorrect)
					{
						// Путь к файлу-источнику малого изображения;
						$param['small_image_source'] = $aSmallFileData['tmp_name'];
						// Оригинальное имя файла малого изображения
						$param['small_image_name'] = $aSmallFileData['name'];
					}

					if ($modelName == 'shop_group')
					{
						// Путь к создаваемому файлу большого изображения;
						$param['large_image_target'] = !empty($large_image)
							? $itemFounded->getGroupPath() . $large_image
							: '';

						// Путь к создаваемому файлу малого изображения;
						$param['small_image_target'] = !empty($small_image)
							? $itemFounded->getGroupPath() . $small_image
							: '' ;
					}
					else
					{
						// Путь к создаваемому файлу большого изображения;
						$param['large_image_target'] = !empty($large_image)
							? $itemFounded->getItemPath() . $large_image
							: '';

						// Путь к создаваемому файлу малого изображения;
						$param['small_image_target'] = !empty($small_image)
							? $itemFounded->getItemPath() . $small_image
							: '' ;
					}

					// Использовать большое изображение для создания малого
					$param['create_small_image_from_large'] = !is_null(Core_Array::getPost('create_small_image_from_large_small_image'));

					// Значение максимальной ширины большого изображения
					$param['large_image_max_width'] = Core_Array::getPost('large_max_width_image', 0);

					// Значение максимальной высоты большого изображения
					$param['large_image_max_height'] = Core_Array::getPost('large_max_height_image', 0);

					// Значение максимальной ширины малого изображения;
					$param['small_image_max_width'] = Core_Array::getPost('small_max_width_small_image');

					// Значение максимальной высоты малого изображения;
					$param['small_image_max_height'] = Core_Array::getPost('small_max_height_small_image');

					// Путь к файлу с "водяным знаком"
					$param['watermark_file_path'] = $currentShop->getWatermarkFilePath();

					// Позиция "водяного знака" по оси X
					$param['watermark_position_x'] = Core_Array::getPost('watermark_position_x_image');

					// Позиция "водяного знака" по оси Y
					$param['watermark_position_y'] = Core_Array::getPost('watermark_position_y_image');

					// Наложить "водяной знак" на большое изображение (true - наложить (по умолчанию), false - не наложить);
					$param['large_image_watermark'] = !is_null(Core_Array::getPost('large_place_watermark_checkbox_image'));

					// Наложить "водяной знак" на малое изображение (true - наложить (по умолчанию), false - не наложить);
					$param['small_image_watermark'] = !is_null(Core_Array::getPost('small_place_watermark_checkbox_small_image'));

					// Сохранять пропорции изображения для большого изображения
					$param['large_image_preserve_aspect_ratio'] = !is_null(Core_Array::getPost('large_preserve_aspect_ratio_image'));

					// Сохранять пропорции изображения для малого изображения
					$param['small_image_preserve_aspect_ratio'] = !is_null(Core_Array::getPost('small_preserve_aspect_ratio_small_image'));

					$itemFounded->createDir();

					$result = Core_File::adminUpload($param);

					if ($result['large_image'])
					{
						$itemFounded->image_large = $large_image;
						$itemFounded->setLargeImageSizes();
					}

					if ($result['small_image'])
					{
						$itemFounded->image_small = $small_image;
						$itemFounded->setSmallImageSizes();
					}
				}

//				$itemFounded->save();

			}
//			utl::p($_FILES['image']);
			break;
	}
} else {
	$result =  array(
		'status' => "ERROR",
		'message' => "Не найден магазин для элемента",
	);
}
echo json_encode($result);
