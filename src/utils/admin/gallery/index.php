<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
require_once(__DIR__.'/../../../../../../../bootstrap.php');

Core_Auth::authorization($sModule = 'utils');

// Код формы
$sFormAction = '/admin/utils/gallery/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid('1F0264AF-0129-1749-96DD-A661BB8ECB00');
$iAdmin_Form_Id = $oAdmin_Form->id;

// Меню формы
//$oMenu = Admin_Form_Entity::factory('Menus');

// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->setUp()
	->path($sFormAction)
	->title('Добавить галереи')
	->pageTitle('Добавить галереи');

//// Добавляем все меню контроллеру
$oAdminFormActionAddGallery = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('addGallery');

if ($oAdminFormActionAddGallery && $oAdmin_Form_Controller->getAction() == 'addGallery')
{
	$oShopItemControllerAddGallery = Admin_Form_Action_Controller::factory(
		'Utils_Admin_Panel_Gallery_Form', $oAdminFormActionAddGallery
	);

	$oShopItemControllerAddGallery
		->title("Добавить галерею")
	;
	// Добавляем типовой контроллер редактирования контроллеру формы
	$oAdmin_Form_Controller->addAction($oShopItemControllerAddGallery);
}

$oDataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Shop_Item'));
// Ограничение источника 0 по родительской группе
$oDataset->addCondition(
	array('where' =>
		array('modification_id', '=', 0)
	)
);

// Действие "Удаление файла большого изображения"
$oAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('deleteLargeImage');

if ($oAction && $oAdmin_Form_Controller->getAction() == 'deleteLargeImage')
{
	$oDeleteLargeImageController = Admin_Form_Action_Controller::factory(
		'Admin_Form_Action_Controller_Type_Delete_File', $oAction
	);

	$oDeleteLargeImageController
		->methodName('deleteLargeImage')
		->divId(array('preview_large_image', 'delete_large_image'));

	// Добавляем контроллер удаления изображения к контроллеру формы
	$oAdmin_Form_Controller->addAction($oDeleteLargeImageController);
}

// Действие "Удаление файла малого изображения"
$oAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('deleteSmallImage');

if ($oAction && $oAdmin_Form_Controller->getAction() == 'deleteSmallImage')
{
	$oDeleteSmallImageController = Admin_Form_Action_Controller::factory(
		'Admin_Form_Action_Controller_Type_Delete_File', $oAction
	);

	$oDeleteSmallImageController
		->methodName('deleteSmallImage')
		->divId(array('preview_small_image', 'delete_small_image'));

	$oAdmin_Form_Controller->addAction($oDeleteSmallImageController);
}

$oAdmin_Form_Controller->addDataset($oDataset);

// Показ формы
$oAdmin_Form_Controller->execute();