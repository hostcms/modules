<?php
use Utils_Utl as utl;
//use PHPExcel;

/**
 * Entity.
 *
 * @package HostCMS
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2013 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
require_once(__DIR__.'/../../../../../../../../bootstrap.php');

Core_Auth::authorization('utils');

$aResult =  array(
	'status' => "ERROR",
	'message' => "Низвестная ошибка",
);

$action = Core_Array::getRequest('action', '');

switch ($action) {
	case 'updateselector':
		switch (true) {
			case isset($_POST['wh_ordered']) && is_array($_POST['wh_ordered']):
				try {
					foreach ($_POST['wh_ordered'] as $shopOrderID=>$whItem) {
						foreach ($whItem as $whOrderItemID=>$arWhOrderItem) {
							$whOrderItemModel = Core_Entity::factory('Utils_Shop_Order_Item_Warehouse');
							$wh = Core_Entity::factory('Shop_Warehouse_Item')->getById($arWhOrderItem['warehouse']*1);
							$whID = (isset($wh->shop_warehouse_id) && $wh->shop_warehouse_id>0) ? $wh->shop_warehouse_id : 0;
							if($whOrderItemID>0) {
								$whOrderItemModel = $whOrderItemModel->getById($whOrderItemID);
							}
							$purshasePrice = 0;
							if($whID>0) {
								$purshasePrice = Utils_Shop_Item_Price_Model::getPurshasePriceByWarehouseID($wh->shop_item_id, $whID);
							}
							$whOrderItemModel->shop_order_item_id=$shopOrderID;
							$whOrderItemModel->shop_warehouse_item_id=$arWhOrderItem['warehouse'];
							$whOrderItemModel->warehouse_order_count=$arWhOrderItem['count'];
//							$whOrderItemModel->order_warehouse_id=$whID;
							$whOrderItemModel->warehouse_order_price=(($arWhOrderItem['price']*1)>0) ? $arWhOrderItem['price'] : $purshasePrice;
							$whOrderItemModel->warehouse_back_count=isset($arWhOrderItem['back']['count']) ? $arWhOrderItem['back']['count'] : 0;
							$whOrderItemModel->warehouse_back_price=isset($arWhOrderItem['back']['price']) ? $arWhOrderItem['back']['price'] : 0;
							$whOrderItemModel->save();
						}
					}
					$aResult =  array(
						'orderItem' => $whOrderItemModel->toArray(),
						'status' => "OK",
						'message' => "OK",
					);
				} catch (Exception $e) {
					$aResult =  array(
						'status' => "ERROR",
						'message' => $e->getMessage(),
					);
				}
				break;
		}
		break;
	case 'addintoorder':
		if(($orderID=Core_Array::getGet('orderid', 0))>0 && ($itemID=Core_Array::getGet('itemid', 0))>0) {
			$shopOrder = Core_Entity::factory('Shop_Order')->getByID($orderID);
			$oShop_Item = Core_Entity::factory('Shop_Item')->getByID($itemID);
			$mShop_Order_Item = Core_Entity::factory('Shop_Order_Item');
			$mShop_Order_Item
				->queryBuilder()
				->where('shop_order_id', '=', $orderID*1)
				->where('price', '=', $oShop_Item->price*1)
			;
			$oShop_Order_Item = $mShop_Order_Item->getByShop_item_id($oShop_Item->id, FALSE);

			// Prices
			$oShop_Item_Controller = new Shop_Item_Controller();
			if (Core::moduleIsActive('siteuser'))
			{
				$oSiteuser = Core_Entity::factory('Siteuser')->getCurrent();
				$oSiteuser && $oShop_Item_Controller->siteuser($oSiteuser);
			}
			$aPrices = $oShop_Item_Controller->calculatePrice($oShop_Item->price, $oShop_Item);

			if(is_null($oShop_Order_Item)) {
				$oShop_Order_Item = Core_Entity::factory('Shop_Order_Item');
				$oShop_Order_Item->quantity = 1;
				$oShop_Order_Item->shop_item_id = $oShop_Item->id;
				$oShop_Order_Item->price = $aPrices['price_tax']; // $aPrices['price_discount'] - $aPrices['tax'];
				$oShop_Order_Item->name = $oShop_Item->name;
				$oShop_Order_Item->type = 0;
				$oShop_Order_Item->marking = $oShop_Item->marking;
				$shopOrder->add($oShop_Order_Item);
			} else {
				$oShop_Order_Item->quantity++;
				$oShop_Order_Item->save();
			}
			$aResult =  array(
				'orderItem' => $oShop_Order_Item->toArray(),
				'status' => "OK",
				'message' => "OK",
			);
		}
		break;
	case 'updateorderstatus':
		$orderIDs = Core_Array::getRequest('id', array());
		$returnes = array();
		$processStatusID = Core_Array::getRequest('statusid', 7);
		$orderStatus = Core_Entity::factory('Shop_Order_Status')->getById($processStatusID);
		if(count($orderIDs)>0 && isset($orderStatus->id) && $orderStatus->id>0) {
			$isError = false;
			foreach ($orderIDs as $orderID) {
				$order = Core_Entity::factory('Shop_Order')->getById($orderID, false);
//				if( $order->shop_order_status_id==3 && !in_array(Core_Page::instance()->user->id, array(20,21,23) ) ) {
//					$returnes[] = array('orderid'=>$orderID, 'name' => $orderStatus->name);
//					$isError = true;
//					$aResult['message']='Недостаточно прав для выполнения операции';
//					break;
//				} else {
				$order->shop_order_status_id = $processStatusID;
				$order->save();
				$returnes[] = array('orderid'=>$orderID, 'name' => $orderStatus->name);
//				}
			}
			if(!$isError) {
				$aResult =  array(
					'orderIDs' => $returnes,
					'status' => "OK",
					'message' => "OK",
				);
			}
		} else {
			$aResult['message']='Ошибка обновления статусов заказа';
		}
		break;
	case 'makeXLSorderShippers':
		$cDt = date('Y-m-d H:i:s');
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()->setCreator("3Mega.ru");
		$objPHPExcel->getProperties()->setLastModifiedBy("3Mega.ru");
		$objPHPExcel->getProperties()->setTitle("Заказать запчасти у поставщиков, {$cDt}");
		$objPHPExcel->getProperties()->setSubject("Заказать запчасти у поставщиков, {$cDt}");
		$objPHPExcel->getProperties()->setDescription("Заказать запчасти у поставщиков, {$cDt}");
		$objPHPExcel->setActiveSheetIndex(0);
		$activeSheet = $objPHPExcel->getActiveSheet();
		$columnsCount = 3;
		$activeSheet->SetCellValue('A1', 'Код');
		$activeSheet->SetCellValue('B1', 'Наименование');
		$activeSheet->SetCellValue('C1', 'Заказы');
		$activeSheet->SetCellValue('D1', 'Кол-во');


		$objPHPExcel->getActiveSheet()->setTitle('Simple');

		$orderIDs = Core_Array::getRequest('orders', array(-1));
		$arItemsShopItems = Utils_Shop_Order_Observers::arOrdersShippers($orderIDs);
		$arItems = $arItemsShopItems['arItems'];
		$shopItems = $arItemsShopItems['shopItems'];

		$colKeyCodeFirst = 0x41;
		$colKeyCodeSecond = $colKeyCodeFirst+1;
		$colKeyCodeLast = $colKeyCodeFirst+$columnsCount;
		$colFirst = chr($colKeyCodeFirst);
		$colSecond = chr($colKeyCodeSecond);
		$colLast = chr($colKeyCodeLast);
		$rowNum = 2;
		foreach ($arItems as $arItemShipperKey => $arItemShipper) {
			$activeSheet->mergeCells("{$colFirst}{$rowNum}:{$colLast}{$rowNum}");
			$activeSheet->SetCellValue("{$colFirst}{$rowNum}", $arItemShipperKey);
			$activeSheet->getStyle("{$colFirst}{$rowNum}")->applyFromArray(array(
				'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'A0D468')
				)
			));
			$activeSheet->getStyle("{$colFirst}{$rowNum}")->getFont()->setBold(true);
			$itemsOrder = utl::getArrayKeyValuesFromArrays($arItemShipper, 'wh');
			$rowNum++;
			foreach ($itemsOrder as $itemsOrderKey=>$itemOrder) {
				$activeSheet->mergeCells("{$colSecond}{$rowNum}:{$colLast}{$rowNum}");
				$activeSheet->SetCellValue("{$colSecond}{$rowNum}", $itemsOrderKey);
				$activeSheet->getStyle("{$colSecond}{$rowNum}")->applyFromArray(array(
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => 'fffe9a'),

					)
				));
				$activeSheet->getStyle("{$colSecond}{$rowNum}")->getFont()->setBold(true);
				$rowNum++;
				foreach ($itemOrder as $itemOrderItem) {
					$activeSheet->SetCellValue("A{$rowNum}", $itemOrderItem['shipper_code']);
					$activeSheet->SetCellValue("B{$rowNum}", $itemOrderItem['name']);
					$activeSheet->SetCellValue("C{$rowNum}", strip_tags($itemOrderItem['orders']));
					$activeSheet->SetCellValue("D{$rowNum}", $itemOrderItem['cnt']);
					$rowNum++;
				}
			}
		}
		foreach(range($colFirst,$colLast) as $columnID) {
			$objPHPExcel->getActiveSheet()->getColumnDimension($columnID)
				->setAutoSize(true);
		}
		$objPHPExcel->getActiveSheet()->calculateColumnWidths();

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="OffersOrders-'.$cDt.'.xls"');
		header('Cache-Control: max-age=0');
		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
		$writer->save('php://output');
		break;
	default:
		$aResult['message']='Неизвестная операция';
}

header('Content-Type: application/json');
echo json_encode($aResult);
