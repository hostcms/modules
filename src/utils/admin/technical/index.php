<?php
/**
 * Online shop.
 *
 * @package HostCMS
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2014 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
// CONST CURRENT_SITE = 3;
!defined('CURRENT_SITE') && define('CURRENT_SITE',2);
!defined('CURRENT_LANGUAGE_ID') && define('CURRENT_LANGUAGE_ID', 1);
require_once(__DIR__.'/../../../../../../../bootstrap.php');

$sModule = 'utils';
//Core_Auth::authorization($sModule = 'utils');

// Код формы
$sFormAction = '/admin/utils/technical/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid('700AE7EB-D9AE-5B58-6460-FEC694851EAA');
$iAdmin_Form_Id = $oAdmin_Form->id;

$oAdmin_Form = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id);

$oShop = Core_Entity::factory('Shop', Core_Array::getGet('shop_id', 0));

// Меню формы
$oMenu = Admin_Form_Entity::factory('Menus');

// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->setUp()
	->path($sFormAction)
	->title('Редактировать технические характеристики')
	->pageTitle('Редактировать технические характеристики')
	->module(Core_Module::factory($sModule))
;

// Элементы меню
//$oMenu->add(
//	Admin_Form_Entity::factory('Menu')
//		->name(Core::_('Shop_Item.links_items'))
//		->add(
//			Admin_Form_Entity::factory('Menu')
//				->name(Core::_('Shop_Item.links_items_add'))
//				->img('/admin/images/page_add.gif')
//				->href(
//					$oAdmin_Form_Controller->getAdminActionLoadHref
//					(
//						$oAdmin_Form_Controller->getPath(), 'editTechnique', NULL, 1, 0
//					)
//				)
//				->onclick(
//					$oAdmin_Form_Controller->getAdminActionLoadAjax
//					(
//						$oAdmin_Form_Controller->getPath(), 'editTechnique', NULL, 1, 0
//					)
//				)
//		)
//);
//// Добавляем все меню контроллеру
//$oAdmin_Form_Controller->addEntity($oMenu);
$oAdminFormActionTechEdit = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('editTechnical');

if ($oAdminFormActionTechEdit && $oAdmin_Form_Controller->getAction() == 'editTechnical')
{
	$oShopItemControllerTechEdit = Admin_Form_Action_Controller::factory(
		'Utils_Admin_Panel_Technical_Form_Action_Controller', $oAdminFormActionTechEdit
	);

//	$oShopItemControllerTechEdit
//		->title("Изменение технических характеристик")
//	;
	// Добавляем типовой контроллер редактирования контроллеру формы
	$oAdmin_Form_Controller->addAction($oShopItemControllerTechEdit);
}

// Действие "Удаление значения свойства"
$oAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('deletePropertyValue');

if ($oAction && $oAdmin_Form_Controller->getAction() == 'deletePropertyValue')
{
	$oDeletePropertyValueController = Admin_Form_Action_Controller::factory(
		'Property_Controller_Delete_Value', $oAction
	);

	$oDeletePropertyValueController
		->linkedObject(array(
			Core_Entity::factory('Shop_Group_Property_List', $oShop->id),
			Core_Entity::factory('Shop_Item_Property_List', $oShop->id)
		));

	$oAdmin_Form_Controller->addAction($oDeletePropertyValueController);
}

//// Источник данных 0
//$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Shop_Group'));
//$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);
// Источник данных 1
$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Shop_Item'));
$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);

//$oDataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Shop_Item'));
//$oAdmin_Form_Controller->addDataset($oDataset);

// Показ формы
$oAdmin_Form_Controller->execute();