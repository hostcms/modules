<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
require_once(__DIR__.'/../../../../../../../bootstrap.php');

Core_Auth::authorization($sModule = 'utils');
Core::parseUrl();

header('Content-Type: application/json');

$oSite = Core_Entity::factory('Site', CURRENT_SITE);

$shopId = 0;
$result =  array();

$operation = Core_Array::getPost("operation", 'none');
$entity = Core_Array::getPost("entity", 'none');
$itemID = Core_Array::getPost("entityId", 0);
$propertyId = Core_Array::getPost("propertyid", 0);
$propertyValue = (Core_Array::getPost("propertyvalue", false) & true);

$itemFounded = ($itemID>0) ? Core_Entity::factory(str_replace('_model', '', $entity), $itemID) : NULL;
(!is_null($itemFounded) && isset($itemFounded->shop_id)) && $shopId=$itemFounded->shop_id;
(!is_null($itemFounded) && isset($itemFounded->shop_id)) && $itemPath = '/'.$itemFounded->getPath();
$currenttime = str_replace('-', '', microtime(true));

if(isset($itemFounded->id)
	&& ($itemFounded->id>0)
	&& ($propertyId > 0)
) {
	/** @var Property_Model $property */
	$property = Core_Entity::factory('Property')->getById($propertyId);
	$propertyValues = $property->getValues($itemFounded->id);
	if(isset($propertyValues[0])) {
		$pValue = $propertyValues[0];
	} else {
		$pValue = $property->createNewValue($itemFounded->id);
	}
	$pValue->value = (!$propertyValue)*1;
	$pValue->save();
	$result =  array(
		'value' => $pValue->value,
		'img' => (!$pValue->value ? 'disabled' : ''),
		'status' => "OK",
		'message' => "",
	);
} else {
	$result =  array(
		'status' => "ERROR",
		'message' => "Не найден магазин для элемента",
	);
}
echo json_encode($result);
