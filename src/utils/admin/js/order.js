function printOrder(thisButton, all){
	all = all || null;
	var ordersid = thisButton.dataset.orderid,
		arrayOrders = ordersid.split(',');

	$('#wrapper-list').remove();
	var $wrapper = $('<div/>',{id:'wrapper-list'}),
		$listClone;
	thisButton.disabled = true;
	arrayOrders.forEach(function(item){
		var thisId = item,
			url = '/admin/shop/order/card/pay.php?hostcms[action]=&hostcms[operation]=&hostcms[current]=1&hostcms[sortingfield]=292&hostcms[sortingdirection]=1&hostcms[window]=id_content&shop_order_id='+thisId;
		var $printCard = $('.print_card[data-orderid='+thisId+']').find('.print_card_order');
		$.ajax({
			method: 'get',
			url: url,
			dataType: 'html',
			async: false,
			success: function(data){
				var bodyContent = data.match(/<body[^>]*>([\s\S]*?)<\/body>/i)[1];
				$printCard.html(bodyContent);
			}
		});
	});

	if(all) {
		$listClone = $('#orderPrintWg').clone();
	}else {
		var $table = $(thisButton).closest('.print_card').find('.print_card_order');
		$listClone = $table.clone();
	}

	$wrapper.append($listClone);
	$('body').append($wrapper);
	window.print();
	$wrapper.remove();
	thisButton.disabled = false;
}

function statusOrder(thisButton){
	var id = thisButton.dataset.orderid,
		arrayId = id.split(','),
		text = 'Изменить статус заказов ('+id+') на Заказано';
	thisButton.disabled = true;
	if(confirm(text)){
		$.ajax({
			method: 'post',
			url: '/admin/utils/shop/order/process.php',
			dataType: 'json',
			data: {
				"action": "updateorderstatus",
				"id": arrayId
			},
			success: function(data){
				console.log(data);
				if(data.status === 'OK'){
					for (var i=0; i<data.orderIDs.length; i++){
						var objElement = data.orderIDs[i];
						var $element = $('.ostat[data-orderid='+objElement.orderid+']');
						$element.text(objElement.name);
						// $element[0].className = 'ostat ostat'+objElement.statusid;
					}
				}else {
					alert(data.message);
				}
				thisButton.disabled = false;
			}
		});
	}else {
		thisButton.disabled = false;
	}
}

function addToOrder(thisButton, id){
	$.fancybox.open('/?shop_order_id='+id+'&action=additemtoorder', {
		type: 'iframe',
		// maxWidth	: 800,
		// maxHeight	: 600,
		width: '90%',
		fitToView: false
	});
}

function changeSelectOnOrder(element){
	var value = element.recent,
		name = element.name,
		sendObj = {};
	sendObj[name] = value;
	sendObj['_'] = 1;
	sendObj['action'] = 'updateselector';
	var tr = $(element).closest('tr').find('input,select');
	if(tr.length>0) {
		for(_i=0; _i<tr.length; _i++) {
			if(element.name.match(/^wh_ordered.*?\[(warehouse)\]$/)!==null
				&& $(tr[_i]).attr('name').match(/^wh_ordered.*?\[(price)\]$/)!==null) {
				$(tr[_i]).val(0);
			}
			sendObj[$(tr[_i]).attr('name')]=$(tr[_i]).val();
		}
	}

	element.disabled = true;
	$.ajax({
		method: 'post',
		url: '/admin/utils/shop/order/process.php',
		data: sendObj,
		dataType: 'json',
		success: function(data){
			element.disabled = false;
			if(data.status == 'OK'){
				element.style.borderColor = '#1ac517';
				element.style.backgroundColor = '#acf8bb';
				// for(_i=0; _i<sendObj.length; _i++) {
				if(tr.length>0) {
					for(_i=0; _i<tr.length; _i++) {
						tr[_i].name = tr[_i].name.replace(/\[0\]/, '['+data.orderItem.id+']');
						// console.log($(tr[_i]));
						// console.log(tr[_i].name);
						if(tr[_i].name.match(/^wh_ordered.*?\[(price)\]$/)!==null) {
							$(tr[_i]).val(data.orderItem.warehouse_order_price);
						}
						// sendObj[$(tr[_i]).attr('name')]=$(tr[_i]).val();
					}
				}
				// sendObj[$(tr[_i]).attr('name')]=$(tr[_i]).val();
				// }
			}else {
				element.style.borderColor = '#e21616';
				element.style.backgroundColor = '#ffc1c1';
				element.title = data.message;
			}
			setTimeout(function(){
				element.style.borderColor = '#d5d5d5';
			},2000);
		},
		error: function(jqXHR, textStatus, errorThrown){
			element.disabled = false;
			element.style.borderColor = '#e21616';
			setTimeout(function(){
				element.style.borderColor = '#d5d5d5';
			},2000);
		}
	});
}

$(document).on('click', '.js-copy_value', function(){
	var range = document.createRange();
	range.selectNode(this);
	window.getSelection().addRange(range);
	try {
		var successful = document.execCommand('copy');
	} catch(err) {
		console.log(err);
	}
	window.getSelection().removeAllRanges();
	this.style.color = '#1aa900';
	var element = this;
	setTimeout(function(){
		element.style.color = '#333';
	},1000);
});

$(document).on('change', '.ajax-order select', function(){
	var element = this,
		orderid = $(this).parent().siblings('.ajax-id').find('span').text();
	this.disabled = true;
	$.ajax({
		method: 'post',
		url: '/admin/utils/shop/order/process.php',
		data: {
			"action": "updateorderstatus",
			"statusid": this.value,
			"id[]": orderid
		},
		dataType: 'json',
		success: function(data){
			if(data.status == 'OK'){
				element.style.borderColor = '#1ac517';
				element.style.backgroundColor = '#acf8bb';
				$('#check_0_'+orderid)[0].checked = false;
			}else {
				element.style.borderColor = '#e21616';
				element.style.backgroundColor = '#ffc1c1';
			}
			element.disabled = false;
			setTimeout(function(){
				element.style.borderColor = '#d5d5d5';
				element.style.backgroundColor = '#fbfbfb';
			},3000);
		},
		error: function(jqXHR, textStatus, errorThrown){
			element.disabled = false;
			element.style.borderColor = '#e21616';
			setTimeout(function(){
				element.style.borderColor = '#d5d5d5';
			},3000);
		}
	});
});

function printOrderChangeStatus(element){
	var $element = $(element),
		orderid = element.dataset.orderid,
		statusid = element.dataset.statusid,
		$linksArray = $element.closest('.dropdown-menu').find('a');
	$linksArray.attr('disabled', true);
	$.ajax({
		method: 'post',
		url: '/admin/utils/shop/order/process.php',
		data: {
			"action": "updateorderstatus",
			"statusid": statusid,
			"id[]": orderid
		},
		dataType: 'json',
		success: function(data){
			if(data.status == 'OK'){
				$linksArray.each(function(){
					$(this).children('i')[0].className = 'btn-label fa fa-square-o';
				});
				$element.children('i')[0].className = 'btn-label fa fa-check-square-o';
				$element.closest('.btn-group').find('.btn-labeled').html('<i class="btn-label fa fa-check-square-o"></i>' + $element.text());
			}else {
				console.log('error from server');
			}
			$linksArray.attr('disabled', false);
			return false;
		},
		error: function(jqXHR, textStatus, errorThrown){
			$linksArray.attr('disabled', false);
			console.log(textStatus);
			return false;
		}
	});
}