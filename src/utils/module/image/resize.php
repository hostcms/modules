<?php

use Utils_Utl as utl;
use Spatie\ImageOptimizer\OptimizerChainFactory;

use Intervention\Image\ImageManagerStatic as iManager;

class Utils_Image_Resize {
	protected $_watermarks = array();
	protected $_renameFileNameSize = true;
	protected $_noWaterMarks = array();
	protected $_debug = false;

	/**
	 * Utils_Image_Resize constructor.
	 * @param bool $_debug
	 */
	public function __construct()
	{
		$this->_debug = (Core_Array::getGet('debug', 0) !== 0);
	}

	/**
	 * @return boolean
	 */
	public function isDebug()
	{
		return $this->_debug;
	}

	/**
	 * @param boolean $debug
	 */
	public function setDebug($debug)
	{
		$this->_debug = $debug;
	}

	/**
	 * @return array
	 */
	public function getNoWaterMarks()
	{
		return $this->_noWaterMarks;
	}

	/**
	 * @param array $noWaterMarks
	 */
	public function setNoWaterMarks($noWaterMarks)
	{
		$this->_noWaterMarks = $noWaterMarks;
	}

	/**
	 * @param array $watermarks
	 */
	public function addNoWatermark($wmPath)
	{
		$this->_noWaterMarks[] = $wmPath;
		return $this;
	}

	/**
	 * @param array $watermarks
	 */
	public function clearNoWatermarks()
	{
		$this->_noWaterMarks = array();
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isRenameFileNameSize()
	{
		return $this->_renameFileNameSize;
	}

	/**
	 * @param boolean $renameFileNameSize
	 */
	public function setRenameFileNameSize($renameFileNameSize)
	{
		$this->_renameFileNameSize = $renameFileNameSize;
	}

	/**
	 * @return array
	 */
	public function getWatermarks()
	{
		return $this->_watermarks;
	}

	/**
	 * @param array $watermarks
	 */
	public function setWatermarks($watermarks)
	{
		$this->_watermarks = $watermarks;
		return $this;
	}

	/**
	 * @param array $watermarks
	 */
	public function addWatermark($key, $wmPath)
	{
		$this->_watermarks[$key] = $wmPath;
		return $this;
	}

	/**
	 * @param array $watermarks
	 */
	public function clearWatermarks()
	{
		$this->_watermarks = array();
		return $this;
	}

	function checkCRC($item, $dir, $tmbdir, $sizes, $watermark='', $function='', $forceupdate = 0, $thumbName='') {
		if(is_string($function)) {
			$function = array($function);
		}
		if (filesize ($dir.'/'.$item )) { // исходный файл существует и больше нуля
			if ( $this->_renameFileNameSize ) {
				$crc = crc32(file_get_contents($dir.'/'.$item)); // crc от исходного файла
				$tmbdir = utl::checkFolder($tmbdir);
				if(!file_exists($tmbdir.'/'.$item.'_'.$crc.'.crc')) // crc изменени или не существует
				{
					$crcFiles = glob($tmbdir."/{$item}_*.crc", GLOB_NOESCAPE);
					$crcFiles=='' && $crcFiles=array();
					foreach ($crcFiles as $filename) { // удаляем старые crc если есть
						if(is_file($filename)) {
							unlink($filename);
						}
					}
					$crcFilePattern = $tmbdir."/*{$item}";
					$aCrcFiles = glob($crcFilePattern);
					$aCrcFiles=='' && $aCrcFiles=array();
					foreach ($aCrcFiles as $filename2) { // удаляем старые фотки
						if(is_file($filename2)) {
							unlink($filename2);
						}
					}
					$f = fopen($tmbdir.'/'.$item.'_'.$crc.'.crc', "w"); // создаем новый crc
					fclose ($f);
				}
			}
			$checkSizes = $this->checkSizes($item, $dir, $tmbdir, $sizes, $watermark, $function, $thumbName);
			return $checkSizes;
		}
	}

	function checkSizes($item, $dir, $tmbdir, $sizes, $watermark='', $function=array(), $thumbName='') {
		foreach ( $sizes as $key => $val ) {
			$tmbdir = utl::checkFolder($tmbdir);
			$filename = $tmbdir.'/' . ($this->_renameFileNameSize ? ('s_'.$val['x'].'x'.$val['y'].'_') : "").($thumbName!='' ? $thumbName : $item);
			$fileSrc = $dir.'/'.$item;
			if($this->_debug || !file_exists($filename) || $thumbName!='') {
				$wImageNew = $val['x']*1;
				$hImageNew = $val['y']*1;

				$image = new \claviska\SimpleImage();
				$img = $image->fromFile($fileSrc);

				switch (true) {
					case in_array('thumb', $function):
						$img->thumbnail($wImageNew, $hImageNew);
						break;
					default:
						$img->bestFit($wImageNew, $hImageNew);
				}
				$areaNew = $wImageNew*$hImageNew;
				$checkedArray = array($val['x']*1, $val['y']*1);

				if(isset($this->_watermarks[$watermark])
					&& $areaNew>62500
					&& (array_search($checkedArray, $this->_noWaterMarks)===FALSE )
				) {
					$wmImagePath = ((!is_array($this->_watermarks[$watermark])) ? $this->_watermarks[$watermark] : ((isset($this->_watermarks[$watermark]['path']) ? $this->_watermarks[$watermark]['path'] : '')));
					$wmSize = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['size'])) ? $this->_watermarks[$watermark]['size'][0] : array(50, '%') );
					$wmPositionY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][0] : 'center' );
					$wmPositionX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['position'])) ? $this->_watermarks[$watermark]['position'][1] : 'center' );
					$wmOffsetY = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsety'])) ? ($this->_watermarks[$watermark]['offsety'][0]*1<0 ? '' : '+').$this->_watermarks[$watermark]['offsety'][0].$this->_watermarks[$watermark]['offsety'][1] : '+0px' );
					$wmOffsetX = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['offsetx'])) ? ($this->_watermarks[$watermark]['offsetx'][0]*1<0 ? '' : '+').$this->_watermarks[$watermark]['offsetx'][0].$this->_watermarks[$watermark]['offsetx'][1] : '+0px' );
					$wmOpacity = ( (is_array($this->_watermarks[$watermark]) && isset($this->_watermarks[$watermark]['opacity'])) ? $this->_watermarks[$watermark]['opacity']*1.0 : 0.9 );

					$watermark = new \claviska\SimpleImage();
					$watermark->fromFile($wmImagePath);
					$wmSizeW = $watermark->getWidth();   //-- ширина watermark
					$wmSizeH = $watermark->getHeight();  //-- высота watermark

					if($wmSizeW>=$wmSizeH) {
						$wmSizeWnew = ($img->getWidth()*$wmSize)/100;
						$wmSizeHnew = $wmSizeWnew/($wmSizeW/$wmSizeH);
					} else {
						$wmSizeHnew = ($img->getHeight()*$wmSize)/100;
						$wmSizeWnew = $wmSizeHnew/($wmSizeH/$wmSizeW);
					}
					$watermark
						->bestFit($wmSizeWnew, $wmSizeHnew);
					$img->overlay($watermark, "{$wmPositionY} {$wmPositionX}", $wmOpacity, $this->translateNumeric($wmOffsetY, $hImageNew), $this->translateNumeric($wmOffsetX, $wImageNew));
				}

				$img->toFile($filename);
//-- Не удалять. Надо переходить на PHP 7.2+ ---------------------------------------------------------------------------
				if(defined('PHP_RELEASE_VERSION') && defined('PHP_MINOR_VERSION')) {
					if(PHP_RELEASE_VERSION>=7 && PHP_MINOR_VERSION>=2) {
						$optimizerChain = OptimizerChainFactory::create();
						$optimizerChain->optimize($filename);
					}
				}
//-- Не удалять. Надо переходить на PHP 7.2+ ---------------------------------------------------------------------------
				return array(
					'processed' => true,
					'sourceFilePath' => $fileSrc,
					'destinationFilePath' => $filename,
				);
			} else {
				return array(
					'processed' => false,
					'sourceFilePath' => $fileSrc,
					'destinationFilePath' => $filename,
				);
			}
		}
	}

	function translateNumeric($value, $compare=0) {
		if(!is_numeric($value)) {
			$matches = [];
			if(preg_match('/^((\+|-|\d|\.)+)\%/', $value, $matches)) {
				$value = $compare/100*$matches[1];
			} else {
				$value = 0;
			}
		}
		return $value;
	}
}