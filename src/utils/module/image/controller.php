<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 05.03.2017
 * Time: 15:59
 */
class Utils_Image_Controller extends Core_Command_Controller {

	/**
	 * Default controller action
	 * @return Core_Response
	 */
	public function showAction() {
		Core_Session::close();
		$oCore_Response = new Core_Response();
		$noimagepath = (isset($this->noimagepath) && !is_null($this->noimagepath)) ? $this->noimagepath : '';

		$file = Core_Str::rtrimUri(CMS_FOLDER.$this->uploadpath.($this->uploadpath!='' ? DIRECTORY_SEPARATOR : '').$this->path.'.'.$this->ext);

		if(!is_file($file)) {
			if( $noimagepath != '' && is_file(CMS_FOLDER.$noimagepath)) {
				$file = CMS_FOLDER.$noimagepath;
			} elseif(defined('HOSTCMS_UTILS_NOIMAGE') && is_file($f=str_replace('//', '/', CMS_FOLDER.HOSTCMS_UTILS_NOIMAGE))) {
				$file = $f;
			} else {
				$file = realpath(__DIR__.DIRECTORY_SEPARATOR.'..'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'no-image.png');
			}
		}
		$contentType = mime_content_type($file);
		$aResult = file_get_contents($file);
		if(preg_match('/^image\//', $contentType)) {
			$width = $this->w;
			$height = $this->h;

			$cropFile = false;
			$imageFile = $file;
			if(file_exists($imageFile) && is_file($imageFile)
				&& (preg_match('/\.gif$/', $imageFile)==0) // || ($width!=460 && $height!=460))
			) {
				$path = explode('/', $imageFile);
				$imageFileName = array_pop($path);
				$imageFilePath = implode('/', $path);
				$imageFileThumb = str_replace(CMS_FOLDER, CMS_FOLDER.'uploadtmp/'.(($this->processing!='') ? $this->processing.'/' : ''), $imageFilePath);
				$img = new Utils_Image_Resize();
				$waterMarkPath = CMS_FOLDER.'upload/shop_3/watermarks/shop_watermark_3.png';
				if(file_exists($waterMarkPath)) {
					$watermarkPosition = ['bottom', 'left'];
					$watermarkSize = [60, '%'];
					$watermarkOffsetX = [-2, '%'];
					$watermarkOffsetY = [4, '%'];
					$img->addWatermark('gallery', [
						'path' => $waterMarkPath,
						'position' => $watermarkPosition,
						'size' => $watermarkSize,
						'opacity' => .8,
						'offsetx' => $watermarkOffsetX,
						'offsety' => $watermarkOffsetY,
					]);
					$img->addNoWatermark([300, 300]);
					$img->addNoWatermark([338, 300]);
					$img->addNoWatermark([365, 365]);
					$img->addNoWatermark([640, 640]);
                    $img->addNoWatermark([480, 600]);
                    $img->addNoWatermark([443, 554]);

				}
				$images = $img->checkCRC(
					$imageFileName
					, $imageFilePath
					, $imageFileThumb
					, [1 => ['x'=> $width,'y'=> $height]]
					, 'gallery'
					, $this->processing
				);
				$thumbFile = $images['destinationFilePath'];

			} else {
				$thumbFile = $imageFile;
			}

			if(file_exists($thumbFile)) {
				$aResult = file_get_contents($thumbFile);
			}
		}
		$oCore_Response
			->header('Content-Type', "{$contentType}")
			->header('Last-Modified', gmdate('D, d M Y H:i:s', time()) . ' GMT')
			->header('Cache-Control', "must-revalidate, max-age=2678400")
			->header('Expires', gmdate('D, d M Y H:i:s', strtotime("+1 month")) . ' GMT')
			->body($aResult)
		;
		return $oCore_Response;
	}
}
