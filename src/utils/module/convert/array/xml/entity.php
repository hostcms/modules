<?php
use Utils_Utl as utl;
//use LSS\Array2XML as axml;
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 10.03.2017
 * Time: 8:36
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

/*
 * пример формата массива для преобразования
 * http://www.lalit.org/lab/convert-php-array-to-xml-with-attributes/
 * http://www.lalit.org/wordpress/wp-content/uploads/2011/07/array2xml_example.html?ver=0.8
 * $inArray = array(
        array(
            '@attributes' => array(
                'author' => 'George Orwell'
            ),
            'title' => '1984'
        ),
        array(
            '@attributes' => array(
                'author' => 'Isaac Asimov'
            ),
            'title' => array('@cdata'=>'Foundation'),
            'price' => '$15.61'
        ),
        array(
            '@attributes' => array(
                'author' => 'Robert A Heinlein'
            ),
            'title' =>  array('@cdata'=>'Stranger in a Strange Land'),
            'price' => array(
                '@attributes' => array(
                    'discount' => '10%'
                ),
                '@value' => '$18.00'
            )
        )
    )
    На выходе
    <books type="fiction">
      <book author="George Orwell">
        <title>1984</title>
      </book>
      <book author="Isaac Asimov">
        <title><![CDATA[Foundation]]></title>
        <price>$15.61</price>
      </book>
      <book author="Robert A Heinlein">
        <title><![CDATA[Stranger in a Strange Land]]</title>
        <price discount="10%">$18.00</price>
      </book>
    </books>
 * */
class Utils_Convert_Array_Xml_Entity {
	protected $_arConverts;
	protected $_rootPath;
	public $name = '';

	function __construct($inArray = array(), $key='', $rootPath = 'newrecords', $attrkey = '') {
		$this->_rootPath = $rootPath;

		if($key != '') {
			if($attrkey != '') {
				foreach ($inArray as $arKey=>$arVal){
					$inArray[$arKey]['@attributes'] = array($attrkey => $arVal[$attrkey]);
				}
			}
			$this->_arConverts[$key] = $inArray;
		} else {
			$this->_arConverts = $inArray;
		}
	}

	public static function createInstance($inArray = array(), $key='', $rootPath = 'newrecords', $attrkey = '')
	{
		return new self($inArray, $key, $rootPath, $attrkey);
	}

	public function getStdObject() {
		$rv = new stdClass();
//		$rv->name = '';

		return $rv;
	}
	/**
	 * Build entity XML
	 *
	 * @return string
	 */
	public function getXml()
	{
/*		$xml = str_replace("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n", "", axml::createXML($this->_rootPath, $this->_arConverts)->saveXML());*/
		$xml = preg_replace("/<\?xml version=\"1.0\"( encoding=\"UTF-8\")?\?>/ui", "", \Spatie\ArrayToXml\ArrayToXml::convert(
			$this->_arConverts,
			$this->_rootPath,
			false,
			"UTF-8",
			"1.0",
			[]
		)
		);

		return $xml;
	}
}