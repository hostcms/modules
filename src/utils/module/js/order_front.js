$(document).ready(function() {
	var _queryString='';

	if(window.frameElement != null && (_queryString=window.frameElement.getAttribute("src"))!='') {
		_found = _queryString.match(/shop_order_id=(\d+)\&action=(additemtoorder)/i);
		if(_found!=null && (_hiddenItem=$('#shop_item_id')).length>0) {
			var _orderID=_found[1];
			if((_goodCard=$('div.good_card')).length>0) {
				console.log(_goodCard);
				if($('a.btn.btn-success').length>0) {
					$('a.btn.btn-success').remove();
				}
				_btnOrder = $('<button/>', {
					'type': 'button',
					'class': 'btn btn-success btn-order-to-admin good__buy button button_success js-modal_stock',
					'onclick': 'return $.addIntoOrder(\'/cart/\', '+_hiddenItem.val()+', '+_orderID+');',
					'html':'<i class="icon-large icon-large-shopping-cart-add"></i> '+'Добавить в заказ '+_orderID
				});
				$('.properties', _goodCard).append(_btnOrder);
			}
		}
	}
});

(function(jQuery){
	// Функции без создания коллекции
	jQuery.extend({
		addIntoOrder: function (path, shop_item_id, order_id) {
			$.clientRequest({
				path: '/admin/utils/shop/order/process.php?action=addintoorder&itemid=' + shop_item_id + '&orderid=' + order_id,
				'callBack': $.addIntoOrderCallback
			});
			return false;
		},
		addIntoOrderCallback: function (data, status, jqXHR) {
			$.loadingScreen('hide');
			switch (data.status) {
				case 'OK':
					$.utlMsgBox('Товар успешно добавлен в заказ '+data.orderItem.shop_order_id, 'success');
					break;
				default:
					$.utlMsgBox(data.message, 'error');
			}
		},
		utlMsgBox: function (message, type){
			var _uinformer = $('#uinformer');
			if(_uinformer.length) {
				_uinformer.remove();
			}
			var _wrap = $("<div/>",{id:"uinformer", class:"uinformer uinformer_"+type, html:"<div class=\"uinformer__message\">"+message+"</div>"});
			_wrap.one('click', function(){
				_wrap.remove();
			});
			$('body').append(_wrap);
			_wrap.delay(4500).fadeOut(500, function(){
				_wrap.remove();
			});
		}

})
})(jQuery);