jQuery.extend({
	utilsAjaxRequest: function (_path, _title) {
		if(typeof(_path)=='undefined' || _path=='') {
			alert('Не определен PATH');
			return false;
		}
		if(typeof(_title)=='undefined' || _title=='') {
			alert('Не определен TITLE');
			return false;
		}
		hQuery.openWindow({
			autoOpen: false,
			destroyOnClose: true,
			addContentPadding: false,
			resizable: true,
			draggable: true,
			Minimize: false,
			Closable: true,
			width: /*'70%',*/hQuery(window).width() * 0.95,
			height: /*500,*/hQuery(window).height() * 0.95,
			modal: true,
			actionName: "click",
			path: _path,
			title: _title
		});
		return false;
	},

	admin_utils_gallery: function (_itemID) {
		var _ret={};
		_ret.path = "/admin/utils/gallery/index.php?hostcms[action]=addGallery&hostcms[checked][0]["+_itemID+"]=1";
		_ret.title = 'Добавить в галерею';

		return _ret;
	},

	admin_utils_articles: function (_itemID) {
		var _ret={};
		_ret.path = "/admin/utils/article/index.php?hostcms[action]=addArticle&hostcms[checked][0]["+_itemID+"]=1";
		_ret.title = 'Добавить статью';

		return _ret;
	},

	admin_utils_technical: function (_itemID) {
		var _ret={};
		_ret.path = "/admin/utils/technical/index.php?hostcms[action]=editTechnical&hostcms[checked][0]["+_itemID+"]=1";
		_ret.title = 'Редактировать техническую информацию';

		return _ret;
	}
});

$(document).ready(function() {
	$('body').on('click', '.admin_utils', function(){
		var _typeAction = $(this).attr('data-operation');
		var _path = '';
		var _title = '';
		var _itemID = 0;
		if( (_elementItemID = $(this).parents('.customAdminItem').find( 'input.shop-item-id:first')).length == 1) {
			_itemID = _elementItemID.val();
		}

		if(_itemID>0) {
			if( _typeAction != '' && typeof($['admin_utils_'+_typeAction])!='undefined' ) {
				obj = $['admin_utils_'+_typeAction](_itemID);
				$.utilsAjaxRequest(obj.path, obj.title);
			} else {
				// alert('Не определен action элемента!!!')
			}
		} else {
			// alert('Не определен идентификатор элемента!!!')
		}
	});
});

function reClickTrigger(item) {
	var _item = $(item),
		_ds = _item.data(),
		_url = '/admin/utils/technical/process.php'
	;
	if(_ds.entity != 'undefined'
		&& _ds.entityId != 'undefined'
		&& _ds.operation != 'undefined'
	) {
		$.ajax({
			url: _url,
			method: 'post',
			dataType: "json",
			data: _ds,
			success: function( responseText ) {
				if( responseText.status == 'OK' ) {
					_ds.operation = _ds.operation.replace('.disabled', '');
					_imageSrc = _ds.imagepath + _ds.operation + (responseText.img != '' ? '.' : '') + responseText.img + '.gif';
					_item.data('operation', _ds.operation);
					_item.data('propertyvalue', responseText.value);
					_item.find('img').attr('src', _imageSrc);
				}
			}
		});
	}

	return false;
}

var Base64 = {
	_keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

	encode: function(input) {
		var output = "";
		var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
		var i = 0;

		input = Base64._utf8_encode(input);

		while (i < input.length) {

			chr1 = input.charCodeAt(i++);
			chr2 = input.charCodeAt(i++);
			chr3 = input.charCodeAt(i++);

			enc1 = chr1 >> 2;
			enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
			enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
			enc4 = chr3 & 63;

			if (isNaN(chr2)) {
				enc3 = enc4 = 64;
			} else if (isNaN(chr3)) {
				enc4 = 64;
			}

			output = output + this._keyStr.charAt(enc1) + this._keyStr.charAt(enc2) + this._keyStr.charAt(enc3) + this._keyStr.charAt(enc4);

		}

		return output;
	},

	decode: function(input) {
		var output = "";
		var chr1, chr2, chr3;
		var enc1, enc2, enc3, enc4;
		var i = 0;

		input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

		while (i < input.length) {

			enc1 = this._keyStr.indexOf(input.charAt(i++));
			enc2 = this._keyStr.indexOf(input.charAt(i++));
			enc3 = this._keyStr.indexOf(input.charAt(i++));
			enc4 = this._keyStr.indexOf(input.charAt(i++));

			chr1 = (enc1 << 2) | (enc2 >> 4);
			chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
			chr3 = ((enc3 & 3) << 6) | enc4;

			output = output + String.fromCharCode(chr1);

			if (enc3 != 64) {
				output = output + String.fromCharCode(chr2);
			}
			if (enc4 != 64) {
				output = output + String.fromCharCode(chr3);
			}

		}

		output = Base64._utf8_decode(output);

		return output;

	},

	_utf8_encode: function(string) {
		string = string.replace(/\r\n/g, "\n");
		var utftext = "";

		for (var n = 0; n < string.length; n++) {

			var c = string.charCodeAt(n);

			if (c < 128) {
				utftext += String.fromCharCode(c);
			}
			else if ((c > 127) && (c < 2048)) {
				utftext += String.fromCharCode((c >> 6) | 192);
				utftext += String.fromCharCode((c & 63) | 128);
			}
			else {
				utftext += String.fromCharCode((c >> 12) | 224);
				utftext += String.fromCharCode(((c >> 6) & 63) | 128);
				utftext += String.fromCharCode((c & 63) | 128);
			}

		}

		return utftext;
	},

	_utf8_decode: function(utftext) {
		var string = "";
		var i = 0;
		var c = c1 = c2 = 0;

		while (i < utftext.length) {

			c = utftext.charCodeAt(i);

			if (c < 128) {
				string += String.fromCharCode(c);
				i++;
			}
			else if ((c > 191) && (c < 224)) {
				c2 = utftext.charCodeAt(i + 1);
				string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
				i += 2;
			}
			else {
				c2 = utftext.charCodeAt(i + 1);
				c3 = utftext.charCodeAt(i + 2);
				string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
				i += 3;
			}

		}

		return string;
	}
}

function showStatMetrika(){
	console.log('complete');
	const wrap = document.createElement('div');
	const close = document.createElement('button');
	close.type = 'button';
	close.style.cssText = 'width:28px;height:28px;position:absolute;top:0;right:0;z-index:1;border:0;padding:0;background:transparent;color:#444;';
	close.innerHTML = '<svg viewBox="0 0 32 32" style="width:100%;height:100%;fill:transparent;stroke:#444;stroke-width:2px;"><path d="M10,10 L22,22 M22,10 L10,22"></path></svg>';

	close.addEventListener('click', closeMetrika, {once:true});

	wrap.id = 'sbMetrikaWrapper';
	wrap.className = 'sbMetrikaWrapper';
	wrap.innerHTML = this.html['#sbMetrikaWrapper'];
	wrap.style.cssText = 'background:#fff; position:absolute; top:40px; left:3px; color:#000; padding: 12px; z-index:999; border-radius:4px; box-shadow:0 16px 35px -8px rgba(30,55,80,.2)';
	wrap.appendChild(close);
	document.body.appendChild(wrap);

	function closeMetrika(){
		document.body.removeChild(wrap);
	}
}
//-- примеры использования ---------
// var encode = document.getElementById('encode'),
// 	decode = document.getElementById('decode'),
// 	output = document.getElementById('output'),
// 	input = document.getElementById('input');
//
// encode.onclick = function() {
// 	output.innerHTML = Base64.encode(input.value);
// }
//
// decode.onclick = function() {
// 	var $str = output.innerHTML;
// 	output.innerHTML = Base64.decode($str);
// }