//$(function () {
//	$('body').on('click', '#editArt,.editArt', function(){
//		var   $itemID = $(this).attr('data-item-id')
//			, $isArticle = $("*[data-isarticlesid]", $(this)).attr('data-isarticlesid')
//			;
//		hQuery.openWindow({
//			autoOpen: false,
//			dialogClass: 'hostcms6',
//			destroyOnClose: true,
//			addContentPadding: true,
//			resizable: true,
//			draggable: true,
//			Minimize: false,
//			Closable: true,
//			modal: true,
//			actionName: "click",
//			path: "/libs/extended/arteditor/index.php?hostcms[action]=editArt&hostcms[checked][0]["+$itemID+"]=1"+ "&isarticlesid="+$isArticle,
//			title: 'Редактирование статей'
//		});
//	});
//
//	var itemId = $('body').find("input[name='itemID']").val();
//	var isarticlesId = $('body').find("input[name='isarticlesid']").val();
//
//	initArtTreeview($('.selectedCategoryTree'), 'arts=1', itemId, isarticlesId );
//});
function initArtTreeview(type, itemId, isarticlesId) {
	function loadError(e,data) {
		var error = data.error;
		if (error.status && error.statusText) {
			data.message = "Ajax error: " + data.message;
			data.details = "Ajax error: " + error.statusText + ", status code = " + error.status;
		} else {
			data.message = "Custom error: " + data.message;
			data.details = "An error occured during loading: " + error;
		}
	}
	//try {
	//	console.log( typeof( object.fancytree("getTree") ));
	//} catch (exception_var) {
	//	console.log( exception_var );
	//}
	object = $('.selectedCategoryTree');
	// console.log(object);

	object.fancytree({
		source: {
			url: "/admin/utils/utils_article/ajax/tree/" + itemId + "/" + (isarticlesId!='' ? isarticlesId : '0') + "/", //(type!='' ? '&'+type : ''),
			cache: false
		},
		extensions: ["table", "edit"], // , "contextMenu"
		edit: {
			triggerStart: ["f2", "dblclick", "shift+click", "mac+enter"],
			beforeEdit: function(event, data){
				if (data.node.parent.title=='root') return false;
				if (data.node.parent.parent.title=='root') {
					data.node.editCreateNode("child", "Наименование статьи");
					return false;
				}
			},
			beforeClose: function(event, data){
				return (data.input.val()!='');
			},
			save: function(event, data){
				// console.log(data.node.data.id);
				operation = ( typeof(data.node.data.id)=='undefined' ? 'add' : 'editname' );

				$.clientRequest({
					context: data.node,
					// path: '/libs/extended/arteditor/actions.php?operation='+operation+'&name='+data.input.val()+'&group='+data.node.parent.data.id+( typeof(data.node.data.id)=='undefined' ? '' : '&item='+data.node.data.id )+( typeof($("input[name='isarticlesid']"))=='undefined' ? '' : '&isarticlesid='+$("input[name='isarticlesid']").val() ),
					path: "/admin/utils/utils_article/ajax/"+operation+"/" + itemId + "/" + (isarticlesId!='' ? isarticlesId : '0') + "/?name="+data.input.val()+"&group="+data.node.parent.data.id+"",
					callBack: function(data) {
						$.loadingScreen('hide');
						$(this)[0].data.id = data.object.id;
						$(this)[0].data.custom = data.object;
						$(this)[0].setActive();
					}
				})
				return true;
			}
		},
		table: {
			indentation: 20,
			nodeColumnIdx: 2,
			checkboxColumnIdx: 0
		},
		renderColumns: function(event, data) {
			var node = data.node,
				$tdList = $(node.tr).find(">td");
			$tdList.eq(0).attr('style', 'display: none');
			$tdList.eq(1).attr('style', 'display: none');
			$tdList.eq(3).attr('style', 'display: none');
		},
		beforeActivate: function(event, data) {
			if( typeof(data.node.data.type)!='undefined' ) {
				return false;
			}
			var $textarea = $('#contents .panel-body textarea');
			if($textarea.length){
				tinymce.remove();
			}
			$('#contents .panel-body').empty();
		},
		activate: function(event, data) {
			$.clientRequest({
				context: $('#contents .panel-body'),
				path: '/admin/utils/utils_article/ajax/readitem/'+data.node.data.id+'/',
				callBack: function(data) {
					$.loadingScreen('hide');
					$(this).html(data.html);
					//-- FIXME -- Start -- MikeBoriscov -- фикс на tinymce -- после обновления все заработало без него -
					//-- /libs/extended/arteditor/artedit.js -----------------------------------------------------------
					//if (typeof(tinyMCE) != "undefined") {
					//	$('textarea[wysiwyg=1]', $(this)).each(function(i, val){
					//		tinymce.execCommand('mceRemoveControl', false, $(this).attr('id'));
					//		tinymce.execCommand('mceAddControl', false, $(this).attr('id'));
					//	})
					//}
					//-- FIXME -- End ----------------------------------------------------------------------------------
					$('form', $(this)).ajaxForm({
						success: function(responseText, statusText, xhr, $form) {
							$('#ControlElements .okMessage', $form).remove();
							if(statusText=='success') {
								var mess = $('<div />', {
									id: 'id_message',
									class: 'okMessage alert alert-success fade in col-lg-8',
									html: '<div id="message">Информационный элемент изменен.</div>'
								}).insertAfter( $('#ControlElements input', $form));
								setTimeout(function() {
									mess.fadeOut(3000);
									setTimeout(function() {mess.remove()}, 3100);
								}, 2000);
								$('.img_preview', $form).attr('src', responseText.object.preview);
							}
						}
					});
				}
			})
		}
	});

	return object;
}