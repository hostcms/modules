<?php
use Utils_Utl as utl;
use Utils_Convert_Array_Xml_Entity as xmls;

class Utils_Shop_Controller_Show extends Shop_Controller_Show
{
	protected $_parentGroups = array();
	protected $_parentGroupIDs = array();
	protected $_currentItem = null;
	/** @var Shop_Group_Model _currentGroup */
	protected $_currentGroup = null;
	protected $_childGroupIDs = null;
	protected $_zero = null;
	protected $_breadcrumbsHTML = '';
	protected $_relatedHTML = '';
	protected $_instance = '';
	protected $_watcher = null;
	protected $_useTWIG = false;
	protected $_route = array();
	protected $_groupXMLProperties = FALSE;
	protected $_parsedSrc = array();
	protected $_splitResultsUrl = '';
	protected $_sparesSplitedUrl = '';
	protected $_noPropertiesPath = '';
	protected $_currentXSLName = '';
	protected $_aTmpProperties = FALSE;
	protected $_qbExtraProcessURL = array();
	protected $_allowCustomProperties = FALSE;
	protected $_allowCustomPropertiesDir = 0;
	protected $_customProperties = FALSE;
	protected $_customGroupProperties = [];
	protected $_customItemProperties = [];
	protected $_countShopItems = false;
	protected $_is_main = false;
	protected $_is_group = false;
	protected $_is_item = false;

	protected $_forbiddenItemTags = array(
		'shortcut_id',
		'shop_tax_id',
		'shop_seller_id',
		'shop_currency_id',
		'shop_producer_id',
		'shop_measure_id',
		'vendorcode',
		'text',
		'weight',
		'active',
		'siteuser_group_id',
		'sorting',
		'path',
		'seo_title',
		'seo_description',
		'seo_keywords',
		'indexing',
		'image_small_height',
		'image_small_width',
		'image_large_height',
		'image_large_width',
		'manufacturer_warranty',
		'country_of_origin',
		'yandex_market_sales_notes',
		'user_id',
		'siteuser_id',
		'modification_id',
		'guid',
		'showed',
		'length',
		'width',
		'height',
		'apply_purchase_discount',
		'delivery',
		'pickup',
		'store',
		'deleted',
		'import_key',
		'insta_uploaded',
		'insta_uploaded_date',
		'date',
		'datetime',
		'start_datetime',
		'end_datetime',
		'rest',
		'reserved',
		'price',
		'discount',
		'tax',
		'price_tax',
		'currency',
		'date_created',
	);

	protected $_forbiddenGroupTags = array(
		'image_large',
		'image_small',
		'sorting',
		'indexing',
		'active',
		'siteuser_group_id',
		'seo_title',
		'seo_description',
		'seo_keywords',
		'user_id',
		'image_large_width',
		'image_large_height',
		'image_small_width',
		'image_small_height',
		'guid',
		'dir',
		'childs_count',
	);

	/**
	 * @return bool
	 */
	public function isMain()
	{
		return $this->_is_main;
	}

	/**
	 * @return bool
	 */
	public function isGroup()
	{
		return $this->_is_group;
	}

	/**
	 * @return bool
	 */
	public function isItem()
	{
		return $this->_is_item;
	}

	/**
	 * @return bool
	 */
	public function isCountShopItems()
	{
		return $this->_countShopItems;
	}

	/**
	 * @param bool $countShopItems
	 */
	public function countShopItems($countShopItems)
	{
		$this->_countShopItems = $countShopItems;
		return $this;
	}

	/**
	 * @return boolean
	 */
	public function isATmpProperties()
	{
		return $this->_aTmpProperties;
	}

	/**
	 * @param boolean $aTmpProperties
	 */
	public function setATmpProperties($aTmpProperties)
	{
		$this->_aTmpProperties = $aTmpProperties;
		return $this;
	}

	/**
	 * @param array $qbExtraProcessURL
	 */
	public function addQbExtraProcessURL($qbExtraProcessURL)
	{
		$this->_qbExtraProcessURL[] = $qbExtraProcessURL;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSplitResultsUrl()
	{
		return $this->_splitResultsUrl;
	}

	/**
	 * @return string
	 */
	public function getCurrentXSLName()
	{
		return $this->_currentXSLName;
	}

	/**
	 * @param string $currentXSLName
	 */
	public function setCurrentXSLName($currentXSLName)
	{
		$this->_currentXSLName = $currentXSLName;
		return $this;
	}

	/**
	 */
	public function allowCustomProperties($custom, $property_dir = 0)
	{
		$this->_allowCustomProperties = $custom;
		$this->_allowCustomPropertiesDir = $property_dir;
		return $this;
	}

	/**
	 */
	public function isAllowCustomProperties()
	{
		return $this->_allowCustomProperties;
	}

	/**
	 * @return bool
	 */
	public function isCustomProperties()
	{
		return $this->_customProperties;
	}

	/**
	 * @return array
	 */
	public function getCustomGroupProperties()
	{
		return $this->_customGroupProperties;
	}

	/**
	 * @return array
	 */
	public function getCustomItemProperties()
	{
		return $this->_customItemProperties;
	}

	/**
	 * @param int $group
	 */
	public function setGroup($group)
	{
		$this->group = $group;
		return $this;
	}

	/**
	 * @param null $item
	 */
	public function setItem($item)
	{
		$this->item = $item;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getSparesSplitedUrl()
	{
		return $this->_sparesSplitedUrl;
	}

	/**
	 * Moto_Shop_Controller_Out constructor.
	 * @param null $_zero
	 */
	public function __construct($oShop, $groupXMLProperties = FALSE)
	{
		parent::__construct($oShop);
		$this->_zero = new stdClass();
		$this->_zero->id = 0;
		$this->_zero->parent_id = 0;
		$this->_zero->modification_id = 0;
		$this->_zero->path = 'path-undefined-zero-class';
		$this->_instance = Core_Page::instance();
		$this->_watcher = Core_ObjectWatcher::instance();
		$this->_route = json_decode($this->_instance->srcrequest->splitedPath, true);
		$this->_groupXMLProperties = $groupXMLProperties;
		$this->_parsedSrc = array_values(json_decode($this->_instance->srcrequest->splitedPath, true));
		$this->_instance->addAllowedProperty('canonicalLink');
		$this->_instance->addAllowedProperty('wheelsFilter');
//		$this->_instance->wheelsFilter = [];
		if(!isset($this->_instance->listItemFilter)) {
			$this->_instance->addAllowedProperty('listItemFilter');
			$this->_instance->listItemFilter = array();
		}
		if(!isset($this->_instance->relationsItemFilter)) {
			$this->_instance->addAllowedProperty('relationsItemFilter');
			$this->_instance->relationsItemFilter = new stdClass();
			$this->_instance->relationsItemFilter->in = '';
			$this->_instance->relationsItemFilter->out = '';
		}
		if(!isset($this->_instance->rangeItemFilter)) {
			$this->_instance->addAllowedProperty('rangeItemFilter');
			$this->_instance->rangeItemFilter = array();
		}
		if(!isset($this->_instance->qbNoFilter)) {
			$this->_instance->addAllowedProperty('qbNoFilter');
			$this->_instance->qbNoFilter = null;
		}
		if(!isset($this->_instance->shopGroupCollections)) {
			$this->_instance->addAllowedProperty('shopGroupCollections');
			$this->_instance->shopGroupCollections = null;
		}
		return $this;
	}

	public function parseUrl()
	{
		$aLastPath = $isCanoniсal = FALSE;
		$this->_instance->canonicalLink = '';
		if(isset($this->_splitResultsUrl[1]['lastPath'])) {
			$aLastPath = array_values(array_filter(explode('/', $this->_splitResultsUrl[1]['lastPath'])));
		} elseif (isset($this->_splitResultsUrl[0]['lastPath'])) {
			$aLastPath = array_values(array_filter(explode('/', $this->_splitResultsUrl[0]['lastPath'])));
		}

		$this->_instance->qbNoFilter = clone $this
			->ShopItems()
			->queryBuilder();

		$wheelMatches = $aWheels = $aWheelPaths = [];
		preg_match('/(.*\/?)(\/wheels\/tires\/(.*?\/)?((r(\d+))\/)?((tw-(\d*)\/)?)((tp-(\d*)\/)?)((thi-(\d*)\/)?)((twi-(\d*)\/)?))$/ui',
			$this->_instance->srcrequest->pathNoPage, $wheelMatches);
//		Skynetcore_Utils::p($wheelMatches);
//		Skynetcore_Utils::p($wheelMatches);
		if(count($wheelMatches)) {
			$aWheelPaths[3304] = trim(Core_Array::get($wheelMatches, 5, false), '/');
			$aWheels[3304] = Core_Array::get($wheelMatches, 6, false);
			$wheelMatchesExclude[] = 'r'.$aWheels[3304];
			$aWheelPaths[3306] = trim(Core_Array::get($wheelMatches, 8, false), '/');
			$aWheels[3306] = Core_Array::get($wheelMatches, 9, false);
			$wheelMatchesExclude[] = 'tw-'.$aWheels[3306];
			$aWheelPaths[3305] = trim(Core_Array::get($wheelMatches, 11, false), '/');
			$aWheels[3305] = $wheelMatchesExclude[] = Core_Array::get($wheelMatches, 12, false);
			$wheelMatchesExclude[] = 'tp-'.$aWheels[3305];
//			$aWheels[5026] = $wheelMatchesExclude[] = Core_Array::get($wheelMatches, 14, false);
//			$wheelMatchesExclude[] = 'thi-'.$aWheels[5026];
//			$aWheels[5027] = $wheelMatchesExclude[] = Core_Array::get($wheelMatches, 17, false);
//			$wheelMatchesExclude[] = 'twi-'.$aWheels[5027];
			$aWheelPaths[3307] = trim(Core_Array::get($wheelMatches, 14, false), '/');
			$aWheels[3307] = $wheelMatchesExclude[] = Core_Array::get($wheelMatches, 15, false);
			$wheelMatchesExclude[] = 'thi-'.$aWheels[3307];
			$aWheelPaths[3308] = trim(Core_Array::get($wheelMatches, 17, false), '/');
			$aWheels[3308] = $wheelMatchesExclude[] = Core_Array::get($wheelMatches, 18, false);
			$wheelMatchesExclude[] = 'twi-'.$aWheels[3308];
			$aWheels = array_filter($aWheels);
			$aWheelPaths = array_filter($aWheelPaths);
			$aLastPath = array_diff($aLastPath, $wheelMatchesExclude);
		}
//		Skynetcore_Utils::tp($aWheels);
		if(Core_Array::getRequest('test', false) !== false) {
//			Skynetcore_Utils::p($this->_instance->shopGroupCollections);
//			Skynetcore_Utils::v($this->_instance->shopGroupCollections);
//			Skynetcore_Utils::p($aLastPath, count($aLastPath) . '-' . count($aWheels));
//			Skynetcore_Utils::p($this->_instance->srcrequest->pathNoPage, count($aLastPath) . '-' . count($aWheels));
//			Skynetcore_Utils::p($wheelMatches);
//			Skynetcore_Utils::p(Core_Page::instance()->listItemFilter);
//			Skynetcore_Utils::p($wheelMatches);
//			Skynetcore_Utils::p($aWheels);
//			Skynetcore_Utils::p($aLastPath);
		}

		$wheelsFilter = is_null($this->_instance->wheelsFilter) ? [] : $this->_instance->wheelsFilter;
		$countLastPath = 3+count($aWheels);
		switch (true) {
			case false:
				break;
		}
		if(is_array($aLastPath)
			&& (
				   count($aLastPath) < $countLastPath
				|| Core_Array::get($aLastPath, 0) === 'related'
			)
		) {
			$aLastPathSource = $aLastPath;
			$_splitResultsUrl = $this->_splitResultsUrl;
			$_splitResultsUrlKeys = array_keys($_splitResultsUrl[0]['shopGroups']);
			$lastGroupKey = end($_splitResultsUrlKeys)*1;

			if(isset($_splitResultsUrl[0]['shopGroups'][$lastGroupKey])) {
				$lastGroup = $_splitResultsUrl[0]['shopGroups'][$lastGroupKey];
				$filter2PropertyIDs = utl::getArrayValuesFromArrays($lastGroup->Shop->Shop_Item_Properties->getAllByFilter(2), 'property_id');
				$linkedObject = Core_Entity::factory('Shop_Item_Property_List', $lastGroup->shop_id);
				$aGroupProperties2 = $linkedObject->getPropertiesForGroup($lastGroup->id, $filter2PropertyIDs);
				$aGroupPropertyIDs = utl::getArrayValuesFromArrays($aGroupProperties2);

				if(false && isset($this->_instance->skynet->request->aSplitedPath) &&
					($topLevel = Core_Array::get($this->_instance->skynet->request->aSplitedPath, 0, 'none-undefined')) != 'none-undefined'
				) {
					/** @var Moto_Cache_Group_Lev1_Property_Model $qGroupPropertiesLev1 */
					$qGroupPropertiesLev1 = Core_Entity::factory('Moto_Cache_Group_Lev1_Property');
					$aGroupPropertiesLev1 = $qGroupPropertiesLev1->getAllByGroup_path($topLevel, false);

					$aGroupPropertyIDs = array_values(array_unique(array_merge($aGroupPropertyIDs, array_map(function($oGroupPropertiesIDs_item) {
						return $oGroupPropertiesIDs_item->property_id;
					}, $aGroupPropertiesLev1))));
				}
				count($aGroupPropertyIDs)==0 && $aGroupPropertyIDs = array(-1);

//				//-- Колеса. Радиус/ширина/высота. Start ---------------------------------------------------------------
				//-- TODO -- Select ListFilterURL
				$mProperties = Core_QueryBuilder::select(array('p.id', 'property_id'))
					->select(array('p.tag_name', 'tag_name'))
					->select(array('li.id', 'list_item_id'))
					->select(array('li.path', 'list_item_path'))
					->from(array('properties', 'p'))
					->join(array('list_items', 'li'), 'li.list_id', '=', 'p.list_id', array(
						array(
							'AND' => array('p.id', 'IN', $aGroupPropertyIDs)
						)
					))
					->where('li.path', 'IN', array_merge($aLastPath, ['undefined-path-to-found-item']))
					->where('li.deleted', '=', 0)
					->where('li.active', '=', 1)
					->orderBy('p.sorting')
				;
				if(count($aWheels)) {
					$mProperties
						->setOr()
						->open();
					foreach ($aWheelPaths as $wheelKey => $wheelValue) {
						$wheelsFilter[$wheelKey] = [
							'path' => $wheelValue,
							'value' => $aWheels[$wheelKey],
						];
						if(array_keys($aWheels)[0] != $wheelKey) {
							$mProperties
								->setOr();
						}
						$mProperties
							->open()
							->where('li.path', '=', $wheelValue)
							->setAnd()
							->where('p.id', '=', $wheelKey)
							->close()
						;
					}
					$mProperties
						->close();
				}
//				Skynetcore_Utils::p($this->_instance->wheelsFilter);
				//-- Колеса. Радиус/ширина/высота. End -----------------------------------------------------------------
				//-- Техника. Объем двигателя. Start -------------------------------------------------------------------
				$engineVolumeMatches = $aEngineVolumes = [];
				$rangeItemFilter = $this->_instance->rangeItemFilter;
				if(preg_match('/\/(volume-)(\d+)(-(\d+))?\/?$/ui', $this->_instance->srcrequest->pathNoPage, $engineVolumeMatches)) {
					$rangeMin = $rangeMinSource = Core_Array::get($engineVolumeMatches, 2, -10000000);
					$rangeMax = $rangeMaxSource = Core_Array::get($engineVolumeMatches, 4, ($rangeMin==-10000000) ? 10000000 : $rangeMin);
					$aVolumeProperties = Core_Entity::factory('Shop_Item_Property')->getAllByPrefix('volume');
					$aVolumePropertiesIDs = utl::getArrayValuesFromArrays($aVolumeProperties, 'property_id');

					switch (true) {
						case $rangeMin <= 191:
							$rangeMin -= 5;
							$rangeMax += 5;
							break;
						case $rangeMin > 191 && $rangeMin <= 400:
							$rangeMin -= 30;
							$rangeMax += 30;
							break;
						default:
							$rangeMin -= 50;
							$rangeMax += 50;
							break;
					}

					if(count($aVolumePropertiesIDs)) {
						$aGroupPropertiesVolume = $linkedObject->getPropertiesForGroup($lastGroup->id, $aVolumePropertiesIDs);
						$aGroupPropertiesVolumeIDs = utl::getArrayValuesFromArrays($aGroupPropertiesVolume);

						if(count($aGroupPropertiesVolumeIDs)) {
							$rangeItemFilter[$aGroupPropertiesVolumeIDs[0]] = [
								'property' => $aGroupPropertiesVolume[0]->getStdObject(),
								'shop_property' => $aGroupPropertiesVolume[0]->shop_item_property->getStdObject(),
								'measure' => $aGroupPropertiesVolume[0]->shop_item_property->shop_measure->getStdObject(),
								'sources' => [
									'range_min' => $rangeMinSource,
									'range_max' => $rangeMaxSource,
								],
								'range_min' => $rangeMin,
								'range_max' => $rangeMax,
							];
						}
						Core::$url['path'] = str_replace($_splitResultsUrl[0]['lastPath'], '/', Core::$url['path']);
						if(isset($this->url)) {
							$this->url = Core::$url['path'];
						}
					}
				}
				$this->_instance->rangeItemFilter = $rangeItemFilter;
				//-- Техника. Объем двигателя. End ---------------------------------------------------------------------
				$aProperties = $mProperties->execute()->asAssoc()->result();
//				Skynetcore_Utils::p($mProperties->build());
				$aSelectedListItemIDs = array_keys(utl::getArrayKeyValuesFromArrays($aProperties, 'list_item_id'));

				switch (true) {
					case ((count($aLastPath)+count($aWheels))==count($aSelectedListItemIDs) || count($aWheels)):
						Core::$url['path'] = str_replace($_splitResultsUrl[0]['lastPath'], '/', Core::$url['path']);
						if(isset($this->url)) {
							$this->url = Core::$url['path'];
						}
						$listItemFilter = $this->_instance->listItemFilter;
						$aSumSelect = [];
						foreach ($aProperties as $aPropertyKey => $aProperty) {
							$listItemFilter[$aProperty['property_id']] = Core_Entity::factory('List_Item')->getById($aProperty['list_item_id']*1);

							$aSumSelect[] = "CASE WHEN NOT (GROUP_CONCAT(DISTINCT plisti".$aProperty['property_id']."_".$aPropertyKey.".id)) IS NULL THEN 1 ELSE NULL END";
							$linked_entity = "shop_items";
							if( preg_match('/'.Core_Array::get(Core_Page::instance()->siteconf, 'property_tag_regexp', 'undefined-value-item').'/', $aProperty['tag_name']) ) {
								$aJoins = $this->ShopItems()->queryBuilder()->getJoin();

								$aJoins = $this->ShopItems()->queryBuilder()->getJoin();
								$fJoins = array_filter($aJoins, function ($joinItem) {
									return (isset($joinItem[0]) && $joinItem[0] == 'INNER JOIN' && isset($joinItem[1][1]) && $joinItem[1][1] == 'sizes_mods');
								});
								if(count($fJoins)==0) {
									$this
										->ShopItems()
										->queryBuilder()
										->join(['shop_items', 'sizes_mods'], 'sizes_mods.modification_id', '=', 'shop_items.id')
										->groupBy('shop_items.id')
									;
									$linked_entity = 'sizes_mods';
								}
							}
							$this
								->ShopItems()
								->queryBuilder()
								->select([Core_QueryBuilder::expression('GROUP_CONCAT(DISTINCT plisti'.$aProperty['property_id'].'_'.$aPropertyKey.".id)"), 'datap'.$aProperty['property_id'].'_'.$aPropertyKey.'_id'])
								->select([Core_QueryBuilder::expression('GROUP_CONCAT(DISTINCT plisti'.$aProperty['property_id'].'_'.$aPropertyKey.".value)"), 'datap'.$aProperty['property_id'].'_'.$aPropertyKey.'_value'])
								->leftJoin(array('property_value_ints', 'plisti'.$aProperty['property_id'].'_'.$aPropertyKey), "{$linked_entity}.id", '=', 'plisti'.$aProperty['property_id'].'_'.$aPropertyKey.'.entity_id',
									array(
										array('AND' => array('plisti'.$aProperty['property_id'].'_'.$aPropertyKey.'.value', '=', $aProperty['list_item_id']*1)),
										array('AND' => array('plisti'.$aProperty['property_id'].'_'.$aPropertyKey.'.property_id', '=', $aProperty['property_id']*1)),
									)
								)
								->open()
								->where('plisti'.$aProperty['property_id'].'_'.$aPropertyKey.'.value' ,'>', 0)
								->setOr()
								->where('shop_items.modification_id','>', 0)
								->close()
							;
						}
						$listItemFilter = array_filter($listItemFilter);

						foreach ($listItemFilter as $tmpPropertyId => $tmpListItem) {
							$tmpListItem->addEntity(
								Core::factory('Core_Xml_Entity')
									->name('property_id')
									->value($tmpPropertyId)
							);
						}
						if(count($listItemFilter)) {
							$tmpListItemPath = [];
							foreach ($listItemFilter as $tf) {
								$tmpListItemPath[] = $tf->path;
								$aLastPath = array_filter($aLastPath, function ($tmpLastPathItem) use ($tf) {
									return $tmpLastPathItem != $tf->path;
								});
							}
							$sTmpListItemPath = '/'.implode('/', $tmpListItemPath).'/';
							$isCanoniсal = !($_splitResultsUrl[0]['lastPath'] == $sTmpListItemPath);
							if($isCanoniсal) {
								$this->_instance->canonicalLink = preg_replace('/\/\//', '/', $_splitResultsUrl[0]['firstPath'] . $sTmpListItemPath);
							}
						}
						$this->_instance->listItemFilter = $listItemFilter;
						$this->_instance->wheelsFilter = $wheelsFilter;
//						Skynetcore_Utils::tp(count($this->_instance->listItemFilter));
						$collection = Core_Array::get($this->_instance->listItemFilter, 1822, false);
						if($collection === false && count($aLastPathSource) == $countLastPath-1) {
							return $this->error404();
						}
						break;
					case Core_Array::get($aLastPath, 0) === 'related':
						unset($aLastPath[0]);
						$this->url = $this->_splitResultsUrl[0]['firstPath'];
						$this->_instance->relationsItemFilter->in = Core::$url['path'] = $this->url;
						$this->_instance->relationsItemFilter->out = "/".implode('/', $aLastPath)."/";
						/** @var Umotors_Cache_Relation_To_Item_Model $qUmotorsCacheRelationToItems */
						$qUmotorsCacheRelationToItems = Core_Entity::factory('Umotors_Cache_Relation_To_Item');
						$qUmotorsCacheRelationToItems
							->queryBuilder()
							->select([Core_QueryBuilder::expression('COUNT(umotors_cache_relation_to_items.id)'), 'datacounts'])
							->where('umotors_cache_relation_to_items.url', 'REGEXP', "^{$this->_instance->relationsItemFilter->in}")
							->where('umotors_cache_relation_to_items.rel_url', 'REGEXP', "^{$this->_instance->relationsItemFilter->out}")
						;
						$aUmotorsCacheRelationToItems = $qUmotorsCacheRelationToItems->findAll(false);
						$dataCounts = Core_Array::get($aUmotorsCacheRelationToItems, 0, json_decode('{"datacounts: 0"}'))->datacounts;
						if($dataCounts == 0) {
							return $this->error404();
						}
						break;
					default:
						$aTmpPropertiesForTranspone = [];
						foreach ($aProperties as $property) {
							$aTmpPropertiesForTranspone[$property['list_item_path']][] = $property;
						}
						$redirect308 = false;

						foreach ($aTmpPropertiesForTranspone as $tmpPropertiesForTransponeKey => $tmpPropertiesForTranspone) {
							if(count($tmpPropertiesForTranspone) > 1) {
								foreach ($tmpPropertiesForTranspone as $listItemKey => $listItemValue) {
									if($listItemKey > 0) {
										$oListItem = Core_Entity::factory('List_Item')->getById(Core_Array::get($listItemValue, 'list_item_id', 0));
										if(isset($oListItem->path) && $oListItem->path != '') {
											Skynetcore_Chat_Telegram_Controller::sendOk(json_encode($_SERVER), 'prod', -610241680);
										}
									}
								}
							}
						}
						if($redirect308) {
							$this->_instance
								->response
								->status(308)
								->header('Location', (isset($this->url) ? $this->url : Core::$url['path']))
								->sendHeaders();
							exit();
						}
				}
			}
		}
//Skynetcore_Utils::tp($this->_instance->listItemFilter);
//		Skynetcore_Utils::p($this->_instance->response->getStatus(), count($aLastPath));
//		Skynetcore_Utils::p($aLastPath, count($aLastPath));
		if((count($aLastPath)
			&& !(count($aLastPath)==1 && preg_match('/gallery(-(\d+|main))?/ui', Core_Array::get($aLastPath, 0, 'undefined')))
			)
//			&& !(preg_match('/^\/spares\/wheels\/tires\//ui', $this->_instance->skynet->request->pathNoPage))
			&& $this->_instance->relationsItemFilter->out == '') {
			return $this->error404();
		}
//		Skynetcore_Utils::p($this->_instance->response->getStatus(), 1);
		parent::parseUrl();
//		Skynetcore_Utils::p($this->_instance->response->getStatus(), 2);
//		if ($this->_instance->response->getStatus() != 200) {
//		}
		if(   (isset($this->getCurrentItem()->id) && $this->getCurrentItem()->id > 0
			&& $this->_instance->srcrequest->pathNoPage != mb_strtolower("/".$this->getCurrentItem()->getPath())
			&& $this->_instance->relationsItemFilter === '')
			|| $this->_instance->response->getStatus() != 200
		) {
			return $this->error404();
		}

		$this->getCurrentGroup();
		//-- Заполнение доп. параметров для XSL ------------------------------------------------------------------------
		if ($this->_instance->response->getStatus() == 200) {
			if($this->_instance->srcrequest->pathdeep == 1) {
				$this->_is_main = true;
			} elseif ($this->item > 0) {
				$this->_is_item = true;
			} elseif ($this->group > 0) {
				$this->_is_group = true;
			}
			/** @var Shop_Group_Model $lastGroup */
			$lastGroup = $this->getLastGroup();
			if(method_exists($lastGroup, 'showXmlProperties')) {
				$lastGroup
					->showXmlProperties(true);
			}
			$this->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('change_url_nopage')->value($this->_instance->srcrequest->pathNoPage)
			)->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('ТекущаяГруппа')->value($this->group)
			)->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('ТекущийТовар')->value($this->item)
			)->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('ТекущийГод')->value(date("Y"))
			);
			($this->getFirstGroup()->id>0) && $this->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('ВерхняяГруппа')->value($this->getFirstGroup()->id)
			);
			($this->getLastGroup()->id>0) && $this->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('CurrentGroup')->addEntity($lastGroup)
			);
		}

		return $this;
	}

	public function getFirstGroup()
	{
		$this->getParentGroups();
		return count($this->_parentGroups) > 0 ? $this->_parentGroups[array_keys($this->_parentGroups)[0]] : $this->_zero;
	}

	public function getLastGroup()
	{
		$this->getParentGroups();
		$parentGroupKeys = array_keys($this->_parentGroups);
		$parentGroupIndexID = end($parentGroupKeys);
		return count($this->_parentGroups) > 0 ? $this->_parentGroups[$parentGroupIndexID] : $this->_zero;
	}

	public function getParentGroupIDs()
	{
		return array_keys($this->getParentGroups());
	}

	public function getParentGroups()
	{
//		Skynetcore_Utils::p($this->group);
		if (count($this->_parentGroups) == 0 && $this->group > 0) {
			$currentGroup = Core_Entity::factory('Shop_Group')->getById($this->group);
			do {
				$currentGroup->showXmlProperties($this->_groupXMLProperties);
				$this->_parentGroups[$currentGroup->id] = $currentGroup;
			} while ($currentGroup = $currentGroup->getParent());
			$this->_parentGroups = array_reverse($this->_parentGroups, true);
		}
		$this->_parentGroupIDs = array_keys($this->_parentGroups);
		return $this->_parentGroups;
	}

	public function getChildGroups() {
		if(is_null($this->_childGroupIDs)) {
			$this->setChildGroups();
		}
		return $this->_childGroupIDs;
	}

	public function setChildGroups($fromLevel=1, $toLevel=10, $additionalGIDs=[])
	{
		$splitedPath = json_decode($this->_instance->srcrequest->splitedPath, true);
		$path = '/'.implode('/', array_slice($splitedPath, 0, $fromLevel)).'/';

		$subgroupsForMenu = Utils_Observers_Shop_Group::getGroups($this->getEntity()->id, $path, $toLevel);
		$childGroups = utl::readTreeIDsFromLevel($subgroupsForMenu, $this->getLastGroup()->id);
		$topChildGroups = utl::readTreeIDsFromLevel($subgroupsForMenu, $this->getFirstGroup()->id);

		$this->_childGroupIDs = count($childGroups)>0 ? $childGroups : array(-1);
		$allGroups = array_merge($this->_childGroupIDs);
		$restrictGroups = array_merge($this->_parentGroupIDs, $topChildGroups, $this->_childGroupIDs);
		$restrictGroups = array_map(function ($val) {
			return $val*1;
		}, $restrictGroups);
		if(count($additionalGIDs)) {
			$restrictGroups = array_merge($restrictGroups, $additionalGIDs);
		}
		sort($allGroups);
		if($this->item==0) {
			$this
				->group(false)
				->shopItems()
				->queryBuilder()
				->where('shop_items.shop_group_id', 'IN', array_unique($allGroups))
			;
		}
		$groupsBuilder = $this->shopGroups()->queryBuilder();
		$groupsBuilder
			->where('id', 'IN', array_unique($restrictGroups))
		;

		return $this;
	}

	public function getCurrentGroup()
	{
		if (is_null($this->_currentGroup) && $this->group > 0) {
			$this->_currentGroup = Core_Entity::factory('Utils_Shop_Group')->getById($this->group);
		} elseif (!is_object($this->_currentGroup)) {
			$this->_currentGroup = $this->_zero;
		}
		return $this->_currentGroup;
	}

	public function getCurrentItem()
	{
		if (is_null($this->_currentItem) && $this->item > 0) {
			$this->_currentItem = Core_Entity::factory('Utils_Shop_Item')->getById($this->item);
		} elseif ($this->item == 0) {
			$this->_currentItem = $this->_zero;
		}
		return $this->_currentItem;
	}

	/**
	 * @return boolean
	 */
	public function isUseTWIG()
	{
		return $this->_useTWIG;
	}

	/**
	 * @param boolean $useTWIG
	 */
	public function useTWIG($useTWIG)
	{
		$this->_useTWIG = $useTWIG;
		return $this;
	}

	/**
	 * @param array $forbiddenItemTags
	 */
	public function setForbiddenItemTags()
	{
		$this->itemsForbiddenTags($this->_forbiddenItemTags);
		return $this;
	}

	/**
	 * @param array $forbiddenGroupTags
	 */
	public function setForbiddenGroupTags()
	{
		$this->groupsForbiddenTags($this->_forbiddenGroupTags);
		return $this;
	}

	/**
	 * @return array
	 */
	public function getForbiddenItemTags()
	{
		return $this->_forbiddenItemTags;
	}

	/**
	 * @return array
	 */
	public function getForbiddenGroupTags()
	{
		return $this->_forbiddenGroupTags;
	}

	public function setDefaultsOff()
	{
		$this
			->groupsProperties(FALSE)
			->groupsPropertiesList(FALSE)
			->propertiesForGroups(FALSE)
			->item(FALSE)
			->itemsProperties(FALSE)
			->itemsPropertiesList(FALSE)
			->modifications(FALSE)
			->modificationsList(FALSE)
			->filterShortcuts(FALSE)
			->specialprices(FALSE)
			->associatedItems(FALSE)
			->comments(FALSE)
			->votes(FALSE)
			->viewed(FALSE)
			->tags(FALSE)
			->siteuser(FALSE)
			->siteuserProperties(FALSE)
			->bonuses(FALSE)
			->comparing(FALSE)
			->favorite(FALSE)
			->cart(FALSE)
			->warehousesItems(FALSE)
			->taxes(FALSE)
			->offset(0)
			->page(0)
			->calculateTotal(TRUE)
			->showPanel(FALSE);
		return $this;
	}

	public function setLimitsBySession($session = 'ALLITEMS')
	{
		if (!isset($_SESSION['SECTION'])) {
			$_SESSION['SECTION'] = array('CURRENT' => '-', 'PREV' => '=');
		}
		$_SESSION['SECTION']['CURRENT'] = Core_Array::get($this->_parsedSrc, 0, '');
		if ($_SESSION['SECTION']['CURRENT'] != $_SESSION['SECTION']['PREV'] && Core_Array::getRequest('action', '') != 'refresh') {
			$_SESSION[$session] = $this->_entity->items_on_page;
			unset($_SESSION['SHOW_ONLY']);
		}

		$_SESSION[$session] = (isset($_REQUEST[mb_strtolower($session)]) ? $_REQUEST[mb_strtolower($session)] : (isset($_SESSION[$session]) ? $_SESSION[$session] : $this->_entity->items_on_page));
		if (!isset($this->limit)
			|| is_null($this->limit)
			|| $this->limit == 0
		) {
			$this
				->limit($_SESSION[$session]);
		}
		(isset($_SESSION['SECTION']['PREV']) && isset($_SESSION['SECTION']['CURRENT'])) && $_SESSION['SECTION']['PREV'] = $_SESSION['SECTION']['CURRENT'];

		return $this;
	}

	/**
	 * Add tree groups to XML
	 * @return self
	 */
	public function addTopGroups()
	{
		$this->_Shop_Groups
			->queryBuilder()
			->where('active', '=', 1);
		$groups = $this->_Shop_Groups->getAllByParent_Id($this->group);
		$this->addEntity(
			Core::factory('Utils_Core_Xml_Entity')
				->name('allTopGroups')->addEntities($groups)
		);
		return $this;
	}

	public function show()
	{
		$this
			->shopItems()
			->queryBuilder()
			->where('shop_items.id', '<>', Core_QueryBuilder::expression('shop_items.modification_id'));
		$entities = array();

		if(is_array($this->_aTmpProperties)) {
			$tmpProperties = array_merge($this->itemsProperties, $this->_aTmpProperties);
			$this->itemsProperties($tmpProperties);
		}
		foreach ($this->_entities as $entity) {
			switch (get_class($entity)) {
				case 'Utils_Core_Xml_Entity':
				case 'Core_Xml_Entity':
					if (is_object($entity) && method_exists($entity, 'getEntities')) {
						$entities['core_xml_entity'][$entity->name] = $entity->getEntities();
					} else {
						$entities['core_xml_entity'][$entity->name] = $entity->value;
					}
					break;
				default:
					$entities[strtolower(get_class($entity))][] = $entity;
			}
		}
		$currentItem = $this->item>0 ? $this->item : 0;
		$this->addEntity(
			Core::factory('Core_Xml_Entity')
				->name('current_item_id')
				->value($currentItem)
		);

		if ($this->_useTWIG && file_exists(CMS_FOLDER . 'hostcmsfiles/twig/' . ($twigPath = $this->_xsl->id . '.twig'))) {
			echo $this->_instance->twig->render($twigPath, array(
				'csrf' => isset($this->_instance->skynet->response->csrf->token) ? $this->_instance->skynet->response->csrf->token : '',
				'entity' => $this->_entity,
				'entities' => $entities,
			));
		} else {
			if($this->item==0 && method_exists($this, 'addExternalEntities')) {
				$this->addExternalEntities();
			}

			if(is_object($this->_watcher) && is_object($this->_currentGroup) && $this->_currentGroup instanceof Core_Entity) {
				$this->_watcher->delete($this->_currentGroup);
			}

			if($this->_countShopItems) {
				Core_Event::attach( get_class($this).'.onBeforeShow', array('Utils_Observers_Shop_Item', 'onAfterFindAllSetCalcFoudRow'));
				$this->_countShopItems = false;
			}

			$rValue = parent::show();
			return $rValue;
		}
	}

	public function get()
	{
		if(Core_Array::getRequest('test', false) !== false) {
			$this->itemsProperties = false;
			$this->itemsProperties = false;
			$this->itemsProperties = false;
		}

		return parent::get();
	}

	public function check404($apply404 = true) {
		if(!$this->_instance->srcrequest->isMain && isset( $this->item ) && $this->item*1==0) {
			$qb = clone $this->shopItems()->queryBuilder();
			$aSelected = $qb->getSelect();
			$qb->clearSelect();
			$aAdditionalSelected = [];
			foreach ($aSelected as $oSelect) {
				if(is_array($oSelect)) {
					$aAdditionalSelected[] = $oSelect;
				}
			}
			$qb->select('shop_items.saled')
				->select('shop_items.active')
				->select('shop_items.deleted');
			if(count($aAdditionalSelected)) {
				array_map(function($tmpSelect) use ($qb) {
					$qb->select($tmpSelect);
				}, $aAdditionalSelected);
				$qb->select(array(Core_QueryBuilder::expression('COUNT(shop_items.id)'), 'cnt'))
				;
			} else {
				$qb->select(array(Core_QueryBuilder::expression('SUM(shop_items.id)'), 'cnt'));
			}
			$qb
				->clearOrderBy()
				->from('shop_items')
				->where('shop_items.active', '=', 1)
				->where('shop_items.deleted', '=', 0)
			;
			if ($this->group)
			{
				// если ID группы не 0, т.е. не корневая группа
				// получаем подгруппы
				if(method_exists($this, 'fillShopGroup')) {
					$aSubGroupsID = $this->fillShopGroup($this->getEntity()->id, $this->group); // добавляем текущую группу в массив
				} else {
					$aSubGroupsID = $this->getChildGroups(); // добавляем текущую группу в массив
				}
				$aSubGroupsID[] = $this->group;
				$aSubGroupsID = array_values(array_filter(array_unique($aSubGroupsID)));

				$qb->where('shop_items.shop_group_id', 'IN', $aSubGroupsID); // получаем все товары из подгрупп
			}
			if(count($qb->getGroupBy()) == 0) {
				$qb->groupBy('shop_items.id');
			}
			$cnt = $qb->execute()->asAssoc()->result(FALSE);
			if(!isset($cnt[0]['cnt']) || (isset($cnt[0]['cnt']) && $cnt[0]['cnt']==0)) {
				if($apply404) {
					return $this->error404();
				} else {
					return $this->_instance->response->status(404);
				}
			}
		}
		return $this;
	}

	public function processURL($shopID1, $shopID2=array(), $linkedPropertyIDs=array(), $topGroupIDs=array(), $entityPropertiesIDs=FALSE)
	{
		if($this->_instance->srcrequest->crc32pathNoPage != 2043925204) {
//			$srcURL = Core::$url['path'];
			$srcURL = $this->_instance->srcrequest->pathNoPage;

			$splitByShops = array($shopID1, $shopID2);
			foreach ($splitByShops as $splitByShopKey => $splitByShop) {
				if (isset($splitByShop['shopID'])) {
					$splitResult = utl::findInEntityByURL('Shop', $splitByShop['shopID'], $srcURL, true);
					$srcURL = $splitResult['lastPath'];
					if ($srcURL != $splitResult) {
						$splitResults[] = $splitResult;
					}
				}
			}
			if (isset($splitResults[1]['lastPath']) && $splitResults[1]['lastPath']!='/') {
				//-- херовая затея, при фильтрах работает плохо --
//				$this->error404();
			}

			$groupIDs0 = array_keys($splitResults[0]['shopGroups']);
			$lastGroupID0 = end($groupIDs0);

//-- Новый код, немного оптимизированный в плане количества запросов --
//			$sqSubGroups = Core_QueryBuilder::select('gid.path')
//				->select(array(Core_QueryBuilder::expression('COALESCE(sg1.active, 1) AND COALESCE(sg2.active, 1) AND COALESCE(sg3.active, 1) AND COALESCE(sg4.active, 1) AND COALESCE(sg5.active, 1) AND COALESCE(sg6.active, 1) AND COALESCE(sg7.active, 1) AND COALESCE(sg8.active, 1) AND COALESCE(sg9.active, 1)'), 'active'))
//				->select(array(Core_QueryBuilder::expression('COALESCE(sg1.deleted, 0) OR COALESCE(sg2.deleted, 0) OR COALESCE(sg3.deleted, 0) OR COALESCE(sg4.deleted, 0) OR COALESCE(sg5.deleted, 0) OR COALESCE(sg6.deleted, 0) OR COALESCE(sg7.deleted, 0) OR COALESCE(sg8.deleted, 0) OR COALESCE(sg9.deleted, 0)'), 'deleted'))
//				->from(array('getGroupsIerarchyDown', 'gid'))
//				->leftJoin(array('shop_groups', 'sg1'), 'sg1.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 1)*1"))
//				->leftJoin(array('shop_groups', 'sg2'), 'sg2.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 2)*1"))
//				->leftJoin(array('shop_groups', 'sg3'), 'sg3.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 3)*1"))
//				->leftJoin(array('shop_groups', 'sg4'), 'sg4.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 4)*1"))
//				->leftJoin(array('shop_groups', 'sg5'), 'sg5.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 5)*1"))
//				->leftJoin(array('shop_groups', 'sg6'), 'sg6.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 6)*1"))
//				->leftJoin(array('shop_groups', 'sg7'), 'sg7.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 7)*1"))
//				->leftJoin(array('shop_groups', 'sg8'), 'sg8.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 8)*1"))
//				->leftJoin(array('shop_groups', 'sg9'), 'sg9.id', '=', Core_QueryBuilder::expression("split_string(gid.path, '/', 9)*1"))
//				->where('gid.id', '=', $lastGroupID0);
//
//			$qSubGroups = Core_QueryBuilder::select('tt.path')
//				->from(array($sqSubGroups, 'tt'))
//				->where('tt.active', '=', 1)
//				->where('tt.deleted', '=', 0)
//			;
			$qSubGroups = Core_QueryBuilder::select(['mciga.dirpath_ids', 'path'])
				->from(['moto_cache_item_group_archives', 'mciga'])
				->where('mciga.dirpath_ids', 'LIKE', "{$lastGroupID0}/%")
			;
			$aSubGroups = $qSubGroups->execute()->asAssoc()->result();
			$aChildrenIDs0 = utl::getArrayFromString(utl::getArrayValuesFromArrays($aSubGroups, 'path'), '/');

			$filterInPath = '';
			if(isset($splitResults[1]) && isset($splitResults[1]['inPath']) && $splitResults[1]['inPath'] != '') {
				$filterInPath = preg_replace('/^(.*)(\/|^)page-[\d]+\/?$/', '$1$2', $splitResults[1]['inPath']);
			}
			if (isset($splitResults[1]) && count($splitResults[1]['shopGroups']) > 0) {
				//-- Удаляем из URL лишнее ---------------------------------------------------------------------------------
				Core::$url['path'] = str_replace("{$splitResults[1]['inPath']}", '/', $this->_instance->srcrequest->pathNoPage).(($this->_instance->srcrequest->currentPage>0) ? 'page-'.($this->_instance->srcrequest->currentPage+1).'/' : '');
				//-- Дополняем контроллер нужным фильтром ------------------------------------------------------------------
				$this
					->shopItems()->queryBuilder()
					->join(array('property_value_ints', 'pvilinked'), 'pvilinked.entity_id', '=', 'shop_items.id', array(
						array('AND' => array('pvilinked.property_id', 'IN', $linkedPropertyIDs))
					))
					->join(array('getGroupsIerarchyUp', 'giu'), 'giu.id', '=', 'pvilinked.value')
					->groupBy('shop_items.id')
					->where('giu.url', 'LIKE', $filterInPath . '%');
			}
			//-- Дополняем XML нужными записями ------------------------------------------------------------------------
			$subOutEntities = array();
			if (isset($splitResults[1]['firstPath'])) {
				$path1Splitted = explode('/', $filterInPath);
				$path1Concat = '';
				$arPathiResultList = array();
				array_pop($path1Splitted);

				$selectListsQueryMain = Core_QueryBuilder::select(array('msc.related_id', 'iid'))
					->from(array('moto_spare_cache', 'msc'))
					->where('msc.sgr1', '>', 0)
				;
				if(count($aChildrenIDs0)>0) {
					$selectListsQueryMain
						->where('msc.group_id', 'IN', $aChildrenIDs0)
					;
				}
				foreach ($path1Splitted as $path1SplitKey => $path1Split) {
					$corrPath1SplitKey = $path1SplitKey + 1;
					if($corrPath1SplitKey<4) {
						$path1Concat .= $path1Split . '/';
						$selectListsQuery = clone $selectListsQueryMain;
						$selectListsQuery
							->select(array("msc.sgr{$corrPath1SplitKey}", "gid"))
							->join(array((($corrPath1SplitKey < 3) ? 'shop_groups' : 'shop_items'), "sg{$corrPath1SplitKey}"), "sg{$corrPath1SplitKey}.id", '=', "msc.sgr{$corrPath1SplitKey}")
						;
						$selectListsQuery
							->where('msc.related_url', 'LIKE', $path1Concat . '%')
							->groupBy("msc.sgr{$corrPath1SplitKey}")
							->orderBy("sg{$corrPath1SplitKey}.name");
						if(count($this->_qbExtraProcessURL)>0) {
							foreach ($this->_qbExtraProcessURL as $qbExtraProcessURL) {
								$selectListsQuery
									->union($qbExtraProcessURL);
							}
						}
						$arListsQuery = $selectListsQuery->execute()->asAssoc()->result();
						$arPathiResultList[$path1SplitKey] = utl::getArrayKeyValuesFromArrays($arListsQuery, 'gid');
					}
				}

				if(isset($arPathiResultList[0]) && count($arPathiResultList[0])>0) {
					$mSubOutEntities = Core_Entity::factory('Shop_Group');
					$mSubOutEntities
						->queryBuilder()
						->where('id', 'IN', array_keys($arPathiResultList[0]))
					;
					$subOutEntities = $mSubOutEntities->findAll(FALSE);

					foreach ($subOutEntities as $seKey => &$subOutEntity) {
						$subOutEntity->showXmlProperties($entityPropertiesIDs);
						if (array_key_exists($subOutEntity->id, $splitResults[1]['shopGroups'])) {
							$l2EntitiesQuery = $subOutEntity->shop_groups;
							$l2EntitiesQuery
								->queryBuilder()
								->where('id', 'IN', array_keys($arPathiResultList[1]))
							;
							$l2Entities = $l2EntitiesQuery->findAll(FALSE);
							$subOutEntity->addEntities($l2Entities);
							foreach ($l2Entities as &$l2Entity) {
								if (array_key_exists($l2Entity->id, $splitResults[1]['shopGroups'])) {
									$arPathiResultList[2][0] = -1;
									if(isset($arPathiResultList[2][$l2Entity->id])) {
										foreach($arPathiResultList[2][$l2Entity->id] as $iid) {
											$arPathiResultList[2][$iid['iid']] = $iid['iid'];
										}
									}
									$l3EntitiesQuery = $l2Entity->shop_items;
									$l3EntitiesQuery
										->queryBuilder()
										->where('id', 'IN', array_keys($arPathiResultList[2]))
									;
									$l3Entities = $l3EntitiesQuery->findAll(FALSE);
									$l2Entity->addEntities($l3Entities);
								}
							}
						}
					}
				}
			}
			$this->_noPropertiesPath = $splitResults[0]['firstPath'];
			$this->addEntity(
				Core::factory('Core_Xml_Entity')->name('splitedCategories')
					->addEntity(Core::factory('Core_Xml_Entity')->name('inPath')->value($splitResults[0]['firstPath']))
					->addEntity(Core::factory('Core_Xml_Entity')->name('outPath')->value((isset($splitResults[1]) ? $splitResults[1]['firstPath'] : $splitResults[0]['firstPath'])))
					->addEntity(Core::factory('Core_Xml_Entity')->name('outGroups')->addEntities(isset($splitResults[1]) ? $splitResults[1]['shopGroups'] : $splitResults[0]['shopGroups']))
					->addEntity(Core::factory('Core_Xml_Entity')->name('outList')->addEntities($subOutEntities))
			);
			$this->_splitResultsUrl = $splitResults;
			$this
				->addCacheSignature('urlfilters=' . md5($splitResults[0]['lastPath']));
			$this->_sparesSplitedUrl = array_merge($splitResults[0]['shopGroups'], isset($splitResults[1]['shopGroups']) ? $splitResults[1]['shopGroups'] : array()); //, array($splitResults[1]['shopItem'])); //, $subOutEntities);
		}

		return $this;
	}

	public function xsl($oXslName=NULL)
	{
		if( is_object($oXslName) && get_class($oXslName)=='Xsl_Model' ) {
			return parent::xsl($oXslName);
		} else {
			$oXslName=='' && $oXslName = $this->_currentXSLName;
			$oXsl = Core_Entity::factory('Xsl')->getByName($oXslName);
			if(is_null($oXsl)) {

			} else {
				return parent::xsl($oXsl);
			}
		}
	}

	public function getAllChildGroups($groupID=-1) {
		$fTopGroup=$this->getCurrentGroup();
		if( !($groupID==-1 && $fTopGroup->id>0) && $groupID>0) {
			$fTopGroup = Core_Entity::factory('Shop_Group')->getById($groupID, false);
		}
		$allChildIDs = [];
		if(isset($fTopGroup->id) && $fTopGroup->id > 0) {
			$oGroups = Core_QueryBuilder::select('gid.path')
				->from(['getGroupsIerarchyDown', 'gid'])
				->where('gid.url', 'REGEXP', "^\/".preg_replace('/\//', '\/', $fTopGroup->getPath()))
			;
			$aGroups = $oGroups->asAssoc()->execute()->result();
			foreach ($aGroups as $aGroup) {
				$aGroupTmp = explode('/', $aGroup['path']);
				$allChildIDs = array_merge($allChildIDs, $aGroupTmp);
			}
			$allChildIDs = array_unique($allChildIDs);
		}
		return $allChildIDs;
	}

	public function processSeo() {
		switch (true) {
			case $this->item==0 && $this->group>0:
				$this->_instance->title = $this->getCurrentGroup()->seo_title!='' ? $this->getCurrentGroup()->seo_title : $this->getCurrentGroup()->name;
				$this->_instance->keywords = $this->getCurrentGroup()->seo_keywords!='' ? $this->getCurrentGroup()->seo_keywords : $this->getCurrentGroup()->name;
				$this->_instance->description = $this->getCurrentGroup()->seo_description!='' ? $this->getCurrentGroup()->seo_description : $this->getCurrentGroup()->name;
				break;
			case $this->item>0:
				$this->_instance->title = $this->getCurrentItem()->seo_title!='' ? $this->getCurrentItem()->seo_title : $this->getCurrentItem()->name;
				$this->_instance->keywords = $this->getCurrentItem()->seo_keywords!='' ? $this->getCurrentItem()->seo_keywords : $this->getCurrentItem()->name;
				$this->_instance->description = $this->getCurrentItem()->seo_description!='' ? $this->getCurrentItem()->seo_description : $this->getCurrentItem()->name;
				break;
		}

		return $this;
	}

	public function processAdditionals() {

	}

	//-- Получение галерей ---------------------------------------------------------------------------------------------
	public function addGalleries($galleryTypes=array(), $galleryID = 2, $className = 'Moto_Gallery') {
		$allGalleryItems = array();
		$item = $this->item*1;

		if($item>0) {
			$this->_instance->addAllowedProperty('galleries');
			/** @var Moto_Shop_Item_Model $oMotoItem */
			$oMotoItem = Core_Entity::factory('Moto_Shop_Item')->getById($item);

			$isUsedCurrentItem = Moto_Shop_Controller_Show::isUsed($oMotoItem->marking);

			$aAllGalleries = [];
			//-- Галереи, если они присутствуют и доступны --
			/** @var Moto_Gallery_Model $qGalleries */
			$qGalleries = Core_Entity::factory($className, $galleryID);
			$qGalleries
				->setShowUsed($isUsedCurrentItem==1);
			$aGalleries = $qGalleries->getGallery($this->item*1, false);
			/** @var Umotors_Shop_Item_Model $qModificationItems */
			$qModificationItems = Core_Entity::factory('Umotors_Shop_Item');
			$aModificationItems = $qModificationItems->getAllByModification_id($this->item*1);
			$aModificationItemIDs = array_values(array_filter(array_map(function($modItem) {
				/** @var Umotors_Shop_Item_Model $modItem */
				return $modItem->getRest() == 0 ? false : $modItem->id;
			}, $aModificationItems)));

			if(count($aModificationItemIDs)) {
				foreach ($aModificationItemIDs as $aModificationItemID) {
					/** @var Moto_Gallery_Model $qModGalleries */
					$qModGalleries = Core_Entity::factory($className, $galleryID);
					$aModGalleries = $qModGalleries->getGallery($aModificationItemID, false);
					if(isset($aModGalleries['main'])) {
						$aAllGalleries = array_merge($aAllGalleries, $aModGalleries['main']);
					}
				}
			}
			if(isset($aGalleries['main'])) {
				$aAllGalleries = array_merge($aAllGalleries, $aGalleries['main']);
			}
//			Skynetcore_Utils::tp($aAllGalleries);
			if(count($aAllGalleries)) {
				$this->_instance->galleries = $aAllGalleries;
				$this->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('galleries')
						->addEntities($aAllGalleries)
						->addEntity(Core::factory('Core_Xml_Entity')
							->name(
								'irrcounts'
							)->value(
								(isset($aGalleries['irr']) ? count($aGalleries['irr']) : 0)
							)
						)
				);
			} else {
//				foreach ($galleryTypes as $galleryType) {
//					$mShopItems = Core_Entity::factory('Shop_Item');
//					$mShopItems
//						->queryBuilder()
//						->join(array('property_value_ints', 'pvigallery'), 'pvigallery.entity_id', '=', 'shop_items.id')
//						->join(array('list_items', 'li'), 'li.id', '=', 'pvigallery.value')
//						->where('shop_items.modification_id', '=', $this->item)
//						->where('shop_items.active', '=', 1)
//						->where('shop_items.deleted', '=', 0)
//						->where('li.value', '=', $galleryType)
//						->order('shop_items.sorting')
//					;
//					$aShopItems = $mShopItems->findAll();
//					foreach ($aShopItems as $aShopItem) {
//						$aShopItem->clearEntities();
//						$aShopItem->addAllowedTag('id');
//						$aShopItem->addAllowedTag('name');
//						$aShopItem->addAllowedTag('image_large');
//						$aShopItem->addAllowedTag('image_small');
//						$aShopItem->addForbiddenTags(array('url', 'date', 'datetime', 'start_datetime', 'end_datetime', 'rest', 'reserved', 'price', 'discount', 'tax', 'price_tax'));
//					}
//					$gtype = 'gallery_'.(($galleryType=='photos')?'photo':(($galleryType=='irrgallery')?'irr':$galleryType));
//					$allGalleryItems[$gtype] = $aShopItems;
//
//					$this->_instance->galleries = $allGalleryItems;
//
//					$this->addEntity(
//						Core::factory('Core_Xml_Entity')
//							->name(
//								$gtype
//							)->addEntities($aShopItems)
//					);
//				}
			}
		}

		return $this;
	}

	public function addBreadcrumbs($breadcrumbsXSLtemplate='') {
		// Вывод строки навигации
		if($breadcrumbsXSLtemplate!='') {
			ob_start();
			$Structure_Controller_Breadcrumbs = new Moto_Structure_Controller_Breadcrumbs(Core_Entity::factory('Site', CURRENT_SITE));
			$Structure_Controller_Breadcrumbs->xsl(Core_Entity::factory('Xsl')->getByName($breadcrumbsXSLtemplate))->show();
			$this->_breadcrumbsHTML = ob_get_clean();
		}

		$requestProcessed = array();
		foreach ($_REQUEST as $rqKey=>$rqItem) {
			$matchRequest = array();
			switch (true) {
				case preg_match('/^property_(.*)$/', $rqKey, $matchRequest):
					if(is_array($rqItem)) {
						foreach ($rqItem as $rqItemValue) {
							$rKey = array(
								'@attributes' => array('id'=>$matchRequest[1], 'value' => $rqItemValue),
							);
							$requestProcessed['property'][] = $rKey;
						}
					} else {
						$rKey = array(
							'@attributes' => array('id'=>$matchRequest[1], 'value' => $rqItem),
						);
						$requestProcessed['property'][] = $rKey;
					}
					break;
				default:
					$requestProcessed[$rqKey] = $rqItem;
			}
		}
		$arParameters = new xmls($requestProcessed, '', 'requests');
		$arCookies = new xmls($_COOKIE, '', 'cookies');
		$this->addEntity(
			Core::factory('Core_Xml_Entity')
				->name('breadcrumbsHTML')->value($this->_breadcrumbsHTML)
		)->addEntity(
			Core::factory('Core_Xml_Entity')
				->name('REQUEST')
				->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('query_string')->value(preg_replace('/&action=refresh/', '', $_SERVER['QUERY_STRING']))
				)
				->addEntity($arParameters)
				->addEntity($arCookies)
		);
		return $this;
	}

	public function add1CEntities() {
		if( Umotors_Ajax_Controller::managersDataAllowed() ) {
			$adminRootEntity = Core::factory('Core_Xml_Entity')
				->name('admininfo');
			$this->addEntity($adminRootEntity);
		}

		return $this;
	}
}