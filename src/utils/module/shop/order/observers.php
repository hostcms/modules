<?php
use Utils_Utl as utl;
use Unirest as rst;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 09.06.2016
 * Time: 20:52
 */
class Utils_Shop_Order_Observers
{
	static public $excludeStatusIDs = 0;

	static function arOrdersShippers($orderIDs) {
		$groupPropertyCode = 0;
		$groupPropertyGUID = '0684B300-D404-30E9-16ED-F0F7A2D8EC94';

		if(is_array($orderIDs) && count($orderIDs)>0) {
			$shopId = Core_Entity::factory('Shop_Order')->find($orderIDs[0])->shop_id;
			$linkedObject = Core_Entity::factory('Shop_Item_Property_List', $shopId);
			$linkedObjectProperties = $linkedObject->Properties;
			$linkedObjectProperties
				->queryBuilder()
				->where('guid', '=', $groupPropertyGUID);

			$aProperties = $linkedObjectProperties->findAll();
			if(isset($aProperties[0])) {
				$groupPropertyCode = $aProperties[0]->id;
			} else {
				throw new Core_Exception("Не найдено свойство магазина, соответствующее `%pname`. Добавьте данной свойство товарам магазина c GUID свойства, равным '%guid'",
					array('%pname' => 'producer_code', '%guid' => $groupPropertyGUID), 0, false
				);
			}
		} else {
			throw new Core_Exception("Не выбран ни один заказ");
		}

//		!is_array(self::$excludeStatusIDs) && self::$excludeStatusIDs = array(self::$excludeStatusIDs);
		$qbSelect = Core_QueryBuilder::select(array('si.id', 'item_id'))
			->select(array(Core_QueryBuilder::expression("COALESCE(NULLIF(TRIM(CONCAT(COALESCE(uss.name, ''), ' (', COALESCE(uss.code, ''), ')')), '()'), 'Поставщик не определен')"), 'shipper'))
			->select(array(Core_QueryBuilder::expression("COALESCE(CONCAT(sw.name, ' (', uswp.warehouse_code, ')'), 'Склад не определен')"), 'wh'))
			->select(array('pvs.value', 'shipper_code'))
			->select(array(Core_QueryBuilder::expression("SUM(usoiw.warehouse_order_count)"), 'cnt'))
//				->select(array('usoiw.warehouse_order_count', 'cnt'))
			->select(array('soi.name', 'name'))
			->select(array(Core_QueryBuilder::expression("GROUP_CONCAT(so.id)"), 'orders_ids'))
			->select(array(Core_QueryBuilder::expression("GROUP_CONCAT(CONCAT(so.vat_invoice, ' (<b class=\'ostat ostat', sos.id, '\' data-orderid=\'', so.id, '\'>', sos.name, '</b>)'))"), 'orders'))
			->from(array('shop_orders', 'so'))
			->join(array('shop_order_statuses', 'sos'), 'so.shop_order_status_id', '=', 'sos.id')
			->join(array('shop_order_items', 'soi'), 'soi.shop_order_id', '=', 'so.id')
			->leftJoin(array('shop_items', 'si'), 'si.id', '=', 'soi.shop_item_id')
			->leftJoin(array('property_value_strings', 'pvs'), 'pvs.entity_id', '=', 'si.id', array(
				array('AND' => array('pvs.property_id', '=', $groupPropertyCode))
			))
			->leftJoin(array('utils_shop_order_item_warehouses', 'usoiw'), 'usoiw.shop_order_item_id', '=', 'soi.id')
			->leftJoin(array('shop_warehouse_items', 'swi'), 'swi.id', '=', 'usoiw.shop_warehouse_item_id')
			->leftJoin(array('shop_warehouses', 'sw'), 'sw.id', '=', 'swi.shop_warehouse_id')
			->leftJoin(array('utils_shop_warehouse_prices', 'uswp'), 'uswp.shop_warehouse_id', '=', 'sw.id')
			->leftJoin(array('utils_shop_shippers', 'uss'), 'uss.id', '=', 'uswp.utils_shop_shipper_id')
			->where('si.id', '>', 0)
			->where('so.id', 'IN', $orderIDs)
			->groupby('pvs.value')
			->groupby(Core_QueryBuilder::expression("COALESCE(sw.name, '')"))
			->orderBy(Core_QueryBuilder::expression('2'));

//		utl::p(Core_Page::instance()->excludedOrderStatus);
		if( isset(Core_Page::instance()->excludedOrderStatus) && is_array(Core_Page::instance()->excludedOrderStatus)
			&& count(Core_Page::instance()->excludedOrderStatus)>0 ) {
			$qbSelect->where('so.shop_order_status_id', 'NOT IN', Core_Page::instance()->excludedOrderStatus);
		}
		$arItems = $qbSelect->asAssoc()->execute()->result();
//		utl::p($orderIDs);
//		utl::p($qbSelect->build());
		$shopIitemIDs = utl::getArrayKeyValuesFromArrays($arItems, 'item_id');
		$shopIitemIDs = array_filter(array_keys($shopIitemIDs));
		$mShopItems = Core_Entity::factory('Shop_Item');
		$mShopItems
			->queryBuilder()
			->where('id', 'IN', $shopIitemIDs);
		$shopItems = $mShopItems->findAll(FALSE);
		$shopItems = utl::getArrayKeyValuesFromArrays($shopItems);
		$arItems = utl::getArrayKeyValuesFromArrays($arItems, 'shipper');
		return array('arItems'=>$arItems, 'shopItems'=>$shopItems);
	}

	static function onCallOrderShippers($object, $args) {
		//-- Обязательно!!! Отключаем обработчик --
		Core_Event::detach('shop_order.onCallorderShippers', array('Utils_Shop_Order_Observers', 'onCallOrderShippers'));

		if( isset($_REQUEST['hostcms']['checked'][0]) && is_array($_REQUEST['hostcms']['checked'][0])) {
			$orderIDs = array_keys($_REQUEST['hostcms']['checked'][0]);
			$sOrderIDs = implode(', ', $orderIDs);
			$orderHeader = Core::factory('Core_Html_Entity_H4');
			$orderHeader
				->style('text-align: left')
				->value("Заказать ({$sOrderIDs}) товары у поставщиков");

			$div = Core::factory('Core_Html_Entity_Div')->id('printShipperOrders');
			$wDiv = Core::factory('Core_Html_Entity_Div');
			$wDiv->id('shipperOrder')
				->class('widget-body');

			if(count($orderIDs)>0) {
				$printAllOrdersButton = Core::factory('Core_Html_Entity_A')
					->class('btn-labeled btn btn-magenta')
					->onclick('statusOrder(this);')
					->add(
						Core::factory('Core_Html_Entity_I')
							->class('btn-label fa fa-print')
					)
					->add(
						Core::factory('Core_Html_Entity_Code')
							->value('Установить статус "Заказан"')
					)
				;
				$printAllOrdersButtonDataID = "data-orderid";
				$printAllOrdersButton->addAllowedProperty($printAllOrdersButtonDataID)->$printAllOrdersButtonDataID = implode(',', $orderIDs);
				$checkedOrders = implode('&', array_map(function ($value) { return "orders[]=$value"; }, $orderIDs));
				$exportOrdersFromShippersToExcel = Core::factory('Core_Html_Entity_A')
					->class('btn-labeled btn btn-palegreen')
					->href('/admin/utils/shop/order/process.php?action=makeXLSorderShippers&'.$checkedOrders)
//					->onclick("$.adminLoad({path: '/admin/shop/order/index.php',action: 'orderShippers', operation: '', additionalParams: 'shop_id=5&shop_group_id=0&{$checkedOrders}',current: '1',windowId: 'id_content'}); return false")
					->add(
						Core::factory('Core_Html_Entity_I')
							->class('btn-label fa fa-file-excel-o')
					)
					->add(
						Core::factory('Core_Html_Entity_Code')
							->value('Сохранить *.xls')
					)
				;
				$exportOrdersFromShippersToExcel->addAllowedProperty($printAllOrdersButtonDataID)->$printAllOrdersButtonDataID = implode(',', $orderIDs);

				$allOrdersPanel = Core::factory('Core_Html_Entity_Div')
					->id('callOrderShippersDataTables_actions')
					->class('dataTables_actions')
					->add($printAllOrdersButton)
					->add($exportOrdersFromShippersToExcel)
				;
				$wDiv
					->add($allOrdersPanel);
			}
			$arItemsShopItems = self::arOrdersShippers($orderIDs);
			$arItems = $arItemsShopItems['arItems'];
			$shopItems = $arItemsShopItems['shopItems'];

			$orderItemsTable = Core::factory('Core_Html_Entity_Table');
			$orderItemsTable
				->class('admin-table table table-hover table-striped')
				->add(Core::factory('Core_Html_Entity_Tr')
					->add(Core::factory('Core_Html_Entity_Th')->width('10%')->colspan(2)->value('Код'))
					->add(Core::factory('Core_Html_Entity_Th')->value('Наименование'))
					->add(Core::factory('Core_Html_Entity_Th')->width('15%')->value('Заказы'))
					->add(Core::factory('Core_Html_Entity_Th')->width('5%')->value('Кол.'))
				);
			foreach ($arItems as $arKey => $arItem) {
				if ($arKey != 'я') {
					$item = utl::getArrayKeyValuesFromArrays($arItem, 'wh');
					$orderItemsTable
						->add(Core::factory('Core_Html_Entity_Tr')
							->add(Core::factory('Core_Html_Entity_Th')->class("left lev0")->colspan(5)->value($arKey))
						);
					foreach ($item as $whKey => $wItem) {
						$orderItemsTable
							->add(Core::factory('Core_Html_Entity_Tr')
								->add(Core::factory('Core_Html_Entity_Th')->class("lev1")->width('20px')->value(""))
								->add(Core::factory('Core_Html_Entity_Th')->class("left lev1")->colspan(4)->value($whKey))
							);
						foreach ($wItem as $whItemKey => $whItem) {
							$orderItemsTable
								->add(Core::factory('Core_Html_Entity_Tr')
									->add(Core::factory('Core_Html_Entity_Td')->colspan(2)->class("right")->value($whItem['shipper_code']))
									->add(Core::factory('Core_Html_Entity_Td')->class("left")
										->add(
											Core::factory('Core_Html_Entity_A')
												->href("/" . $shopItems[$whItem['item_id']][0]->getPath())
												->target('_blank')
												->add(Core::factory('Core_Html_Entity_Code')->value($whItem['name']))
										)
									)
									->add(Core::factory('Core_Html_Entity_Td')->class("left")
										->add(
											Core::factory('Core_Html_Entity_Span')
												->value($whItem['orders'])
										)
									)
									->add(Core::factory('Core_Html_Entity_Td')->class("left")->value($whItem['cnt']))
								);
						}
					}
				}
			}
			$wDiv
				->add(
					Core::factory('Core_Html_Entity_Div')
						->add(
							Core::factory('Core_Html_Entity_Div')
								->add($orderHeader)
								->add($orderItemsTable)
						)
				);
			$div->class('row')
				->add(
					Core::factory('Core_Html_Entity_Div')
						->class('col-md-12')
						->add(
							Core::factory('Core_Html_Entity_Div')
								->class('widget')
								->add($wDiv)
						)
				);
			echo $div->execute();
			?>
			<link rel="stylesheet" href="/admin/utils/css/order.css">
<!--			<script src="/admin/utils/js/order.js"></script>-->
			<?
		}
	}

	static function onCallOrderprint($object, $args) {
		//-- Обязательно!!! Отключаем обработчик --
		Core_Event::detach('shop_order.onCallorderPrint', array('Utils_Shop_Order_Observers', 'onCallOrderprint'));

		$div = Core::factory('Core_Html_Entity_Div')->id('printOrders');
		$wDiv = Core::factory('Core_Html_Entity_Div');
		$printOrderButtonDataID = "data-orderid";

		ob_start();
		if( isset($_REQUEST['hostcms']['checked'][0]) && is_array($_REQUEST['hostcms']['checked'][0])) {
			$orderIDs = array_keys($_REQUEST['hostcms']['checked'][0]);
			if(count($orderIDs)>1) {
				$printAllOrdersButton = Core::factory('Core_Html_Entity_A')
					->class('btn-labeled btn btn-magenta')
					->onclick('printOrder(this, "all");')
					->add(
						Core::factory('Core_Html_Entity_I')
							->class('btn-label fa fa-print')
					)
					->add(
						Core::factory('Core_Html_Entity_Code')
							->value('Распечатать все заказы')
					)
				;

				$printAllOrdersButtonDataID = "data-orderid";
				$printAllOrdersButton->addAllowedProperty($printAllOrdersButtonDataID)->$printAllOrdersButtonDataID = implode(',', $orderIDs);
				$checkedOrders = implode('&', array_map(function ($value) { return "hostcms[checked][0][$value]=1"; }, $orderIDs));
				$getAllOrdersFromShippers = Core::factory('Core_Html_Entity_A')
					->class('btn-labeled btn btn-yellow')
					->onclick("$.adminLoad({path: '/admin/shop/order/index.php',action: 'orderShippers', operation: '', additionalParams: 'shop_id=5&shop_group_id=0&{$checkedOrders}',current: '1',windowId: 'id_content'}); return false")
					->add(
						Core::factory('Core_Html_Entity_I')
							->class('btn-label fa fa-keyboard-o')
					)
					->add(
						Core::factory('Core_Html_Entity_Code')
							->value('Заказать у поставщиков')
					)
				;
				$getAllOrdersFromShippers->addAllowedProperty($printAllOrdersButtonDataID)->$printAllOrdersButtonDataID = implode(',', $orderIDs);

				$allOrdersPanel = Core::factory('Core_Html_Entity_Div')
					->id('printOrderDataTables_actions')
					->class('dataTables_actions')
					->style('padding: 0 0 10px 0;')
					->add($printAllOrdersButton)
					->add($getAllOrdersFromShippers)
				;
				$wDiv
					->add($allOrdersPanel);
			}
			$statusesDdButton = NULL;
			$arOrderStatuses = array();
			foreach($orderIDs as $orderID) {
				$order = Core_Entity::factory('Shop_Order')->getById($orderID);
				if(!is_null($order)) {
					$printOrderButton =
						Core::factory('Core_Html_Entity_A')
							->class('btn-labeled btn btn-magenta')
							->onclick('printOrder(this);')
							->add(
								Core::factory('Core_Html_Entity_I')
									->class('btn-label fa fa-print')
							)
							->add(
								Core::factory('Core_Html_Entity_Code')
									->value("Заказ №{$orderID}")
							);
					$addToOrderButton = Core::factory('Core_Html_Entity_A')
							->class('btn-labeled btn btn-success')
							->onclick('addToOrder(this, '.$orderID.');')
							->add(
								Core::factory('Core_Html_Entity_I')
									->class('btn-label fa fa-plus')
							)
							->add(
								Core::factory('Core_Html_Entity_Code')
									->value('Добавить товар')
							);
					if(is_null($statusesDdButton)) {
						$arOrderStatuses = $order->shop->shop_order_status->findAll(FALSE);
					}
					$orderStatuses = Core::factory('Core_Html_Entity_Ul')
						->class('dropdown-menu dropdown-warning')
					;
					foreach ($arOrderStatuses as $arOrderStatus) {
						$orderStatusLinkProperties = array(
							'data-orderid', 'data-statusid'
						);
						$orderStatusLink = Core::factory('Core_Html_Entity_A')
							->onclick('return printOrderChangeStatus(this);')
							->add(
								Core::factory('Core_Html_Entity_I')
									->class('btn-label fa fa'.($arOrderStatus->id == $order->shop_order_status_id ? '-check' : '').'-square-o')
							)
							->add(
								Core::factory('Core_Html_Entity_Code')
									->value($arOrderStatus->name)
							)
						;
						$orderStatusLink->addAllowedProperties($orderStatusLinkProperties);
						$orderStatusLink->$orderStatusLinkProperties[0] = $orderID;
						$orderStatusLink->$orderStatusLinkProperties[1] = $arOrderStatus->id;
						$orderStatuses->add(
							Core::factory('Core_Html_Entity_Li')
								->add($orderStatusLink)
						);
					}
					$propertyOrderToggle = 'data-toggle';
					$propertyOrderAreaExpanded = "aria-expanded";
					$changeOrderStatusButtonArea = Core::factory('Core_Html_Entity_Div')
						->class('btn-labeled btn btn-warning')
						->add(
							Core::factory('Core_Html_Entity_I')
								->class('btn-label fa fa-check-square-o')
						)
						->add(
							Core::factory('Core_Html_Entity_Code')
								->value($order->shop_order_status->name)
						)
					;
					$changeOrderStatusButtonArea->addAllowedProperty($propertyOrderToggle)->$propertyOrderToggle = "dropdown";
					$changeOrderStatusButtonArea->addAllowedProperty($propertyOrderAreaExpanded)->$propertyOrderAreaExpanded = "false";
					$changeOrderStatusButtonToggle = Core::factory('Core_Html_Entity_A')
						->class('btn btn-yellow dropdown-toggle')
						->add(
							Core::factory('Core_Html_Entity_I')
								->class('fa fa-angle-down')
						)
					;
					$changeOrderStatusButtonToggle->addAllowedProperty($propertyOrderToggle)->$propertyOrderToggle = "dropdown";
					$changeOrderStatusButtonToggle->addAllowedProperty($propertyOrderAreaExpanded)->$propertyOrderAreaExpanded = "true";

					$changeOrderStatusButton =
						Core::factory('Core_Html_Entity_Div')
						->class('btn-group')
						->add($changeOrderStatusButtonArea)
						->add($changeOrderStatusButtonToggle)
						->add($orderStatuses)
					;
					$orderHeader = Core::factory('Core_Html_Entity_Div')->style('padding: 0 0 5px 0;');
					$orderDate = Core_Date::sql2date($order->datetime);

					$printOrderButton->addAllowedProperty($printOrderButtonDataID)->$printOrderButtonDataID = $orderID;
					$orderHeader
						->add(Core::factory('Core_Html_Entity_Div')->class('btn-group')->add($printOrderButton))
						->add(Core::factory('Core_Html_Entity_Div')->class('btn-group')->add($addToOrderButton))
						->add($changeOrderStatusButton)
					;

					$orderItemsTable = Core::factory('Core_Html_Entity_Table');
					$orderItemsTable
						->class('admin-table table table-hover table-striped')
						->add( Core::factory('Core_Html_Entity_Tr')
							->add( Core::factory('Core_Html_Entity_Th')->width('7%')->value('Артикул') )
							->add( Core::factory('Core_Html_Entity_Th')->width('8%')->value('Код') )
							->add( Core::factory('Core_Html_Entity_Th')->value('Наименование') )
							->add( Core::factory('Core_Html_Entity_Th')->width('5%')->value('Кол.') )
							->add( Core::factory('Core_Html_Entity_Th')->width('10%')->value('Цена') )
							->add( Core::factory('Core_Html_Entity_Th')->width('10%')->value('Сумма') )
							->add( Core::factory('Core_Html_Entity_Th')->width('17%')->value('Заказ') )
//							->add( Core::factory('Core_Html_Entity_Th')->width('100px')->value('Возврат') )
						)
					;
					$orderItems = $order->Shop_Order_Items->findAll(FALSE);
					$sumOrder = 0;
					if(is_array($orderItems)) {
						foreach($orderItems as $orderItem) {
							$orderItemSum = $orderItem->quantity*$orderItem->price;
							$sumOrder += $orderItemSum;
							$orderItemsTable
								->add( $orderItemsTableTr = Core::factory('Core_Html_Entity_Tr') );
							$orderItemsTableTr
								->add( Core::factory('Core_Html_Entity_Td')
									->value($orderItem->marking)
									->class('js-copy_value')
								)
							;
							$producerCodes = '';
							$oProperty = Core_Entity::factory('Property', 124);
							$aPropertyValues = $oProperty->getValues($orderItem->Shop_Item->id);
							foreach($aPropertyValues as $oPropertyKey => $oPropertyValue)
							{
								$producerCodes .= ($oPropertyKey>0 ? ', ' : '') . $oPropertyValue->value;
							}
//									->add( Core::factory('Core_Html_Entity_Td')->value($orderItem->) )
							$nameCell = Core::factory('Core_Html_Entity_Code')->value($orderItem->name);
							$nameTD = NULL;
							$whTD = NULL;
							if(isset($orderItem->Shop_Item->id) && $orderItem->Shop_Item->id>0) {
								$currShopItem=$orderItem->Shop_Item;
								$nameTD = Core::factory('Core_Html_Entity_Div')
									->class('order_item_cell');
								$nameTD->add(
									Core::factory('Core_Html_Entity_A')
											->href('/'.$orderItem->Shop_Item->getPath())
											->target('_blank')
											->add(Core::factory('Core_Html_Entity_Code')->value($orderItem->name))
								);

								$selected = Core_Entity::factory('Utils_Shop_Order_Item_Warehouse')->getByOrderItem($orderItem->id, false);

//								$whItemPrices = Core_Entity::factory('Utils_Shop_Item_Price')->getAllByShop_Item_Id($orderItem->Shop_Item->id, FALSE);
//								$whItemPrices = utl::getArrayKeyValuesFromArrays($whItemPrices, 'shop_item_price_id');

								$whItems = $currShopItem->shop_warehouse_items->findAll(FALSE);
								$qPrices = Core_QueryBuilder::select(array('uswp.shop_warehouse_id', 'warehouse'))
									->select(array('uss.name', 'shipper_name'))
									->select(array('uss.discount', 'shipper_discount'))
									->select(array('sw.name', 'wh_name'))
									->select(array(Core_QueryBuilder::expression("CONCAT(uss.code, '/', uswp.warehouse_code)"), 'wh_code'))
									->select(array('usip.shop_item_price_id', 'item_price'))
									->select(array(Core_QueryBuilder::expression("usip.purchase_price*((100-uss.discount)/100)"), 'purshase_price'))
									->from(array('shop_warehouse_items', 'swi'))
									->join(array('utils_shop_warehouse_prices', 'uswp'), 'uswp.shop_warehouse_id', '=', 'swi.shop_warehouse_id')
									->join(array('utils_shop_item_prices', 'usip'), 'usip.shop_price_id', '=', 'uswp.shop_price_id', array(
										array('AND'=>array('usip.shop_item_id', '=', Core_QueryBuilder::expression('swi.shop_item_id')))
									))
									->join(array('shop_warehouses', 'sw'), 'sw.id', '=', 'uswp.shop_warehouse_id')
									->join(array('utils_shop_shippers', 'uss'), 'uss.id', '=', 'uswp.utils_shop_shipper_id')
									->join(array('shop_item_prices', 'sip'), 'sip.shop_price_id', '=', 'uswp.shop_price_id', array(
										array('AND'=>array('usip.shop_item_id', '=', Core_QueryBuilder::expression('sip.shop_item_id')))
									))
									->where('swi.shop_item_id', '=', $orderItem->Shop_Item->id*1)
									->where('swi.count', '>', 0)
								;
//								utl::p($qPrices->build());
								$whItemPrices = $qPrices->execute()->asAssoc()->result();
								$whItemPrices = utl::getArrayKeyValuesFromArrays($whItemPrices, 'warehouse');
								$aWhOptions = array(-1 => 'Не заказано');
//								utl::p($whItemPrices);
								foreach ($whItems as $whItem) {
									$purchasePrice = 0;
									if(isset($whItemPrices[$whItem->shop_warehouse_id][0])) {
										$purchasePrice = money_format('%.2n', $whItemPrices[$whItem->shop_warehouse_id][0]['purshase_price']);
									}
									$aWhOptions[$whItem->id] = '['.$purchasePrice.'] ('.($whItem->count*1).'), '.$whItem->shop_warehouse->name;
								}

								if(count($selected)==0) {
									$selected=array(Core_Entity::factory('Utils_Shop_Order_Item_Warehouse'));
									$selected[0]->id=-1;
								}
								$whTD = Core::factory('Core_Html_Entity_Div');
								$whTDback = Core::factory('Core_Html_Entity_Div');
								foreach ($selected as $select) {
									$whLinkOptions = Core::factory('Core_Html_Entity_Div');
									$whLinkOptionsBack = Core::factory('Core_Html_Entity_Div');
									$whLinkOptionSelect = Core::factory('Core_Html_Entity_Select')
										->style('display: inline-block; width: 50%')
										->class('form-control input-sm')
										->options($aWhOptions)
										->onchange("changeSelectOnOrder(this);")
										->name('wh_ordered['.$orderItem->id.']['.($select->id==-1 ? '0' : $select->id).'][warehouse]')
										->value($select->id==-1 ? -1 : $select->shop_warehouse_item_id)
									;
									$whLinkOptionCount = Core::factory('Core_Html_Entity_Input')
										->style('display: inline-block;width: 15%;position: relative;top: -1px;')
										->class('form-control input-sm')
										->value($select->id==-1 ? 1 : $select->warehouse_order_count)
										->name('wh_ordered['.$orderItem->id.']['.($select->id==-1 ? '0' : $select->id).'][count]')
										->onchange("changeSelectOnOrder(this);")
									;
									$whLinkOptionPrice = Core::factory('Core_Html_Entity_Input')
										->style('display: inline-block;width: 28%;position: relative;top: -1px;')
										->class('form-control input-sm')
										->value($select->id==-1 ? '0.00' : $select->warehouse_order_price)
										->name('wh_ordered['.$orderItem->id.']['.($select->id==-1 ? '0' : $select->id).'][price]')
										->onchange("changeSelectOnOrder(this);")
									;
									$whLinkOptions
										->add($whLinkOptionSelect)
										->add($whLinkOptionCount)
										->add($whLinkOptionPrice)
									;
									$whTD->add($whLinkOptions);
									$whLinkOptionBackCount = Core::factory('Core_Html_Entity_Input')
										->style('display: inline-block;width: 15%;position: relative;top: -1px;')
										->class('form-control input-sm')
										->value($select->id==-1 ? 1 : $select->warehouse_back_count)
										->name('wh_ordered['.$orderItem->id.']['.($select->id==-1 ? '0' : $select->id).'][back][count]')
										->disabled('disabled')
										->onchange("changeSelectOnOrder(this);")
									;
									$whLinkOptionBackPrice = Core::factory('Core_Html_Entity_Input')
										->style('display: inline-block;width: 73%;position: relative;top: -1px;')
										->class('form-control input-sm')
										->value($select->id==-1 ? '0.00' : $select->warehouse_back_price)
										->name('wh_ordered['.$orderItem->id.']['.($select->id==-1 ? '0' : $select->id).'][back][price]')
										->disabled('disabled')
										->onchange("changeSelectOnOrder(this);")
									;
									$whLinkOptionsBack
										->add($whLinkOptionBackCount)
										->add($whLinkOptionBackPrice)
									;
									$whTDback->add($whLinkOptionsBack);

//									utl::v($select->toArray());
								}
							} else {
								$nameTD = $nameCell;
								$whTD = Core::factory('Core_Html_Entity_Code')->value("");
								$whTDback = Core::factory('Core_Html_Entity_Code')->value("");
							}
							$orderItemsTableTr
								->add( Core::factory('Core_Html_Entity_Td')
									->value($producerCodes)
									->class('js-copy_value')
								)
								->add( Core::factory('Core_Html_Entity_Td')->add($nameTD) )
								->add( Core::factory('Core_Html_Entity_Td')->value($orderItem->quantity) )
								->add( Core::factory('Core_Html_Entity_Td')->value($orderItem->price) )
								->add( Core::factory('Core_Html_Entity_Td')->style('text-align: left')->value($orderItemSum) )
								->add( Core::factory('Core_Html_Entity_Td')->style('text-align: left')->add($whTD) )
//								->add( Core::factory('Core_Html_Entity_Td')->style('text-align: left')->add($whTDback) )
							;
						}
					}
					$orderItemsTable
						->add( Core::factory('Core_Html_Entity_Tr')
							->class('dataTables_actions')
							->add( Core::factory('Core_Html_Entity_Td')->colspan(3)->style('text-align: left')
								->add(
									Core::factory('Core_Html_Entity_Code')
										->style('color: green')
										->value("{$orderDate}, {$order->shop_order_status->name}, от {$order->name}, <b><i>{$order->phone}</i></b>")
								)
							)
							->add( Core::factory('Core_Html_Entity_Td')->colspan(2)->style('text-align: right')->value("<b><i>Итого:</i></b>") )
							->add( Core::factory('Core_Html_Entity_Td')->style('text-align: left')->value("<b><i>{$sumOrder}</i></b> руб.") )
							->add( Core::factory('Core_Html_Entity_Td')->value("") )
						)
					;
					//--------------------------------------------------------------------------------------------------
					$allHeaders = getallheaders();
					// Создаем поток
					$headers = array(
						'Cookie'=>'PHPSESSID='.session_id(),
					);
					isset($allHeaders['Authorization']) && $headers['Authorization']=$allHeaders['Authorization'];
					$query = array(
						'remove_ip_lock'=>1,
						'shop_order_id'=>$orderID,
					);
					$response = @rst\Request::get('http'.(isset($_SERVER['HTTPS']) ? 's' : '').'://'.$_SERVER['HTTP_HOST'].'/admin/shop/order/card/pay.php'
						, $headers
						, $query);
					$matches = array();
					preg_match("/(<body.*?>)(.*)(<\/body>)/uis", $response->body, $matches);
					//--------------------------------------------------------------------------------------------------

					$printCard = Core::factory('Core_Html_Entity_Div')
						->class('print_card')
						->add(
							Core::factory('Core_Html_Entity_Div')
								->class('print_card_view')
								->add($orderHeader)
								->add($orderItemsTable)
						)
						->add( Core::factory('Core_Html_Entity_Code')->value('<hr/>') )
						->add(Core::factory('Core_Html_Entity_Code')->value('<div class="print_card_order">'.(isset($matches[2])?$matches[2]:"NOT FOUND CONTENT").'</div>'))
					;
					$printCard->addAllowedProperty($printOrderButtonDataID)->$printOrderButtonDataID = $orderID;
					$wDiv
						->add($printCard)
					;
				}
			}
		}
		$st = ob_get_clean();

		$wDiv->id('orderPrintWg')
			->class('widget-body')
			->value($st)
		;

		$div->class('row')
			->add(
				Core::factory('Core_Html_Entity_Div')
					->class('col-md-12')
					->add(
						Core::factory('Core_Html_Entity_Div')
							->class('widget')
							->add($wDiv)
					)
			)
		;
		echo $div->execute();
		?>
		<link rel="stylesheet" href="/admin/utils/css/order.css">
		<script src="/admin/utils/js/order.js"></script>
		<?
	}
}
