<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Utils_Shop_Order_Item_Warehouse_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author Mike Borisov
 * @copyright © 2005-2016 Mike Borisov
 */
class Utils_Shop_Order_Item_Warehouse_Model extends Core_Entity
{
	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
		'shop_order_item' => array(),
		'shop_warehouse_item' => array(),
	);

	/**
	 * Get item by group id
	 * @param int $group_id group id
	 * @return array
	 */
	public function getByOrderItemWarehouse($order_item_id, $warehouse_item_id, $cache=true)
	{
		$this->queryBuilder()
			//->clear()
			->where('shop_order_item_id', '=', $order_item_id)
			->where('shop_warehouse_item_id', '=', $warehouse_item_id);
		$founded = $this->findAll($cache);

		return Core_Array::get($founded, 0);
	}

	/**
	 * Get item by group id
	 * @param int $group_id group id
	 * @return array
	 */
	public function getByOrderItem($order_item_id, $cache=true)
	{
		$this->queryBuilder()
			//->clear()
			->where('shop_order_item_id', '=', $order_item_id)
		;
		$founded = $this->findAll($cache);

		return $founded;
	}
}