<?php
use Utils_Utl as utl;

class Utils_Shop_Item_Controller
{
	const MAX_COUNTER_RETRYES = 10;

	public static function make_seed()
	{
		list($usec, $sec) = explode(' ', microtime());
		return (float)$sec + ((float)$usec * 100000);
	}

	public static function generateMarking($itemObject, $currentValue=-1, $counterRetryes=0, $currentShopId=3)
	{
		if(strlen($currentValue)==13) {
			$mShopItem = Core_Entity::factory('Shop_Item');
			$mShopItem
				->queryBuilder()
				->where('shop_id', '=', $itemObject->shop_id);

			$foundedItem = $mShopItem->getByMarking($currentValue);
			if( is_null($foundedItem) ) {
				return $currentValue;
			}
		}
		if($counterRetryes > self::MAX_COUNTER_RETRYES) {
			return $currentValue;
		}


		$shop_group_id = $itemObject->shop_group_id;
		if($shop_group_id==0 && $itemObject->modification_id>0) {
			$shop_group_id = $itemObject->modification->shop_group_id;
		}

		$groupsNav = array_values(Utils_Shop_Group_Controller::getAllParenGroups($shop_group_id));

		$oProperty = Core_Entity::factory('Property')->getByTag_Name('utils_group_code_marking_generate_shop_'.$itemObject->shop_id);
		$retValue = '';
		if(isset($groupsNav[0]->id) && $groupsNav[0]->id>0 && isset($oProperty->id) && $oProperty->id>0) {
			$aPropertyValues = $oProperty->getValues($groupsNav[0]->id);
			if(isset($aPropertyValues[0]->value) && $aPropertyValues[0]->value>0) {
				mt_srand((int)self::make_seed());
				$randval = mt_rand();
				$randval = strval($randval);
				if (strlen($randval) > 9) {
					$randval = substr($randval, 0, 9);
				}
				if (strlen($randval) < 9) {
					$randval = '0' . $randval;
				}
				$param1 = $aPropertyValues[0]->value;
				strlen($param1.$randval)==13 && $retValue = $param1 . $randval;
				return self::generateMarking($itemObject, $retValue, $counterRetryes+1, $currentShopId);
			}
		}
		return 0;
	}

	public static function getBarcodeFromMarking($oShopItem) {
		$value = $oShopItem->marking;
		if(strlen($value)==13) {
			$trimValue = substr($value, 0, 1).substr($value, 2, 11);
			return $trimValue.self::getCheckSum($trimValue.'0');
		} else {
			return $value;
		}
	}

	public static function getCheckSum($value)
	{
		$calculation = 0;

		for ($i = 0; $i < strlen($value) - 1; $i++)
		{
			$calculation += $i % 2 ? $value[$i] * 3 : $value[$i];
		}

		return substr(10 - substr($calculation, -1), -1);
	}
}