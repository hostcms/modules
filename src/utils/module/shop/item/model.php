<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Utils_Shop_Item_Model
 *
 * @package Utils
 * @subpackage Shop
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2005-2018 Борисов Михаил Юрьевич m.u.borisov@gmail.com
 */
class Utils_Shop_Item_Model extends Shop_Item_Model
{
	protected $_localObject = null;

	protected $_currentGroupID = 0;
	protected $_currentGroup = null;
	protected $_currentGroups = null;
	protected $_instance = null;

	public function __construct($id = NULL)
	{
		parent::__construct($id);
		$this->_instance = Core_Page::instance();
	}

	public function getLocalObject() {
		if($this->_localObject === null) {
			$this->_localObject = $this->getStdObject();
		}
		return $this->_localObject;
	}

	/**
	 * @return array
	 */
	public function showJSONLdScheme()
	{
		$mainDomain = Core_Array::get($this->_instance->skynet->config, 'main_domain', '').'/';
		$tmpItemListElements = $this->getGroups();
		$itemListElements = array_values(
			array_map(function($item) use ($mainDomain) {
				return [
					"@type" => "ListItem",
					"position" => 1,
					"name" => $item->name,
					"item" => $mainDomain . $item->getPath()
				];
			}, $tmpItemListElements)
		);
		if($this->modification_id > 0) {
			$itemListElements[] = [
				"@type" => "ListItem",
				"position" => 1,
				"name" => $this->Modification->name,
				"item" => $mainDomain . $this->Modification->getPath()
			];
		}
		$itemListElements[] = [
			"@type" => "ListItem",
			"position" => 1,
			"name" => $this->name,
			"item" => $mainDomain . $this->getPath()
		];

		$aLdScheme = [
			"@context" => "https://schema.org",
			"@type" => "BreadcrumbList",
			"itemListElement" => $itemListElements,
		];
		echo "<script type=\"application/ld+json\">\n";
		echo json_encode($aLdScheme, JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT);
		echo "</script>\n";

		if(Core_Array::getRequest('og', false) !== false) {
		}
	}

	/**
	 * @return array
	 */
	public function showOgScheme($xslName = '', $options = [
		'title' => '',
		'description' => '',
		'keywords' => '',
	])
	{
		$mainDomain = Core_Array::get($this->_instance->skynet->config, 'main_domain', '').'/';
		$return = '';
		$options += [
			'type' => 'product.item',
			'url' => $mainDomain.$this->getPath(),
			'site_name ' => $this->shop->site->name,
		];
		$this->addForbiddenTags(['modificationSizes', 'allPrices']);
		$this->addAllowedTags(['images_seo']);
		$sObject = $this->getLocalObject();
		if($this->image_large != '') {
			$options += [
//				'image' => str_replace('https:', 'http:', $mainDomain).trim($sObject->images_seo->_prefix.$sObject->images_seo->_mid.'2000x2000'.$sObject->images_seo->_postfix, "/"),
				'image' => $mainDomain.trim($sObject->images_seo->_prefix.
						$sObject->images_seo->_marking.
						$sObject->images_seo->_mid.'1920x1080'.
						$sObject->images_seo->_postfix, "/"),
				'image.repl.points.secure_url' => $mainDomain.trim($sObject->images_seo->_prefix.
						$sObject->images_seo->_marking.
						$sObject->images_seo->_mid.'1920x1080'.
						$sObject->images_seo->_postfix, "/"),
			];
		}
		if(Core_Array::getRequest('test', false) !== false) {
		}

		$ogEntity = Core::factory('Core_Xml_Entity')->name('og');
		foreach ($options as $optionKey => $optionValue) {
			$ogEntity->addEntity(
				Core::factory('Core_Xml_Entity')
					->name($optionKey)
					->value($optionValue)
			);
		}
		$this->addEntity($ogEntity);
		$return = Xsl_Processor::instance()
			->xml($this->getXml())
			->xsl(Core_Entity::factory('Xsl')->getByName($xslName))
			->process();
		return $return;
	}

	public function getTopGroup($level=0) {
		$oGroup = $this->getGroup();
		if($oGroup instanceof Shop_Group_Model) {
			return $this->getGroup()->getTop($level);
		}
	}

	public function currentModel()
	{
		return preg_replace('/\_Model$/i', '', get_class($this));
	}

	public function getGroup($cache=true)
	{
		$oItem = (isset($this->Modification->id) && $this->Modification->id>0) ? $this->Modification : $this;
		if ($oItem->shop_group_id > 0 || (isset($oItem->_currentGroupID) && $oItem->_currentGroupID != $oItem->shop_group_id)) {
			$currentGroup = Core_Entity::factory('Utils_Shop_Group')->getById($oItem->shop_group_id, false);
			return $currentGroup;
		} else {
			return NULL;
		}
	}

	public function getGroups($cache=true)
	{
		if($cache && !is_null($this->_currentGroups)) {
			return $this->_currentGroups;
		}

		if(!is_null($this->getGroup($cache))) {
			$this->_currentGroups = $this->getGroup($cache)->getParents();
		} else {
			$this->_currentGroups = [];
		}
		return $this->_currentGroups;
	}

	/** @var Utils_Shop_Item_Model $item */
	public function b64ItemImage($b64_noimage='')
	{
		if (is_file($file = $this->getSmallFilePath())) {
			return base64_encode(file_get_contents($file));
		} else {
			return $b64_noimage;
		}
	}

	public function getPropertyValue($property_id) {
		/** @var Utils_Property_Model $property */
		$property = Core_Entity::factory('Utils_Property')->getById($property_id);
		return $property->getPropertyValue($this->id, '');
	}

	public function getRestCount($warehouses=[], $func='NOT IN') {
		$count = 0;
		/** @var Shop_Warehouse_Item_Model $oWhCount */
		$oWhCount = Core_Entity::factory('Shop_Warehouse_Item');
		$oWhCount
			->queryBuilder()
			->where('shop_item_id', '=', $this->id)
		;
		if(count($warehouses)>0) {
			$oWhCount
				->queryBuilder()
				->where('shop_warehouse_id', $func, $warehouses);
			$aWhCount = $oWhCount->findAll();
			foreach ($aWhCount as $whCount) {
				$count += $whCount->count;
			}
		}

		return $count*1.0;
	}
}