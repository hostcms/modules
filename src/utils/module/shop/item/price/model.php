<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Utils_Shop_Order_Item_Warehouse_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author Mike Borisov
 * @copyright © 2005-2016 Mike Borisov
 */
class Utils_Shop_Item_Price_Model extends Core_Entity
{
	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	public static function setPriceValue($itemObject, $priceGUID, $value, $purshasePrice=0) {
		$pr = Core_Entity::factory('Shop_Price')->getByGuid($priceGUID, FALSE);
		if(!is_null($pr)) {
			if(is_null($prValue = $itemObject->shop_item_prices->getByPriceId($pr->id, FALSE))) {
				$prValue=Core_Entity::factory('Shop_Item_Price');
				$prValue->shop_item_id = $itemObject->id;
				$prValue->shop_price_id = $pr->id;
				$prValue->save();
			}
			if($prValue->value != $value) {
				$prValue->value = $value;
				$prValue->save();
			}
			if(class_exists('Utils_Shop_Item_Price_Model')) {
				$prLinkedValue = Core_Entity::factory('Utils_Shop_Item_Price')->getByShop_Item_Price_Id($prValue->id, FALSE);
				if(is_null($prLinkedValue)) {
					$prLinkedValue=Core_Entity::factory('Utils_Shop_Item_Price');
					$prLinkedValue->shop_item_id = $prValue->shop_item_id;
					$prLinkedValue->shop_price_id = $prValue->shop_price_id;
					$prLinkedValue->shop_item_price_id = $prValue->id;
					$prLinkedValue->save();
				}
				if($prLinkedValue->purchase_price != $purshasePrice) {
					$prLinkedValue->purchase_price = $purshasePrice;
					$prLinkedValue->save();
				}
			}
		} else {
			throw new Core_Exception("Склад '{$priceGUID}' не существует");
		}
	}

	public static function getPurshasePriceByWarehouseID($itemObjectID, $warehouseID) {
		$purshasePrice = 0;
		$qPurshasePrice = Core_QueryBuilder::select(array(Core_QueryBuilder::expression("usip.purchase_price*((100-uss.discount)/100)"), 'purchase_price'))
			->from(array('utils_shop_item_prices', 'usip'))
			->join(array('utils_shop_warehouse_prices', 'uswp'), 'uswp.shop_price_id', '=', 'usip.shop_price_id')
			->join(array('shop_warehouses', 'sw'), 'sw.id', '=', 'uswp.shop_warehouse_id')
			->join(array('utils_shop_shippers', 'uss'), 'uss.id', '=', 'uswp.utils_shop_shipper_id')
			->where('usip.shop_item_id', '=', $itemObjectID)
			->where('uswp.shop_warehouse_id', '=', $warehouseID)
		;
		$arPurshasePrice = $qPurshasePrice->execute()->asAssoc()->result(FALSE);
		if(isset($arPurshasePrice[0]['purchase_price'])) {
			$purshasePrice = money_format('%.2n', $arPurshasePrice[0]['purchase_price']*1);
		}
		return $purshasePrice;
	}
}