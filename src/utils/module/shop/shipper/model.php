<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Utils_Shop_Warehouse_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author Mike Borisov
 * @copyright © 2005-2016 Mike Borisov
 */
class Utils_Shop_Shipper_Model extends Core_Entity
{
	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

}