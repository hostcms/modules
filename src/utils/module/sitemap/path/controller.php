<?php
use Utils_Utl as utl;

class Utils_Sitemap_Path_Controller extends Core_Command_Controller
{
	/**
	 * Default controller action
	 * @return Core_Response
	 * @hostcms-event Core_Command_Controller_Benchmark.onBeforeShowAction
	 */
	public function showAction()
	{
		$shopID = 4;
		Core_Page::instance()->twigLoader->setPaths(
			array(CMS_FOLDER.'modules/utils/templates/twig/'),
			'utils'
		);
		$qbFeeds = Core_QueryBuilder::select(array('sg3.id', 'sg3_id'))
			->select(array(Core_QueryBuilder::expression("CONCAT('/', sg1.path, '/', sg2.path, '/', sg3.path, '/'/*, si.path, '/'*/)"), 'urls'))
//			->select(array('sg1.parent_id', 'sg1_parent_id'))
//			->select(array('sg1.name', 'sg1_name'))
//			->select(array('sg1.path', 'sg1_path'))
//			->select(array('sg2.id', 'sg2_id'))
//			->select(array('sg2.parent_id', 'sg2_parent_id'))
//			->select(array('sg2.name', 'sg2_name'))
//			->select(array('sg2.name_rus', 'nameRus'))
//			->select(array('sg2.path', 'sg2_path'))
//			->select(array('sg3.id', 'sg3_id'))
//			->select(array('sg3.parent_id', 'sg3_parent_id'))
//			->select(array('sg3.name', 'sg3_name'))
//			->select(array('sg3.path', 'sg3_path'))
//			->select(array('si.id', 'si_id'))
//			->select(array('si.name', 'si_name'))
//			->select(array('si.path', 'si_path'))
			->from(array('shop_groups', 'sg1'))
			->join(array('shop_groups', 'sg2'), 'sg2.parent_id', '=', 'sg1.id')
			->join(array('shop_groups', 'sg3'), 'sg3.parent_id', '=', 'sg2.id')
			->leftJoin(array('property_value_ints', 'pvi'), 'pvi.value', '=', 'sg3.id', array(
				array('AND' => array('pvi.property_id', '=', 125))
			))
			->join(array('shop_items', 'si'), 'si.id', '=', 'pvi.entity_id')
			->where('sg1.parent_id', '=', 0)
			->where('sg1.shop_id', '=', $shopID)
			->where('si.image_large', '<>', '')
			->where('si.shop_group_id', 'NOT IN', array(25577))
			->where('sg1.path', 'IN', array('audi'))
			->groupby('urls')
		;
		utl::p($qbFeeds->build());

		$xml = Core_Page::instance()->twig->render(
			"@utils/sitemap.twig",
			array('p1' => 1)
		);
		$oCore_Response = new Core_Response();
		$oCore_Response
			->status(200)
			->header('Pragma', 'no-cache')
			->header('Cache-Control', 'private, no-cache')
			->header('Vary', 'Accept')
			->header('Content-Type', 'text/xml')
			->header('Last-Modified', gmdate('D, d M Y H:i:s', time()) . ' GMT')
//			->header('X-Powered-By', 'HostCMS')
			->body($xml)
		;

		return $oCore_Response;
	}
}