<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Gallery_Type_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author art Борисов Михаил Юрьевич
 * @copyright © 2017 Борисов Михаил Юрьевич
 */
class Utils_Gallery_Type_Model extends Utils_Core_Entity
{

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * One-to-many or many-to-many relations
	 * @var array
	 */
	protected $_hasMany = array(
	);

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
	);

}