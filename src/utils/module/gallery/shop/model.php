<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Gallery_Shop_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author art Борисов Михаил Юрьевич
 * @copyright © 2017 Борисов Михаил Юрьевич
 */
class Utils_Gallery_Shop_Model extends Utils_Gallery_Model
{

	/**
	 * One-to-many or many-to-many relations
	 * @var array
	 */
	protected $_hasMany = array(
		'gallery' => array(
			'model' => 'utils_gallery_shop_entity',
		),
	);

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
	);
//
//	public function getGallery($entity_id, $cache=true)
//	{
//		if(!$cache || !$this->_galleryItemsLoaded) {
//			$oGalleryItems = $this->gallery_items;
//			$oGalleryItems
//				->queryBuilder()
//				->select('utils_gallery_items.*')
//				->where('utils_gallery_items.entity_id', '=', $entity_id)
//				->where('utils_gallery_items.utils_gallery_id', '=', $this->id)
//			;
//
//			$aGalleries = $oGalleryItems->findAll(false);
//
//			$aTypesResults =array();
//			foreach ($aGalleries as $aGallery) {
//				$aTypesResults[$aGallery->utils_gallery_type->guid][] = $aGallery;
//			}
//
//			$this->_galleryItems = $aTypesResults;
//			$this->_galleryItemsLoaded = true;
//		} else {
////			Utils_Utl::p('From Cache');
//		}
//
//		return $this->_galleryItems;
//	}
}