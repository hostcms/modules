<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Gallery_Shop_Item_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author art Борисов Михаил Юрьевич
 * @copyright © 2017 Борисов Михаил Юрьевич
 */
class Utils_Gallery_Shop_Entity_Model extends Utils_Gallery_Item_Model
{
	/**
	 * @var mixed
	 */
	protected $_type='shop';

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	public function __construct($primaryKey = NULL)
	{
		$this->_belongsTo = array_merge($this->_belongsTo, [
			'gallery_group' => [
					'model' => 'shop_group'
				],
			'gallery_item' => [
					'model' => 'shop_item'
				],
		]);

		parent::__construct($primaryKey);
	}
}