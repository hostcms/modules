<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Gallery_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author art Борисов Михаил Юрьевич
 * @copyright © 2017 Борисов Михаил Юрьевич
 */
abstract class Utils_Gallery_Model extends Utils_Core_Entity
{

//	private $_galleryItems = array();
//	private $_galleryItemsLoaded = false;
//
//	/**
//	 * One-to-many or many-to-many relations
//	 * @var array
//	 */
//	protected $_hasMany = array(
//		'gallery_item' => array(
//			'model' => 'utils_gallery_item',
//		),
//	);
//
//	/**
//	 * Belongs to relations
//	 * @var array
//	 */
//	protected $_belongsTo = array(
//	);
//
//	public function getGallery($entity_id, $cache=true)
//	{
//		if(!$cache || !$this->_galleryItemsLoaded) {
//			$oGalleryItems = $this->gallery_items;
//			$oGalleryItems
//				->queryBuilder()
//				->select('utils_gallery_items.*')
//				->where('utils_gallery_items.entity_id', '=', $entity_id)
//				->where('utils_gallery_items.utils_gallery_id', '=', $this->id)
//			;
//
//			$aGalleries = $oGalleryItems->findAll(false);
//
//			$aTypesResults =array();
//			foreach ($aGalleries as $aGallery) {
//				$aTypesResults[$aGallery->utils_gallery_type->guid][] = $aGallery;
//			}
//
//			$this->_galleryItems = $aTypesResults;
//			$this->_galleryItemsLoaded = true;
//		} else {
////			Utils_Utl::p('From Cache');
//		}
//
//		return $this->_galleryItems;
//	}
}