<?php
use Utils_Utl as utl;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Utils_Gallery_Observers_Module {

	static public function onBeforeGetXml($entity, $args) {
		$entityTypes = [];
		preg_match('/((Shop|Informationsystem)_)(Group|Item)(_Model)/', get_class($entity), $entityTypes);
		if(is_array($entityTypes)) {
			$entityName = Core_Array::get($entityTypes, 2);
			$entityType = Core_Array::get($entityTypes, 3);
		}
		$oGalleryItems = Core_Entity::factory("Utils_Gallery_{$entityName}_Entity");
		$mGalleryItems = "getAllBy{$entityName}_{$entityType}_id";
		$aGalleryItems = $oGalleryItems->$mGalleryItems($entity->id);
		if(count($aGalleryItems)>0) {
			$entity
				->addEntity(
					Core::factory('Core_Xml_Entity')
						->name('gallery')
						->addEntities($aGalleryItems)
				);
		}
	}
}