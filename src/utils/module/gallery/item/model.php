<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Gallery_Item_Model
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.x
 * @author art Борисов Михаил Юрьевич
 * @copyright © 2017 Борисов Михаил Юрьевич
 */
abstract class Utils_Gallery_Item_Model extends Utils_Core_Entity
{
	/**
	 * @var mixed
	 */
	protected $_type='undefined';

	/**
	 * @var mixed
	 */
	protected $_entity_id=0;

	/**
	 * @var mixed
	 */
	protected $_entity_type='undefined';

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
		'gallery_type' => [
			'model' => 'utils_gallery_type'
		],
	);

	public function getDir() {
		$groupField = $this->_type.'_group_id';
		$itemField = $this->_type.'_item_id';
		if($this->$groupField > 0) {
			$this->_entity_type = 'groups';
			$this->_entity_id = $this->$groupField;
		}
		if($this->$itemField > 0) {
			$this->_entity_type = 'items';
			$this->_entity_id = $this->$itemField;
		}

		return DIRECTORY_SEPARATOR.UPLOADDIR.'galleries'.DIRECTORY_SEPARATOR.$this->_type.DIRECTORY_SEPARATOR.$this->_entity_type.DIRECTORY_SEPARATOR.$this->gallery_type->guid.DIRECTORY_SEPARATOR.Core_File::getNestingDirPath($this->_entity_id, 3).DIRECTORY_SEPARATOR.$this->_entity_id.DIRECTORY_SEPARATOR; //.preg_replace('/s$/', '', $this->_entity_type).'_gallery_'.$this->id.'/';
	}

	public function getPath() {
		return preg_replace('/\/\//iu', '/', CMS_FOLDER.$this->getDir());
	}

	public function getImageHref() {
		return $this->getDir().$this->filename;
	}

	public function getImagePath() {
		return $this->getPath().$this->filename;
	}

	public function getXml()
	{
		$this
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('dir')
					->value($this->getDir())
			)
			->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('src')
					->value($this->getImageHref())
			)
			->addXmlTag('crc_id', crc32($this->id))
		;
		return parent::getXml();
	}

	public function delete($primaryKey = NULL)
	{
		if(is_file($this->getPath().$this->filename)) {
			unlink($this->getPath().$this->filename);
		}

		return parent::delete($primaryKey);
	}

}