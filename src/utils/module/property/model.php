<?php
use Utils_Utl as utl;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 15.11.2016
 * Time: 12:15
 */
class Utils_Property_Model extends Property_Model {
	/**
	 * Model name
	 * @var mixed
	 */
	protected $_modelName = 'property';

	public function getPropertyValue($entityID, $defaultValue='12', $field='value', $index=0) {
		$retValue = $defaultValue;
		$aPropertyValues = $this->getValues($entityID, false);
		if(isset($aPropertyValues[$index]) && $aPropertyValues[$index]->id>0) {
			if($field=='object') {
				$retValue = $aPropertyValues[$index];
			} else {
				(isset($aPropertyValues[$index]->$field) && $aPropertyValues[$index]->$field!='') && $retValue = $aPropertyValues[$index]->$field;
			}
		}
		return $retValue;
	}

	public function setPropertyValue($entityID, $value, $field='value', $index=0)
	{
		$foundValue = NULL;
		if($this->id>0) {
			$foundValue = $this->getPropertyValue($entityID, 'undefined', 'object', $index);
			switch ($foundValue) {
				case 'undefined':
					$oPropertyValue = $this->createNewValue($entityID);
					$oPropertyValue->value = $value;
					$oPropertyValue->save();
					break;
				default:
					$oPropertyValue = $foundValue;
					if(isset($oPropertyValue->value) && $oPropertyValue->value != $value) {
						$oPropertyValue->value = $value;
						$oPropertyValue->save();
					}
			}
			$foundValue = (($field=='object') ? $oPropertyValue : $oPropertyValue->value);
		}
		return $foundValue;
	}
}