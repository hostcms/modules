<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */

class Utils_Observers_Admin_Panel {

	static public function onBeforeMakePanel($adminPanel, $args) {
		list($currentXSLPanel, $object) = $args;
		$oXSLSubPanel = Core::factory('Core_Html_Entity_Div')
			->class('hostcmsSubPanel hostcmsWindow hostcmsXsl')
			->add(
				Core::factory('Core_Html_Entity_Img')
					->width(3)->height(16)
					->src('/hostcmsfiles/images/drag_bg.gif')
			)->add(
				Core::factory('Core_Html_Entity_Input')
					->type('hidden')
					->id('item'.$object->id)
					->name("item[{$object->id}]")
					->class('shop-item-id')
					->value($object->id)
			);
		$adminPanel->addItemPanelButton($oXSLSubPanel, 'gallery');
		$object->modification_id == 0 && $adminPanel->addItemPanelButton($oXSLSubPanel, 'articles');
		$adminPanel->addItemPanelButton($oXSLSubPanel, 'technical');
		$adminPanel->addSubPanel($oXSLSubPanel);
	}
//
//	static public function onBeforeAddItemPanelButton($oXslSubPanel, $args) {
//	}
}