<?php
use Utils_Utl as utl;
use GeoIp2\Database\Reader;

/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Utils_Observers_Core {
	static $_utilsAllowedProperties = array(
		'srcrequest' => NULL,
		'seopage' => NULL,
		'h1' => '',
		'content_description' => '',
		'content_description_large' => '',
		'content_last_counts' => '',
		'user' => NULL,
		'siteuser' => NULL,
		'siteusergroups' => NULL,
		'utilspanel' => NULL,
		'currentsiteconf' => [],
		'canonical' => false,
		'noindex' => false,
	);

	static public function onBeforeInitConstants($object, $args) {
		if ((Core::$url['path'] == '/edit-in-place.php'
			|| Core::$url['path'] == '/robots.txt'
			|| Core::$url['path'] == '/users/administration/items-are-added/'
			|| Core_Array::getRequest('action', '') != ''
			|| preg_match('/sitemap.xml/', Core::$url['path'])==1
			|| Core_Array::getRequest('ajax', false)
//			|| (isset(Core_Page::instance()->restrictByIP) && !Core_Page::instance()->restrictByIP)
			) && Core_Array::getRequest('debug', '') == ''
		) {
			if(!defined('ALLOW_PANEL')) define('ALLOW_PANEL', FALSE);
		}
	}

	static public function onAfterInitConstants($object, $args) {
		$instance = Core_Page::instance();
		if(!$instance->srcrequest->isMain && isset(Core::$url['path']) && Core::$url['path']!='') {
			$oSite = Core_Entity::factory('Site')->getById(CURRENT_SITE);
			$qb = clone $oSite->informationsystems;
			$qb->queryBuilder()->where('name', 'LIKE', 'СтатьиДляМагазина.%');
			$iss = $qb->findAll();
			foreach ($iss as $is) {
				$structurePath = $is->structure->path;
				$founded = utl::findInEntityByURL('Informationsystem', $is->id, $instance->srcrequest->pathNoPage, true);
				if(preg_match("/\/{$structurePath}\//", Core::$url['path'])==1 && (Core::$url['path'] != "/{$structurePath}/")) {
					Core::$url['path'] = '/notworkingpath/';
					break;
				}
				if(is_object($founded['shopItem']) || preg_match("/\/{$structurePath}\//", Core::$url['path'])==1) {
					$instance->addAllowedProperty('articleContentPrefix');
					$instance->articleContentPrefix = $is->structure->path;
					is_object($founded['shopItem']) && Core::$url['path'] = '/'.$is->structure->path.Core::$url['path'];
					break;
				}
			}
		}
		if(defined('CURRENT_SITE') && isset(Core_Config::instance()->get('site_config')[CURRENT_SITE])) {
			$instance->currentsiteconf = Core_Config::instance()->get('site_config')[CURRENT_SITE];
//			Skynetcore_Utils::tp($_SERVER);
		}
		$instance->srcrequest->isProd = (is_array($instance->currentsiteconf)
			&& isset($instance->currentsiteconf['prod'])
			&& in_array($instance->srcrequest->domain, $instance->currentsiteconf['prod'])
		);
		$instance->srcrequest->isManage = (is_array($instance->currentsiteconf)
			&& isset($instance->currentsiteconf['manage'])
			&& in_array($instance->srcrequest->domain, $instance->currentsiteconf['manage'])
		);

		self::checkCanonical();
	}

	static public function checkCanonical() {
		$instance = Core_Page::instance();

		if($instance->srcrequest->currentPage > 0
			|| $instance->srcrequest->query != ''
		) {
			$instance->canonical = $instance->srcrequest->pathNoPage;
		}
	}

	static public function onAfterLoadModuleList($object, $args) {
		$instance = Core_Page::instance();

		$instance->addAllowedProperties(array_keys(self::$_utilsAllowedProperties));
		foreach (self::$_utilsAllowedProperties as $projectAllowedProperty => $projectAllowedPropertyValue) {
			$instance->$projectAllowedProperty = $projectAllowedPropertyValue;
		}

		$instance->user = !is_null($u=Core_Entity::factory('User')->getCurrent()) ? $u : FALSE;
		if(Core::moduleIsActive('siteuser')) {
			/** @var Siteuser_Model $su */
			$instance->siteuser = !is_null($su=Core_Entity::factory('Siteuser')->getCurrent()) ? $su : FALSE;
			if(isset($instance->siteuser->id) && $instance->siteuser->id>0) {
				$suGroups = $instance->siteuser->siteuser_groups->findAll();
				$instance->siteusergroups = utl::getArrayKeyValuesFromArrays($suGroups);
			}
		}
		$instance->utilspanel = FALSE;

		if($instance->user!=FALSE || $instance->siteuser!==FALSE) {
			$instance->utilspanel = TRUE;
		};

		$srcRequest = new Utils_Path_Info();
		if(isset($_SERVER['REQUEST_URI']) && isset($_SERVER['HTTP_HOST'])) {
			$srcRequestMathches = array();
			preg_match('/([^?]*)\??(.*)/', strtolower($_SERVER['REQUEST_URI']), $srcRequestMathches);
			$hostPort = explode(':', $_SERVER['HTTP_HOST']);
			$splitedPath = array_filter(explode('/', Core_Array::get($srcRequestMathches, 1, '')));

			$pathPattern = '/\/page-([1-9]\d*)\/$/i';
			$pathSearch = Core_Array::get($srcRequestMathches, 1, '');
			$pathNoPage = preg_replace($pathPattern, '/', $pathSearch);
			$pathMatches = array();
			preg_match($pathPattern, $pathSearch, $pathMatches);
			$numPage = ((isset($pathMatches[1]) ? $pathMatches[1] : 1)*1)-1;
			$referer = Core_Array::get($_SERVER, 'HTTP_REFERER', $pathSearch);

			$srcRequest->remoteip = Core_Array::get($_SERVER, 'HTTP_X_FORWARDED_FOR', Core_Array::get($_SERVER, 'REMOTE_ADDR', '0.0.0.0'));
			$srcRequest->proto = Core_Array::get($_SERVER, 'HTTP_X_FORWARDED_PROTO', 'http');
			$srcRequest->domain = $hostPort[0];
			$srcRequest->port = Core_Array::get($hostPort, 1, '80');
			$srcRequest->path = Core_Array::get($srcRequestMathches, 1, '');

			$srcRequest->pathNoPage = $pathNoPage;
			$srcRequest->pathdeep = count($splitedPath);
			$srcRequest->splitedPath = json_encode($splitedPath);
			$srcRequest->crc32path = crc32(Core_Array::get($srcRequestMathches, 1, ''));
			$srcRequest->crc32pathNoPage = crc32($pathNoPage);
			$srcRequest->isMain = (crc32($pathNoPage)==2043925204);
			$srcRequest->query = Core_Array::get($srcRequestMathches, 2, '');
			$srcRequest->currentPage = $numPage;
			$srcRequest->referer = '/'.preg_replace('/https?:\/\/(?:[-\w]+\.)?([-\w]+)\.\w+(?:\.\w+)?\/?(.*)/i', '$2', $referer) ;

			$localGet = $_GET;
			if(isset($_GET['context_html']) && is_array($_GET['context_html']) && count($_GET['context_html'])) {
				foreach ($_GET['context_html'] as $localKey => $localValue) {
					if(preg_match('/#/ui', $localKey)) {
						$localGet['context_html'][preg_replace('/#/ui', '', $localKey)] = $localValue;
						unset($localGet['context_html'][$localKey]);
					}
				}
			}
			$srcRequest->get = isset($localGet) ? $localGet : [];

			$localPost = $_POST;
			if(isset($_POST['context_html']) && is_array($_POST['context_html']) && count($_POST['context_html'])) {
				foreach ($_POST['context_html'] as $localKey => $localValue) {
					if(preg_match('/#/ui', $localKey)) {
						$localPost['context_html'][preg_replace('/#/ui', '', $localKey)] = $localValue;
						unset($localPost['context_html'][$localKey]);
					}
				}
			}
			$srcRequest->post = isset($localPost) ? $localPost : [];
//			$srcRequest->refererNoPage = '';
			$srcRequest->utm = array(
				'source' => Core_Array::getGet('utm_source', Core_Array::get($_COOKIE, 'hostcms_source_service', '')),
				'medium' => Core_Array::getGet('utm_medium', Core_Array::get($_COOKIE, 'hostcms_source_medium', '')),
				'campaign' => Core_Array::getGet('utm_campaign', Core_Array::get($_COOKIE, 'hostcms_source_campaign', '')),
				'content' => Core_Array::getGet('utm_content', Core_Array::get($_COOKIE, 'hostcms_source_content', '')),
				'term' => Core_Array::getGet('utm_term', Core_Array::get($_COOKIE, 'hostcms_source_term', '')),
			);

			try {
				$reader = new Reader('/opt/geoip/base/GeoLite2-City.mmdb');
				/** @var \GeoIp2\Record\City $record */
				$record = $reader->city($srcRequest->remoteip);
				$srcRequest->geoip = array(
					'country' => $record->country->name,
					'city' => $record->city->name,
				);
			} catch (\Exception $e) {
				$srcRequest->geoip = array(
					'country' => 'Russia',
					'city' => 'N/D',
				);
			}
		}
		$instance->srcrequest = $srcRequest;
		$srcRequest->browser = new Utils_Browser_Info();
		$instance->srcrequest = $srcRequest;
	}

//-- Удалил, потому что где-то было зацикливание. Могут быть ошибки --
//	static public function onBeforeGetXml($object, $args) {
//		Core_Event::detach('shop.onBeforeGetXml', array('Utils_Observers_Core', 'onBeforeGetXml'));
//		$instance = Core_Page::instance();
////		if(isset($instance->skynet->response->csrf->token)) {
////			$object->addEntity(Core::factory('Core_Xml_Entity')->name('csrf')->value(base64_encode($instance->skynet->response->csrf->token)));
////		}
////		Skynetcore_Utils::p($instance->srcrequest); die();
//		//-- Надо проверить во всех проектах в XSL шаблонах на присутствие ...shop_item/pathinfo/....
//		//-- Если есть, заменить на /shop/pathinfo/...
////		if(isset($instance->srcrequest) && in_array(get_class($object), ['Shop_Model'])) {
////			$object->addEntity($instance->srcrequest);
////		}
//		Core_Event::attach('shop.onBeforeGetXml', array('Utils_Observers_Core', 'onBeforeGetXml'));
//	}

	static public function onBeforeShowCss($object, $args) {
		$instance = Core_Page::instance();
		if($instance->user != FALSE) {
			$instance->css('/modules/utils/css/adminutils.css');
		}
	}

	static public function onBeforeShowJs($object, $args) {
		$instance = Core_Page::instance();
		if($instance->user != FALSE) {
			$instance->js('/hostcmsfiles/main.js');
			$instance->js('/libs/js/universaladmin.js');
			$instance->js('/libs/extended/adminutils.js');
			$instance->js('/modules/utils/js/adminutils.js');
			$instance->js('/modules/utils/js/togglepanel.js');
			$instance->js('/modules/utils/js/order_front.js');
		}
	}
}