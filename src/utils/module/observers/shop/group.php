<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */

class Utils_Observers_Shop_Group {

	static public function getGroups($shopID, $pathCriteria=NULL, $levelto=5, $entity_name='shop', $setorderby=false) {
		$level = 1;
		$countsQuery = Core_QueryBuilder::select()
			->from(array("{$entity_name}_groups", 'sg1'))
			->where("sg1.{$entity_name}_id", '=', $shopID)
		;
		$pathExpression = '';
		$groupsExpression = '';

		do {
			$levelNext = $level+1;
			$levelPrev = $level-1;
			if( $level>1 ) {
				$pathExpression .= ", ";
				$groupsExpression .= ", ";
				$countsQuery
					->leftJoin(
						array("{$entity_name}_groups", "sg{$level}")
						, "sg{$level}.parent_id", '=',  "sg{$levelPrev}.id"
						, array(array('AND' => array("sg{$level}.deleted", '=', 0)),array('AND' => array("sg{$level}.active", '=', 1))
						)
					);
			}
			if($setorderby) {
				$countsQuery
//					->clearOrderBy()
					->orderBy("sg{$level}.sorting")
				;
			}
			$pathExpression .= "COALESCE(CONCAT('/', sg{$level}.path), '')";
			$groupsExpression .= "sg{$level}.id";

			$countsQuery
				->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.id, '')"), "id{$level}"))
				->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.parent_id, '')"), "parent_id{$level}"))
				->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.name, '')"), "name{$level}"))
				->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.sorting, '')"), "sorting{$level}"))
				->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.path, '')"), "path{$level}"))
				->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.items_count, '')"), "items_count{$level}"))
				->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.items_total_count, '')"), "items_total_count{$level}"))
			;

			$countsQuery
				->groupBy(Core_QueryBuilder::expression("COALESCE(sg{$level}.id, 0)"));

			$level++;
		} while ($level < $levelto);
		$countsQuery->select(array(Core_QueryBuilder::expression("CONCAT({$pathExpression}, '/')"), 'fullpath'));
		$countsQuery
			->where("sg1.{$entity_name}_id", '=', $shopID)
			->where('sg1.parent_id', '=', 0)
			->where(Core_QueryBuilder::expression("COALESCE(sg1.deleted, 0)"), '=', 0)
			->where(Core_QueryBuilder::expression("COALESCE(sg1.active, 1)"), '=', 1)
		;
		if (!is_null($pathCriteria)) {
			$paths = explode('/', $pathCriteria);
			$paths = array_filter($paths);

			foreach ($paths as $pathKey => $path) {
				$countsQuery
					->where("sg{$pathKey}.path", '=', $path);
			}
		}
//		utl::p($countsQuery->build());
		$groupCounts = $countsQuery->execute()->asAssoc()->result();
		$arTreeItems = array();
		foreach ($groupCounts as $groupCount) {
			for ($i=1; $i<$level; $i++) {
				if($groupCount["id{$i}"]!="") {
					$arTreeItems[$groupCount["id{$i}"]] = array(
						'id'=>$groupCount["id{$i}"],
						'parent_id'=>$groupCount["parent_id{$i}"],
						'name'=>$groupCount["name{$i}"],
						'sorting'=>$groupCount["sorting{$i}"],
						'path'=>$groupCount["path{$i}"],
						'items_count'=>$groupCount["items_count{$i}"],
						'items_total_count'=>$groupCount["items_total_count{$i}"],
					);
				}
			}
		}
		$tree = utl::buildTree($arTreeItems);
//		utl::p($tree);
		return $tree;
	}

	/*
	 * Вызов метода:
	 *
	 * Utils_Observers_Shop_Group::recountGroups(shop_id, '/spares/', levelTo);
	 */
	static public function recountGroups($shopID, $pathCriteria=NULL, $levelto=8, $recursion=0) {
		$levelto>8 && $levelto=8;
		if($recursion==0) {
			for($recIndex=1; $recIndex<=$levelto; $recIndex++) {
				self::recountGroups($shopID, $pathCriteria, $recIndex, $recursion+1);
			}
		} else {
			$level = 1;
			$countsQuery = Core_QueryBuilder::select(array(Core_QueryBuilder::expression('COUNT(si.id)'), 'items_total_count'))
				->from(array('shop_groups', 'sg1'))
			;
			$pathExpression = '';
			$groupsExpression = '';
			$levelIndex = 0;
			do {
				$levelNext = $level+1;
				$levelPrev = $level-1;
				if( $level>1 ) {
					$pathExpression .= ", ";
					$groupsExpression .= ", ";
					$countsQuery
						->leftJoin(
							array('shop_groups', "sg{$level}")
							, "sg{$level}.parent_id", '=',  "sg{$levelPrev}.id"
							, array(array('AND' => array("sg{$level}.deleted", '=', 0)),array('AND' => array("sg{$level}.active", '=', 1))
							)
						);
				}
				$pathExpression .= "COALESCE(CONCAT('/', sg{$level}.path), '')";
				$groupsExpression .= "sg{$level}.id";

				if($level==$levelto) {
					$countsQuery
						->groupBy(Core_QueryBuilder::expression("COALESCE(sg{$level}.id, 0)"));
					$levCounerString = '';
					for ($levCounter=$levelto; $levCounter<=9; $levCounter++) {
						$levCounerString.=(($levCounter>$level)?', ':'')."sg{$levCounter}.id";
					}
//					utl::p($levCounerString);
					$countsQuery
						->select(array(Core_QueryBuilder::expression("(SELECT COUNT(sii.id) FROM shop_items sii WHERE sii.shop_group_id = sg{$level}.id AND sii.active=1 AND sii.deleted=0)"), "items_count"))
						->select(array(Core_QueryBuilder::expression("SUM(CASE WHEN si.shop_group_id IN ({$levCounerString}) THEN 1 ELSE 0 END)"), "items_total_count"))
						->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.id, '')"), "id"))
						->select(array(Core_QueryBuilder::expression("COALESCE(sg{$level}.name, '')"), "name"))
						->where(Core_QueryBuilder::expression("NOT sg{$level}.id"), 'IS', NULL)
					;
				}

				$level++;
				$levelIndex++;
			} while ($levelIndex < 9);
//		$countsQuery->select(array(Core_QueryBuilder::expression("CONCAT({$pathExpression}, '/')"), 'fullpath'));
			$countsQuery->join(
				array('shop_items', 'si')
				, 'si.shop_group_id', 'IN', Core_QueryBuilder::expression("({$groupsExpression})"))
				->where('sg1.shop_id', '=', $shopID)
				->where('sg1.parent_id', '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg1.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(sg1.active, 1)"), '=', 1)
				->where(Core_QueryBuilder::expression("COALESCE(si.deleted, 0)"), '=', 0)
				->where(Core_QueryBuilder::expression("COALESCE(si.active, 1)"), '=', 1)
			;
			if (!is_null($pathCriteria)) {
				$paths = explode('/', $pathCriteria);
				$paths = array_filter($paths);

				foreach ($paths as $pathKey => $path) {
					$countsQuery
						->where("sg{$pathKey}.path", '=', $path);
				}
			}
//			utl::p($countsQuery->build());
			$groups = $countsQuery->execute()->asAssoc()->result();
			if(count($groups)>0) {
				$groups = utl::getArrayKeyValuesFromArrays($groups);
				$groupModel = Core_Entity::factory('Shop_Group');
				$groupModel
					->queryBuilder()
					->where('id', 'IN', array_keys($groups))
				;
				$allGroups = $groupModel->findAll(FALSE);
//				utl::p($allGroups);
				foreach ($allGroups as $group) {
					$group->items_count = $groups[$group->id][0]['items_count'];
					$group->items_total_count = $groups[$group->id][0]['items_total_count'];
					$group->save();
				}
//				utl::p($groups);
			}
		}
//SELECT
//    CONCAT(COALESCE(CONCAT('/', sg1.path), ''), COALESCE(CONCAT('/', sg2.path), ''), COALESCE(CONCAT('/', sg3.path), ''), COALESCE(CONCAT('/', sg4.path), ''), COALESCE(CONCAT('/', sg5.path), ''), COALESCE(CONCAT('/', sg6.path), ''), COALESCE(CONCAT('/', sg7.path), ''), COALESCE(CONCAT('/', sg8.path), ''), COALESCE(CONCAT('/', sg9.path), ''), '/') fullpath
//  , COUNT(si.id) cntall
//  , COALESCE(sg1.id, '') id1, COALESCE(sg1.name, '') name1
//  , SUM(CASE WHEN si.shop_group_id IN (sg1.id) THEN 1 ELSE 0 END) cnt1
//  , COALESCE(sg2.id, '') id2, COALESCE(sg2.name, '') name2
//  , SUM(CASE WHEN si.shop_group_id IN (sg2.id) THEN 1 ELSE 0 END) cnt2
//  , COALESCE(sg3.id, '') id3, COALESCE(sg3.name, '') name3
//  , SUM(CASE WHEN si.shop_group_id IN (sg3.id) THEN 1 ELSE 0 END) cnt3
//  , COALESCE(sg4.id, '') id4, COALESCE(sg4.name, '') name4
//  , SUM(CASE WHEN si.shop_group_id IN (sg4.id) THEN 1 ELSE 0 END) cnt4
//  , COALESCE(sg5.id, '') id5, COALESCE(sg5.name, '') name5
//  , SUM(CASE WHEN si.shop_group_id IN (sg5.id) THEN 1 ELSE 0 END) cnt5
//  , COALESCE(sg6.id, '') id6, COALESCE(sg6.name, '') name6
//  , SUM(CASE WHEN si.shop_group_id IN (sg6.id) THEN 1 ELSE 0 END) cnt6
//  , COALESCE(sg7.id, '') id7, COALESCE(sg7.name, '') name7
//  , SUM(CASE WHEN si.shop_group_id IN (sg7.id) THEN 1 ELSE 0 END) cnt7
//  , COALESCE(sg8.id, '') id8, COALESCE(sg8.name, '') name8
//  , SUM(CASE WHEN si.shop_group_id IN (sg8.id) THEN 1 ELSE 0 END) cnt8
//  , COALESCE(sg9.id, '') id9, COALESCE(sg9.name, '') name9
//  , SUM(CASE WHEN si.shop_group_id IN (sg9.id) THEN 1 ELSE 0 END) cnt9
//FROM shop_groups sg1
//  LEFT JOIN shop_groups sg2 ON sg2.parent_id = sg1.id AND sg2.deleted=0 AND sg2.active=1
//  LEFT JOIN shop_groups sg3 ON sg3.parent_id = sg2.id AND sg3.deleted=0 AND sg3.active=1
//  LEFT JOIN shop_groups sg4 ON sg4.parent_id = sg3.id AND sg4.deleted=0 AND sg4.active=1
//  LEFT JOIN shop_groups sg5 ON sg5.parent_id = sg4.id AND sg5.deleted=0 AND sg5.active=1
//  LEFT JOIN shop_groups sg6 ON sg6.parent_id = sg5.id AND sg6.deleted=0 AND sg6.active=1
//  LEFT JOIN shop_groups sg7 ON sg7.parent_id = sg6.id AND sg7.deleted=0 AND sg7.active=1
//  LEFT JOIN shop_groups sg8 ON sg8.parent_id = sg7.id AND sg8.deleted=0 AND sg8.active=1
//  LEFT JOIN shop_groups sg9 ON sg9.parent_id = sg8.id AND sg9.deleted=0 AND sg9.active=1
//  JOIN shop_items si ON si.shop_group_id IN (
//			sg1.id, sg2.id, sg3.id, sg4.id, sg5.id, sg6.id, sg7.id, sg8.id, sg9.id
//		)
//WHERE 1=1
//		AND sg1.shop_id=3
//		AND sg1.parent_id=0
//		AND COALESCE(sg1.deleted, 0)=0 AND COALESCE(sg1.active, 1)=1
//		AND COALESCE(si.deleted, 0)=0 AND COALESCE(si.active, 1)=1
//		AND sg1.path IN ('cars', 'zap')
//GROUP BY COALESCE(sg1.id, 0)
//  , COALESCE(sg2.id, 0)
//  , COALESCE(sg3.id, 0)
//  , COALESCE(sg4.id, 0)
//  , COALESCE(sg5.id, 0)
//  , COALESCE(sg6.id, 0)
//  , COALESCE(sg7.id, 0)
//  , COALESCE(sg8.id, 0)
//  , COALESCE(sg9.id, 0)
//		;
	}
}