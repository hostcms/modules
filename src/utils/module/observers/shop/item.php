<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */

class Utils_Observers_Shop_Item {

	static public function onBeforeRedeclaredGetXml($object, $args) {
		$object->addEntity(
			Core::factory('Core_Xml_Entity')
				->name('yandex_market')->value($object->yandex_market)
		);
	}

	static public function onAfterFindAllSetCalcFoudRow($object, $args) {
		Core_Event::detach( get_class($object).'.onBeforeShow', array('Utils_Observers_Shop_Item', 'onAfterFindAllSetCalcFoudRow'));

		if(isset(Core_Page::instance()->content_last_counts)) {
			Core_Page::instance()->content_last_counts = $object->total;
		}
	}

	static public function onBeforeCopy($newObject, $args) {
		list($object) = $args;

		$group_id = $newObject->shop_group_id;
		if($group_id==0 && $newObject->Modification->id>0 && $newObject->Modification->shop_group_id>0) {
			$group_id = $newObject->Modification->shop_group_id;
		}
		$groupsNav = array_values(Utils_Shop_Group_Controller::getAllParenGroups($group_id));
		$oProperty = Core_Entity::factory('Property')->getByTag_Name('utils_group_code_marking_generate_shop_'.$newObject->shop_id);
//		Utils_Utl::p("{$newObject->shop_group_id}, {$newObject->Modification->shop_group_id}, {$groupsNav[0]->name}", "{$group_id}, {$groupsNav[0]->id}");
		if(isset($groupsNav[0]->id) && $groupsNav[0]->id>0 && isset($oProperty->id) && $oProperty->id>0) {
			$aPropertyValues = $oProperty->getValues($groupsNav[0]->id);
			if (isset($aPropertyValues[0]->value) && $aPropertyValues[0]->value > 0) {
				if(strlen($newObject->marking)==13 && substr($newObject->marking, 0, 4)==$aPropertyValues[0]->value ) {
					$newObject->marking = Utils_Shop_Item_Controller::generateMarking($newObject);
				}
			}
		}
//		Utils_Utl::p("mod = {$newObject->Modification->id}, {$object->path} - {$newObject->path}, {$object->marking} - {$object->name}, {$newObject->marking} - {$newObject->name}", "{$newObject->id}");
	}

	static public function onAfterRedeclaredPrepareForm($controller, $args) {
		list($object, $Admin_Form_Controller) = $args;

		$objectFields = array_keys($object->getTableColums());
		// Данное событие будет вызываться для всех форм, определяем с каким контроллером работаем
		switch (true)
		{
			case $controller instanceof Shop_Item_Controller_Edit:
				$oMainTab = $controller->getTab('main');
				$oLastTab = $controller->getTab('additional');
				$oAdditionalTab = Admin_Form_Entity::factory('Tab')
					->caption('Другие поля')
					->name('tab_fields_others');

				$controller->addTabAfter($oAdditionalTab, $oLastTab);
				$oAdditionalTab
					->add($oAdditionalRow1 = Admin_Form_Entity::factory('Div')->class('row'));
				$objectFieldKey = array_search('deleted', $objectFields);
				if($objectFieldKey!==FALSE) {
					for($i=$objectFieldKey+1; $i<count($objectFields); $i++) {
//							После обновления 6.8.8 начал валить в лог то что было в try/catch. Почему - непонятно
							try {
								$oMainTab
									->move($controller->getField($objectFields[$i]), $oAdditionalRow1);
							} catch (Core_Exception $e) {}
					}
				}

				switch (true) {
					case $object instanceof Shop_Item_Model:
						$groupId = (isset($object->modification->id) && $object->modification->id>0) ? $object->modification->shop_group_id : $object->shop_group_id;
						$groupsNav = array_values(Utils_Shop_Group_Controller::getAllParenGroups($groupId));
						$oProperty = Core_Entity::factory('Property')->getByTag_Name('utils_group_code_marking_generate_shop_'.$object->shop_id);
						if(isset($groupsNav[0]->id) && $groupsNav[0]->id>0 && isset($oProperty->id) && $oProperty->id>0) {
							$marking = $controller->getField('marking');
							if (trim(''.$marking->value) == '' || trim(''.$marking->value) == '0') {
								$marking->value = Utils_Shop_Item_Controller::generateMarking($object);
								$marking->value == '0' && $marking->value = '';
							}
							if (strlen($marking->value) == 13) {
								$marking->readonly('readonly');
//									$regFormat = '/^' . substr($marking->value, 0, 4) . '\d{9}$/';
//									$marking->format(
//										array(
//											'minlen' => array('value' => 1),
//											'reg' => array('value' => $regFormat, 'message' => 'Не соответствует формату "' . $regFormat . '"')
//										)
//									);
							}
							$name = $controller->getField('name');
							$name->format(
								array(
									'minlen' => array('value' => 2),
								)
							);
						}
						break;
				}
				break;
		}

	}

	static public function onBeforeExecute($controller, $args) {
		$contents = '';
		list($operation, $Admin_Form_Controller) = $args;
		$object = NULL;
		if(method_exists($controller, 'getObject')) {
			$object = $controller->getObject();
		}
		ob_start();
		//-- пишем код для отладки ---
		$contents = ob_get_clean();

		if (is_null($operation))
		{
			$objectFields = array_keys($object->getTableColums());
			// Данное событие будет вызываться для всех форм, определяем с каким контроллером работаем
			switch (get_class($controller))
			{
				case 'Shop_Item_Controller_Edit':
					$oMainTab = $controller->getTab('main');
					$oLastTab = $controller->getTab('additional');
					$oAdditionalTab = Admin_Form_Entity::factory('Tab')
						->caption('Другие поля')
						->name('tab_fields_others');

					$controller->addTabAfter($oAdditionalTab, $oLastTab);
					$oAdditionalTab
						->add($oAdditionalRow1 = Admin_Form_Entity::factory('Div')->class('row'));
					$objectFieldKey = array_search('deleted', $objectFields);
					if($objectFieldKey!==FALSE) {
						for($i=$objectFieldKey+1; $i<count($objectFields); $i++) {
//							После обновления 6.8.8 начал валить в лог то что было в try/catch. Почему - непонятно
//							try {
//								$oMainTab
//									->move($controller->getField($objectFields[$i]), $oAdditionalRow1);
//							} catch (Core_Exception $e) {}
						}
					}

					switch (true) {
						case $object instanceof Shop_Item_Model:
							$groupId = (isset($object->modification->id) && $object->modification->id>0) ? $object->modification->shop_group_id : $object->shop_group_id;
							$groupsNav = array_values(Utils_Shop_Group_Controller::getAllParenGroups($groupId));
							$oProperty = Core_Entity::factory('Property')->getByTag_Name('utils_group_code_marking_generate_shop_'.$object->shop_id);
							if(isset($groupsNav[0]->id) && $groupsNav[0]->id>0 && isset($oProperty->id) && $oProperty->id>0) {
								$marking = $controller->getField('marking');
								if (trim(''.$marking->value) == '' || trim(''.$marking->value) == '0') {
									$marking->value = Utils_Shop_Item_Controller::generateMarking($object);
									$marking->value == '0' && $marking->value = '';
								}
								if (strlen($marking->value) == 13) {
									$marking->readonly('readonly');
//									$regFormat = '/^' . substr($marking->value, 0, 4) . '\d{9}$/';
//									$marking->format(
//										array(
//											'minlen' => array('value' => 1),
//											'reg' => array('value' => $regFormat, 'message' => 'Не соответствует формату "' . $regFormat . '"')
//										)
//									);
								}
								$name = $controller->getField('name');
								$name->format(
									array(
										'minlen' => array('value' => 2),
									)
								);
							}
							break;
					}
					break;
			}
		}
		$controller->addContent($contents);
	}

	static public function onBeforePriceDelete($object, $args) {
		$prLinkedValue = Core_Entity::factory('Utils_Shop_Item_Price')->getByShop_Item_Price_Id($object->id, FALSE);
		if(!is_null($prLinkedValue) && is_object($prLinkedValue)) {
			$prLinkedValue->delete();
		}
	}

	static public function onBeforeGetXml($object, $args) {
		$instance = Core_Page::instance();

		if($instance->utilspanel) {
			$adminPanel = Utils_Admin_Panel::createInstance()->shopItemAdminPanel($object);

			$object->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('adminPanel')->value($adminPanel)
			);
		}
		$object->addEntity(
			Core::factory('Core_Xml_Entity')
				->name('originalprice')
				->value($object->price * $object->shop_currency->exchange_rate)
		);
	}

	static function getProperties($itemQueryBuilder, &$customGroupProperties, &$customItemProperties) {
		$qItemProperties = clone $itemQueryBuilder;
		$qItemProperties
				->sqlCalcFoundRows(false)
				->clearSelect()
				->clearLimit()
				->clearOffset()
				->select('propertyfiltersip.property_id')
				->select('propertyfiltersip.show_in_group')
				->select('propertyfiltersip.show_in_item')
				->from('shop_items')
				->join(['shop_item_property_for_groups', 'propertyfiltersipfg'], 'propertyfiltersipfg.shop_group_id', '=', 'shop_items.shop_group_id')
				->join(['shop_item_properties', 'propertyfiltersip'], 'propertyfiltersip.id', '=', 'propertyfiltersipfg.shop_item_property_id')
				->groupBy('propertyfiltersip.property_id')
			;
			$aProperties = $qItemProperties->execute()->asAssoc()->result();

		foreach ($aProperties as $aProperty) {
			if($aProperty['show_in_group']==1) {
				$customGroupProperties[] = $aProperty['property_id'];
			}
			if($aProperty['show_in_item']==1) {
				$customItemProperties[] = $aProperty['property_id'];
			}
		}
	}
}