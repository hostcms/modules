<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Utils_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '6.1';
	/**
	 * Module date
	 * @var date
	 */
	public $date = '2016-02-29';


	protected $_afSettings = array(
		'gallery' => array(											//-- Форма
			'guid' => '1F0264AF-0129-1749-96DD-A661BB8ECB00',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Фотогалерея к товару',									//-- по-русски - 1=идентификатор языка
				2 => 'Photogallery to shop items'									//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
				'marking' =>  array(
					'name' => array(								//-- название поля в админке
						1 => 'Артикул',				//-- по-русски - 1=идентификатор языка
						2 => 'Marking'						//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,
					'width' => '180px',								//-- ширина поля, например '55px'
				),
				'name' =>  array(
					'name' => array(
						1 => 'Наименование товара',
						2 => 'Name'
					),
					'sorting' => 30,
				),
			),
			'actions' => array(
				'addGallery' => array(								//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Добавить галерею к товару',			//-- по-русски - 1=идентификатор языка
						2 => 'Add gallery to item'					//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '',			//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => 0,
					'confirm' => 0,
				),
				'deleteLargeImage' => array(	//-- ключевое наименование действия для формы
					'name' => array(			//-- название действия в админке
						1 => '',				//-- по-русски - 1=идентификатор языка
						2 => ''					//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,			//-- сортировка для действий
					'picture' => '',			//--
					'icon' => 'fa fa-trash-o',
					'color' => 'darkorange',
					'single' => 1,
					'group' => 0,
					'dataset' => -1,
					'confirm' => 0,
				),
				'deleteSmallImage' => array(	//-- ключевое наименование действия для формы
					'name' => array(			//-- название действия в админке
						1 => '',				//-- по-русски - 1=идентификатор языка
						2 => ''					//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 30,			//-- сортировка для действий
					'picture' => '',			//--
					'icon' => 'fa fa-trash-o',
					'color' => 'darkorange',
					'single' => 1,
					'group' => 0,
					'dataset' => -1,
					'confirm' => 0,
				),
			)
		),
		'article' => array(											//-- Форма
			'guid' => 'FBEF86C8-9BE1-9AD8-381A-D8CC7D6FD46E',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Статья к товару',								//-- по-русски - 1=идентификатор языка
				2 => 'Article to shop items'						//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
			),
			'actions' => array(
				'addArticle' => array(								//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Добавить статью к товару',			//-- по-русски - 1=идентификатор языка
						2 => 'Add article to item'					//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '',			//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => 0,
					'confirm' => 0,
				),
			)
		),
		'technical' => array(											//-- Форма
			'guid' => '700AE7EB-D9AE-5B58-6460-FEC694851EAA',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 0,									//-- Показывать операции
			'show_group_operations' => 0,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Редактор технических характеристик',			//-- по-русски - 1=идентификатор языка
				2 => 'Edit technical'								//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
			),
			'actions' => array(
				'editTechnical' => array(							//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '',			//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => 0,
					'confirm' => 0,
				),
			)
		)
	);
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		// Add template observer
		Core_Event::attach('Core.onBeforeInitConstants', array('Utils_Observers_Core', 'onBeforeInitConstants'));
		Core_Event::attach('Core.onAfterInitConstants', array('Utils_Observers_Core', 'onAfterInitConstants'));
		Core_Event::attach('Core.onAfterLoadModuleList', array('Utils_Observers_Core', 'onAfterLoadModuleList'));
		Core_Event::attach('Core_Page.onBeforeShowCss', array('Utils_Observers_Core', 'onBeforeShowCss'));
		Core_Event::attach('Core_Page.onBeforeShowJs', array('Utils_Observers_Core', 'onBeforeShowJs'));
//		Core_Event::attach('shop_item.onBeforeGetXml', array('Utils_Observers_Core', 'onBeforeGetXml'));
//-- Удалил, потому что где-то было зацикливание. Могут быть ошибки --
//		Core_Event::attach('shop.onBeforeGetXml', array('Utils_Observers_Core', 'onBeforeGetXml'));
//		Core_Event::attach('form.onBeforeGetXml', array('Utils_Observers_Core', 'onBeforeGetXml'));
		Core_Event::attach('shop_item.onBeforeGetXml', array('Utils_Observers_Shop_Item', 'onBeforeGetXml'));
		Core_Event::attach('shop_item.onBeforeCopy', array('Utils_Observers_Shop_Item', 'onBeforeCopy'));
		Core_Event::attach('informationsystem_group.onBeforeGetXml', array('Utils_Observers_Informationsystem_Item', 'onBeforeGetXml'));
		Core_Event::attach('informationsystem_item.onBeforeGetXml', array('Utils_Observers_Informationsystem_Item', 'onBeforeGetXml'));
		Core_Event::attach('shop_order.onCallorderPrint', array('Utils_Shop_Order_Observers', 'onCallOrderprint'));
		Core_Event::attach('shop_order.onCallorderShippers', array('Utils_Shop_Order_Observers', 'onCallOrderShippers'));
		Core_Event::attach('shop_item_price.onBeforeDelete', array('Utils_Observers_Shop_Item', 'onBeforePriceDelete'));
		Core_Event::attach('Core_Response.onAfterShowBody', array('Utils_Admin_Panel', 'sitePanelShow'));
		Core_Event::attach('Structure_Controller_Show.onBeforeShowPanel', array('Utils_Admin_Panel', 'onBeforeShowPanel'));
		Core_Event::attach('Shop_Controller_Show.onBeforeShowPanel', array('Utils_Admin_Panel', 'onBeforeShowPanel'));
		Core_Event::attach('Moto_Shop_Controller_Show.onBeforeShowPanel', array('Utils_Admin_Panel', 'onBeforeShowPanel'));
		Core_Event::attach('Informationsystem_Controller_Show.onBeforeShowPanel', array('Utils_Admin_Panel', 'onBeforeShowPanel'));
		Core_Event::attach('Informationsystem_Controller_Show.onBeforeShowPanel', array('Utils_Admin_Panel', 'onBeforeShowPanel'));
		Core_Event::attach('site.onBeforeGetXml', array('Utils_Observers_Site', 'onBeforeGetXml'));

		//-- Gallery observers --
		//-- Это для модуля галерей в отдельной сущности. Может когда-нибудь доделаю. Пока что некогда... --
		//Core_Event::attach('shop_item.onBeforeGetXml', array('Utils_Gallery_Observers_Module', 'onBeforeGetXml'));
		//Core_Event::attach('shop_group.onBeforeGetXml', array('Utils_Gallery_Observers_Module', 'onBeforeGetXml'));

		if(!is_null($user=Core_Entity::factory('User')->getCurrent()) && $user->id > 0) {
			Core_Event::attach('Admin_Form_Controller.onBeforeAddEntity', array('Utils_Observers_Admin_Form', 'onBeforeAddEntity'));
			//-- вместо этого
//			Core_Event::attach('Admin_Form_Action_Controller_Type_Edit.onBeforeExecute', array('Utils_Observers_Shop_Item', 'onBeforeExecute'));
			//-- это
			Core_Event::attach('Admin_Form_Action_Controller_Type_Edit.onAfterRedeclaredPrepareForm', array('Utils_Observers_Shop_Item', 'onAfterRedeclaredPrepareForm'));
			Core_Event::attach('shop_item.onBeforeRedeclaredGetXml', array('Utils_Observers_Shop_Item', 'onBeforeRedeclaredGetXml'));

			Core_Skin::instance()->addJs('/modules/utils/js/fancybox/jquery.fancybox.pack.js');
			Core_Skin::instance()->addJs('/admin/utils/js/order.js');

			Core_Skin::instance()->addCss('/modules/utils/js/fancybox/jquery.fancybox.css');
			Core_Skin::instance()->addCss('/modules/utils/css/adminutils.css');

			Core_Skin::instance()->addCss('/admin/utils/css/order.css');

			//-- Расширение языковых файлов ----------------------------------------------------------------------------
			Core_I18n::instance()->expandLng('shop_item', //-- rus --
				array(
					'editTechnical_success' => 'Редактирование выполнено успешно',
				), 'ru'
			);
			Core_I18n::instance()->expandLng('shop_item', //-- eng --
				array(
					'editTechnical_success' => 'Edit success',
				), 'en'
			);
			//-- Панели администрирования ------------------------------------------------------------------------------
			Core_Event::attach('Utils_Admin_Panel.onBeforeMakePanel', array('Utils_Observers_Admin_Panel', 'onBeforeMakePanel'));
//			Core_Event::attach('Utils_Admin_Panel.onBeforeAddItemPanelButton', array('Utils_Observers_Admin_Panel', 'onBeforeAddItemPanelButton'));
//			Core_Event::attach('Utils_Admin_Panel.onAfterAddItemPanelButton', array('Utils_Observers_Admin_Panel', 'onBeforeAddItemPanelButton'));

			Core_Router::add('utils_articles_ajax', '/admin/utils/utils_article/ajax/{operation}/{srcItemId}/({isAtricleId}/)')->controller('Utils_Admin_Panel_Article_Ajax_Controller');
		}

		$currentVersion = preg_replace('/^(\d+.\d+)(.*)/', '$1', CURRENT_VERSION)*1;
		if($currentVersion >= 6.7) {
			Core_Router::add(
				  'utils_images'
				, '/{w}x{h}(/fn/{processing})(/{uploadpath})/({path}).{ext}(/{noimagepath})'
				, array('w' => '\d+', 'h' => '\d+'
				, 'uploadpath' => 'upload')
			)->controller('Utils_Image_Controller');
		}
		Core_Router::add(
			'utils_gallery_ajax',
			'/admin/utils/utils-gallery/ajax/{operation}/{srcItemId}.json(/{wpref}x{hpref})({tprefix}.tp)',
			['wpref' => '\d+', 'hpref' => '\d+']
			)
			->controller('Utils_Admin_Panel_Gallery_Ajax_Controller');

		if( preg_match('/\/admin\/shop\/order\/card\//', $_SERVER['SCRIPT_NAME'])!==FALSE
			&& Core_Array::getRequest('remove_ip_lock', 0)!=0) {
			if(isset($_SESSION['current_user_ip'])) {
				unset($_SESSION['current_user_ip']);
			}
		}

		$this->menu = array(
			array(
				'sorting' => 270,
				'block' => 1,
				'ico' => 'fa fa-globe',
				'name' => Core::_('Utils.model_name'),
				'href' => "/admin/utils/gallery/index.php",
				'onclick' => "$.adminLoad({path: '/admin/utils/gallery/index.php'}); return false"
			)
		);
	}

	/**
	 * Install module.
	 */
	public function install() {
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			//-- Добавляем форму --
			$adminForm = $this->adminForm($afSetting['name'], $afSetting);
		}
	}

	/**
	 * Uninstall.
	 */
	public function uninstall()
	{
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (!is_null($oAdmin_Form)) {
				// Удаление формы
				$oAdmin_Form->delete();
			}
		}
	}

	protected function adminForm($afName, $afSetting) {
		$oAdmin_Form = NULL;
		if(isset($afSetting['guid'])) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (is_null($oAdmin_Form)) {
				$oAdmin_Word_Form = $this->adminWordForm($afName);
				$oAdmin_Form = Core_Entity::factory('Admin_Form');
				$oAdmin_Form->admin_word_id = $oAdmin_Word_Form->id;
				$oAdmin_Form->key_field = $afSetting['key_field'];
				$oAdmin_Form->default_order_field = Core_Array::get($afSetting, 'default_order_field', $afSetting['key_field']);

				$oAdmin_Form->on_page = Core_Array::get($afSetting, 'on_page', 20);
				$oAdmin_Form->show_operations = Core_Array::get($afSetting, 'show_operations', 1);
				$oAdmin_Form->show_group_operations = Core_Array::get($afSetting, 'show_group_operations', 0);
				$oAdmin_Form->default_order_direction = Core_Array::get($afSetting, 'default_order_direction', 0);
				$oAdmin_Form->guid = $afSetting['guid'];
				$oAdmin_Form->save();
			}
			if(isset($afSetting['fields']) && is_array($afSetting['fields'])) {
				//-- Добавляем поля в форму --
				foreach ($afSetting['fields'] as $dbFieldKey => $field) {
					$adminFormField = $this->adminFormField($dbFieldKey, $field);
					$oAdmin_Form->add($adminFormField);
				}
			}
			if(isset($afSetting['actions']) && is_array($afSetting['actions'])) {
				//-- Добавляем действия в форму --
				foreach ($afSetting['actions'] as $dbActionKey => $action) {
					$adminFormAction = $this->adminFormAction($dbActionKey, $action);
					$oAdmin_Form->add($adminFormAction);
				}
			}
		}

		return $oAdmin_Form;
	}

	protected function adminFormAction($fDbName, $action) {
		$oAdmin_Word_Form = $this->adminWordForm($action['name']);
		$oAdmin_Form_Action = Core_Entity::factory('Admin_Form_Action')->save();
		$oAdmin_Form_Action->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Action->name = $fDbName;
		$oAdmin_Form_Action->picture = Core_Array::get($action, 'picture', '');
		$oAdmin_Form_Action->icon = Core_Array::get($action, 'icon', '');
		$oAdmin_Form_Action->color = Core_Array::get($action, 'color', '');
		$oAdmin_Form_Action->single = Core_Array::get($action, 'single', 0);
		$oAdmin_Form_Action->group = Core_Array::get($action, 'group', 0);
		$oAdmin_Form_Action->sorting = Core_Array::get($action, 'sorting', 1000);
		$oAdmin_Form_Action->dataset = Core_Array::get($action, 'dataset', '-1');
		$oAdmin_Form_Action->confirm = Core_Array::get($action, 'confirm', 0);
//		$oAdmin_Form_Action->save();
		return $oAdmin_Form_Action;
	}

	protected function adminFormField($fDbName, $field) {
		$oAdmin_Word_Form = $this->adminWordForm($field['name']);
		$oAdmin_Form_Field = Core_Entity::factory('Admin_Form_Field')->save();
		$oAdmin_Form_Field->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Field->name = $fDbName;
		$oAdmin_Form_Field->sorting = Core_Array::get($field, 'sorting', 1000);
		$oAdmin_Form_Field->ico = Core_Array::get($field, 'ico', '');
		$oAdmin_Form_Field->type = Core_Array::get($field, 'type', 1);
		$oAdmin_Form_Field->format = Core_Array::get($field, 'format', '');
		$oAdmin_Form_Field->allow_sorting = Core_Array::get($field, 'allow_sorting', 1);
		$oAdmin_Form_Field->allow_filter = Core_Array::get($field, 'allow_filter', 1);
		$oAdmin_Form_Field->editable = Core_Array::get($field, 'editable', 1);
		$oAdmin_Form_Field->filter_type = Core_Array::get($field, 'filter_type', 0);
		$oAdmin_Form_Field->class = Core_Array::get($field, 'class', '');
		$oAdmin_Form_Field->width = Core_Array::get($field, 'width', '');
		$oAdmin_Form_Field->image = Core_Array::get($field, 'image', '');
		$oAdmin_Form_Field->link = Core_Array::get($field, 'link', '');
		$oAdmin_Form_Field->onclick = Core_Array::get($field, 'onclick', '');
		$oAdmin_Form_Field->list = Core_Array::get($field, 'list', '');
		return $oAdmin_Form_Field;
	}

	protected function adminWordForm($afName) {
		$oAdmin_Word_Form = Core_Entity::factory('Admin_Word')->save();
		foreach ($afName as $langID => $langName) {
			$oAdmin_Word_Value = $this->adminWordValue($langID, $langName);
			$oAdmin_Word_Form->add($oAdmin_Word_Value);
		}
		return $oAdmin_Word_Form;
	}

	protected function adminWordValue($language_id, $name) {
		$oAdmin_Word_Value = Core_Entity::factory('Admin_Word_Value')->save();
		$oAdmin_Word_Value->admin_language_id = $language_id;
		$oAdmin_Word_Value->name = $name;
		$oAdmin_Word_Value->save();
		return $oAdmin_Word_Value;
	}
}