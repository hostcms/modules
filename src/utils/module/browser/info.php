<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Utils_Browser_Info extends Utils_Core_Servant_Properties {

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'mobile',
		'realmobile',
		'w',
		'h',
	);

	/**
	 * Utils_Browser_Info constructor.
	 * @param array $_allowedProperties
	 */
	public function __construct()
	{
		parent::__construct();

		$detect = new \Detection\MobileDetect;
//		$needPart = preg_match('/^\/(snowmobiles|gsm)\//ui', Core_Array::get($_SERVER, 'REQUEST_URI', ''));
//		$this->realmobile = $detect->isMobile() && $needPart==1;
		$this->realmobile = $detect->isMobile() && !$detect->isTablet(); // && $needPart==1;
		$this->mobile = false;
	}
}