<?php
use Utils_Utl as utl;
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Admin forms.
 * Типовой контроллер редактирования
 *
 * @package HostCMS 6\Admin
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Utils_Admin_Form_Action_Controller_Type_Edit extends Admin_Form_Action_Controller_Type_Edit
{
	protected $_mainTab = NULL;
	protected $_additionalTab = NULL;
	protected $_windowID = NULL;

	protected function createSelect($modelFrom, $lngClass, $fieldName, $selectedValue, $divAttr=array(), $keyField='id', $valueField='name') {
		$aSelectValues = array(' … ');
		if(is_array($modelFrom)) {
			foreach($modelFrom as $modelArrayKey => $modelArrayValue) {
				$aSelectValues[$modelArrayKey] = $modelArrayValue;
			}
		} else {
			$mEntities = (gettype($modelFrom)=='string') ? Core_Entity::factory($modelFrom) : $modelFrom;
			$allEntities = $mEntities->findAll();
			foreach($allEntities as $entity) {
				$aSelectValues[$entity->$keyField] = $entity->$valueField;
			}
		}

		$selectEntity = Admin_Form_Entity::factory('Select')
			->name($fieldName)
			->id($fieldName)
			->caption(Core::_("$lngClass.$fieldName"))
			->divAttr($divAttr)
			->options($aSelectValues)
			->value($selectedValue);

		return $selectEntity;
	}

	/**
	 * Set object
	 * @param object $object object
	 * @return self
	 */
	public function setObject($object)
	{
		parent::setObject($object);

		(!is_null($this->getTab('main'))) && $this->_mainTab = $this->getTab('main');
		if(!is_null($this->getTab('additional'))) {
			$this->_additionalTab = $this->getTab('additional');
		} else {
			$this
				->addTabAfter($this->_additionalTab = Admin_Form_Entity::factory('Tab')
					->caption(Core::_('Utils.tab_Additional'))
					->name('Formats'), $this->_mainTab);
		}
		$this->_windowID = $this->_Admin_Form_Controller->getWindowId();

		foreach($this->_additionalTab->getFields() as $field) {
			$this->_additionalTab->delete($field);
		}
		$this->_additionalTab
			->add($oAdditionalRow1 = Admin_Form_Entity::factory('Div')->class('row'));
		(isset($this->_object->id) && $this->_object->id>0) && $oAdditionalRow1->add($this->getField('id'));
		(isset($this->_object->parent_id)) && $oAdditionalRow1->add($this->getField('parent_id'));

		foreach($this->_mainTab->getFields() as $field) {
//			$this->_mainTab->delete($field);
			$this->_mainTab->move($field, $this->_additionalTab);
		}
		(isset($this->_object->site_id)) && $this->getField('site_id')->readonly('readonly');

		return $this;
	}

	/**
	 * Processing of the form. Apply object fields.
	 * @hostcms-event Advertisement_Controller_Edit.onAfterRedeclaredApplyObjectProperty
	 */
	protected function _applyObjectProperty()
	{
		parent::_applyObjectProperty();
	}
}