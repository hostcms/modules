<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 *
 * @package HostCMS
 * @subpackage Utils
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */

class Utils_Admin_Panel {

	protected $_currentXSLPanel=NULL;
	protected $_currentXSLSubPanels=NULL;

	/**
	 * @return null
	 */
	public function getCurrentXSLPanel()
	{
		return $this->_currentXSLPanel;
	}

	/**
	 * @param null $currentXSLPanel
	 */
	public function setCurrentXSLPanel($currentXSLPanel)
	{
		$this->_currentXSLPanel = $currentXSLPanel;
	}

	/**
	 * @return null
	 */
	public function getCurrentXSLSuubPanels()
	{
		return $this->_currentXSLSubPanels;
	}

	/**
	 * @param null $currentXSLSuubPanels
	 */
	public function setCurrentXSLSuubPanels($currentXSLSuubPanels)
	{
		$this->_currentXSLSubPanels = $currentXSLSuubPanels;
	}

	/**
	 * Utils_Admin_Panel constructor.
	 */
	public function __construct()
	{
	}

	static public function createInstance()
	{
		return new self();
	}

	//-- Метод инвертирован, если вернет FALSE, панель будет показана, TRUE - не будет ---------------------------------
	static function checkShow($showPanel = true)
	{
		$retValue = (Core::$url['path'] == '/edit-in-place.php'
			|| Core::$url['path'] == '/robots.txt'
			|| Core::$url['path'] == '/hostcms-benchmark.php'
			|| Core::$url['path'] == '/users/administration/items-are-added/'
			|| Core_Array::getRequest('action', '') != ''
			|| (isset(Core_Page::instance()->restrictByIP) && !Core_Page::instance()->restrictByIP)
			|| is_null(Core_Page::instance()->template)
			|| !$showPanel
		);
		return $retValue;
	}

	static function sitePanelShow()
	{
		if ( self::checkShow() ) {
			return;
		}

		if (Core::checkPanel() && Core_Array::getGet('ajaxload', false)==false) {
			$oHostcmsTopPanel = Core::factory('Core_Html_Entity_Div')
				->class('hostcmsPanel togglePanel');

			$oHostcmsSubPanel = Core::factory('Core_Html_Entity_Div')
				->class('hostcmsSubPanel hostcmsWindow');

			$oHostcmsTopPanel->add($oHostcmsSubPanel);

			// Separator
			$oHostcmsSubPanel
				->add(
					Core::factory('Core_Html_Entity_Span')
						->id('togglePanel')
						->add(
							Core::factory('Core_Html_Entity_Img')
								->width(16)->height(16)
								->src('/modules/utils/images/togglepanel.png')
								->alt('Скрыть\Показать панели')
								->title('Скрыть\Показать панели')
						)
				);
			Core_Event::notify('Utils_Admin_Panel.onBeforeMakeSitePanel', $oHostcmsTopPanel);

			echo $oHostcmsTopPanel->execute();
		}
	}

	public static function onBeforeShowPanel($object, $args) {
		if ( self::checkShow() /* || !is_array($this->_currentXSLSubPanels) */ ) {
			return '';
		}
		$instance = Core_Page::instance();
		/** @var Core_Html_Entity_Div $oXslSubPanels*/
		list($oXslSubPanels) = $args;
		if(!$instance->superuser) {
			$aChildSubPanels = $oXslSubPanels->getChildren();
			/** @var Core_Html_Entity_A $oXslSubPanel */
			foreach ($aChildSubPanels as $iXslSubPanel => $oXslSubPanel) {
				$oXslSubPanels->deleteChild(0);
			}
		}
	}

	function shopItemAdminPanel($object) {
		if ( self::checkShow() /* || !is_array($this->_currentXSLSubPanels) */ ) {
			return '';
		}

		$panelHTML = '';
		$dateAttrOperation = "data-entityid";
		// Panel
		$this->_currentXSLPanel = Core::factory('Core_Html_Entity_Div')
			->class('hostcmsPanel customAdminItem')
			->addAllowedProperty($dateAttrOperation);
		;
		$this->_currentXSLPanel->$dateAttrOperation = $object->id;

		Core_Event::notify(get_class($this).'.onBeforeMakePanel', $this, array($this->_currentXSLPanel, $object));
		if(is_array($this->_currentXSLSubPanels)){
			foreach($this->_currentXSLSubPanels as $currentXSLSuubPanel) {
				$this->_currentXSLPanel
					->add($currentXSLSuubPanel);
			}
			Core_Event::notify(get_class($this).'.onAfterMakePanel', $this, array($this->_currentXSLPanel, $object));
			ob_start();
			$this->_currentXSLPanel
				->execute();
			$panelHTML = ob_get_clean();
		}

		return $panelHTML;
	}

	function addSubPanel($oXSLSubPanel) {
		$this->_currentXSLSubPanels[] = $oXSLSubPanel;
		return $this;
	}

	function addItemPanelButton(&$oXslSubPanel
			, $propertyXmlTag
			, $setImage=true
			, $imagePath='/modules/utils/images/'
			, $title=''
			, $href=''
			, $click=''
			, $text=['header'=>'', 'text'=>'', 'class'=>'', 'parentclass'=>'']
			, $entity=[]
	) {
		$intContent = NULL;
		if(is_array($text)) {
			!isset($text['header']) && $text['header'] = '';
			!isset($text['text']) && $text['text'] = '';
			!isset($text['class']) && $text['class'] = '';
		} elseif (is_string($text)) {
			$text=['header'=>'', 'text'=>$text];
		} else {
			$text=['header'=>'', 'text'=>''];
		}
		$text['class'] = Core_Array::get($text, 'class', '');
		if($setImage && $imagePath!='') {
			$intContent = Core::factory('Core_Html_Entity_Img')
				->width(16)->height(16)
				->src("{$imagePath}{$propertyXmlTag}.gif")
			;
		} else {
			$intContent = Core::factory('Core_Html_Entity_Span')
				->value($text['text'])
			;
		}
		if($setImage) {
			/** @var Core_Html_Entity_A $iconLink */
			$iconLink = Core::factory('Core_Html_Entity_A')
				->href($href=='' ? "javascript:void(0);" : $href)
			;
			if( $imagePath!='') {
				$iconLink->class(trim("admin_utils"));
			} else {
				$iconLink->class(trim("counts_key ".Core_Array::get($text, 'parentclass', '')));
				$text['header']!='' && $iconLink->add(Core::factory('Core_Html_Entity_Div')->value($text['header']));
				$intContent = Core::factory('Core_Html_Entity_Div')
					->class('adm_prefix'.(($text['class'] != '') ? ' ' : '').$text['class'])
					->add($intContent)
				;
			}
			$iconLink->add($intContent);
			;
		} else {
			$iconLink = Core::factory('Core_Html_Entity_Div')
				->class('adm_prefix'.(($text['class'] != '') ? ' ' : '').$text['class'])
			;
			$text['header']!='' && $iconLink->add(Core::factory('Core_Html_Entity_Div')->value($text['header']));
			$intContent!='' && $iconLink->add($intContent);
		}
		($click != '') && $iconLink->onclick($click);
		($title != '') && $iconLink->title($title);
		$dateAttrOperation = "data-operation";
		$iconLink->addAllowedProperty($dateAttrOperation);
		$iconLink->$dateAttrOperation = $propertyXmlTag;
		$dateImagePath = "data-imagepath";
		$iconLink->addAllowedProperty($dateImagePath);
		$iconLink->$dateImagePath = $imagePath;

		if(isset($entity['object']) && is_object($entity['object'])) {
			$aEntityName = 'data-entity';
			$aEntityId = 'data-entity-id';
			$iconLink->addAllowedProperty($aEntityName);
			$iconLink->addAllowedProperty($aEntityId);
			$iconLink->$aEntityName = strtolower(get_class($entity['object']));
			$iconLink->$aEntityId = $entity['object']->id;
			if(isset($entity['property_id'])) {
				$propName = 'data-propertyid';
				$iconLink->addAllowedProperty($propName);
				$iconLink->$propName = $entity['property_id'];
			}
			if(isset($entity['property_value'])) {
				$propName = 'data-propertyvalue';
				$iconLink->addAllowedProperty($propName);
				$iconLink->$propName = $entity['property_value'];
			}
		}

		Core_Event::notify(get_class($this).'.onBeforeAddItemPanelButton', $oXslSubPanel, array($propertyXmlTag));
		$oXslSubPanel
			->add( $iconLink )
		;
		Core_Event::notify(get_class($this).'.onAfterAddItemPanelButton', $oXslSubPanel, array($propertyXmlTag));
		return $this;
	}
}