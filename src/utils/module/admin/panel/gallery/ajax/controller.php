<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 05.03.2017
 * Time: 15:59
 */

class Utils_Admin_Panel_Gallery_Ajax_Controller extends Core_Command_Controller {

	public static function getGalleryListByShopItem($shop_item_id, $prefix="", $thumbprefix="")
	{
		$srcItem = Core_Entity::factory('Shop_Item')->getById($shop_item_id);
		$is = Core_Entity::factory('Informationsystem')->getByName("Фотогалерея.{$srcItem->shop_id}");
		$src = $srcAll = $oAll = [];
		if (isset($is->id) && $is->id > 0) {
			$linkedObject = Core_Entity::factory('Informationsystem_Group_Property_List', $is->id);
			$aProperties = $linkedObject->Properties->findAll(false);
			/** @var Property_Model $oProperty */
			foreach ($aProperties as $oProperty) {
				if ($oProperty->tag_name == 'utils.properties.shop_item_link') {
					$aPropertyGroups = $oProperty->getValuesByValue($srcItem->id, '=', false);
					if (isset($aPropertyGroups[0]->id) && $aPropertyGroups[0]->id > 0) {
						$galleryGroup = Core_Entity::factory('Informationsystem_group')->getById($aPropertyGroups[0]->entity_id);
						if (isset($galleryGroup->id) && $galleryGroup->id > 0) {
							$galleryItems = $galleryGroup->informationsystem_items->findAll(false);
							/** @var Informationsystem_Item_Model $galleryItem */
							foreach ($galleryItems as $index => $galleryItem) {
								if ($index < 9) {
									$src[] = $prefix.$thumbprefix.$galleryItem->getLargeFileHref();
								}
								$oAll[] = $galleryItem;
								$srcAll[] = $galleryItem->getLargeFileHref();
							}
						}
					}
				}
			}
			return [
				"src" => $src,
				"srcAll" => $srcAll,
				"objects" => $oAll,
			];
		}
	}

	/**
	 * Default controller action
	 * @return Core_Response
	 */
	public function showAction() {
		$aResult = array('status' => 'ERROR', 'message' => 'неизвестная ошибка');
		$oCore_Response = new Core_Response();
		$oSite = Core_Entity::factory('Site')->getById(CURRENT_SITE);

		switch ($this->operation) {
			case 'listsmall':
				$srcItem = Core_Entity::factory('Shop_Item')->getById($this->srcItemId);
				if( isset($srcItem->id) && $srcItem->id>0) {
					$prefix = "";
					if(isset($this->wpref)) {
						$prefix = "/{$this->wpref}x{$this->hpref}";
					}
					$tprefix = "";
					if(isset($this->tprefix)) {
						$tprefix = $this->tprefix;
					}
					$srcItems = self::getGalleryListByShopItem($srcItem->id, $prefix, $tprefix);

					$aResult = [
						  'status' => 'success'
						, 'message' => ''
						, "src" => $srcItems['src']
						, "srcAll" => $srcItems['srcAll']
						, "left" => count($srcItems['srcAll'])
					];
				} else {
					$aResult['message'] = 'Не найдена фотогалерея';
				}
				break;
		}

		$oCore_Response
			->status(200)
			->header('Content-Type', "application/json; charset={$oSite->coding}")
			->header('Last-Modified', gmdate('D, d M Y H:i:s', time()) . ' GMT')
			->body(json_encode($aResult))
		;
		Core_Event::notify(get_class($this) . '.onAfterShowAction', $this, array($oCore_Response));
		return $oCore_Response;
	}
}
