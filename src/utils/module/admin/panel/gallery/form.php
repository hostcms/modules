<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Admin forms.
 * Типовой контроллер редактирования
 *
 * @package HostCMS 6\Admin
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2014 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */

//class Utils_Gallery_Adminform extends Admin_Form_Action_Controller
class Utils_Admin_Panel_Gallery_Form extends Admin_Form_Action_Controller
{
	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'title', // Form Title
		'skipColumns' // Array of skipped columns
	);

	/**
	 * Model's key list
	 * @var array
	 */
	protected $_keys = array();

	/**
	 * Form's ID
	 * @var string
	 */
	protected $_formId = 'formEdit';

	/**
	 * Stores POST, which can change the controller
	 * @var mixed
	 */
	protected $_formValues = NULL;

	/**
	 * Constructor.
	 * @param Admin_Form_Action_Model $oAdmin_Form_Action action
	 */
	public function __construct(Admin_Form_Action_Model $oAdmin_Form_Action)
	{
		parent::__construct($oAdmin_Form_Action);

		// Set default title
		$oAdmin_Word = $this->_Admin_Form_Action->Admin_Word->getWordByLanguage(
			Core_Entity::factory('Admin_Language')->getCurrent()->id
		);
		$this->title = is_object($oAdmin_Word) ? $oAdmin_Word->name : 'undefined';

		// Пропускаемые свойства модели
		$this->skipColumns = array(
			'user_id',
			'deleted'
		);

		$this->skipColumns = array_combine($this->skipColumns, $this->skipColumns);

		// Далее может быть изменено
		$this->_formValues = $_POST;
	}

	/**
	 * Add skiping column
	 * @param string $column column name
	 * @return self
	 */
	public function addSkipColumn($column)
	{
		$this->skipColumns += array($column => $column);
		return $this;
	}

	/**
	 * Get model's key list
	 * Получение списка ключей модели (PK и FK)
	 * @return self
	 */
	protected function _loadKeys()
	{
		// Массив ключей, которые будут выводиться на дополнительной вкладке
		$this->_keys = array(
			$this->_object->getPrimaryKeyName()
		);

		$aRelations = $this->_object->getRelations();

		foreach ($aRelations as $relation)
		{
			$this->_keys[] = $relation['foreign_key'];
		}

		if (!empty($this->_keys))
		{
			$this->_keys = array_combine($this->_keys, $this->_keys);
		}

		return $this;
	}

	/**
	 * Form fields
	 * @var array
	 */
	protected $_fields = array();

	/**
	 * Form tabs
	 * @var array
	 */
	protected $_tabs = array();

	/**
	 * Get all ordinary fields, created by table's fileds
	 * @return array
	 */
	public function getFields()
	{
		return $this->_fields;
	}

	/**
	 * Get form field by name
	 * @param string $fieldName name
	 * @return Admin_Form_Entity
	 */
	public function getField($fieldName)
	{
		if (isset($this->_fields[$fieldName]))
		{
			return $this->_fields[$fieldName];
		}

		throw new Core_Exception("Field %fieldName does not exist.", array('%fieldName' => $fieldName));
	}

	/**
	 * Add field
	 * @param Admin_Form_Entity $oAdmin_Form_Entity field
	 * @return self
	 */
	public function addField(Admin_Form_Entity $oAdmin_Form_Entity)
	{
		$this->_fields[$oAdmin_Form_Entity->name] = $oAdmin_Form_Entity;
		return $this;
	}

	/**
	 * Add new tab into form
	 * @param Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab new tab
	 * @return self
	 */
	public function addTab(Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab)
	{
		$this->_tabs[$oAdmin_Form_Entity_Tab->name] = $oAdmin_Form_Entity_Tab;
		return $this;
	}

	/**
	 * Load object's fields when object has been set
	 * После установки объекта загружаются данные о его полях
	 * @param object $object
	 * @return Admin_Form_Action_Controller_Type_Edit
	 * @hostcms-event Admin_Form_Action_Controller_Type_Edit.onBeforeSetObject
	 * @hostcms-event Admin_Form_Action_Controller_Type_Edit.onAfterSetObject
	 */
	public function setObject($object)
	{
		parent::setObject($object);
		$this->_loadKeys();

		// Получение списка полей объекта
		$aColumns = $this->_object->getTableColums();

		// Список закладок
		// Основная закладка
		$oAdmin_Form_Tab_EntityMain = Admin_Form_Entity::factory('Tab')
			->caption(Core::_('admin_form.form_forms_tab_1'))
			->name('main');

		$this->addTab($oAdmin_Form_Tab_EntityMain);

		//if (!is_null($this->_object->id))
		//{
		// Дополнительные (ключи)
		$oAdmin_Form_Tab_EntityAdditional = Admin_Form_Entity::factory('Tab')
			->caption(Core::_('admin_form.form_forms_tab_2'))
			->name('additional');

		$oUser = Core_Entity::factory('User')->getCurrent();

		!$oUser->superuser && $oAdmin_Form_Tab_EntityAdditional->active(FALSE);

		$this->addTab($oAdmin_Form_Tab_EntityAdditional);
		//}

		$modelName = $this->_object->getModelName();
		$primaryKeyName = $this->_object->getPrimaryKeyName();

		foreach ($aColumns as $columnName => $columnArray)
		{
			if (!isset($this->skipColumns[$columnName]))
			{
				$sTabName = isset($this->_keys[$columnName])
					? 'additional'
					: 'main';

				switch ($columnArray['datatype'])
				{
					case 'datetime':
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('DateTime');

						/*$date = ($this->_object->$columnName == '0000-00-00 00:00:00')
							? $this->_object->$columnName
							: Core_Date::sql2datetime($this->_object->$columnName);*/

						$oAdmin_Form_Entity_For_Column
							->value(
							//Core_Date::sql2datetime($this->_object->$columnName)
								$this->_object->$columnName
							);

						break;
					case 'date':
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Date');

						$oAdmin_Form_Entity_For_Column
							->value(
								$this->_object->$columnName
							);

						break;
					case 'tinytext':
					case 'text':
					case 'mediumtext':
					case 'longtext':
					case 'tinyblob':
					case 'blob':
					case 'mediumblob':
					case 'longblob':
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Textarea');

						$oAdmin_Form_Entity_For_Column
							->value(
								$this->_object->$columnName
							);
						break;
					case 'tinyint':
					case 'tinyint unsigned':
						// Только при длине 1 символ
						if ($columnArray['max_length'] == 1)
						{
							$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Checkbox');

							$oAdmin_Form_Entity_For_Column
								->value(
									$this->_object->$columnName
								);
							break;
						}
					default:
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Input');

						$oAdmin_Form_Entity_For_Column
							//->size(12) // изменить на расчет
							->value($this->_object->$columnName);

						if ($sTabName == 'main'
							&& $this->_tabs[$sTabName]->getCountChildren() == 0)
						{
							$oAdmin_Form_Entity_For_Column->class('large');
						}

						$columnName == 'id' && $oAdmin_Form_Entity_For_Column->readonly('readonly');

						break;
				}

				$format = array();

				// Найден формат по названию столбца
				if (!is_null($oAdmin_Form_Entity_For_Column->getFormat($columnName)))
				{
					$format += array('lib' => array('value' => $columnName));
				}

				switch ($columnArray['type'])
				{
					case 'string':
						if (!is_null($columnArray['max_length']))
						{
							$format += array('maxlen' =>
							// ограничение длины поля
								array('value' => $columnArray['max_length'])
							);
						}

						if (is_null($columnArray['default']) && !$columnArray['null'])
						{
							$format += array('minlen' =>
							// ограничение длины поля
								array('value' => 1)
							);
						}
						break;
					case 'int':
						$format += array('lib' => array(
							'value' => 'integer'
						));
						// В ограничение значений
						// $columnArray['min']
						// $columnArray['max']
						break;
				}

				if (!empty($format))
				{
					$oAdmin_Form_Entity_For_Column
						->format($format);
				}

				$oAdmin_Form_Entity_For_Column
					->name($columnName)
					->caption(Core::_($modelName . '.' . $columnName));

				// На дополнительную или основную вкладку
				if (!(is_null($this->_object->getPrimaryKey()) && $columnName == $primaryKeyName))
				{
					$this->_tabs[$sTabName]->add(
						$oAdmin_Form_Entity_For_Column
					);
				}

				$this->addField($oAdmin_Form_Entity_For_Column);
			}
		}

		Core_Event::notify(get_class($this) . '.onAfterSetObject', $this, array($object, $this->_Admin_Form_Controller));

		return $this;
	}

	/**
	 * Executes the business logic.
	 * @param mixed $operation Operation for action
	 * @return boolean
	 */
	public function execute($operation = NULL)
	{
		switch ($operation)
		{
			case 'save':
				$return = TRUE; // Показываем форму
				break;
			default:
				$aChecked = $this->_Admin_Form_Controller->getChecked();
				$this->_showEditForm();
				$this->addMessage(ob_get_clean());
				$return = TRUE; // Показываем форму
				break;
		}
		return $return;
	}


	/**
	 * Processing of the form. Apply object fields.
	 * @return self
	 */
	protected function _applyObjectProperty()
	{
		$changedItems = array();
		foreach($this->_formValues['items'] as $itemID => $item) {
			$newValue = 0;
			if( (isset($item['irr']) && isset($item['irr']['checked']) && $item['irr']['checked']!=$item['irr']['old']) ) {
				$changedItems[$itemID] = 1;
			}
			if( (isset($item['irr']) && (!isset($item['irr']['checked']) && $item['irr']['old']==1) )
			) {
				$changedItems[$itemID] = 0;
			}
		}
		foreach($changedItems as $entityID => $changedItem) {
			CUtil::setIntPropertyById($entityID, 179, $changedItem);
		}
		Core_Message::show("Операция выполнена успешно");

		return $this;
	}

	/**
	 * Show edit form
	 * @return boolean
	 */
	protected function _showEditForm()
	{
		ob_start();
		$aChecked = $this->_Admin_Form_Controller->getChecked();

		$oShop = $this->_object->shop;
		$sFormPath = $this->_Admin_Form_Controller->getPath();
		$windowId = $this->_Admin_Form_Controller->getWindowId();

		$oImageFormField = Core::factory('Core_Html_Entity_Div')
			->class('form-group col-lg-6 col-md-6 col-sm-12 col-xs-12')
		;

		$oImageFormField->add(
			Core::factory('Core_Html_Entity_Form')
				->id("main-image-form")
				->name("main_image_form")
				->method('post')
				->enctype('multipart/form-data')
				->class('mainimage')
				->add(
					Core::factory('Core_Html_Entity_H3')
						->class("before-pink")
						->value("Основное изображение")
				)
				->add( $this->_addButtonSave() )
				->add(
					Core::factory('Core_Html_Entity_Div')
						->class('row')
						->add( $oImageField = Admin_Form_Entity::factory('File') )
//						->add(
//							Admin_Form_Entity::factory('Button')
//								->name('saveImage')
//								->class('saveButton btn btn-palegreen')
//								->value('Выделить')
//						)
				)
		);
		$oImageField->controller($this->_Admin_Form_Controller);
		$oLargeFilePath = is_file($this->_object->getLargeFilePath())
			? $this->_object->getLargeFileHref()
			: '';

		$oSmallFilePath = is_file($this->_object->getSmallFilePath())
			? $this->_object->getSmallFileHref()
			: '';

		$oImageField
			->name("image")
			->id("image")
			->largeImage(array('max_width' => $oShop->image_large_max_width, 'max_height' => $oShop->image_large_max_height, 'path' => $oLargeFilePath, 'show_params' => TRUE, 'watermark_position_x' => $oShop->watermark_default_position_x, 'watermark_position_y' => $oShop->watermark_default_position_y, 'place_watermark_checkbox_checked' => $oShop->watermark_default_use_large_image, 'delete_onclick' =>
					"$.adminLoad({path: '{$sFormPath}', additionalParams:
							'hostcms[checked][{$this->_datasetId}][{$this->_object->id}]=1', action: 'deleteLargeImage', windowId: '{$windowId}'}); return false", 'caption' => Core::_('Shop_Item.items_catalog_image'), 'preserve_aspect_ratio_checkbox_checked' => $oShop->preserve_aspect_ratio)
			)
			->smallImage(array('max_width' => $oShop->image_small_max_width, 'max_height' => $oShop->image_small_max_height, 'path' => $oSmallFilePath, 'create_small_image_from_large_checked' =>
					$this->_object->image_small == '', 'place_watermark_checkbox_checked' =>
					$oShop->watermark_default_use_small_image, 'delete_onclick' => "$.adminLoad({path: '{$sFormPath}', additionalParams:
							'hostcms[checked][{$this->_datasetId}][{$this->_object->id}]=1', action: 'deleteSmallImage', windowId: '{$windowId}'}); return false", 'caption' => Core::_('Shop_Item.items_catalog_image_small'), 'show_params' => TRUE, 'preserve_aspect_ratio_checkbox_checked' => $oShop->preserve_aspect_ratio_small)
			)
		;
//		utl::p($oImageField);

		$dzFormPhotos = $this->dropZone('photos', 'Фотогалерея для товара');
		$dzFormFiles = $this->dropZone('files', 'Файлы', 'files');
		$dzFormDopImages = $this->dropZone('images', 'Дополнительные изображения', 'images');


		$oXslPanel = Core::factory('Core_Html_Entity_Div')
			->class('page-body')
			->add(
				Core::factory('Core_Html_Entity_H5')
					->class("row-title before-pink")
					->value('Фотогалереи "'.$this->_object->name.'"')
			)
			->add(
				Core::factory('Core_Html_Entity_Div')
					->class('row')
					->add(
						Core::factory('Core_Html_Entity_Div')
							->class('col-md-12')
							->add(
								Core::factory('Core_Html_Entity_Div')
									->class('widget')
									->add(
										$addDropZones =
										Core::factory('Core_Html_Entity_Div')
											->class('row widget-body')
											->add($oImageFormField)
											->add($dzFormPhotos)
											->add($dzFormDopImages)
											->add($dzFormFiles)
									)
							)
					)
			)
//			->add(
//				Core::factory('Core_Html_Entity_Code')
//					->value('<link type="text/css" href="/modules/utils/js/dropzone/css/basic.css?'.microtime().'" rel="stylesheet">')
//			)
//			->add(
//				Core::factory('Core_Html_Entity_Code')
//					->value('<link type="text/css" href="/modules/utils/js/dropzone/css/dropzone.css?'.microtime().'" rel="stylesheet">')
//			)
//			->add(
//				Core::factory('Core_Html_Entity_Code')
//					->value('<link type="text/css" href="/modules/utils/css/galleries.css?'.microtime().'" rel="stylesheet">')
//			)
//			->add(
//				Core::factory('Core_Html_Entity_Script')
//					->src('/modules/utils/js/dropzone/dropzone.min.js?'.microtime())
//			)
//			->add(
//				Core::factory('Core_Html_Entity_Script')
//					->src('/modules/utils/js/gallery.js?'.microtime())
//			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->value('if(typeof(Dropzone)!==\'undefined\')
								Dropzone.autoDiscover = false;
								initGalleries();')
			)
		;

		$oProperties = Core_Entity::factory('Property');
		$oProperties->queryBuilder()
			->join(array('property_dirs','pd'), 'pd.id', '=', 'properties.property_dir_id')
		->where('pd.name','=','GalleryModule');

		$aProp = $oProperties->findAll();
		foreach ($aProp as $pr){
			$addDropZones->add($this->dropZone('prop_img_' . $pr->id, $pr->name , 'prop_img_' . $pr->id));
		}

		Core_Skin::instance()->addCss('/modules/utils/js/dropzone/css/basic.css');
		Core_Skin::instance()->addCss('/modules/utils/js/dropzone/css/dropzone.css');
		Core_Skin::instance()->addCss('/modules/utils/css/galleries.css');
		Core_Skin::instance()->addJs('/modules/utils/js/gallery.js');

		// Форма добавляется к контроллеру
//		$oXslPanel = Core::factory('Core_Html_Entity_Div')
//			->class('page-body')
//			->add(
//				Core::factory('Core_Html_Entity_H5')
//					->class("row-title before-pink")
//					->value('Фотогалереи "'.$this->_object->name.'"')
//			)
//			->add(Core::factory('Core_Html_Entity_Div')->class('dropzone_clear'))
//			->add($dzFormPhotos)
//			->add($dzFormIrr)
//			->add($dzFormFiles)
//			->add(
//				Core::factory('Core_Html_Entity_Script')
//					->src("/libs/extended/galleries/galleryeditor.js")
//					->value('$galleryItemId = '.$this->_object->id.';')
//			)
//		;
		$oXslPanel->execute();
		$this->addContent(
			ob_get_clean()
		);

		return TRUE;
	}

	protected function dropZone($id='dropzone', $title='DropZone', $type='photos') {
		// Заголовок формы добавляется до вывода крошек, которые могут быть добавлены в контроллере
		$oAdmin_Form_Entity_Form_Div = Core::factory('Core_Html_Entity_Div')
			->class('form-group col-lg-6 col-md-6 col-sm-12 col-xs-12')
			->add(
				$oAdmin_Form_Entity_Form = Core::factory('Core_Html_Entity_Form')
					->id($id)
					->name("Form_".$type)
					->method('post')
					->enctype('multipart/form-data')
					->class('dz-utils dropzone')
			)
		;
//		$oAdmin_Form_Entity_Form = Core::factory('Core_Html_Entity_Form')
//			->id($id)
//			->name("Form_".$type)
//			->method('post')
//			->enctype('multipart/form-data')
//			->class('dropzone');

		$oAdmin_Form_Entity_Form
			->add(
				Core::factory('Core_Html_Entity_H3')
					->class("before-pink")
					->value($title)
			)
			->add(
				$this->_addButtons()
			);
		$fallbackLayer = Core::factory('Core_Html_Entity_Div');
		$fallbackLayer
			->class('fallback')
			->add( Core::factory('Core_Html_Entity_Code')->value('<input name="file".$id type="file" multiple="multiple" />') )
		;
		$oAdmin_Form_Entity_Form
			->add( Core::factory('Core_Html_Entity_Input')->type('hidden')->name('operation')->value('addItem') )
			->add( Core::factory('Core_Html_Entity_Input')->type('hidden')->name('galleryType')->value($type) )
			->add( Core::factory('Core_Html_Entity_Input')->type('hidden')->name('itemID')->value($this->_object->id) )
			->add( $fallbackLayer )
		;
		return $oAdmin_Form_Entity_Form_Div;
	}

	/**
	 * Add save and apply buttons
	 * @return Admin_Form_Entity_Buttons
	 */
	protected function _addButtons()
	{
		// Кнопки
		$oAdmin_Form_Entity_Buttons = Admin_Form_Entity::factory('Buttons');
		$oAdmin_Form_Entity_Buttons->controller($this->_Admin_Form_Controller);

		$oAdmin_Form_Entity_Button_Mark = Admin_Form_Entity::factory('Button')
			->name('markAll')
			->class('saveButton dz-button btn btn-palegreen')
			->value('Выделить')
		;
		$oAdmin_Form_Entity_Button_UnMark = Admin_Form_Entity::factory('Button')
			->name('unMarkAll')
			->class('saveButton dz-button btn btn-warning')
			->value('Снять')
		;
		$oAdmin_Form_Entity_Button_Remove = Admin_Form_Entity::factory('Button')
			->name('deleteMarked')
			->class('applyButton dz-button btn btn-danger')
			->value('Удалить')
		;
		$oAdmin_Form_Entity_Buttons
			->controller($this->_Admin_Form_Controller)
			->add($oAdmin_Form_Entity_Button_Mark)
			->add($oAdmin_Form_Entity_Button_UnMark)
			->add($oAdmin_Form_Entity_Button_Remove)
		;

		return $oAdmin_Form_Entity_Buttons;
	}

	/**
	 * Add save and apply buttons
	 * @return Admin_Form_Entity_Buttons
	 */
	protected function _addButtonSave()
	{
		// Кнопки
		$oAdmin_Form_Entity_Buttons = Admin_Form_Entity::factory('Buttons');
		$oAdmin_Form_Entity_Buttons->controller($this->_Admin_Form_Controller);

		$idAttr = 'data-item-id';
		$oAdmin_Form_Entity_Button_Save = Admin_Form_Entity::factory('Button')
			->name('saveMainImages')
			->class('saveButton dz-button btn btn-palegreen')
			->value('Сохранить')
			->addAllowedProperty($idAttr)
		;
		$oAdmin_Form_Entity_Button_Save->$idAttr = $this->_object->id;
		$oAdmin_Form_Entity_Buttons
			->add($oAdmin_Form_Entity_Button_Save)
		;

		return $oAdmin_Form_Entity_Buttons;
	}
}