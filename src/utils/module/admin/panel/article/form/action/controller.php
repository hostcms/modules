<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Admin forms.
 * Типовой контроллер редактирования
 *
 * @package HostCMS 6\Admin
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2014 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
//class Utils_Article_Admin_Form_Action_Controller extends Admin_Form_Action_Controller
class Utils_Admin_Panel_Article_Form_Action_Controller extends Admin_Form_Action_Controller
{
	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'title', // Form Title
		'skipColumns' // Array of skipped columns
	);

	/**
	 * Model's key list
	 * @var array
	 */
	protected $_keys = array();

	/**
	 * Form's ID
	 * @var string
	 */
	protected $_formId = 'formEdit';

	/**
	 * Stores POST, which can change the controller
	 * @var mixed
	 */
	protected $_formValues = NULL;

	/**
	 * Constructor.
	 * @param Admin_Form_Action_Model $oAdmin_Form_Action action
	 */
	public function __construct(Admin_Form_Action_Model $oAdmin_Form_Action)
	{
		parent::__construct($oAdmin_Form_Action);

		// Set default title
		$oAdmin_Word = $this->_Admin_Form_Action->Admin_Word->getWordByLanguage(
			Core_Entity::factory('Admin_Language')->getCurrent()->id
		);
		$this->title = is_object($oAdmin_Word) ? $oAdmin_Word->name : 'undefined';

		// Пропускаемые свойства модели
		$this->skipColumns = array(
			'user_id',
			'deleted'
		);

		$this->skipColumns = array_combine($this->skipColumns, $this->skipColumns);

		// Далее может быть изменено
		$this->_formValues = $_POST;
	}

	/**
	 * Add skiping column
	 * @param string $column column name
	 * @return self
	 */
	public function addSkipColumn($column)
	{
		$this->skipColumns += array($column => $column);
		return $this;
	}

	/**
	 * Get model's key list
	 * Получение списка ключей модели (PK и FK)
	 * @return self
	 */
	protected function _loadKeys()
	{
		// Массив ключей, которые будут выводиться на дополнительной вкладке
		$this->_keys = array(
			$this->_object->getPrimaryKeyName()
		);

		$aRelations = $this->_object->getRelations();

		foreach ($aRelations as $relation)
		{
			$this->_keys[] = $relation['foreign_key'];
		}

		if (!empty($this->_keys))
		{
			$this->_keys = array_combine($this->_keys, $this->_keys);
		}

		return $this;
	}

	/**
	 * Form fields
	 * @var array
	 */
	protected $_fields = array();

	/**
	 * Form tabs
	 * @var array
	 */
	protected $_tabs = array();

	/**
	 * Add new tab into form
	 * @param Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab new tab
	 * @return self
	 */
	public function addTab(Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab)
	{
		$this->_tabs[$oAdmin_Form_Entity_Tab->name] = $oAdmin_Form_Entity_Tab;
		return $this;
	}

	/**
	 * Add new tab into form before $oAdmin_Form_Entity_Tab_Before
	 * @param Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab new tab
	 * @param Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab_Before old tab
	 * @return self
	 */
	public function addTabBefore(Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab, Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab_Before)
	{
		// Find key for before object
		$key = array_search($oAdmin_Form_Entity_Tab_Before, $this->_tabs, $strict = TRUE);

		if ($key !== FALSE)
		{
			$aArrayKeys = array_keys($this->_tabs);
			// Порядковый номер для найденного символьного ключа
			$key = array_search($key, $aArrayKeys, TRUE);

			array_splice($this->_tabs, $key, 0, array($oAdmin_Form_Entity_Tab->name => $oAdmin_Form_Entity_Tab));
			return $this;
		}

		throw new Core_Exception("Before adding tab does not exist.");
	}

	/**
	 * Add new tab into form after $oAdmin_Form_Entity_Tab_After
	 * @param Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab new tab
	 * @param Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab_After old tab
	 * @return self
	 */
	public function addTabAfter(Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab, Skin_Default_Admin_Form_Entity_Tab $oAdmin_Form_Entity_Tab_After)
	{
		// Find key for after object
		$key = array_search($oAdmin_Form_Entity_Tab_After, $this->_tabs, $strict = FALSE);

		if ($key !== FALSE)
		{
			$aArrayKeys = array_keys($this->_tabs);
			// Порядковый номер для найденного символьного ключа
			$key = array_search($key, $aArrayKeys, TRUE);

			array_splice($this->_tabs, $key + 1, 0, array($oAdmin_Form_Entity_Tab->name => $oAdmin_Form_Entity_Tab));
			return $this;
		}

		throw new Core_Exception("After adding tab does not exist.");
	}

	/**
	 * Get all ordinary fields, created by table's fileds
	 * @return array
	 */
	public function getFields()
	{
		return $this->_fields;
	}

	/**
	 * Get all tabs
	 * @return array
	 */
	public function getTabs()
	{
		return $this->_tabs;
	}

	/**
	 * Check is tab isset
	 * @param string $tabName tab name
	 * @return boolean
	 */
	public function issetTab($tabName)
	{
		foreach ($this->_tabs as $oTab)
		{
			if ($oTab->name == $tabName)
			{
				return TRUE;
			}
		}

		return FALSE;
		//return isset($this->_tabs[$tabName]);
	}

	/**
	 * Get tab
	 * @param string $tabName
	 * @return Admin_Form_Entity_Tab
	 */
	public function getTab($tabName)
	{
		/*if (isset($this->_tabs[$tabName]))
		{
			return $this->_tabs[$tabName];
		}*/
		foreach ($this->_tabs as $oTab)
		{
			if ($oTab->name == $tabName)
			{
				return $oTab;
			}
		}

		throw new Core_Exception("Tab %tab does not exist.", array('%tab' => $tabName));
	}

	/**
	 * Get form field by name
	 * @param string $fieldName name
	 * @return Admin_Form_Entity
	 */
	public function getField($fieldName)
	{
		if (isset($this->_fields[$fieldName]))
		{
			return $this->_fields[$fieldName];
		}

		throw new Core_Exception("Field %fieldName does not exist.", array('%fieldName' => $fieldName));
	}

	/**
	 * Add field
	 * @param Admin_Form_Entity $oAdmin_Form_Entity field
	 * @return self
	 */
	public function addField(Admin_Form_Entity $oAdmin_Form_Entity)
	{
		$this->_fields[$oAdmin_Form_Entity->name] = $oAdmin_Form_Entity;
		return $this;
	}

	/**
	 * Load object's fields when object has been set
	 * После установки объекта загружаются данные о его полях
	 * @param object $object
	 * @return Admin_Form_Action_Controller_Type_Edit
	 * @hostcms-event Admin_Form_Action_Controller_Type_Edit.onBeforeSetObject
	 * @hostcms-event Admin_Form_Action_Controller_Type_Edit.onAfterSetObject
	 */
	public function setObject($object)
	{
		Core_Event::notify(get_class($this) . '.onBeforeSetObject', $this, array($object, $this->_Admin_Form_Controller));

		parent::setObject($object);

		$this->_loadKeys();

		// Получение списка полей объекта
		$aColumns = $this->_object->getTableColums();

		// Список закладок
		// Основная закладка
		$oAdmin_Form_Tab_EntityMain = Admin_Form_Entity::factory('Tab')
			->caption(Core::_('admin_form.form_forms_tab_1'))
			->name('main');

		$this->addTab($oAdmin_Form_Tab_EntityMain);

		//if (!is_null($this->_object->id))
		//{
		// Дополнительные (ключи)
		$oAdmin_Form_Tab_EntityAdditional = Admin_Form_Entity::factory('Tab')
			->caption(Core::_('admin_form.form_forms_tab_2'))
			->name('additional');

		$oUser = Core_Entity::factory('User')->getCurrent();

		!$oUser->superuser && $oAdmin_Form_Tab_EntityAdditional->active(FALSE);

		$this->addTab($oAdmin_Form_Tab_EntityAdditional);
		//}

		$modelName = $this->_object->getModelName();
		$primaryKeyName = $this->_object->getPrimaryKeyName();

		foreach ($aColumns as $columnName => $columnArray)
		{
			if (!isset($this->skipColumns[$columnName]))
			{
				$sTabName = isset($this->_keys[$columnName])
					? 'additional'
					: 'main';

				switch ($columnArray['datatype'])
				{
					case 'datetime':
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('DateTime');

						$oAdmin_Form_Entity_For_Column
							->value(
								$this->_object->$columnName
							);

						break;
					case 'date':
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Date');

						$oAdmin_Form_Entity_For_Column
							->value(
								$this->_object->$columnName
							);

						break;
					case 'tinytext':
					case 'text':
					case 'mediumtext':
					case 'longtext':
					case 'tinyblob':
					case 'blob':
					case 'mediumblob':
					case 'longblob':
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Textarea');

						$oAdmin_Form_Entity_For_Column
							->value(
								$this->_object->$columnName
							);
						break;
					case 'tinyint':
					case 'tinyint unsigned':
						// Только при длине 1 символ
						if ($columnArray['max_length'] == 1)
						{
							$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Checkbox');

							$oAdmin_Form_Entity_For_Column
								->value(
									$this->_object->$columnName
								);
							break;
						}
					default:
						$oAdmin_Form_Entity_For_Column = Admin_Form_Entity::factory('Input');

						$oAdmin_Form_Entity_For_Column
							//->size(12) // изменить на расчет
							->value($this->_object->$columnName);

						if ($sTabName == 'main'
							&& $this->_tabs[$sTabName]->getCountChildren() == 0)
						{
							$oAdmin_Form_Entity_For_Column->class('large');
						}

						$columnName == 'id' && $oAdmin_Form_Entity_For_Column->readonly('readonly');

						break;
				}

				$format = array();

				// Найден формат по названию столбца
				if (!is_null($oAdmin_Form_Entity_For_Column->getFormat($columnName)))
				{
					$format += array('lib' => array('value' => $columnName));
				}

				switch ($columnArray['type'])
				{
					case 'string':
						if (!is_null($columnArray['max_length']))
						{
							$format += array('maxlen' =>
							// ограничение длины поля
								array('value' => $columnArray['max_length'])
							);
						}

						if (is_null($columnArray['default']) && !$columnArray['null'])
						{
							$format += array('minlen' =>
							// ограничение длины поля
								array('value' => 1)
							);
						}
						break;
					case 'int':
						$format += array('lib' => array(
							'value' => 'integer'
						));
						// В ограничение значений
						// $columnArray['min']
						// $columnArray['max']
						break;
				}

				if (!empty($format))
				{
					$oAdmin_Form_Entity_For_Column
						->format($format);
				}

				$oAdmin_Form_Entity_For_Column
					->name($columnName)
					->caption(Core::_($modelName . '.' . $columnName));

				// На дополнительную или основную вкладку
				if (!(is_null($this->_object->getPrimaryKey()) && $columnName == $primaryKeyName))
				{
					$this->_tabs[$sTabName]->add(
						$oAdmin_Form_Entity_For_Column
					);
				}

				$this->addField($oAdmin_Form_Entity_For_Column);
			}
		}

		Core_Event::notify(get_class($this) . '.onAfterSetObject', $this, array($object, $this->_Admin_Form_Controller));

		return $this;
	}

	/**
	 * Executes the business logic.
	 * @param mixed $operation Operation for action
	 * @return boolean
	 */
	public function execute($operation = NULL)
	{
//		echo $this;
		switch ($operation)
		{
			case 'save':
				$this->_applyObjectProperty();
				$windowId = $this->_Admin_Form_Controller->getWindowId();
				?>
				<script type="text/javascript">
					$('.ui-icon.ui-icon-closethick').click();
					location.reload();
				</script>
				<?php
				$this->addMessage(ob_get_clean());

				$return = TRUE; // Показываем форму
				break;
			default:
				$aChecked = $this->_Admin_Form_Controller->getChecked();
				$this->_showEditForm();
				/* ?>
					<script type="text/javascript">
						$(".hostcmsWindow").parent().addClass('hostcms6');
						initArtTreeview( $('.selectedCategoryTree'), 'arts=1', <?= key($aChecked[0]) ?> );
					</script>
				<?php */
				$return = TRUE; // Показываем форму
				break;
		}
		return $return;
	}


	/**
	 * Processing of the form. Apply object fields.
	 * @return self
	 */
	protected function _applyObjectProperty()
	{
		return $this;
	}

	/**
	 * Show edit form
	 * @return boolean
	 */
	protected function _showEditForm()
	{
		$isArticles = Core_Entity::factory('Informationsystem')->getByName('СтатьиДляМагазина.'.$this->_object->shop_id);

		ob_start();
		$oXslPanel = Core::factory('Core_Html_Entity_Div')
			->add(
				Core::factory('Core_Html_Entity_Code')
					->value('<link type="text/css" href="/modules/utils/js/jQuery-fancyTree/src/skin-win7/ui.fancytree.css?'.microtime().'" rel="stylesheet">')
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->src('/modules/skin/default/js/default-hostcms.js?'.microtime())
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->src('/hostcmsfiles/main.js?'.microtime())
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->src('/modules/utils/js/jQuery-fancyTree/src/jquery.fancytree.js?'.microtime())
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->src('/modules/utils/js/jQuery-fancyTree/src/jquery.fancytree.table.js?'.microtime())
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->src('/modules/utils/js/jQuery-fancyTree/src/jquery.fancytree.gridnav.js?'.microtime())
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->src('/modules/utils/js/jQuery-fancyTree/src/jquery.fancytree.edit.js?'.microtime())
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->src("/modules/utils/js/artedit.js")
//					->value('$techItemId = '.$this->_object->id.';')
			)
			->add(
				Core::factory('Core_Html_Entity_Script')
					->value('	$(document).ready(function(){
									var itemId = '.$this->_object->id.',
									isarticlesId = '.$isArticles->id.';
									$techItemId = '.$this->_object->id.';
									initArtTreeview(\'arts=1\', itemId, isarticlesId );
								});')
			)

		;
		$oXslPanel->execute();

		// Заголовок формы добавляется до вывода крошек, которые могут быть добавлены в контроллере
		// Форма
		$oAdmin_Form_Entity_Form = new Admin_Form_Entity_Form(
			$this->_Admin_Form_Controller
		);

		$oAdmin_Form_Entity_Form
			->id($this->_formId)
			->class('page-body adminForm artEdit customFormController')
			->action(
				$this->_Admin_Form_Controller->getPath()
			);

//		// Закладки
//		$oAdmin_Form_Entity_Tabs = Admin_Form_Entity::factory('Tabs');
//		$oAdmin_Form_Entity_Tabs->formId($this->_formId);
//
//		// Все закладки к форме
//		$oAdmin_Form_Entity_Form->add(
//			$oAdmin_Form_Entity_Tabs
//		);
		$sectionTree = Admin_Form_Entity::factory('Section')
			->caption("Дерево элементов")
			->id('elementsTree')
			->add( Admin_Form_Entity::factory('Div')
					->add( Admin_Form_Entity::factory('Code')
							->html('<table class="selectedCategoryTree admin_table" cellpadding="2" cellspacing="2" style="width: 100%; margin-bottom: 15px">
							<thead>
								<tr class="admin_table_title">
									<th style="text-align: center; display: none"></th>
									<th style="text-align: center; display: none"></th>
									<th style="text-align: left">Наименование</th>
									<th style="width: 50px; display: none"></th>
								</tr>
							</thead>
							<tbody>
							</tbody>
						</table>')
					)
			)
		;
		$divTree = Admin_Form_Entity::factory('Div')
			->style( 'float: left; width: 45%; margin-right: 1%; display: block')
			->add($sectionTree);
		$contents = Admin_Form_Entity::factory('Section')
			->caption("Контент")
			->id('contents')
		;
		$aChecked = $this->_Admin_Form_Controller->getChecked();

		foreach($aChecked[0] as $keyChecked=>$valChecked) {
			$itemsContent = Admin_Form_Entity::factory('Div')
				->class("itemsContent");
		}
		$contents->add(Admin_Form_Entity::factory('Separator'));
		$divContents = Admin_Form_Entity::factory('Div')
			->style( 'float: left; width: 54%; display: block')
			->divAttr(array("data-section-content"=>1))
			->add($contents);


		$oAdmin_Form_Entity_Form
			->add( Admin_Form_Entity::factory('Input')->type('hidden')->name('itemID')->value($this->_object->id) )
			->add( Admin_Form_Entity::factory('Input')->type('hidden')->name('isarticlesid')->value(Core_Array::getGet('isarticlesid', -1)) )
			->add($divTree)
			->add($divContents)
		;
		// Форма добавляется к контроллеру
		$this->addEntity($oAdmin_Form_Entity_Form);

		foreach ($this->_children as $oAdmin_Form_Entity)
		{
			$oAdmin_Form_Entity->execute();
		}
		?><script type="text/javascript">$(document).ready(
			function(){
				$('body').addClass('artedit');
				$('.loading-container').remove();
//				$('#accordion_4').parent().removeClass('panel-group')
//				console.log('Mike');
			}
		)</script><?

		$this->addContent(
			ob_get_clean()
		);

		return TRUE;
	}

	/**
	 * Add save and apply buttons
	 * @return Admin_Form_Entity_Buttons
	 */
	protected function _addButtons()
	{
		// Кнопки
		$oAdmin_Form_Entity_Buttons = Admin_Form_Entity::factory('Buttons');
		$oAdmin_Form_Entity_Buttons->controller($this->_Admin_Form_Controller);

		// Кнопка Сохранить
		$oAdmin_Form_Entity_Button_Save = Admin_Form_Entity::factory('Button')
			->name('save')
			->class('saveButton btn btn-blue')
			->value(Core::_('admin_form.save'))
			->onclick(
				$this->_Admin_Form_Controller->getAdminSendForm(NULL, 'save')
			);

		$oAdmin_Form_Entity_Buttons
			->add($oAdmin_Form_Entity_Button_Save)
		;

		return $oAdmin_Form_Entity_Buttons;
	}
}
