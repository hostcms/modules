<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 05.03.2017
 * Time: 15:59
 */

class Utils_Admin_Panel_Article_Ajax_Controller extends Core_Command_Controller {

	/**
	 * Default controller action
	 * @return Core_Response
	 */
	public function showAction() {
		$aResult = array('status' => 'ERROR', 'message' => 'неизвестная ошибка');
		$oCore_Response = new Core_Response();
		$oSite = Core_Entity::factory('Site')->getById(CURRENT_SITE);

		switch ($this->operation) {
			case 'tree':
				$srcItem = Core_Entity::factory('Shop_Item')->getById($this->srcItemId);
				$is = Core_Entity::factory('Informationsystem')->getById($this->isAtricleId);
				if(!is_null($srcItem)) {
					utl::getGroupObjectByPath('/'.$srcItem->getPath(), $srcItem->shop->id, $this->isAtricleId, true);
					$results = utl::findInEntityByURL('Informationsystem', $this->isAtricleId, '/'.$srcItem->getPath(), false);
					$childs = array_values($results['shopGroups']);
					$childItems = array();
					foreach($childs as $child) {
						$childIsItems = array();
						$arIsItems = $child->informationsystem_items->findAll();
						foreach( $arIsItems as $childIsItem ) {
							$childIsItems[] = array(
								"id" => $childIsItem->id,
								"title" => $childIsItem->name,
								"folder" => false,
								"custom" => $childIsItem->toArray(),
							);
						}
						$childItem = array(
							"id" => $child->id,
							"title" => $child->name,
							"folder" => true,
							"expanded" => true,
							"type" => "group",
							"custom" => $child->toArray(),
						);
						if(count($childIsItems)>0) {
							$childItem['children'] = $childIsItems;
						}
						$childItems[] = $childItem;
					}
					$dataArray[] = array(
						"id" => $srcItem->id,
						"title" => $srcItem->name,
						"folder" => true,
						"expanded" => true,
						"type" => "group",
						"children" => $childItems,
						"custom" => $srcItem->toArray(),
					);
				}
				$aResult = $dataArray;
				break;
			case 'add':
				$mIsItem = Core_Entity::factory('Informationsystem_Item');
				$mIsItem->name = Core_Array::getRequest('name', '');
				$mIsItem->informationsystem_id = $this->isAtricleId;
				$mIsItem->description = "";
				$mIsItem->seo_title = "";
				$mIsItem->seo_description = "";
				$mIsItem->seo_keywords = "";
				$mIsItem->informationsystem_group_id = Core_Array::getRequest('group');
				$mIsItem->save();
				$aResult = array(
					'error' => 'Ok',
					'object' => $mIsItem->toArray(),
				);
				break;
			case 'readitem':
				$mIsItem = Core_Entity::factory('Informationsystem_Item')->getById($this->srcItemId);
				$dataArray['object'] = $mIsItem->toArray();

				$oAdminForm = Core_Entity::factory('Admin_Form')->getByGuid('1F0264AF-0129-1749-96DD-A661BB8ECB00');
				$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdminForm);
				$oAdmin_Form_Controller->window("editArtWindow");
				ob_start();
				$oAdmin_Form_Entity_Form = new Admin_Form_Entity_Form($oAdmin_Form_Controller);
				$oAdmin_Form_Entity_Form->id("editArtWindow");
				$oAdmin_Form_Entity_Form->addAllowedProperty('action');
				$oAdmin_Form_Entity_Form->action(str_replace('readitem', 'edititem', Core::parseUrl()['path']));

				$fileArt = Admin_Form_Entity::factory('File');
				$fileArt
					->type('file')
					->name('source')
					->id('source')
					->caption('Превью')
					->largeImage(
						array(
							'show_params' => FALSE,
						)
					)->smallImage(
						array('show' => FALSE)
					)
				;

				$nameArt = Admin_Form_Entity::factory('Input')
					->value($mIsItem->name)
					->caption('Наименование статьи')
				;
				$pathArt = Admin_Form_Entity::factory('Input')
					->value($mIsItem->path)
					->caption('URL статьи')
				;
				$imgArt = Admin_Form_Entity::factory('Code');
				$imgPath = ($mIsItem->image_small != '') ? $mIsItem->getSmallFileHref() : '/images/no-image.png';
				$imgArt->html("<img class='img_preview' src='".$imgPath."?".microtime(true)."' />");
				$imgPreview = Admin_Form_Entity::factory('Div')
					->class('form-group col-lg-12')
					->add($imgArt)
				;


				$textArt = Admin_Form_Entity::factory('Textarea')->value($mIsItem->text);
				$textArt->caption('Текст статьи');
				$textArt->wysiwyg(true);

				$oAdmin_Form_Entity_Buttons = Admin_Form_Entity::factory('Buttons');
				if(isset($this->_Admin_Form_Controller)) {
					$oAdmin_Form_Entity_Buttons->controller($this->_Admin_Form_Controller);
				}
				// Кнопка Сохранить
				$oAdmin_Form_Entity_Button_Save = Admin_Form_Entity::factory('Div')
					->class('form-group col-lg-12')
					->style('margin-top: 10px')
					->add(
						Admin_Form_Entity::factory('Code')
							->html('<input name="save" type="submit" value="Сохранить" class="saveButton btn btn-blue col-lg-3" id="field_save">')
					)
				;
				$oAdmin_Form_Entity_Buttons->add($oAdmin_Form_Entity_Button_Save);
				$oAdmin_Form_Entity_Buttons->add(Admin_Form_Entity::factory('Separator'));

				$oAdmin_Form_Entity_Form->add($nameArt);
				$oAdmin_Form_Entity_Form->add($pathArt);
				$oAdmin_Form_Entity_Form->add($fileArt);
				$oAdmin_Form_Entity_Form->add($imgPreview);
				$oAdmin_Form_Entity_Form->add($textArt);
				$oAdmin_Form_Entity_Form->add($oAdmin_Form_Entity_Buttons);
				$oAdmin_Form_Entity_Form->execute();
				$html = ob_get_clean();
				$aResult = array(
					'error' => 'Ok',
					'html' => $html,
					'object' => $mIsItem->toArray(),
				);
				break;
			case 'edititem':
				$mIsItem = Core_Entity::factory('Informationsystem_Item')->getById($this->srcItemId);
				$mIsItem->name = Core_Array::getRequest('field_id_1');
				$mIsItem->path = Core_Array::getRequest('field_id_2');
				$mIsItem->text = Core_Array::getRequest('field_id_3');
				$mIsItem->save();
//				utl::p($_FILES['source']['tmp_name']);
				if( count($_FILES)>0 ) {
					utl::setShopImage($mIsItem, $_FILES['source']['tmp_name'], $mIsItem->informationsystem);
				}
				$aResult = array(
					'error' => 'Ok',
					'file' => $_FILES,
					'object' => $mIsItem->toArray(),
				);
				break;
		}

		$oCore_Response
			->status(200)
			->header('Content-Type', "application/json; charset={$oSite->coding}")
			->header('Last-Modified', gmdate('D, d M Y H:i:s', time()) . ' GMT')
			->body(json_encode($aResult))
		;
		Core_Event::notify(get_class($this) . '.onAfterShowAction', $this, array($oCore_Response));
		return $oCore_Response;
	}
}
