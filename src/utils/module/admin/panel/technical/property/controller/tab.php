<?php
use Utils_Utl as utl;

class Utils_Admin_Panel_Technical_Property_Controller_Tab extends Skin_Bootstrap_Shop_Item_Property_Controller_Tab
{
	protected $_property_category_id=-1;
	private $_countInRow = 0;
	private $_countRow = 0;

	public function __construct(Admin_Form_Controller $Admin_Form_Controller, $property_dir_id=array(0))
	{
		parent::__construct($Admin_Form_Controller);
		$this->_property_category_id = $property_dir_id;
	}

	/**
	 * Show properties on tab
	 * @return self
	 */
	public function fillTab()
	{
		$aTmp_Property_Values = $this->_object->id
			? $this->_object->getPropertyValues(FALSE)
			: array();

		$this->_property_values = array();
		foreach ($aTmp_Property_Values as $oProperty_Value)
		{
			$this->_property_values[$oProperty_Value->property_id][] = $oProperty_Value;
		}

		foreach ($this->_property_category_id as $property_category) {
			$this->_setPropertyDir($property_category, $this->_tab);
		}
		return $this;
//		$shop_group_id = $this->_object->modification_id
//			? $this->_object->Modification->shop_group_id
//			: $this->_object->shop_group_id;
//
//		$linkedObject = Core_Entity::factory('Shop_Item_Property_List', $this->_object->shop_id);
//		$subDirs = array(-1);
//		if(is_array($this->_property_category_id)) {
//			$subDirs = array_merge($subDirs, $this->_property_category_id);
//			foreach($this->_property_category_id as $subparentDir) {
//				$aTmp = $this->_getAllSubDirs($subparentDir);
//				$subDirs = array_merge($subDirs, $aTmp);
//			}
//		} else {
//			$subDirs = $this->_getAllSubDirs($this->_property_category_id);
//		}
//		$aProperties = $linkedObject->getPropertiesForGroup($shop_group_id);
//		$needPropertyDirs = utl::getArrayValuesFromArrays($aProperties, 'property_dir_id', true);
//		if(is_array($needPropertyDirs) && count($needPropertyDirs)>0) {
//			foreach ($needPropertyDirs as $needPropertyDirID) {
//				if(array_search($needPropertyDirID, $subDirs)) {
//					$this->_setPropertyDir($needPropertyDirID, $this->_tab);
//				}
//			}
//		}
//		return $this;
	}

	protected function _setPropertyDir($parent_id = 0, $parentObject)
	{
		if(isset($parentObject->getChildren()[$this->_countRow])) {
			$divRowClass = 'form-group col-lg-3 col-md-3 col-sm-6 col-xs-12';
			$maxDivCountRows = 4;

			switch ($parentObject->name) {
				case 'additional':
//				case 'descriptions':
					$divRowClass = 'form-group col-lg-6 col-md-6 col-sm-12 col-xs-12';
					$maxDivCountRows = 2;
					break;
			}
			$oSection_Tehnic = Admin_Form_Entity::factory('Div')
				->class($divRowClass)
			;
			parent::_setPropertyDirs($parent_id, $oSection_Tehnic);

			if(count($oSection_Tehnic->getChildren())) {
				if($this->_countInRow==$maxDivCountRows) {
					$this->_countInRow=0;
					$this->_countRow++;
					$techSection = Admin_Form_Entity::factory('Div')
						->class('row minimized');
					$this->_tab->add($techSection);
				}
				$rowDiv = $parentObject->getChildren()[$this->_countRow];
				$rowDiv->add($oSection_Tehnic);
				$this->_countInRow++;
			}
		}
	}

	/**
	 * Get properties
	 * @return array
	 */
	protected function _getAllPropertiesDirs()
	{
		$shop_group_id = $this->_object->modification_id
			? $this->_object->Modification->shop_group_id
			: $this->_object->shop_group_id;

		$subDirs = array(-1);
		if(is_array($this->_property_category_id)) {
			$subDirs = array_merge($subDirs, $this->_property_category_id);
			foreach($this->_property_category_id as $subparentDir) {
				$aTmp = $this->_getAllSubDirs($subparentDir);
				$subDirs = array_merge($subDirs, $aTmp);
			}
		} else {
			$subDirs = $this->_getAllSubDirs($this->_property_category_id);
		}
		// Properties
		$oProperties = $this->linkedObject->Properties;
		$oProperties
			->queryBuilder()
			->join('shop_item_property_for_groups', 'shop_item_property_for_groups.shop_item_property_id', '=', 'shop_item_properties.id')
			->where('shop_item_property_for_groups.shop_id', '=', $this->_object->shop_id)
			->where('shop_item_property_for_groups.shop_group_id', '=', $shop_group_id)
			->where('properties.property_dir_id', 'IN', $subDirs);
		return $oProperties;
	}

	protected function _getAllSubDirs($dirID) {
		$mPropertyDirs = Core_Entity::factory('Property_Dir');
		$propertyDirs = $mPropertyDirs->getAllByParent_Id($dirID);
		$allSubDirs = array();
		foreach($propertyDirs as $propertyDir) {
			$allSubDirs[] = $propertyDir->id;
			$sd = $this->_getAllSubDirs($propertyDir->id);
			foreach($sd as $s) {
				$allSubDirs[] = $s;
			}
		}
		return $allSubDirs;
	}
}