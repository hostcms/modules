<?php
use Utils_Utl as utl;

class Utils_Informationsystem_Controller_Show extends Informationsystem_Controller_Show
{
	protected $_sparesSplitedUrl = '';
	protected $_instance = '';
	protected $_noPropertiesPath = '';
	protected $_parentGroups = array();
	protected $_currentItem = null;
	protected $_currentGroup = null;
	protected $_childGroupIDs = null;
	protected $_zero = null;
	protected $_groupXMLProperties = FALSE;

	/**
	 * Moto_Shop_Controller_Out constructor.
	 * @param null $_zero
	 */
	public function __construct($oIS)
	{
		parent::__construct($oIS);
		$this->_instance = Core_Page::instance();
		$this->_zero = new stdClass();
		$this->_zero->id = 0;
		$this->_zero->parent_id = 0;
		$this->_zero->modification_id = 0;
		$this->_zero->path = 'path-undefined-zero-class';

		return $this;
	}

	/**
	 * @return string
	 */
	public function getSparesSplitedUrl()
	{
		return $this->_sparesSplitedUrl;
	}

	/**
	 * @return string
	 */
	public function setSparesSplitedUrl($splittedUrl)
	{
		if(!is_array($splittedUrl)) {
			$splittedUrl = array($splittedUrl);
		}
		$this->_sparesSplitedUrl = $splittedUrl;

		return $this;
	}

	public function processURL($isID1, $isID2=array(), $linkedPropertyIDs=array(), $topGroupIDs=array()) {
		$srcURL = $this->_instance->srcrequest->pathNoPage;
		$splitByISs = array($isID1, $isID2);
		foreach ($splitByISs as $splitByIsKey => $splitByIS) {
			if(isset($splitByIS['isID'])) {
				$splitResult = utl::findInEntityByURL('Informationsystem', $splitByIS['isID'], $srcURL, true);
				$srcURL = $splitResult['lastPath'];
				if($srcURL != $splitResult) {
					$splitResults[] = $splitResult;
				}
			}
		}
		if(isset($splitResults[1]) && count($splitResults[1]['shopGroups'])>0) {
			//-- Удаляем из URL лишнее ---------------------------------------------------------------------------------
//			utl::p("{$splitResults[1]['inPath']}", Core::$url['path']);
			Core::$url['path'] = str_replace("{$splitResults[1]['inPath']}", '/', Core::$url['path']);
			//-- Дополняем контроллер нужным фильтром ------------------------------------------------------------------
			$this
				->shopItems()->queryBuilder()
				->join(array('property_value_ints', 'pvi'), 'pvi.entity_id', '=', 'shop_items.id', array(
					array('AND' => array('pvi.property_id', 'IN', $linkedPropertyIDs))
				))
				->join(array('getGroupsIerarchyUp', 'giu'), 'giu.id', '=', 'pvi.value')
				->groupBy('shop_items.id')
				->where('giu.url', 'LIKE', $splitResults[1]['inPath'].'%')
			;
		}
		//-- Дополняем XML нужными записями ------------------------------------------------------------------------
		$subOutEntities = array();
		if(isset($splitResults[1]['firstPath'])) {
			$mSubOutEntities = Core_Entity::factory('Informationsystem_Group');
			$mSubOutEntities
				->queryBuilder()
				->where('id', 'IN', $topGroupIDs);
			$subOutEntities = $mSubOutEntities->findAll(FALSE);

			foreach ($subOutEntities as $seKey => &$subOutEntity) {
				if(array_key_exists($subOutEntity->id, $splitResults[1]['shopGroups'])) {
					$l2Entities = $subOutEntity->shop_groups->findAll(FALSE);
					$subOutEntity->addEntities($l2Entities);
					foreach ($l2Entities as &$l2Entity) {
						if(array_key_exists($l2Entity->id, $splitResults[1]['shopGroups'])) {
							$l3Entities = $l2Entity->shop_items->findAll(FALSE);
							$l2Entity->addEntities($l3Entities);
						}
					}
				}
			}
		}
		$this->_noPropertiesPath = $splitResults[0]['firstPath'];
		$this->addEntity(
			Core::factory('Core_Xml_Entity')->name('splitedCategories')
				->addEntity(Core::factory('Core_Xml_Entity')->name('inPath')->value($splitResults[0]['firstPath']))
				->addEntity(Core::factory('Core_Xml_Entity')->name('outPath')->value((isset($splitResults[1]) ? $splitResults[1]['firstPath'] : $splitResults[0]['firstPath'])))
				->addEntity(Core::factory('Core_Xml_Entity')->name('outGroups')->addEntities(isset($splitResults[1]) ? $splitResults[1]['shopGroups'] : $splitResults[0]['shopGroups']))
				->addEntity(Core::factory('Core_Xml_Entity')->name('outList')->addEntities($subOutEntities))
		);
		$this->_sparesSplitedUrl = array_merge($splitResults[0]['shopGroups'], isset($splitResults[1]['shopGroups']) ? $splitResults[1]['shopGroups'] : array()); //, array($splitResults[1]['shopItem'])); //, $subOutEntities);
		return $this;
	}

	public function getFirstGroup()
	{
		$this->getParentGroups();
		return count($this->_parentGroups) > 0 ? $this->_parentGroups[array_keys($this->_parentGroups)[0]] : $this->_zero;
	}

	public function getLastGroup()
	{
		$this->getParentGroups();
		$parentGroupKeys = array_keys($this->_parentGroups);
		$parentGroupIndexID = end($parentGroupKeys);
		return count($this->_parentGroups) > 0 ? $this->_parentGroups[$parentGroupIndexID] : $this->_zero;
	}

	public function getParentGroupIDs()
	{
		return array_keys($this->getParentGroups());
	}

	public function getParentGroups()
	{
		if (count($this->_parentGroups) == 0 && $this->group > 0) {
			$currentGroup = Core_Entity::factory('Informationsystem_Group')->getById($this->group);
			do {
				$currentGroup->showXmlProperties($this->_groupXMLProperties);
				$this->_parentGroups[$currentGroup->id] = $currentGroup;
			} while ($currentGroup = $currentGroup->getParent());
			$this->_parentGroups = array_reverse($this->_parentGroups, true);
		}
		return $this->_parentGroups;
	}

	public function getChildGroups() {
		if(is_null($this->_childGroupIDs)) {
			$this->setChildGroups();
		}
		return $this->_childGroupIDs;
	}

//	public function setChildGroups()
//	{
//		$subgroupsForMenu = Utils_Observers_Shop_Group::getGroups($this->getEntity()->id, '/'.json_decode($this->_instance->srcrequest->splitedPath, true)[1].'/', 6);
//		$childGroups = utl::readTreeIDsFromLevel($subgroupsForMenu, $this->getLastGroup()->id);
//		$this->_childGroupIDs = count($childGroups)>0 ? $childGroups : array(-1);
//		$this
//			->group(false)
//			->shopItems()
//			->queryBuilder()
//			->where('shop_group_id', 'IN', $this->_childGroupIDs)
//		;
//		return $this;
//	}

	public function getCurrentGroup()
	{
		if (is_null($this->_currentGroup) && $this->group > 0) {
			$this->_currentGroup = Core_Entity::factory('Informationsystem_Group')->getById($this->group);
		} elseif ($this->group == 0) {
			$this->_currentGroup = $this->_zero;
		}
		return $this->_currentGroup;
	}

	public function getCurrentItem()
	{
		if (is_null($this->_currentItem) && $this->item > 0) {
			$this->_currentItem = Core_Entity::factory('Informationsystem_Item')->getById($this->item);
		} elseif ($this->item == 0) {
			$this->_currentItem = $this->_zero;
		}
		return $this->_currentItem;
	}

	public function makeSeoHeaders() {
		return $this;
	}

	public function show()
	{
		$this
			->addEntity(Core::factory('Core_Xml_Entity')
				->name('currentPath')->value($this->_instance->srcrequest->pathNoPage));

		return parent::show(); // TODO: Change the autogenerated stub
	}


}