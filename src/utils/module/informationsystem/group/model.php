<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Utils_Informationsystem_Group_Model
 *
 * @package Utils
 * @subpackage Shop
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2005-2018 Борисов Михаил Юрьевич m.u.borisov@gmail.com
 */
class Utils_Informationsystem_Group_Model extends Informationsystem_Group_Model
{
}