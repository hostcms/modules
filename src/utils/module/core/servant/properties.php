<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 14.06.2017
 * Time: 12:31
 */
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Utils_Core_Servant_Properties extends Core_Servant_Properties {

	protected $_tagName = 'propertytag';

	public function toArray() {
		$returnes = array();
		foreach ($this->_allowedProperties as $allowedPropertyKey => $allowedProperty) {
			$returnes[$allowedPropertyKey] = $this->$allowedPropertyKey;
		}
		return $returnes;
	}

	public function getXml() {
		Core_Event::notify(get_class($this) . '.onBeforeGetXml', $this);

		$xmlTranslator = Utils_Convert_Array_Xml_Entity::createInstance($this->toArray(), '', $this->_tagName);
		Core_Event::notify(get_class($this) . '.onAfterGetXml', $this);
		return $xmlTranslator->getXml();
	}
}