<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

class Utils_Path_Info extends Utils_Core_Servant_Properties {

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'remoteip',
		'proto',
		'domain',
		'port',
		'path',
		'pathNoPage',
		'pathdeep',
		'splitedPath',
		'crc32path',
		'crc32pathNoPage',
		'isMain',
		'isManage',
		'isProd',
		'query',
		'currentPage',
		'utm',
		'geoip',
		'referer',
		'browser',
		'get',
		'post',
	);

	protected $_tagName = 'pathinfo';

	/**
	 * Utils_Path_Info constructor.
	 * @param string $_tagName
	 */
	public function __construct()
	{
		parent::__construct();

		$this->isMain   = false;
		$this->isProd   = false;
		$this->isManage = false;
	}


}