#!/bin/bash

_CURRENTPATH=`pwd`

echo $_CURRENTPATH

if [ ! -d ../../../../../modules/utils ]; then 
    ln -s ../vendor/mikeborisov/hostcms-modules/src/utils/module ../../../../../modules/utils;
fi

if [ ! -d ../../../../../admin/utils ]; then 
    ln -s ../vendor/mikeborisov/hostcms-modules/src/utils/admin ../../../../../admin/utils;
fi

if [ ! -d ../../../../../modules/skin/bootstrap/module/utils ]; then 
    ln -s ../../../../vendor/mikeborisov/hostcms-modules/src/utils/skin ../../../../../modules/skin/bootstrap/module/utils;
fi