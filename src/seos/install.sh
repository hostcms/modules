#!/bin/bash

_CURRENTPATH=`pwd`

echo $_CURRENTPATH

if [ ! -d ../../../../../modules/seos ]; then 
    ln -s ../vendor/mikeborisov/hostcms-modules/src/seos/module ../../../../../modules/seos;
fi

if [ ! -d ../../../../../admin/seos ]; then 
    ln -s ../vendor/mikeborisov/hostcms-modules/src/seos/admin ../../../../../admin/seos;
fi

if [ ! -d ../../../../../modules/skin/bootstrap/module/seos ]; then
    ln -s ../../../../vendor/mikeborisov/hostcms-modules/src/seos/skin ../../../../../modules/skin/bootstrap/module/seos;
fi