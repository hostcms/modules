<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Utils.
 *
 * @package HostCMS 6\Skin
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © Михаил Борисов
 */
class Skin_Bootstrap_Module_Seos_Module extends Seos_Module
{
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Core_Event::attach('Skin_Bootstrap.onLoadSkinConfig', array('Skin_Bootstrap_Module_Seos_Module', 'onLoadSkinConfig'));
	}

	static public function onLoadSkinConfig($object, $args)
	{
		// Load config
		$aConfig = $object->getConfig();

		if(!isset($aConfig['adminMenu']['utilites'])) {
			$aConfig['adminMenu']['utilites'] = array(
				'ico' => 'fa fa-wrench',
				'caption' => 'Утилиты',
			);
		}

		$aConfig['adminMenu']['utilites']['modules'][] = 'seos';

		// Set new config
		$object->setConfig($aConfig);
	}
}