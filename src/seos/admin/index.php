<?php
require_once(__DIR__.'/../../../../../../bootstrap.php');

/**
 * Seos.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
use Utils_Utl as utl;

Core_Auth::authorization($sModule = 'seos');

// Код формы
$sAdminFormAction = '/admin/seos/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGUID('77982333-E0A9-E7BE-E068-1FF448B99BEE');
$iAdmin_Form_Id = $oAdmin_Form->id;
$siteID = CURRENT_SITE;

$oShop = Core_Entity::factory('Shop', Core_Array::getGet('shop_id', 0));


//// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->module(Core_Module::factory($sModule))
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Seos.model_name'))
	->pageTitle(Core::_('Seos.model_name'));

// Меню формы

////$additionalParams = "shop_id={$oShop->id}&shop_group_id={$oShopGroup->id}";
//$model = Core_Entity::factory('Seos_Path');
//$f = $model->findAll();

//-- Элементы меню -----------------------------------------------------------------------------------------------------
$oMenu = Admin_Form_Entity::factory('Menus');
$oMenu->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Seos.links_items'))
		->icon('fa fa-file-text-o')
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Seos.links_items_add'))
				->icon('fa fa-plus')
				->img('/admin/images/page_add.gif')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref
					(
						$oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0
					)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax
					(
						$oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0
					)
				)
		)
);
// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oMenu);
//-- Меню --------------------------------------------------------------------------------------------------------------

// Хлебные крошки
$oBreadcrumbs = Admin_Form_Entity::factory('Breadcrumbs');

// Первая крошка на список магазинов
$oBreadcrumbs->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Seos.menu'))
		->href($oAdmin_Form_Controller->getAdminLoadHref(
			'/admin/seos/index.php', NULL, NULL, ''
		))
		->onclick($oAdmin_Form_Controller->getAdminLoadAjax(
			'/admin/seos/index.php', NULL, NULL, ''
		))
);
// Добавляем все хлебные крошки контроллеру
$oAdmin_Form_Controller->addEntity($oBreadcrumbs);
if(($parentID=Core_Array::getGet('parent_id', 0))>0) {
	$oParent = Core_Entity::factory('Seos_Path')->getById($parentID);
	$parents = array($oParent);
	if($oParent->parent_id>0) {
		do {
			$oParent = Core_Entity::factory('Seos_Path')->getById($oParent->parent_id);
			$parents[] = $oParent;
		} while ($oParent->parent_id>0);
	}
	$parents = array_reverse($parents);
	foreach ($parents as $parent) {
		$oBreadcrumbs->add(
			Admin_Form_Entity::factory('Breadcrumb')
				->name($parent->path)
				->href($oAdmin_Form_Controller->getAdminLoadHref(
					'/admin/seos/index.php', NULL, NULL, "site_id={$siteID}&parent_id={$parent->id}"
				))
				->onclick($oAdmin_Form_Controller->getAdminLoadAjax(
					'/admin/seos/index.php', NULL, NULL, "site_id={$siteID}&parent_id={$parent->id}"
				))
		);
	}
}

//-- Действия ----------------------------------------------------------------------------------------------------------
// Действие "Редактировать"
$oEditAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('edit');

if ($oEditAction && $oAdmin_Form_Controller->getAction() == 'edit')
{
	$oEditController = Admin_Form_Action_Controller::factory(
		'Seos_Path_Controller_Edit', $oEditAction
	);
	$oEditController->addEntity($oBreadcrumbs);
	$oAdmin_Form_Controller->addAction($oEditController);
}
//----------------------------------------------------------------------------------------------------------------------

$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Seos_Path'));
// Ограничение источника 0 по родительской группе
$oAdmin_Form_Dataset->addCondition(
	array(
		'where' => array('site_id', '=', CURRENT_SITE),
		'where' => array('parent_id', '=', Core_Array::getGet('parent_id', 0)),
	)
);

$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);

// Показ формы
$oAdmin_Form_Controller->execute();
