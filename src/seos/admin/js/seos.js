jQuery.extend({
	seosCopySeoField: function(_element) {
		_element.parent().next().val(_element.text().trim());
	},
	seosClearSeoField: function(_element) {
		_element.parent().next().val('');
	}
});

$().ready(function(){
	$("#seosPanel").on('click', function(e) {
		_itemID = $(this).attr('data-crc-id');
		_seoParams = $(this).attr('data-seo-params').toString();
		nSeoParams = encodeURIComponent(_seoParams);
		_h1 = $('h1, .good__title').html().toString();
		nH1 = encodeURIComponent(Base64.encode(_h1));
		_path = "/admin/seos/index.php?hostcms[action]=edit&hostcms[checked][0]["+_itemID+"]=1&seoparams="+nSeoParams+"&seoh1="+nH1;
		_title = 'Редактировать SEO';
		$.utilsAjaxRequest(_path, _title);
	});
});