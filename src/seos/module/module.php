<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Entity.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
class Seos_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '0.1';
	/**
	 * Module date
	 * @var date
	 */
	public $date = '2015-07-02';

	/**
	 * Module form settings
	array(
	'form1' => array(										//-- Форма
	'guid' => 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX',		//-- guid формы
	'key_field' => 'id',									//-- Наименование ключевого поля из БД
	'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
	'on_page' => 30,										//-- Количество строк на странице
	'show_operations' => 0,									//-- Показывать операции
	'show_group_operations' => 0,							//-- Показывать групповые операции
	'default_order_direction' => 0,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
	'name' => array(										//-- название формы
	1 => 'Наименование модуля',							//-- по-русски - 1=идентификатор языка
	2 => 'Module name'									//-- по-английски - 2=идентификатор языка
	),
	'fields' => array(										//-- поля на отображаемой форме
	'id' => array(										//-- наименование поля из сущности БД(название столбца)
	'name' => array(								//-- название поля в админке
	1 => 'Код',									//-- по-русски - 1=идентификатор языка
	2 => 'ID'									//-- по-английски - 2=идентификатор языка
	),
	'sorting' => 10,								//-- поле сортировки
	'ico' => '',									//-- иконка поля
	'type' => 1,									//-- тип поля
	'format' => '',									//-- формат поля
	'allow_sorting' => 1,							//-- разрешить сортировку по полю 0-нет, 1-да
	'allow_filter' => 1,							//-- разрешить фильтрацию по полю 0-нет, 1-да
	'editable' => 1,								//-- разрешить inline-редактирование по полю 0-нет, 1-да
	'filter_type' => 0,								//-- тип фильтрации
	'class' => '',									//-- класс для поля
	'width' => '',									//-- ширина поля, например '55px'
	'image' => '',									//-- картинка для поля
	'link' => '',									//-- ссылка для поля
	'onclick' => '',								//-- событие нажатия на поле
	'list' => '',									//--
	),
	'field2' =>  array(
	'name' => array(								//-- название поля в админке
	1 => 'Наименование кампании',				//-- по-русски - 1=идентификатор языка
	2 => 'Campaign name'						//-- по-английски - 2=идентификатор языка
	),
	'sorting' => 20,
	.....
	),
	),
	'actions' => array (
	'apply' => array (									//-- ключевое наименование действия для формы
	'name' => array (								//-- название действия в админке
	1 => 'Код',									//-- по-русски - 1=идентификатор языка
	2 => 'ID'									//-- по-английски - 2=идентификатор языка
	),
	'sorting' => 1000,								//-- сортировка для действий
	'picture' => '/admin/images/apply.gif',			//--
	'icon' => 'fa fa-check',
	'color' => 'palegreen',
	'single' => 0,
	'group' => 0,
	'dataset' => '-1',
	'confirm' => 0,
	),
	'action2' => array ( ... )
	)
	),
	'form2' => array( ... )
	)
	 * @var afSettings
	 */
	protected $_afSettings = array(
		'seos' => array(											//-- Форма
			'guid' => '77982333-E0A9-E7BE-E068-1FF448B99BEE',								//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'url',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 0,							//-- Направление сортировки: 0 - по возрастанию, 1 - по убыванию
			'name' => array(										//-- название формы
				1 => 'СЕО администрирование',						//-- по-русски - 1=идентификатор языка
				2 => 'Seo administration'							//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '100px',								//-- ширина поля, например '55px'
				),
				'parent_id' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Родитель',							//-- по-русски - 1=идентификатор языка
						2 => 'Parent'								//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- поле сортировки
					'width' => '100px',								//-- ширина поля, например '55px'
				),
				'level' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Уровень',								//-- по-русски - 1=идентификатор языка
						2 => 'Level'								//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 25,								//-- поле сортировки
					'width' => '90px',								//-- ширина поля, например '55px'
				),
				'path' =>  array(
					'name' => array(
						1 => 'Путь',
						2 => 'Path'
					),
					'type' => 10,									//-- тип кампании, вычисляемое поле
					'allow_sorting' => 0,							//-- Сортировать
					'link' => '/admin/seos/index.php?site_id={site_id}&parent_id={id}',
					'onclick' => "$.adminLoad({path: '/admin/seos/index.php', additionalParams: 'site_id={site_id}&parent_id={id}', windowId: '{windowId}'}); return false",
					'sorting' => 30,								//-- поле сортировки
				),
				'url' =>  array(
					'name' => array(
						1 => 'URL',
						2 => 'URL'
					),
					'type' => 10,									//-- тип кампании, вычисляемое поле
					'allow_sorting' => 0,							//-- Сортировать
					'link' => '{url}',
//					'onclick' => "$.adminLoad({path: '/admin/seos/index.php', additionalParams: 'site_id={site_id}&parent_id={id}', windowId: '{windowId}'}); return false",
					'sorting' => 40,								//-- поле сортировки
				),
				'name' =>  array(
					'name' => array(								//-- название поля в админке
						1 => 'Заголовок H1',						//-- по-русски - 1=идентификатор языка
						2 => 'H1'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 50,
					'width' => '400px',
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '/admin/images/edit.gif',			//--
					'icon' => 'fa fa-pencil',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => '0',
					'confirm' => 0,
				),
//				'markDeleted' => array(								//-- ключевое наименование действия для формы
//					'name' => array(								//-- название действия в админке
//						1 => 'Удалить',								//-- по-русски - 1=идентификатор языка
//						2 => 'Delete'								//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 20,								//-- сортировка для действий
//					'picture' => '/admin/images/delete.gif',			//--
//					'icon' => 'fa fa-trash-o',
//					'color' => 'darkorange',
//					'single' => 1,
//					'group' => 1,
//					'dataset' => '0',
//					'confirm' => 1,
//				),
//				'apply' => array(									//-- ключевое наименование действия для формы
//					'name' => array(								//-- название действия в админке
//						1 => 'Применить',							//-- по-русски - 1=идентификатор языка
//						2 => 'Apply'								//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 20,								//-- сортировка для действий
//					'picture' => '/admin/images/apply.gif',			//--
//					'icon' => 'fa fa-check',
//					'color' => 'palegreen',
//					'single' => 0,
//					'group' => 0,
//					'dataset' => '-1',
//					'confirm' => 0,
//				),
			)
		),
	);

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		if(!is_null($user=Core_Entity::factory('User')->getCurrent()) && $user->id > 0) {
			Core_Event::attach('Core_Page.onBeforeShowCss', array('Seos_Observers_Core', 'onBeforeShowCss'));
			Core_Event::attach('Core_Page.onBeforeShowJs', array('Seos_Observers_Core', 'onBeforeShowJs'));
			Core_Event::attach('Utils_Admin_Panel.onBeforeMakeSitePanel', array('Seos_Observers_Site', 'onBeforeMakeSitePanel'));

			Core_Skin::instance()->addJs(Seos_Observers_Core::SEOS_JS);
			Core_Skin::instance()->addCss(Seos_Observers_Core::SEOS_CSS);
		}
		Core_Event::attach('Core_Command_Controller_Default.onBeforeSetTemplate', array('Seos_Observers_Site', 'onBeforeSetTemplate'));
		Core_Event::attach('Core_Response.onBeforeShowBody', array('Seos_Observers_Response', 'onBeforeShowBody'));

		$this->menu = array(
			array(
				'sorting' => 260,
				'block' => 1,
				'ico' => 'fa fa-magic',
				'name' => Core::_('Seos.model_name'),
				'href' => "/admin/seos/index.php",
				'onclick' => "$.adminLoad({path: '/admin/seos/index.php'});return false"
			)
		);
	}

	/**
	 * Install module.
	 */
	public function install()
	{
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			//-- Добавляем форму --
			$adminForm = $this->adminForm($afSetting['name'], $afSetting);
		}
	}

	/**
	 * Uninstall.
	 */
	public function uninstall()
	{
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (!is_null($oAdmin_Form)) {
				// Удаление формы
				$oAdmin_Form->delete();
			}
		}
	}

	protected function adminForm($afName, $afSetting) {
		$oAdmin_Form = NULL;
		if(isset($afSetting['guid'])) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (is_null($oAdmin_Form)) {
				$oAdmin_Word_Form = $this->adminWordForm($afName);
				$oAdmin_Form = Core_Entity::factory('Admin_Form');
				$oAdmin_Form->admin_word_id = $oAdmin_Word_Form->id;
				$oAdmin_Form->key_field = $afSetting['key_field'];
				$oAdmin_Form->default_order_field = Core_Array::get($afSetting, 'default_order_field', $afSetting['key_field']);

				$oAdmin_Form->on_page = Core_Array::get($afSetting, 'on_page', 20);
				$oAdmin_Form->show_operations = Core_Array::get($afSetting, 'show_operations', 1);
				$oAdmin_Form->show_group_operations = Core_Array::get($afSetting, 'show_group_operations', 0);
				$oAdmin_Form->default_order_direction = Core_Array::get($afSetting, 'default_order_direction', 0);
				$oAdmin_Form->guid = $afSetting['guid'];
				$oAdmin_Form->save();
			}
			if(isset($afSetting['fields']) && is_array($afSetting['fields'])) {
				//-- Добавляем поля в форму --
				foreach ($afSetting['fields'] as $dbFieldKey => $field) {
					$adminFormField = $this->adminFormField($dbFieldKey, $field);
					$oAdmin_Form->add($adminFormField);
				}
			}
			if(isset($afSetting['actions']) && is_array($afSetting['actions'])) {
				//-- Добавляем действия в форму --
				foreach ($afSetting['actions'] as $dbActionKey => $action) {
					$adminFormAction = $this->adminFormAction($dbActionKey, $action);
					$oAdmin_Form->add($adminFormAction);
				}
			}
		}

		return $oAdmin_Form;
	}

	protected function adminFormAction($fDbName, $action) {
		$oAdmin_Word_Form = $this->adminWordForm($action['name']);
		$oAdmin_Form_Action = Core_Entity::factory('Admin_Form_Action')->save();
		$oAdmin_Form_Action->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Action->name = $fDbName;
		$oAdmin_Form_Action->picture = Core_Array::get($action, 'picture', '');
		$oAdmin_Form_Action->icon = Core_Array::get($action, 'icon', '');
		$oAdmin_Form_Action->color = Core_Array::get($action, 'color', '');
		$oAdmin_Form_Action->single = Core_Array::get($action, 'single', 0);
		$oAdmin_Form_Action->group = Core_Array::get($action, 'group', 0);
		$oAdmin_Form_Action->sorting = Core_Array::get($action, 'sorting', 1000);
		$oAdmin_Form_Action->dataset = Core_Array::get($action, 'dataset', '-1');
		$oAdmin_Form_Action->confirm = Core_Array::get($action, 'confirm', 0);
//		$oAdmin_Form_Action->save();
		return $oAdmin_Form_Action;
	}

	protected function adminFormField($fDbName, $field) {
		$oAdmin_Word_Form = $this->adminWordForm($field['name']);
		$oAdmin_Form_Field = Core_Entity::factory('Admin_Form_Field')->save();
		$oAdmin_Form_Field->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Field->name = $fDbName;
		$oAdmin_Form_Field->sorting = Core_Array::get($field, 'sorting', 1000);
		$oAdmin_Form_Field->ico = Core_Array::get($field, 'ico', '');
		$oAdmin_Form_Field->type = Core_Array::get($field, 'type', 1);
		$oAdmin_Form_Field->format = Core_Array::get($field, 'format', '');
		$oAdmin_Form_Field->allow_sorting = Core_Array::get($field, 'allow_sorting', 1);
		$oAdmin_Form_Field->allow_filter = Core_Array::get($field, 'allow_filter', 1);
		$oAdmin_Form_Field->editable = Core_Array::get($field, 'editable', 0);
		$oAdmin_Form_Field->filter_type = Core_Array::get($field, 'filter_type', 0);
		$oAdmin_Form_Field->class = Core_Array::get($field, 'class', '');
		$oAdmin_Form_Field->width = Core_Array::get($field, 'width', '');
		$oAdmin_Form_Field->image = Core_Array::get($field, 'image', '');
		$oAdmin_Form_Field->link = Core_Array::get($field, 'link', '');
		$oAdmin_Form_Field->onclick = Core_Array::get($field, 'onclick', '');
		$oAdmin_Form_Field->list = Core_Array::get($field, 'list', '');
		return $oAdmin_Form_Field;
	}

	protected function adminWordForm($afName) {
		$oAdmin_Word_Form = Core_Entity::factory('Admin_Word')->save();
		foreach ($afName as $langID => $langName) {
			$oAdmin_Word_Value = $this->adminWordValue($langID, $langName);
			$oAdmin_Word_Form->add($oAdmin_Word_Value);
		}
		return $oAdmin_Word_Form;
	}

	protected function adminWordValue($language_id, $name) {
		$oAdmin_Word_Value = Core_Entity::factory('Admin_Word_Value')->save();
		$oAdmin_Word_Value->admin_language_id = $language_id;
		$oAdmin_Word_Value->name = $name;
		$oAdmin_Word_Value->save();
		return $oAdmin_Word_Value;
	}
}