<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

abstract class Seos_Sitemap_Core extends Core_Command_Controller
{
	protected $_content = array();
	protected $_instance = array();
	protected $_baseCache = 'uploadtmp/seos/sitemap';
	protected $_domainName = 'undefined';
	protected $_domain = 'undefined';
	protected $_currentCache = '';
	protected $_currentCacheTmp = '';
	protected $_totalRecords = 0;
	protected $_maxRecordsOnPage = 45000;
	protected $_maxPages = 500;
	protected $_entity = NULL;
	protected $_site_id = 0;
	protected $_cache = TRUE;

	/**
	 * Moto_Sitemap_Core constructor.
	 * @param array $_content
	 */
	public function __construct($cache=TRUE, $site_id=FALSE)
	{
		$this->_instance = Core_Page::instance();
		$this->_domainName = $this->_instance->srcrequest->domain;
		$this->_domain = $this->_instance->srcrequest->proto."://{$this->_domainName}";
		$this->_currentCache = CMS_FOLDER.$this->_baseCache.'/'.$this->_domainName;
		$this->_currentCacheTmp = $this->_currentCache.'/tmp-'.str_replace(' ', '', microtime(FALSE));
		utl::checkFolder($this->_currentCache);
		if(!file_exists($this->_currentCache.'/../../.htaccess')) {
			file_put_contents($this->_currentCache.'/../../.htaccess', "deny from all\n");
		}
		(defined('CURRENT_SITE')) && $this->_site_id = CURRENT_SITE;
		($site_id!==FALSE) && $this->_site_id = $site_id;
		$this->_cache = $cache;

		$this->_entity = Core_Entity::factory('Seos_Path');
		$this->_entity
			->queryBuilder()
			->where('gen_sitemap', '=', 1)
			->where('last_status', '=', 200)
			->where('site_id', '=', $this->_site_id*1)
			->where('deleted', '=', 0)
			->where('last_counts', '<>', 0)
			->orderBy('level')
		;
	}

	/**
	 * @return bool
	 */
	public function cacheStatus()
	{
		return $this->_cache;
	}

	/**
	 * @param bool $cache
	 */
	public function cacheOn()
	{
		$this->_cache = TRUE;
		return $this;
	}

	/**
	 * @param bool $cache
	 */
	public function cacheOff()
	{
		$this->_cache = FALSE;
		return $this;
	}

	/**
	 * Build statement
	 */
	public function buildContent() {
		$this->_entity->getTableColums();
		$this->_entity->queryBuilder()
			->limit($this->_maxRecordsOnPage)
		;
		$currentTmp = $this->_currentCacheTmp;
		$changefreq = 'daily';
		$priority = '0.5';
		$page=0;
		$this->_totalRecords = 0;
		$xmlSitemapContent = '<?xml version="1.0" encoding="UTF-8"?>'."\n";
		$xmlSitemapContent .= '<sitemapindex xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">'."\n";

		do {
			$entityQB = clone $this->_entity->queryBuilder();
			$entityQB
				->clearSelect()
				->select('id')
				->select('url')
			;
			$entityQB->from($this->_entity->getTableName());
			if($page==0) {
				$entityQB->sqlCalcFoundRows();
			} else {
				$entityQB->offset($page*$this->_maxRecordsOnPage);
			}
			$pageRecords = $entityQB->execute()->asAssoc()->result();
			if(count($pageRecords)>0) {
				if($page==0) {
					$row = Core_QueryBuilder::select(array('FOUND_ROWS()', 'count'))->asAssoc()->execute()->current();
					$this->_totalRecords = $row['count'];
				}
				$items = array();
				$xmlContent = sprintf('<?xml version="1.0" encoding="UTF-8"?>')."\n";
				$xmlContent .= sprintf('<urlset xmlns="https://www.sitemaps.org/schemas/sitemap/0.9">')."\n";
				foreach ($pageRecords as $pageRecord) {
					$xmlContent .= sprintf("	<url>\n");
					$xmlContent .= sprintf("		<loc>%s%s</loc>\n", $this->_domain, $pageRecord['url']);
					$xmlContent .= sprintf("		<changefreq>%s</changefreq>\n", $changefreq);
					$xmlContent .= sprintf("		<priority>%s</priority>\n", $priority);
					$xmlContent .= sprintf("	</url>\n");
				}
				$xmlContent .= sprintf('</urlset>');
				utl::checkFolder($currentTmp);
				file_put_contents($currentTmp.'/../update.lock', date('Y-m-d h:i:s'));
				file_put_contents($currentTmp.'/sitemap-'.$page.'.xml', $xmlContent);

				unset($entityQB);
				$xmlSitemapContent .= '	<sitemap>'."\n";
				$xmlSitemapContent .= sprintf("		<loc>%s/sitemap.xml/sitemap-{$page}.xml</loc>\n", $this->_domain);
				$xmlSitemapContent .= sprintf('		<lastmod>%s</lastmod>'."\n", date('Y-m-d'));
				$xmlSitemapContent .= '	</sitemap>'."\n";
			} else {
				$page = $this->_maxPages-1;
			}
			$page++;
		} while ($page<$this->_maxPages);

		$xmlSitemapContent .= '</sitemapindex>'."\n";
		file_put_contents($currentTmp.'/pages.xml', $xmlSitemapContent);

		Core_File::deleteDir($this->_currentCache.DIRECTORY_SEPARATOR.'files');
		rename($currentTmp, $this->_currentCache.DIRECTORY_SEPARATOR.'files');
		Core_File::deleteDir($this->_currentCacheTmp);
	}

	/**
	 * Default controller action
	 * @return Core_Response
	 * @hostcms-event Moto_Sitemap_Core.onBeforeShowAction
	 * @hostcms-event Moto_Sitemap_Core.onAfterShowAction
	 */
	public function showAction()
	{
		$oCore_Response = new Core_Response();
		Core_Event::notify(get_class($this) . '.onBeforeShowAction', $this);
		$replaceAll = false;
		$currentTmp = $this->_currentCacheTmp;

		if (isset($this->page)) {
			$cacheFileName = $this->_currentCache . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'sitemap-' . $this->page . '.xml';
		} else {
			$cacheFileName = $this->_currentCache . DIRECTORY_SEPARATOR . 'files' . DIRECTORY_SEPARATOR . 'pages.xml';
		}
		$hoursDiff = 0;

		//-- Файл кеша существует --------------------------------------------------------------------------------------
		if (file_exists($cacheFileName) && is_file($cacheFileName)) {
			$fileDiff = date_diff(date_create(date('Y-m-d H:i:s')), date_create(date("Y-m-d H:i:s", filemtime($cacheFileName))));
			$hoursDiff = (24*$fileDiff->d)+$fileDiff->h;

			if($hoursDiff>12) {
				$replaceAll = true;
			}
		} else {
			//-- Возможно идет или недавно прошел апдейт ---------------------------------------------------------------
			if(file_exists($this->_currentCache.'/update.lock') && is_file($this->_currentCache.'/update.lock')) {
				$fileTmpDiff = date_diff(date_create(date('Y-m-d H:i:s')), date_create(date("Y-m-d H:i:s", filemtime($this->_currentCache.'/update.lock'))));
				$fileLockSecondsDiff = $fileTmpDiff->h*24*60*60+$fileTmpDiff->i*60+$fileTmpDiff->s;
				if($fileLockSecondsDiff>600) {
					$dirs = glob($this->_currentCache.'/tmp-*');
					(is_array($dirs)&&count($dirs)>0) && array_map('Core_File::deleteDir', $dirs);
					unlink($this->_currentCache.'/update.lock');
					$replaceAll = true;
				} else {
					$oCore_Response->status(404);
					return $oCore_Response;
				}
			} else { //-- Если файл не был найден, обновить весь кеш ---------------------------------------------------
				$replaceAll = true;
			}
		}

		if( $replaceAll || !$this->_cache ) {
			$this->buildContent();
		}
		$xmlContent = '';
		if (file_exists($cacheFileName) && is_file($cacheFileName)) {
			$xmlContent = file_get_contents($cacheFileName);
		}
		$oSite = Core_Entity::factory('Site')->getByAlias($this->_domainName);

		if ($xmlContent != '')
		{
			$oCore_Response
				->status(200)
				->header('Content-Type', "application/xml; charset={$oSite->coding}")
				->header('Last-Modified', gmdate('D, d M Y H:i:s', time()) . ' GMT')
				->body($xmlContent);
		}
		else
		{
			$oCore_Response->status(404);
		}

		Core_Event::notify(get_class($this) . '.onAfterShowAction', $this, array($oCore_Response));

		return $oCore_Response;
	}
}