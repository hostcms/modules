<?php
use Utils_Utl as utl;

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 04.08.2016
 * Time: 13:57
 */
class Seos_Sitemap_Controller extends Seos_Sitemap_Core
{
	public function __construct($cache = TRUE, $site_id = FALSE)
	{
		parent::__construct($cache, $site_id);
//		$this->cacheOff();
		$this->cacheOn();
	}
}