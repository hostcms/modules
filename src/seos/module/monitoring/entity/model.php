<?php
use Utils_Utl as utl;
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 02.05.2017
 * Time: 12:05
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 *
 */
class Seos_Monitoring_Entity_Model extends Utils_Core_Entity {

	protected $_current=null;

	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * List of preloaded values
	 * @var array
	 */
	protected $_preloadValues = array(
		'seos_path_id' => 0
	);

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
		'seos_path' => array(),
	);

	/**
	 * Constructor.
	 * @param int $id entity ID
	 */
	public function __construct($id = NULL)
	{
		parent::__construct($id);

		if (is_null($id))
		{
			$this->_preloadValues['entity_date'] = Core_Date::datetime2sql(date('Y-m-d H:i:s'));
		}
	}
}
