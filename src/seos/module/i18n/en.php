<?php
/**
 * SEO admins.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
    'menu' => 'SEO admins',
    'model_name' => 'SEO admins',
    'links_items' => 'SEO admins',
    'links_items_add' => 'Add',
);