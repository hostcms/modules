<?php
/**
 * SEO администрирование.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	'menu' => 'SEO по URL',
	'model_name' => 'SEO администрирование',
	'links_items' => 'SEO администрирование',
	'links_items_add' => 'Добавить',
);