<?php
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 19.08.2015
 * Time: 15:49
 */
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 */
class Seos_Path_Controller_Edit extends Utils_Admin_Form_Action_Controller_Type_Edit
{
	/**
	 * Set object
	 * @param object $object object
	 * @return self
	 */
	public function setObject($object)
	{
		$seo=NULL;
		if(($base64Seo=Core_Array::getGet('seoparams', ''))!='') {
			$seo = json_decode(base64_decode($base64Seo));
		}
		if(isset($seo->url) && (is_null($object) ||
			!(isset($object->id) && $object->id > 0))) {
			$object = Core_Entity::factory('Seos_Path')
				->findByPath($seo->url, CURRENT_SITE);
		}
		parent::setObject($object);

		if(($base64h1=Core_Array::getGet('seoh1', ''))!='') {
			$seo->h1 = mb_strtoupper(base64_decode($base64h1));
		}
		$this->_mainTab
			->add($oMainRow1 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow2 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow3 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow4 = Admin_Form_Entity::factory('Div')->class('row'))
		;
		$this->_additionalTab
			->add($oAddRow1 = Admin_Form_Entity::factory('Div')->class('row'))
		;

		$this->getField('parent_id')->readonly('readonly');
		$this->getField('domain')
			->divAttr(array('class' => 'form-group col-lg-3 col-md-3 col-sm-4 col-xs-12'));
		$urlEntity = Admin_Form_Entity::factory('Input')
//			->format(
//				array(
//					'minlen' => array('value' => 1),
//					'reg' => array('value' => '/^\/([\/\w\.%-]*\/)*$/i'),
//				)
//			)
			->name('url')
			->id('url')
			->caption(Core::_("Seos_Path.url"))
			->divAttr(array('class' => 'form-group col-lg-5 col-md-5 col-sm-5 col-xs-12'))
			->value($this->_object->url)
		;
		if($this->_object->id>0) {
			$urlEntity->readonly('readonly');
		}

		$this->getField('querystring')
			->divAttr(array('class' => 'form-group col-lg-4 col-md-4 col-sm-3 col-xs-12'));

		$this->getField('name')
			->divAttr(array('class' => 'form-group col-lg-9 col-md-9 col-sm-9 col-xs-12 seo_field'));
		$this->getField('path')
			->divAttr(array('class' => 'form-group col-lg-3 col-md-3  col-sm-3 col-xs-12'))
			->readonly('readonly')
			->format(array())
		;

		$this->getField('content_description')
			->divAttr(array('class' => 'form-group col-lg-6 col-md-6 col-sm-12'))
			->rows(12)
			->wysiwyg(TRUE);
		$this->getField('content_description_large')
			->divAttr(array('class' => 'form-group col-lg-6 col-md-6 col-sm-12'))
			->rows(12)
			->wysiwyg(TRUE);
		$this->getField('seo_title')
			->divAttr(array('class' => 'form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 seo_field'));
		$this->getField('seo_keywords')
			->divAttr(array('class' => 'form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 seo_field'));
		$this->getField('seo_description')
			->divAttr(array('class' => 'form-group col-lg-4 col-md-4 col-sm-4 col-xs-12 seo_field'));

		$this->getField('crc32url')
			->readonly('readonly')->caption('')->type('hidden')->format(array());
		$this->getField('level')
			->readonly('readonly')->caption(Core::_("Seos_Path.level"))
			->divAttr(array('class' => 'form-group col-lg-12 col-md-12 col-sm-12'));

		$this->_additionalTab
			->delete($this->getField('url'))
			->delete($this->getField('domain'))
			->delete($this->getField('querystring'))
			->delete($this->getField('name'))
			->delete($this->getField('path'))
			->delete($this->getField('seo_title'))
			->delete($this->getField('seo_keywords'))
			->delete($this->getField('seo_description'))
			->delete($this->getField('content_description'))
			->delete($this->getField('content_description_large'))
		;

		if (isset($seo->h1) && $seo->h1!='') {
			$this->getField('name')->caption =
				'<a class=\'seo_remove fa fa-remove\' href=\'javascript:void(0);\' title=\'Удалить\' onclick=$.seosClearSeoField($(this))></a>'
				.'<a class=\'seo_link\' href=\'javascript:void(0);\' title=\''.$seo->h1.'\' onclick=$.seosCopySeoField($(this))>'.$seo->h1.'</a>'
				.$this->getField('name')->caption
			;
		}
		if (isset($seo->seoTitle) && $seo->seoTitle!='') {
			$this->getField('seo_title')->caption =
				'<a class=\'seo_remove fa fa-remove\' href=\'javascript:void(0);\' title=\'Удалить\' onclick=$.seosClearSeoField($(this))></a>'
				.'<a class=\'seo_link\' href=\'javascript:void(0);\' title=\''.$seo->seoTitle.'\' onclick=$.seosCopySeoField($(this))>'.$seo->seoTitle.'</a>'
				.$this->getField('seo_title')->caption
			;
		}
		if (isset($seo->seoKeywords) && $seo->seoKeywords!='') {
			$this->getField('seo_keywords')->caption =
				'<a class=\'seo_remove fa fa-remove\' href=\'javascript:void(0);\' title=\'Удалить\' onclick=$.seosClearSeoField($(this))></a>'
				.'<a class=\'seo_link\' href=\'javascript:void(0);\' title=\''.$seo->seoKeywords.'\' onclick=$.seosCopySeoField($(this))>'.$seo->seoKeywords.'</a>'
				.$this->getField('seo_keywords')->caption
			;
		}
		if (isset($seo->seoDescription) && $seo->seoDescription!='') {
			$this->getField('seo_description')->caption =
				'<a class=\'seo_remove fa fa-remove\' href=\'javascript:void(0);\' title=\'Удалить\' onclick=$.seosClearSeoField($(this))></a>'
				.'<a class=\'seo_link\' href=\'javascript:void(0);\' title=\''.$seo->seoDescription.'\' onclick=$.seosCopySeoField($(this))>'.$seo->seoDescription.'</a>'
				.$this->getField('seo_description')->caption
			;
		}

		$oMainRow1
			->add($urlEntity)
			->add($this->getField('domain'))
			->add($this->getField('querystring'))
		;
		$oMainRow2
			->add($this->getField('name'))
			->add($this->getField('path'))
		;

		$oMainRow3
			->add($this->getField('seo_title'))
			->add($this->getField('seo_keywords'))
			->add($this->getField('seo_description'))
		;
		$oMainRow4
			->add($this->getField('content_description'))
			->add($this->getField('content_description_large'));

		return $this;
	}


	/**
	 * Executes the business logic.
	 * @param mixed $operation Operation name
	 * @return boolean
	 */
	public function execute($operation = NULL)
	{
		if (!is_null($operation) && $operation != '')
		{
			switch($operation) {
				case 'save':
				case 'apply':
					$this->_formValues['crc32url'] = crc32(Core_Array::getPost('url'));
					if(is_null($this->_object->id)) {
						$result = $this->_object->processFullPath(Core_Array::getPost('url'), CURRENT_SITE);
						$last = end($result);
						$this->_formValues['id'] = $last->id;
						$this->_formValues['parent_id'] = $last->parent_id;
						$this->_formValues['level'] = $last->level;
						$this->_formValues['path'] = $last->path;
						$this->_object = $last;
					}
					break;
			}
		}

		return parent::execute($operation);
	}

}