<?php
/**
 * SEO администрирование.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	'model_name' => 'SEO пути',
	'tab_SEOSheads' => 'SEO titles',
	'id' => 'Идентификатор записи',
	'parent_id' => 'Идентификатор родительской записи',
	'level' => 'Уровень вложености',
	'path' => 'Локальный путь',
	'site_id' => 'Идентификатор сайта',
	'domain' => 'Домен',
	'url' => 'Путь для SEO оптимизации',
	'querystring' => 'GET параметры',
	'seo_title' => 'SEO title',
	'seo_keywords' => 'SEO keywords',
	'seo_description' => 'SEO description',
	'name' => 'H1 для страницы',
	'content_description' => 'Текст для страницы',
	'content_description_large' => 'Большой текст для страницы',
	'last_update' => 'Дата последнего обновления',
	'last_status' => 'Статус последнего запроса к странице',
	'last_counts' => 'Количество товаров на странице',
	'gen_sitemap' => 'Добавлять раздел в Sitemap.xml',
//	'level_template' => 'Шаблон для страниц выбранного уровня',

	'tab_SEOS' => 'SEO тексты',
	'tab_SEOSheads' => 'SEO метатеги',
	'markDeleted_success' => 'Удаление выполнено успешно',
	'delete_success' => 'Удаление выполнено успешно',
	'edit_success' => 'Редактирование выполнено успешно',
);