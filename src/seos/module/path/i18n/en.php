<?php
/**
 * SEO admins.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	'model_name' => 'SEO paths',
	'edit_success' => 'Successfully edited',
	'id' => 'Entity ID',
	'site_id' => 'Site ID',
	'url' => 'SEO optimization',
	'seo_title' => 'SEO title',
	'seo_keywords' => 'SEO keywords',
	'seo_description' => 'SEO description',
	'name' => 'H1',
	'content_description' => 'Pages text',
	'content_description_large' => 'Big pages text',
	'tab_SEOS' => 'SEO texts',
	'tab_SEOSheads' => 'SEO titles',
	'markDeleted_success' => 'Successefully deleted',
);