<?php
use Utils_Utl as utl;

class Seos_Observers_Site_Alias
{
	static public function onAfterFindAll($object, $args)
	{
		Core_Event::detach('site_alias.onAfterFindAll', array('Seos_Observers_Site_Alias', 'onAfterFindAll'));
		$instance = Core_Page::instance();
		$oSite_Alias = Core_Entity::factory('Site_Alias')->findAlias(Core::$url['host']);

		if(!is_null($oSite_Alias) && isset($oSite_Alias->site_id)) {
			$seoEntity = utl::getModelItemsByArrayIds(
				  'Seos_Path'
				, array($oSite_Alias->site_id)
				, 'site_id'
				, false
				, ''
				, false
				, array(
					array('field'=>'url', 'operation'=>'=', 'value'=>Core::$url['path']),
					array('field'=>'deleted', 'operation'=>'=', 'value'=>0),
				)
			);
			$instance->addAllowedProperty('seopage');
			$instance->seopage = null;

			if (is_array($seoEntity) && count($seoEntity) == 1 && get_class($seoEntity[0]) == 'Seos_Path_Model') {
				$instance->seopage = $seoEntity[0];
				$instance->title($seoEntity[0]->seo_title);
				$instance->description($seoEntity[0]->seo_description);
				$instance->keywords($seoEntity[0]->seo_keywords);
			}

//-- UPD: 2024-06-04. Отключено давным-давно. Пристрелить за ненадобностью эту херь ------------------------------------
//			if( false && $instance->title=='' && $instance->description=='' && $instance->keywords=='' ) {
//				$splitedPaths = utl::getCRC32fromURL(Core::$url['path']);
//				$shopID = 3;
////				utl::p($splitedPaths);
//
//				$analyzedRouting = new Seos_Route_Entity();
//
//				$checkedPartsOfURL = array_keys($splitedPaths['crc']);
//				unset($checkedPartsOfURL[0]);
//				$checkedPartsOfURL = array_reverse($checkedPartsOfURL);
//				$foundedGroupURL = '';
//				$notWorkedGroupURL='';
//				foreach ($checkedPartsOfURL as $checkedKey=>$checkedPartOfURL) {
////					utl::p($checkedPartOfURL);
//					if($checkedKey==0) {
//						$notWorkedGroupURL = $checkedPartOfURL;
//						// SELECT * FROM getGroupsIerarchyUp giu WHERE giu.url LIKE '/equipment/helmets/icon/helmet-icon-mainframe-legion-silver/';
//						$findItem = Core_QueryBuilder::select('id', 'path')
//							->from('getGroupsIerarchyUp')
//							->where('url', '=', $checkedPartOfURL)
//							->where('shop_id', '=', $shopID)
//							->execute()->asAssoc()->result()
//						;
//						if(count($findItem)>0) {
//							$analyzedRouting->item = $findItem[0]['id'];
//							$analyzedRouting->groups = explode('/', $findItem[0]['path']);
//							$notWorkedGroupURL='';
//							break;
//						}
//					}
//					if(count($analyzedRouting->groups)==0) {
//						// SELECT path FROM getGroupsIerarchyDown WHERE url LIKE '/equipment/helmets/%' LIMIT 1;
//						$findGroup = Core_QueryBuilder::select('url', 'path')
//							->from('getGroupsIerarchyDown')
//							->where('url', 'LIKE', $checkedPartOfURL.'%')
//							->where('shop_id', '=', $shopID)
//							->limit(1)
//							->execute()->asAssoc()->result()
//						;
//						if(count($findGroup)>0) {
//							$splitURL = array_values(array_filter(explode('/', $checkedPartOfURL)));
//							$splitPath = array_values(array_filter(explode('/', $findGroup[0]['path'])));
//							$mapURL = array_values(array_filter(array_map(function($a, $b) { return ($b!='' ? $a : ''); }, $splitPath, $splitURL)));
//							if(count($mapURL)>0) {
//								$analyzedRouting->groups = $mapURL;
//								$foundedGroupURL = $checkedPartOfURL;
//								$notWorkedGroupURL = '/'.str_replace($checkedPartOfURL, '', $notWorkedGroupURL);
//								break;
//							}
//						} else {
//
//						}
//					}
//				}
//				//------------------------------------------------------------------------------------------------------
//				$requestType = 'g';
//				if($analyzedRouting->item > 0) {
//					$requestType = 'i';
//				}
//				$seosSettingsRequest = Core_QueryBuilder::select('sp.id')
//					->select('sp.level')
//					->select('sp.path')
//					->select(array('sps.id', 'sps_id'))
//					->select(array('sps.name', 'sps_h1_template'))
//					->select(array('sps.title', 'sps_title_template'))
//					->select(array('sps.keywords', 'sps_keywords_template'))
//					->select(array('sps.description', 'sps_description_template'))
//					->from(array('seos_paths', 'sp'))
//					->join(array('seos_path_settings', 'sps'), 'sps.seos_path_id', '=', 'sp.id')
//					->where('sp.crc32url', 'IN', array_values($splitedPaths['crc']))
//					->where('sp.site_id', '=', $oSite_Alias->site_id)
//					->where('sps.type', '=', $requestType)
//					->where(Core_QueryBuilder::expression('(sp.level+sps.level_plus)'), '=', $splitedPaths['endlevel'])
//				;
//				$seosSettings = $seosSettingsRequest->execute()->asAssoc()->result();
//				if(isset($seosSettings[0])) {
//					$h1 = self::processTemplate($analyzedRouting, $seosSettings, 'sps_h1_template');
//					$title = self::processTemplate($analyzedRouting, $seosSettings, 'sps_title_template');
//					$keywords = self::processTemplate($analyzedRouting, $seosSettings, 'sps_keywords_template');
//					$description = self::processTemplate($analyzedRouting, $seosSettings, 'sps_description_template');
////					$h1matches = array();
////					preg_match_all('/({.*?})/i', $seosSettings[0]['sps_h1_template'], $h1matches);
////					if(isset($h1matches[0])) {
////						$entities = array();
////						$h1s = array();
////						foreach ($h1matches[0] as $h1match) {
////							$rH1match = preg_replace('/[{}]/', '', $h1match);
////							$h1fields = explode(':', $rH1match);
////							$h1field = $h1fields[0];
////							$h1s[$h1match] = '';
////							if(isset($analyzedRouting->$h1field)) {
////								$h1FieldValue = $analyzedRouting->$h1field;
////								if(isset($h1FieldValue[$h1fields[1]*1])) {
////									$entityID = $h1FieldValue[$h1fields[1]*1]*1;
////									$entityField = $h1fields[2];
////									switch ($h1field) {
////										case 'groups':
////											$currentGroup=Core_Entity::factory('Shop_Group')->getById($entityID);
////											switch (true) {
////												case $entityField=='property':
////													$propertyIDl1 = $h1fields[3];
////													$propertyField = $propertyFieldl1 = $h1fields[4];
////													$oProperty = $oPropertyl1 = Core_Entity::factory('Property')->getByID($propertyIDl1, FALSE);
////													$aPropertyValues = $aPropertyValuesl1 = $oPropertyl1->getValues($currentGroup->id);
////
////													if($propertyFieldl1=='property') {
////														$propertyIDl2 = $h1fields[5];
////														$oProperty = $oPropertyl2 = Core_Entity::factory('Property')->getByID($propertyIDl2, FALSE);
////														$propertyField = $propertyFieldl2 = $h1fields[6];
////														$aPropertyValues = $aPropertyValuesl2 = $oPropertyl2->getValues($aPropertyValues[0]->value);
////													}
////													switch ($oProperty->type) {
////														case 0:
////														case 1:
////															$h1s[$h1match] = $aPropertyValues[0]->$propertyField;
////															break;
////														case 3:
////															$h1s[$h1match] = $aPropertyValues[0]->list_item->$propertyField;
////															break;
////														case 5:
////															$h1s[$h1match] = $aPropertyValues[0]->informationsystem_item->$propertyField;
////															break;
////													}
////													break;
////												default:
////													isset($currentGroup->$entityField) && $h1s[$h1match] = $currentGroup->$entityField;
////											}
////											break;
////									}
////								}
////							}
////						}
////						foreach ($h1s as $h1Key => $h1) {
////							$seosSettings[0]['sps_h1_template'] = str_replace($h1Key, $h1, $seosSettings[0]['sps_h1_template']);
////						}
////					}
////					utl::p($seosSettings[0]);
////					utl::p($h1, 'h1');
////					utl::p($title, 'title');
////					utl::p($keywords, 'keywords');
////					utl::p($description, 'description');
//////					utl::p($h1matches, $seosSettings[0]['sps_h1_template']);
//				}
				//------------------------------------------------------------------------------------------------------
			}
		}
	}

	static public function processTemplate($analyzedRouting, $settings, $templateName) {
		$retValue = '';
		$h1matches = array();
		preg_match_all('/({.*?})/i', $settings[0][$templateName], $h1matches);
		if(isset($h1matches[0])) {
			$entities = array();
			$h1s = array();
			foreach ($h1matches[0] as $h1match) {
				$rH1match = preg_replace('/[{}]/', '', $h1match);
				$h1fields = explode(':', $rH1match);
				$h1field = $h1fields[0];
				$h1s[$h1match] = '';
				if(isset($analyzedRouting->$h1field)) {
					$h1FieldValue = $analyzedRouting->$h1field;
					if(isset($h1FieldValue[$h1fields[1]*1])) {
						$entityID = $h1FieldValue[$h1fields[1]*1]*1;
						$entityField = $h1fields[2];
						switch ($h1field) {
							case 'groups':
								$currentGroup=Core_Entity::factory('Shop_Group')->getById($entityID);
								switch (true) {
									case $entityField=='property':
										$propertyIDl1 = $h1fields[3];
										$propertyField = $propertyFieldl1 = $h1fields[4];
										$oProperty = $oPropertyl1 = Core_Entity::factory('Property')->getByID($propertyIDl1, FALSE);
										$aPropertyValues = $aPropertyValuesl1 = $oPropertyl1->getValues($currentGroup->id);

										if($propertyFieldl1=='property') {
											$propertyIDl2 = $h1fields[5];
											$oProperty = $oPropertyl2 = Core_Entity::factory('Property')->getByID($propertyIDl2, FALSE);
											$propertyField = $propertyFieldl2 = $h1fields[6];
											$aPropertyValues = $aPropertyValuesl2 = $oPropertyl2->getValues($aPropertyValues[0]->value);
										}
										if(isset($aPropertyValues[0])) {
											switch ($oProperty->type) {
												case 0:
												case 1:
													$h1s[$h1match] = $aPropertyValues[0]->$propertyField;
													break;
												case 3:
													$h1s[$h1match] = $aPropertyValues[0]->list_item->$propertyField;
													break;
												case 5:
													$h1s[$h1match] = $aPropertyValues[0]->informationsystem_item->$propertyField;
													break;
											}
										}
										break;
									case $entityField=='lowerPrice':
										$lowerPrice=0;
										$childGroupIDsModel = Core_QueryBuilder::select(array(Core_QueryBuilder::expression('split_string(gid.back_path, \'/\', 1)'), 'gids'))
											->from(array('getGroupsIerarchyDown', 'gid'))
											->where('url', 'LIKE', preg_replace('/page-[0-9]+\//', '', Core::$url['path']).'%')
										;
										$childGroupIDs = $childGroupIDsModel->execute()->asAssoc()->result();
										$childGroupIDs = utl::getArrayValuesFromArrays($childGroupIDs, 'gids');
										if(isset($childGroupIDs) && count($childGroupIDs)>0) {
											// select split_string(gid.back_path, '/', 1) FROM getGroupsIerarchyDown gid WHERE url LIKE '/equipment/protection/%';
											$lowerPriceModel = Core_QueryBuilder::select(array(Core_QueryBuilder::expression('MIN(price)'), 'minprice'))
												->from(array('shop_items', 'si'))
												->where('shop_group_id', 'IN', $childGroupIDs)
												->where('deleted', '=', 0)
												->where('active', '=', 1)
												->where('price', '>', 0)
												->groupBy('shop_id')
											;
											$rLowerPrice = $lowerPriceModel
												->execute()
												->asAssoc()
												->result()
											;
											isset($rLowerPrice[0]) && $h1s[$h1match] = Core_Array::get($rLowerPrice[0], 'minprice');
										}
										break;
									default:
										isset($currentGroup->$entityField) && $h1s[$h1match] = $currentGroup->$entityField;
								}
								break;
						}
					}
				}
			}
			$retValue = $settings[0][$templateName];
			foreach ($h1s as $h1Key => $h1) {
				$retValue = str_replace($h1Key, $h1, $retValue);
			}
		}

		return $retValue;
	}
}
