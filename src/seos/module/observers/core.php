<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Direct
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Seos_Observers_Core {
	const SEOS_CSS = '/admin/seos/css/seos.css';
	const SEOS_JS = '/admin/seos/js/seos.js';

	static public function onBeforeShowCss($object, $args) {
		$instance = Core_Page::instance();
		if($instance->skynet->user != FALSE) {
			$instance->css(self::SEOS_CSS);
		}
	}

	static public function onBeforeShowJs($object, $args) {
		$instance = Core_Page::instance();
		if($instance->skynet->user != FALSE) {
			$instance->js(self::SEOS_JS);
		}
	}
}