<?php
use Utils_Utl as utl;


class Seos_Observers_Site
{

	static function onBeforeGetXml($object, $args)
	{
		$seoEntity = Core_Page::instance()->seopage;
		if(!is_null($seoEntity)) {
			$object->addEntity(
				Core::factory('Core_Xml_Entity')
					->name('seos')->addEntity($seoEntity)
			);
		}
	}

	static function onBeforeMakeSitePanel($object, $args)
	{
		$instance = Core_Page::instance();
		$firstSubPanel = $object->getChildren();

		if(isset(Core_Page::instance()->response)
			&& method_exists(Core_Page::instance()->response, 'getStatus')
			&& $instance->response->getStatus()==200
			&& $instance->srcrequest->crc32path!==2043925204
		) {
//			Skynetcore_Utils::tp($instance->skynet->user);
			$foundedPart = Core_Entity::factory('Seos_Path')
				->findByPath($instance->srcrequest->path, CURRENT_SITE);
//			Skynetcore_Utils::p($foundedPart->id);
			if(isset($foundedPart->id) && $instance->seoEditor) {
				$keySpan = Core::factory('Core_Html_Entity_Span')
					->id('seosPanel')
					->add(
						Core::factory('Core_Html_Entity_Img')
							->width(16)->height(16)
							->src('/modules/seos/images/seostags.gif')
							->alt('SEOs'.$instance->skynet->user->id)
							->title('SEOs'."-{$instance->skynet->user->id}-{$instance->skynet->user->id}".(isset($instance->skynet->siteuser->id) ? '-'.$instance->skynet->siteuser->id : '') )
				);
				$dataKey = 'data-crc-id';
				$dataSeoParams = 'data-seo-params';
				$keySpan->addAllowedProperties(array($dataKey, $dataSeoParams));

				$keySpan->$dataKey = $foundedPart->id;
				$keySpan->$dataSeoParams = base64_encode(json_encode(
					array(
//						'h1' =>$instance->h1,
						'url' => $instance->srcrequest->path,
						'seoTitle' => $instance->title,
						'seoKeywords' => $instance->keywords,
						'seoDescription' => $instance->description,
					)
				));

				$firstSubPanel[0]
					->add(
						Core::factory('Core_Html_Entity_Img')
							->width(3)->height(16)
							->style("margin: -3px 5px 0")
							->src('/hostcmsfiles/images/drag_bg.gif')
					)
					->add($keySpan);
			}
		}
	}

	static function onBeforeSetTemplate($object, $args) {
		$instance = Core_Page::instance();
		if(isset(Core_Page::instance()->response)
			&& method_exists(Core_Page::instance()->response, 'getStatus')
			&& Core_Page::instance()->response->getStatus()==200
			&& $instance->srcrequest->crc32path!==2043925204
		) {
			$foundedPart = Core_Entity::factory('Seos_Path')->findByPath($instance->srcrequest->pathNoPage, CURRENT_SITE, false);

			if(isset($foundedPart->id) && $foundedPart->id>0) {
				$instance->seopage = $foundedPart;
				if(trim($foundedPart->name.'')!='') {
					$instance->h1 = $foundedPart->name;
				}
				if(trim($foundedPart->seo_title.'')!='') {
					$instance->title = $foundedPart->seo_title;
				}
				if(trim($foundedPart->seo_keywords.'')!='') {
					$instance->keywords = $foundedPart->seo_keywords;
				}
				if(trim($foundedPart->seo_description.'')!='') {
					$instance->description = $foundedPart->seo_description;
				}
				$object = NULL;
				if(is_array($instance->object)
					&& isset($instance->object['Shop_Controller_Show'])
					&& is_object($object=$instance->object['Shop_Controller_Show'])
					&& method_exists($object, 'addEntity')
				) {
					$object->addEntity($foundedPart);
				} elseif (isset($instance->object)) {
					$object = $instance->object;
				}
				if(is_object($object) && method_exists($object, 'addEntity')) {
					$object->addEntity($foundedPart);
					$object->addEntity(
						Core::factory('Core_Xml_Entity')
							->name('h1Page')->value($instance->h1)
					);
				}
			}
			Core_Event::notify('Seos_Observers_Site.onAfterSetSeosTemplate', false, array());

		}
	}
}