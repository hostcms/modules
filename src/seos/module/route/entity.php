<?php

/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 04.08.2016
 * Time: 13:57
 */
class Seos_Route_Entity extends Core_Servant_Properties
{
	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'item',
		'groups',
	);

	/**
	 * Seos_Route_Entity constructor.
	 * @param array $_allowedProperties
	 */
	public function __construct()
	{
		parent::__construct();
		$this->item=0;
		$this->groups=array();
	}


}