<?php
use Utils_Utl as utl;
/**
 * Created by PhpStorm.
 * User: MikeBorisov
 * Date: 21.06.2016
 * Time: 11:46
 */
require_once(__DIR__.'/../../../../../../../bootstrap.php');

$returnes = array();

$id = Core_Array::getGet('id', '#');
$campaignID = Core_Array::getGet('campaign_id', 0);
$parentGroupID = 0;
if($id=='#') {
	$oShops = Core_Entity::factory('Shop')->getAllBySite_Id(Core_Array::getGet('site_id', 0));
	$oCurrentLinkedGroups = NULL;
	$checkedGroups=array();

	foreach($oShops as $oShop) {
		$aShop = array(
			'id' => "s{$oShop->id}",
			'text' => $oShop->name,
			'state' => array('disabled'=>true),
			'children' => true,
		);
		if($campaignID>0) {
			$mCurrentLinkedGroups = Core_Entity::factory('Direct_Campaign_Link');
			$oCurrentLinkedGroups = $mCurrentLinkedGroups->getCurrentLinkedGroups($campaignID);
			$shopGroupIDs = array_keys(utl::getArrayKeyValuesFromArrays(utl::setArrayOfObjectsToArray($oCurrentLinkedGroups), 'shop_group_id'));
			$checkedGroups = $mCurrentLinkedGroups->getParentGroupsTree($oShop->id, $shopGroupIDs);

			$tree = utl::buildTree(utl::setArrayOfObjectsToArray($checkedGroups));
			$updatedTree = convertTreeArrayToJSONForTree($tree, $shopGroupIDs, $oShop->id);
//			utl::p($tree);
//			exit;
//			utl::p($updatedTree);
//			exit;
			if(count($updatedTree)>0) {
				$aShop['children'] = $updatedTree;
			}
		}
		$returnes[] = $aShop;
	}
} else {
	$shopID = str_replace('s', '', $id);
	$mGroups = Core_Entity::factory('Shop_Group');
	$mGroups
		->queryBuilder()
		->where('active', '=', 1)
		->where('deleted', '=', 0)
		->orderBy('name')
	;
	$matches = NULL;
	if(preg_match('/^s([0-9]*)$/', $id, $matches)) {
		$mGroups
			->queryBuilder()
			->where('shop_id', '=', $matches[1]);
		$parentGroupID = 0;
	} else {
		$parentGroupID = str_replace('g', '', $id);
	}
	$oGroups = $mGroups
		->getAllByParent_Id($parentGroupID);
	$aGroupChilds = array();
	if($parentGroupID>0) {
		$parentGroup = Core_Entity::factory('Shop_Group')->getById($parentGroupID);
		if(count($oGroups)>0) {
			$parentHref = $parentGroup->getPath();
			$aGroupChilds[] = array(
				'id' => "gr{$parentGroup->id}",
				'text' => "Родительская группа ({$parentGroup->name}), [<b>/{$parentHref}</b>]",
			);
		}
	}
	foreach ($oGroups as $oGroup) {
		$localHref = $oGroup->getPath();
		$aGroupChild = array(
			'id' => "g{$oGroup->id}",
			'text' => "{$oGroup->name}, [<b>/{$localHref}</b>]",
//			'children' => true,
		);
//		if(isset($checkedGroups) && array_key_exists($oGroup->id, $checkedGroups)) {
//			$aGroupChild['state']['selected'] = true;
//		}
		$grChilds = Core_Entity::factory('Shop_Group');
		$grChilds->queryBuilder()
			->where('active', '=', 1)
			->where('deleted', '=', 0)
			->orderBy('name');
		$grChilds=$grChilds->getAllByParent_Id($oGroup->id, FALSE);
		$grItems = array();
		if((Core_Array::getGet('adv_type', '-')=='rItems')) {
			$grItems = Core_Entity::factory('Shop_Item')->getAllByShop_Group_Id($oGroup->id);
			count($grItems)==0 && $aGroupChild['state']['disabled'] = true;
		}
		if((count($grChilds)+count($grItems))>0) {
			$aGroupChild['children'] = true;
			$aGroupChild['data'] = array('control'=>true);
		}

		$aGroupChilds[] = $aGroupChild;
	}
	if(Core_Array::getGet('adv_type', '-') == 'rItems') {
		$oItems = Core_Entity::factory('Shop_Item')->getAllByShop_Group_Id($parentGroupID);
		$aItemChilds = array();
		foreach ($oItems as $oItem) {
			$aGroupChilds[] = array(
				'id' => "i{$oItem->id}",
				'text' => $oItem->name,
				'icon' => 'glyphicon glyphicon-file',
			);
		}
	}
	$returnes = $aGroupChilds;
}

function convertTreeArrayToJSONForTree($treeItems, $checkedGroups, $shopID) {
	$returnes = array();

	foreach ($treeItems as $treeItemKey => $treeItem) {
		$parentGroup = Core_Entity::factory('Shop_Group')->getById($treeItem['id']);
		$parentHref = $parentGroup->getPath();

		//-- Рассматриваемый элемент --
		$return['id'] = 'g'.$treeItem['id'];
		$return['text'] = "{$treeItem['name']}, [<b>/{$parentHref}</b>]";
		if (!isset($treeItem['childs'])) {
			$return['state']['selected'] = true;
		}

		if (isset($treeItem['childs'])) {
			$childsSelected = convertTreeArrayToJSONForTree($treeItem['childs'], $checkedGroups, $shopID);
			$allChildsSelected = array();
			$mCurrentLevelGroups = Core_Entity::factory('Shop_Group');
			$mCurrentLevelGroups
				->queryBuilder()
				->where('shop_id', '=', $shopID)
				->where('parent_id', '=', $treeItem['id'])
				->where('active', '=', 1)
				->where('deleted', '=', 0)
				->orderBy('name')
			;
			$aCurrentLevelGroups = $mCurrentLevelGroups->findAll(FALSE);
			if(count($aCurrentLevelGroups)>0) {
				$parentGroup = Core_Entity::factory('Shop_Group')->getById($treeItem['id']);
				$parentHref = $parentGroup->getPath();
				$parentChild = array(
					'id' => "gr{$parentGroup->id}",
					'text' => "Родительская группа ({$parentGroup->name}), [<b>/{$parentHref}</b>]",
				);
				if (array_search($treeItem['id'], $checkedGroups)!==false) {
					$parentChild['state']['selected'] = true;
				}

				$allChildsSelected[] = $parentChild;
			}
			foreach ($aCurrentLevelGroups as $aCurrentLevelKey=>$aCurrentLevelGroup) {
				$parentGroup = Core_Entity::factory('Shop_Group')->getById($aCurrentLevelGroup->id);
				$parentHref = $parentGroup->getPath();

				$return3['id'] = 'g'.$aCurrentLevelGroup->id;
				$return3['text'] = "{$aCurrentLevelGroup->name}, [<b>/{$parentHref}</b>]";
				$selectedItem = $return3;
				foreach ($childsSelected as $childsSel) {
					if('g'.$aCurrentLevelGroup->id==$childsSel['id']) {
						$selectedItem = $childsSel;
					}
				}
				$allChildsSelected[] = $selectedItem;
			}
			$return['children'] = $allChildsSelected;
		}
		$returnes[] = $return;
		$return = array();
	}

	if(isset($treeItems[0]) && $treeItems[0]['parent_id']==0) {
		$rets = array_keys(utl::getArrayKeyValuesFromArrays($returnes, 'id'));
		$tmpReturnes = utl::getArrayKeyValuesFromArrays($returnes, 'id');
		$returnes = array();

		$mCurrentLevelGroups = Core_Entity::factory('Shop_Group');
		$mCurrentLevelGroups
			->queryBuilder()
			->where('shop_id', '=', $shopID)
			->where('parent_id', '=', 0)
			->where('active', '=', 1)
			->where('deleted', '=', 0)
			->orderBy('name')
		;
		$aCurrentLevelGroups = $mCurrentLevelGroups->findAll(FALSE);
		foreach ($aCurrentLevelGroups as $aCurrentLevelKey=>$aCurrentLevelGroup) {
			$parentGroup = Core_Entity::factory('Shop_Group')->getById($aCurrentLevelGroup->id);
			$parentHref = $parentGroup->getPath();
			$return0 = array(
				'id' => 'g'.$aCurrentLevelGroup->id,
				'text' => "{$aCurrentLevelGroup->name}, [<b>/{$parentHref}</b>]",
				'children' => true,
			);
			if( array_search('g'.$aCurrentLevelGroup->id, $rets)===false) {
				$returnes[] = $return0;
			} else {
				$returnes[] = $tmpReturnes['g'.$aCurrentLevelGroup->id][0];
			}
		}
	}

	return $returnes;
}
// or array version
//function buildTree($items) {
//	$childs = array();
//	foreach($items as &$item) {
//		$item['checked'] = true;
//		$childs[$item['parent_id']][] = &$item;
//	}
//	unset($item);
//	foreach($items as &$item) {
//		if (isset($childs[$item['id']])) {
//			$item['childs'] = $childs[$item['id']];
//		}
//	}
//	return isset($childs[0]) ? $childs[0] : $childs;
//}

header('Content-type: application/json; charset=utf-8');
echo json_encode($returnes);
exit;
