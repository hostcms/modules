<?php
// echo dirname(dirname(__DIR__));
// echo readlink(__FILE__);
// echo dirname(__FILE__)."\n";
// echo __DIR__.'/../../../../../../bootstrap.php'."\n";
// echo getcwd();
// exit;
require_once(__DIR__.'/../../../../../../../bootstrap.php');

/**
 * Direct.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */

Core_Auth::authorization('direct');

// Код формы
$sAdminFormAction = '/admin/direct/template/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGUID('3A83219E-852B-44CD-ADF5-C921427C229A');
$iAdmin_Form_Id = $oAdmin_Form->id;

// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Direct_Template.model_name'))
	->pageTitle(Core::_('Direct_Template.model_name'))
;
//-- Элементы меню -----------------------------------------------------------------------------------------------------
$oMenu = Admin_Form_Entity::factory('Menus');
$oMenu->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Direct_Template.actions'))
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Direct_Template.add_template'))
				->icon('fa fa-plus')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($sAdminFormAction, 'edit', NULL, 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($sAdminFormAction, 'edit', NULL, 0, 0)
				)
		)
);
// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oMenu);
//-- Действия ----------------------------------------------------------------------------------------------------------
// Действие "Редактировать"
$oEditAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('edit');

if ($oEditAction && $oAdmin_Form_Controller->getAction() == 'edit')
{
	$oEditController = Admin_Form_Action_Controller::factory(
		'Direct_Template_Controller_Edit', $oEditAction
	);
	$oAdmin_Form_Controller->addAction($oEditController);
}
//----------------------------------------------------------------------------------------------------------------------
$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Direct_Template'));
// Ограничение источника 0 по родительской группе
$oAdmin_Form_Dataset->addCondition(
	array('where' =>
		array('site_id', '=', CURRENT_SITE)
	)
);
$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);
$oAdmin_Form_Controller->execute();
