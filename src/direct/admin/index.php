<?php
// echo dirname(dirname(__DIR__));
// echo readlink(__FILE__);
// echo dirname(__FILE__)."\n";
// echo __DIR__.'/../../../../../../bootstrap.php'."\n";
// echo getcwd();
// exit;
require_once(__DIR__.'/../../../../../../bootstrap.php');

/**
 * Direct.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */

Core_Auth::authorization('direct');

// Код формы
$sAdminFormAction = '/admin/direct/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGUID('16CD2C5D-9BC1-937E-4FC7-AFCA22EA0FA4');
$iAdmin_Form_Id = $oAdmin_Form->id;

$oShop = Core_Entity::factory('Shop', Core_Array::getGet('shop_id', 0));

//// Контроллер формы
$oAdmin_Form_Controller = Direct_Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Direct.model_name'))
	->pageTitle(Core::_('Direct.model_name'))
;
//
//// Меню формы
//
//////$additionalParams = "shop_id={$oShop->id}&shop_group_id={$oShopGroup->id}";
////$model = Core_Entity::factory('Direct_Path');
////$f = $model->findAll();
$profilesListHref = '/admin/direct/profile/index.php';
$templatesListHref = '/admin/direct/template/index.php';
$campaignsListHref = '/admin/direct/campaign/index.php';
//-- Элементы меню -----------------------------------------------------------------------------------------------------
$oMenu = Admin_Form_Entity::factory('Menus');
$oMenu->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Direct.campaigns'))
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Direct.campaigns_list'))
				->icon('fa fa-list')
//				->img('/admin/images/page_add.gif')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($campaignsListHref, '', NULL, 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($campaignsListHref, '', NULL, 0, 0)
				)
		)
)->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Direct.profiles'))
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Direct.profiles_list'))
				->icon('fa fa-list')
//				->img('/admin/images/page_add.gif')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($profilesListHref, '', NULL, 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($profilesListHref, '', NULL, 0, 0)
				)
		)
)->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Direct.templates'))
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Direct.templates_list'))
				->icon('fa fa-list')
//				->img('/admin/images/page_add.gif')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($templatesListHref, '', NULL, 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($templatesListHref, '', NULL, 0, 0)
				)
		)
);
//	Admin_Form_Entity::factory('Menu')
//		->name(Core::_('Direct.links_items'))
////		->icon('fa fa-file-text-o')
//		->add(
//			Admin_Form_Entity::factory('Menu')
//				->name(Core::_('Direct.links_items_create_company'))
//				->icon('fa fa-plus')
////				->img('/admin/images/page_add.gif')
//				->href(
//					$oAdmin_Form_Controller->getAdminActionLoadHref($oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0)
//				)
//				->onclick(
//					$oAdmin_Form_Controller->getAdminActionLoadAjax($oAdmin_Form_Controller->getPath(), 'edit', NULL, 0, 0)
//				)
//		)
//		->add(
//			Admin_Form_Entity::factory('Menu')
//				->name(Core::_('Direct.links_items_profiles'))
////				->icon('fa fa-plus')
////				->img('/admin/images/page_add.gif')
//				->href('return false;')
//				->onclick('return false;')
//		)
//		->add(
//			Admin_Form_Entity::factory('Menu')
//				->name(Core::_('Direct.links_items_templates'))
////				->icon('fa fa-plus')
////				->img('/admin/images/page_add.gif')
//				->href('return false;')
//				->onclick('return false;')
//		)
//		->add(
//			Admin_Form_Entity::factory('Menu')
//				->name(Core::_('Direct.links_items_campaign'))
////				->icon('fa fa-plus')
////				->img('/admin/images/page_add.gif')
//				->href('return false;')
//				->onclick('return false;')
//		)
//		->add(
//			Admin_Form_Entity::factory('Menu')
//				->name(Core::_('Direct.links_items_reports'))
////				->icon('fa fa-plus')
////				->img('/admin/images/page_add.gif')
//				->href('return false;')
//				->onclick('return false;')
//		)
//		->add(
//			Admin_Form_Entity::factory('Menu')
//				->name(Core::_('Direct.links_items_system_settings'))
////				->icon('fa fa-plus')
////				->img('/admin/images/page_add.gif')
//				->href('return false;')
//				->onclick('return false;')
//		)

// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oMenu);
//-- Меню --------------------------------------------------------------------------------------------------------------

// Хлебные крошки
$oBreadcrumbs = Admin_Form_Entity::factory('Breadcrumbs');

//// Первая крошка на список магазинов
//$oBreadcrumbs->add(
//	Admin_Form_Entity::factory('Breadcrumb')
//		->name(Core::_('Direct.menu'))
//		->href($oAdmin_Form_Controller->getAdminLoadHref(
//			'/admin/direct/index.php', NULL, NULL, ''
//		))
//		->onclick($oAdmin_Form_Controller->getAdminLoadAjax(
//			'/admin/direct/index.php', NULL, NULL, ''
//		))
//);
//
//-- Действия ----------------------------------------------------------------------------------------------------------
// Действие "Редактировать"
$oEditAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('edit');

if ($oEditAction && $oAdmin_Form_Controller->getAction() == 'edit')
{
	$oEditController = Admin_Form_Action_Controller::factory(
		'Direct_Controller_Edit', $oEditAction
	);
	$oEditController->addEntity($oBreadcrumbs);
	$oAdmin_Form_Controller->addAction($oEditController);
}
//----------------------------------------------------------------------------------------------------------------------
$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Direct_Profile'));
// Ограничение источника 0 по родительской группе
$oAdmin_Form_Dataset->addCondition(
	array('where' =>
		array('site_id', '=', CURRENT_SITE)
	)
);
//$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);
// Показ формы
ob_start();
?>
	<div class="row direct">
		<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="admin-table-name">Краткие данные</div>
					<table class="admin-table table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th>Название</th>
								<th>Название</th>
								<th>Название</th>
								<th>Название</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td></td>
								<td></td>
								<td></td>
								<td></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					<div class="admin-table-name">Общая сводка по всем компаниям</div>
					<table class="admin-table table table-bordered table-hover table-striped">
						<thead>
							<tr>
								<th>Период</th>
								<th>Показы</th>
								<th>Клики</th>
								<th>CTR (%)</th>
								<th>Расход, всего (руб.)</th>
								<th>Ср. цена клика (руб.)</th>
								<th>Ср. расход (руб.)</th>
							</tr>
						</thead>
						<tbody>
						<tr>
							<td><?php echo date('01.m.Y')?>-<?php echo date('d.m.Y')?></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
							<td></td>
						</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
<?php
$oAdmin_Form_Controller->addEntity(
	Admin_Form_Entity::factory('Code')
		->html(ob_get_clean())
);
$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);
$oAdmin_Form_Controller->execute();