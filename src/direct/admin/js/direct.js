$(document).on('change', '.js-create_cam_radio', function(){
	var id = this.value,
		$activeTab = $('#'+id),
		$activeInputs = $activeTab.find('input'),
		$hiddenInputs = $activeTab.siblings().find('input');
	for(var i=0; i<$activeInputs.length; i++){
		$activeInputs[i].disabled = false;
	}
	for(var i=0; i<$hiddenInputs.length; i++){
		$hiddenInputs[i].disabled = true;
	}
	$activeTab.show();
	$activeTab.siblings().hide();
	checkChekboxes($('.c-fieldset [type=checkbox]'));
	$('#c-tree').jstree("refresh");
});

$(document).on('keyup', '#tree-search', function(e){
	e.preventDefault();
	var _this = $(this);
	if(e.keyCode != 13){
		$('#c-tree').jstree(true).search(_this.val());
	}
	return false;
});


$(document).on('change', '.c-fieldset [type=checkbox]', function(){
	var _this = $(this),
		$parent = _this.closest('.form-group'),
		$input = $parent.children('input');
	if($input.length){
		$input[0].disabled = !this.checked;
	}
});
function checkChekboxes(array){
	array.each(function(){
		var _this = $(this),
			$parent = _this.closest('.form-group'),
			$input = $parent.children('input');
		if($input.length){
			$input[0].disabled = !this.checked;
		}
	});
}