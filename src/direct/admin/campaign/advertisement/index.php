<?php
require_once(__DIR__.'/../../../../../../../../bootstrap.php');

/**
 * Direct.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */

Core_Auth::authorization('direct');

// Код формы
$sAdminFormAction = '/admin/direct/campaign/advertisement/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGUID('914320EC-8ED6-423C-6195-73DB8BED9B1C');
$iAdmin_Form_Id = $oAdmin_Form->id;
$campaignID = Core_Array::getGet('campaign_id', 0);
$campaign = Core_Entity::factory('Direct_Campaign', $campaignID);
$siteID = Core_Array::getGet('site_id', 0);

// Контроллер формы
$oAdmin_Form_Controller = Direct_Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Direct_Campaign_Advertisement.model_name'))
	->pageTitle(Core::_('Direct_Campaign_Advertisement.model_name'))
;
//-- Элементы меню -----------------------------------------------------------------------------------------------------
$oMenu = Admin_Form_Entity::factory('Menus');
$oMenu->add(
	Admin_Form_Entity::factory('Menu')
		->name(Core::_('Direct_Campaign.actions'))
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Direct_Campaign_Advertisement.add_advertisement'))
				->icon('fa fa-plus')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($sAdminFormAction, 'edit', NULL, 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($sAdminFormAction, 'edit', NULL, 0, 0)
				)
		)
);
//-- Хлебные крошки ----------------------------------------------------------------------------------------------------
// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oMenu);

// Элементы строки навигации
$oAdmin_Form_Entity_Breadcrumbs = Admin_Form_Entity::factory('Breadcrumbs');
// Путь к контроллеру формы разделов информационных систем
$sDirectPath = '/admin/direct/index.php';
$sCampaignPath = '/admin/direct/campaign/index.php';
$sCampaignAdditionalParams = "site_id={$siteID}&campaign_id={$campaignID}&profile_id={$campaign->direct_profile_id}";
// Элементы строки навигации
$oAdmin_Form_Entity_Breadcrumbs->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Direct.menu'))
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref($sDirectPath, NULL, NULL, '')
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax($sDirectPath, NULL, NULL, '')
		)
)->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Direct.campaigns'))
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref($sCampaignPath, NULL, NULL, '')
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax($sCampaignPath, NULL, NULL, '')
		)
);
if(($profID=Core_Array::getGet('profile_id', 0)*1)>0) {
	$profile = Core_Entity::factory('Direct_Profile', $profID);
	$oAdmin_Form_Entity_Breadcrumbs->add(
		Admin_Form_Entity::factory('Breadcrumb')
			->name($profile->name)
			->href(
				$oAdmin_Form_Controller->getAdminLoadHref($sCampaignPath, NULL, NULL, "site_id=".CURRENT_SITE."&profile_id={$profID}")
			)
			->onclick(
				$oAdmin_Form_Controller->getAdminLoadAjax($sCampaignPath, NULL, NULL, "site_id=".CURRENT_SITE."&profile_id={$profID}")
			)
	);
}
$oAdmin_Form_Entity_Breadcrumbs->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Direct_Campaign.advertisements'))
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref($sAdminFormAction, NULL, NULL, $sCampaignAdditionalParams)
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax($sAdminFormAction, NULL, NULL, $sCampaignAdditionalParams)
		)
);

// Добавляем все хлебные крошки контроллеру
$oAdmin_Form_Controller->addEntity($oAdmin_Form_Entity_Breadcrumbs);

//-- Действия ----------------------------------------------------------------------------------------------------------
// Действие "Редактировать"
$oEditAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('edit');

if ($oEditAction && $oAdmin_Form_Controller->getAction() == 'edit')
{
	$oEditController = Admin_Form_Action_Controller::factory(
		'Direct_Campaign_Advertisement_Controller_Edit', $oEditAction
	);
	$oEditController
		->addEntity($oAdmin_Form_Entity_Breadcrumbs);

	$oAdmin_Form_Controller->addAction($oEditController);
}
//----------------------------------------------------------------------------------------------------------------------
$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Direct_Campaign_Advertisement'));
// Ограничение источника 0 по родительской группе
$oAdmin_Form_Dataset->addCondition(
	array('where' =>
		array('direct_campaign_id', '=', $campaignID)
	)
);
$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);
$oAdmin_Form_Controller->execute();
