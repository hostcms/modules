<?php
use Utils_Utl as utl;
require_once(__DIR__.'/../../../../../../../bootstrap.php');

/**
 * Direct.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */

Core_Auth::authorization('direct');

// Код формы
$sAdminFormAction = '/admin/direct/campaign/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGUID('2388B1EC-ECB0-18B9-E605-FD59BB63CE30');
$iAdmin_Form_Id = $oAdmin_Form->id;

// Контроллер формы
$oAdmin_Form_Controller = Direct_Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Direct_Campaign.model_name'))
	->pageTitle(Core::_('Direct_Campaign.model_name'))
;
//-- Элементы меню -----------------------------------------------------------------------------------------------------
$oMenu = Admin_Form_Entity::factory('Menus');
$oMenuL1Items = Admin_Form_Entity::factory('Menu')
	->name(Core::_('Direct_Campaign.actions'));
$oMenuL1Items
	->add(
		Admin_Form_Entity::factory('Menu')
			->name(Core::_('Direct_Campaign.add_campaign'))
			->icon('fa fa-plus')
			->href(
				$oAdmin_Form_Controller->getAdminActionLoadHref($sAdminFormAction, 'edit', NULL, 0, 0)
			)
			->onclick(
				$oAdmin_Form_Controller->getAdminActionLoadAjax($sAdminFormAction, 'edit', NULL, 0, 0)
			)
	);
if(($profID=Core_Array::getGet('profile_id', 0)*1)>0) {
	$oMenuL1Items
		->add(
			Admin_Form_Entity::factory('Menu')
				->name(Core::_('Direct_Campaign.get_campaigns'))
				->icon('fa fa-plus')
				->href(
					$oAdmin_Form_Controller->getAdminActionLoadHref($sAdminFormAction, 'syncampaigns', "profile_id={$profID}", 0, 0)
				)
				->onclick(
					$oAdmin_Form_Controller->getAdminActionLoadAjax($sAdminFormAction, 'syncampaigns', "profile_id={$profID}", 0, 0)
				)
		)
	;
}
$oMenu->add($oMenuL1Items);
//-- Хлебные крошки ----------------------------------------------------------------------------------------------------
// Добавляем все меню контроллеру
$oAdmin_Form_Controller->addEntity($oMenu);

// Элементы строки навигации
$oAdmin_Form_Entity_Breadcrumbs = Admin_Form_Entity::factory('Breadcrumbs');
// Путь к контроллеру формы разделов информационных систем
$sDirectPath = '/admin/direct/index.php';
// Элементы строки навигации
$oAdmin_Form_Entity_Breadcrumbs->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Direct.menu'))
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref($sDirectPath, NULL, NULL, '')
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax($sDirectPath, NULL, NULL, '')
		)
)->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Direct.campaigns'))
		->href(
			$oAdmin_Form_Controller->getAdminLoadHref($sAdminFormAction, NULL, NULL, '')
		)
		->onclick(
			$oAdmin_Form_Controller->getAdminLoadAjax($sAdminFormAction, NULL, NULL, '')
		)
);
if($profID>0) {
	$profile = Core_Entity::factory('Direct_Profile', $profID);
	$oAdmin_Form_Entity_Breadcrumbs->add(
		Admin_Form_Entity::factory('Breadcrumb')
			->name($profile->name)
			->href(
				$oAdmin_Form_Controller->getAdminLoadHref($sAdminFormAction, NULL, NULL, "site_id=".CURRENT_SITE."&profile_id={$profID}")
			)
			->onclick(
				$oAdmin_Form_Controller->getAdminLoadAjax($sAdminFormAction, NULL, NULL, "site_id=".CURRENT_SITE."&profile_id={$profID}")
			)
	);
}
// Добавляем все хлебные крошки контроллеру
$oAdmin_Form_Controller->addEntity($oAdmin_Form_Entity_Breadcrumbs);

//-- Действия ----------------------------------------------------------------------------------------------------------
// Действие "Редактировать"
$oEditAction = Core_Entity::factory('Admin_Form', $iAdmin_Form_Id)
	->Admin_Form_Actions
	->getByName('edit');

if ($oEditAction && $oAdmin_Form_Controller->getAction() == 'edit')
{
	$oEditController = Admin_Form_Action_Controller::factory(
		'Direct_Campaign_Controller_Edit', $oEditAction
	);
	$oEditController
		->addEntity($oAdmin_Form_Entity_Breadcrumbs);

	$oAdmin_Form_Controller->addAction($oEditController);
}
//----------------------------------------------------------------------------------------------------------------------
$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Direct_Campaign'));
// Ограничение источника 0 по родительской группе
$oAdmin_Form_Dataset->addCondition(
	array('where' =>
		array('site_id', '=', CURRENT_SITE)
	)
);
if($profID>0) {
	$oAdmin_Form_Dataset->addCondition(
		array('where' =>
			array('direct_profile_id', '=', $profID)
		)
	);
}
$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);
$oAdmin_Form_Controller->execute();
