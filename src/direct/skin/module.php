<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Direct.
 *
 * @package HostCMS 6\Skin
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © Михаил Борисов
 */
class Skin_Bootstrap_Module_Direct_Module extends Direct_Module
{
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Core_Event::attach('Skin_Bootstrap.onLoadSkinConfig', array('Skin_Bootstrap_Module_Direct_Module', 'onLoadSkinConfig'));
	}

	static public function onLoadSkinConfig($object, $args)
	{
		// Skin_Bootstrap_Admin_Form_Controller
		// Load config
		$aConfig = $object->getConfig();

		if(!isset($aConfig['adminMenu']['seo'])) {
			$aConfig['adminMenu']['seo'] = array(
				'ico' => 'fa fa-wrench',
				'caption' => Core::_('Skin_Bootstrap.admin_menu_seo'),
			);
		}

		$aConfig['adminMenu']['seo']['modules'][] = 'direct';

		// Set new config
		$object->setConfig($aConfig);
	}
}