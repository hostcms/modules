<?php
/**
 * SEO администрирование.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	'undefined' => 'Неопределено',
	'menu' => 'Директ.Реклама',
	'model_name' => 'Директ.Реклама',
	'profiles' => 'Профили',
	'profiles_list' => 'Список профилей',
	'templates' => 'Шаблоны',
	'templates_list' => 'Список шаблонов',
	'campaigns' => 'Кампании',
	'campaigns_list' => 'Список Кампаний',
//	'add_campaign_title' => 'Добавить кампанию',
//	'edit_campaign_title' => 'Редактировать кампанию',
//
//	'links_items' => 'Меню',
//	'links_items_create_company' => 'Создать кампанию',
//	'links_items_templates' => 'Шаблоны',
//	'links_items_campaign' => 'Список кампаний',
//	'links_items_reports' => 'Отчеты',
//	'links_items_system_settings' => 'Настройки системы',
);