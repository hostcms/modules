<?php
use Utils_Utl as utl;
use Unirest\Request as rest;

class Direct_Drivers_Yandex_Driver extends Core_Servant_Properties
{
	private $_sandboxApiAddr = 'https://api-sandbox.direct.yandex.ru/json/v5/';
	private $_realApiAddr = 'https://api.direct.yandex.com/json/v5/';

	/**
	 * Allowed object properties
	 * @var array
	 */
	protected $_allowedProperties = array(
		'apiAddr',
		'token',
		'headers',
		'client',
	);

	/**
	 * Direct_Drivers_Yandex_Driver constructor.
	 */
	public function __construct($token, $client='', $sandboxMode=true)
	{
		parent::__construct();

		$this->apiAddr = $sandboxMode ? $this->_sandboxApiAddr : $this->_realApiAddr;
		$this->token = $token;
		$this->client = $client;
		$this->headers = array(
			  'Accept' => 'application/json'
			, 'Accept-Language' => 'ru'
			, 'Authorization' => 'Bearer '.$this->token
		);
		if($this->client != '')  {
			$headers = $this->headers;
			$headers['Client-Login'] = $client;
			$this->headers = $headers;
		}
	}

	public static function createInstance($token, $client='', $sandboxMode=true) {
		return new self($token, $client, $sandboxMode);
	}

	public function callDirectApiMethod($entity, $method, $params) {
		$entityName = $entity;
		$entity = strtolower($entity);
		$data = array(
			"method" => $method,
			"params" => $params,
		);
		$body = rest\Body::json($data);
		$response = rest::post($this->apiAddr.$entity, $this->headers, $body);
//		utl::p($this->apiAddr.$entity);
//		utl::p($this->headers);
//		utl::p($body);
//		utl::p($response);
		if($response->code==200) {
			if(isset($response->body->error)) {
				throw new Core_Exception("Yandex API error:<br/><b>%ecode</b> (%eid)<br/>%err<br/>%edetail", array(
					  '%ecode'=>$response->body->error->error_code
					, '%eid'=>$response->body->error->request_id
					, '%err'=>$response->body->error->error_string
					, '%edetail'=>$response->body->error->error_detail
				), 0, false);
			} else {
				if($method=='get') {
					if (isset($response->body->result->$entityName)) {
						return $response->body->result->$entityName;
					} else {
						throw new Core_Exception("Неизвестная сущность %erentity", array('%erentity' => $entityName), 0, false);
					}
				}
				return $response->body->result;
			}
		} else {
			throw new Core_Exception($response->body, array(), 0, false);
		}
	}

	public function getCampaigns($addFieldNames=array(), $sectionCriteria=array(
		"Types" => array("TEXT_CAMPAIGN")
	)) {
		$fieldNames = array(
			  'Id'
			, 'Name'
			, 'StartDate'
			, 'Statistics'
			, 'State'
			, 'Status'
			, 'StatusPayment'
			, 'StatusClarification'
//					  'BlockedIps', 'ExcludedSites', 'Currency', 'DailyBudget', 'Notification', 'EndDate', 'Funds'
//					, 'ClientInfo', 'Id', 'Name', 'NegativeKeywords', 'RepresentedBy', 'StartDate', 'Statistics'
//					, 'State', 'Status', 'StatusPayment', 'StatusClarification', 'SourceId', 'TimeTargeting'
//					, 'TimeZone', 'Type'
		);
		$fieldNames = array_merge($fieldNames, $addFieldNames);

		return $this->callDirectApiMethod('Campaigns', 'get', array(
			"SelectionCriteria" => $sectionCriteria,
			"FieldNames" => $fieldNames,
		));
	}

	public function getAdGroups($addFieldNames=array(), $sectionCriteria=array(
		"CampaignIds" => array(0)
	)) {
		$fieldNames = array(
			  'Id', 'CampaignId', 'Status', 'Name', 'RegionIds', 'NegativeKeywords', 'TrackingParams', 'Type'
		);
		$fieldNames = array_merge($fieldNames, $addFieldNames);

		return $this->callDirectApiMethod('AdGroups', 'get', array(
			"SelectionCriteria" => $sectionCriteria,
			"FieldNames" => $fieldNames,
//			"MobileAppAdGroupFieldNames" => array("StoreUrl", "TargetDeviceType", "TargetCarrier", "TargetOperatingSystemVersion", "AppIconModeration", "AppAvailabilityStatus", "AppOperatingSystemType"),
//			"DynamicTextAdGroupFieldNames" => array("DomainUrl"),
		));
	}

	public function suspendAds($ads=array()) {
		return $this->callDirectApiMethod('Ads', 'suspend', array(
			"SelectionCriteria" => array('Ids'=>$ads)
		));
	}

	public function resumeAds($ads=array()) {
		return $this->callDirectApiMethod('Ads', 'resume', array(
			"SelectionCriteria" => array('Ids'=>$ads)
		));
	}

	public function getAdsByGroup($addFieldNames=array(), $sectionCriteria=array(
		"AdGroupIds" => array(0)
	)) {
		$fieldNames = array(
			"AdCategories", "AgeLabel", "AdGroupId", "CampaignId", "Id", "State", "Status", "StatusClarification", "Type"
		);
		$fieldNames = array_merge($fieldNames, $addFieldNames);

		return $this->callDirectApiMethod('Ads', 'get', array(
			"SelectionCriteria" => $sectionCriteria,
			"FieldNames" => $fieldNames,
			"TextAdFieldNames" => array(
				"Title", "Text", "Href", "Mobile", "DisplayDomain", "DisplayUrlPath", "DisplayUrlPathModeration", "VCardId", "VCardModeration", "SitelinkSetId", "SitelinksModeration", "AdImageHash", "AdImageModeration", "AdExtensions"
			),
//			"MobileAppAdGroupFieldNames" => array("StoreUrl", "TargetDeviceType", "TargetCarrier", "TargetOperatingSystemVersion", "AppIconModeration", "AppAvailabilityStatus", "AppOperatingSystemType"),
//			"DynamicTextAdGroupFieldNames" => array("DomainUrl"),
		));
	}
}