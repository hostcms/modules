<?php
use Utils_Utl as utl;
use HCMS\Shop\Groups as gr;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Directs.
 *
 * @package HostCMS 6\Direct
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Campaign_Link_Model extends Core_Entity {

	const TL_GROUP = 'group';
	const TL_ITEM = 'item';
	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	/**
	 * One-to-many or many-to-many relations
	 * @var array
	 */
	protected $_hasMany = array(
		'shop_group' => array(),
	);

	public function getCurrentLinkedGroups($campaign_id) {
		$this
			->queryBuilder()
			->where('type', '=', 'group')
		;
		$oCurrentLinkedGroups = $this->getAllByCampaign_Id($campaign_id, FALSE);
		return $oCurrentLinkedGroups;
	}

	public function getParentGroupsTree($shopID, $groupIDs = array()) {
		$allTreeIDs = array();
		foreach ($groupIDs as $groupID) {
			$currentGroup = Core_Entity::factory('Shop_Group')->getById($groupID);
			$parentGroups = gr::createInstance($currentGroup->shop_id);
			$allParents = $parentGroups->getAllParenGroups($currentGroup->id, true);
			$allTreeIDs = array_merge($allTreeIDs, array_keys(utl::setArrayOfObjectsToArray($allParents)));
		}
		$allGroups = array();
		if(count($allTreeIDs)>0) {
			$mAllGroups = Core_Entity::factory('Shop_Group');
			$mAllGroups
				->queryBuilder()
				->where('id', 'IN', $allTreeIDs)
				->where('shop_id', '=', $shopID)
				->orderBy('parent_id')
				->orderBy('id')
			;
			$aAllGroups = $mAllGroups->findAll(FALSE);
			foreach ($aAllGroups as $aAllGroup) {
				$allGroups[] = $aAllGroup;
			}
		}
		return $allGroups;
	}
}