<?php
use Utils_Utl as utl;
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Advertisement.
 *
 * @package HostCMS 6\Advertisement
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Campaign_Controller_Edit extends Utils_Admin_Form_Action_Controller_Type_Edit
{
	/**
	 * Set object
	 * @param object $object object
	 * @return self
	 */
	public function setObject($object)
	{
		parent::setObject($object);

		$this->title(
			$this->_object->id
				? Core::_('Direct_Campaign.edit_campaign')
				: Core::_('Direct_Campaign.add_campaign')
		);
		$this->addTabBefore($yaTab=Admin_Form_Entity::factory('Tab')
			->caption('Яндекс')
			->name('YaFields'), $this->_additionalTab);
		foreach($this->_additionalTab->getFields() as $field) {
			if(isset($field->getChildren()[0]->name) && preg_match('/^ya_/', $field->getChildren()[0]->name)>0) {
				$this->getField($field->getChildren()[0]->name)->readonly('readonly');
				$this->_additionalTab->move($field, $yaTab);
			}
		}

		$this->_mainTab
			->add($oMainRow1 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow2 = Admin_Form_Entity::factory('Div')->class('row'))
		;
//
//		$oMainRow2
//			->add($oMainColLeft = Admin_Form_Entity::factory('Div')->class('col-lg-4 col-md-3 col-sm-12 col-xs-12'))
//			->add($oMainColRight = Admin_Form_Entity::factory('Div')->class('col-lg-8 col-md-9 col-sm-12 col-xs-12'))
//		;
//		$oMainColRight
//			->add($oMainColRightRow1 = Admin_Form_Entity::factory('Div')->class('row'))
//			->add($oMainColRightRow2 = Admin_Form_Entity::factory('Div')->class('row'))
//		;
		//-- Профиль --
		$profiles = Core_Entity::factory('Direct_Profile');
		$profiles
			->queryBuilder()
			->where('site_id', '=', CURRENT_SITE);
		$profileID = $this->createSelect($profiles, 'Direct_campaign', 'direct_profile_id', $this->_object->direct_profile_id, array('class' => 'form-group col-lg-3 col-md-3 col-sm-12 col-xs-12'));
		$oMainRow1->add($profileID);
		//-- Наименование --
		$this->_additionalTab->delete($this->getField('name'));
		$oMainRow1
			->add($this->getField('name')->format(array('minlen' => array('value' => 4)))->divAttr(array('class'=>'form-group col-lg-9 col-md-9 col-sm-12 col-xs-12')));
//		//-- Шаблон группы --
//		$templateGroups = Core_Entity::factory('Direct_Template');
//		$templateGroups
//			->queryBuilder()
//			->where('site_id', '=', CURRENT_SITE)
//			->where('tmpl_type', '=', 1)
//		;
//		$templateGroupsID = $this->createSelect($templateGroups, 'Direct_campaign', 'tmpl_group_id', $this->_object->tmpl_group_id, array('class' => 'form-group col-lg-3 col-md-6 col-sm-12 col-xs-12'));
//		$oMainColRightRow1->add($templateGroupsID);
//		//-- Шаблон заголовка --
//		$templateHeads = Core_Entity::factory('Direct_Template');
//		$templateHeads
//			->queryBuilder()
//			->where('site_id', '=', CURRENT_SITE)
//			->where('tmpl_type', '=', 2)
//		;
//		$templateHeadsID = $this->createSelect($templateHeads, 'Direct_campaign', 'tmpl_head_id', $this->_object->tmpl_head_id, array('class' => 'form-group col-lg-3 col-md-6 col-sm-12 col-xs-12'));
//		$oMainColRightRow1->add($templateHeadsID);
//		//-- Шаблон текста --
//		$templateTexts = Core_Entity::factory('Direct_Template');
//		$templateTexts
//			->queryBuilder()
//			->where('site_id', '=', CURRENT_SITE)
//			->where('tmpl_type', '=', 3)
//		;
//		$templateTextsID = $this->createSelect($templateTexts, 'Direct_campaign', 'tmpl_text_id', $this->_object->tmpl_text_id, array('class' => 'form-group col-lg-3 col-md-6 col-sm-12 col-xs-12'));
//		$oMainColRightRow1->add($templateTextsID);
//		//-- Шаблон ключей --
//		$templateKeys = Core_Entity::factory('Direct_Template');
//		$templateKeys
//			->queryBuilder()
//			->where('site_id', '=', CURRENT_SITE)
//			->where('tmpl_type', '=', 4)
//		;
//		$templateKeysID = $this->createSelect($templateKeys, 'Direct_campaign', 'tmpl_key_id', $this->_object->tmpl_key_id, array('class' => 'form-group col-lg-3 col-md-6 col-sm-12 col-xs-12'));
//		$oMainColRightRow1->add($templateKeysID);
//
//		$settings = array(
//			'rGroups' => array(
//				'k1001' => array('Автоматически выключать объявления при нулевом остатке на складе у всех товаров', NULL),
//				'k1002' => array('Автоматически включать объявления при положительном остатке на складе у минимального количества товаров в группе', NULL),
//				'k1003' => array('Создавать объявления только для групп в которых есть товары с остатком на складе больше, чем', 0),
//				'k1004' => array('Создавать объявления при наличие товара в группе стоимостью выше', 10),
//				'k1005' => array('Создавать объявления только при наличие следующего минимального количества товаров в группе', 1),
//				'k1006' => array('Создавать объявления только при наличии положительной стоимости у минимального количества товаров в группе', NULL),
//				'k1007' => array('Создать объявления только при наличие изображения хотя бы у одного товара в группе.', NULL),
//				'k1008' => array('Не создавать объявления для группы, если минимальное количество товаров в группе имеют ошибку 404', NULL),
//				'k1009' => array('Не создавать объявления превышающие количество знаков', NULL),
//				'k1010' => array('Автоматически заменить шаблон, если объявление превышает количество знаков', NULL),
//				'k1011' => array('Добавлять к ссылкам UTM-метки', NULL),
//			),
//			'rItems' => array(
//				'k2001' => array('Автоматически выключать объявления при нулевом остатке товара на складе', NULL),
//				'k2002' => array('Автоматически включать объявления при положительном остатке товара на складе', NULL),
//				'k2003' => array('Создавать объявление только для товаров у которых остаток на складе больше, чем', 0),
//				'k2004' => array('Создавать объявление для товара стоимостью выше', 10),
//				'k2005' => array('Создавать объявление товара только при наличии положительной стоимости', 1),
//				'k2006' => array('Создать объявление товара только при наличие изображения', NULL),
//				'k2007' => array('Не создавать объявления превышающие количество знаков', NULL),
//				'k2007' => array('Не создавать объявление для товара, если товар имеют ошибку 404', NULL),
//				'k2008' => array('Автоматически заменить шаблон, если объявление превышает количество знаков', NULL),
//				'k2008' => array('Автоматически заменить шаблон, если объявление превышает количество знаков', NULL),
//				'k2009' => array('Добавлять к ссылкам UTM-метки', NULL),
//			),
//		);
//		$objectSettings = json_decode($this->_object->type_settings);
//		$bGrChecked = (!isset($objectSettings->adv_type) || (isset($objectSettings->adv_type) && $objectSettings->adv_type=='rGroups'));
//		$bItChecked = (isset($objectSettings->adv_type) && $objectSettings->adv_type=='rItems');
//
//		$grChecked = $bGrChecked ? ' checked="checked"' : '';
//		$itChecked = $bItChecked ? ' checked="checked"' : '';
//		$grEnabled = $bGrChecked ? '' : ' style="display: none;"';
//		$itEnabled = $bItChecked ? '' : ' style="display: none;"';
//		$grDisabled = $bGrChecked ? '' : ' disabled="disabled"';
//		$itDisabled = $bItChecked ? '' : ' disabled="disabled"';
/*
		ob_start();
		?>
			<div class="c-block">
				<div class="c-nav">
					<label class="radio">
						<input name="adv_type" type="radio" value="rGroups" class="form-control js-create_cam_radio" id="field_id_groups" <?=$grChecked ?> />
						<span class="text">Рекламировать группы</span>
					</label>
					<label class="radio">
						<input name="adv_type" type="radio" value="rItems" class="form-control js-create_cam_radio" id="field_id_items"<?=$itChecked ?> disabled="disabled" >
						<span class="text">Рекламировать товары</span>
					</label>
				</div>
				<div class="c-tabs-content">
					<div id="rGroups" class="c-fieldset"<?=$grEnabled?>>
						<?php
						foreach($settings['rGroups'] as $keyRgroup => $sRgroup) {
							$grMarkChecked = (isset($objectSettings->typeSettings->$keyRgroup->v)) ? $grChecked : '';
							$grInputValue = (isset($objectSettings->typeSettings->$keyRgroup->i)) ? $objectSettings->typeSettings->$keyRgroup->i : 1;
							$grInputDisabled = (isset($objectSettings->typeSettings->$keyRgroup->i)) ? '' : ' disabled="disabled"';
						?>
							<div class="form-group">
								<label class="checkbox c-checkbox">
									<input name="gr[<?php echo $keyRgroup ?>][v]" type="checkbox" value="<?php echo $keyRgroup ?>" class="form-control" id="field_id_<?php echo $keyRgroup ?>"<?=$grMarkChecked?><?=$grDisabled?>>
									<span class="text"><?php echo $sRgroup[0] ?></span>
								</label>
								<? if(isset($sRgroup[1]) && !is_null($sRgroup[1])) { ?> <input type="text" name="gr[<?php echo $keyRgroup ?>][i]" class="form-control" value="<?=$grInputValue?>" id="field_id_i<?php echo $keyRgroup ?>"<?=$grInputDisabled?>> <? } ?>
							</div>
						<?php
						}
						?>
					</div>
					<div id="rItems" class="c-fieldset"<?=$itEnabled?>>
						<?php
						foreach($settings['rItems'] as $keyRitem => $sRitem) {
							$itMarkChecked = (isset($objectSettings->typeSettings->$keyRitem->v)) ? $itChecked : '';
							$itInputValue = (isset($objectSettings->typeSettings->$keyRitem->i)) ? $objectSettings->typeSettings->$keyRitem->i : 1;
							?>
							<div class="form-group">
								<label class="checkbox c-checkbox">
									<input name="it[<?php echo $keyRitem ?>][v]" type="checkbox" value="<?php echo $keyRitem ?>" class="form-control" id="field_id_<?php echo $keyRitem ?>"<?=$itMarkChecked?><?=$itDisabled?> />
									<span class="text"><?php echo $sRitem[0] ?></span>
								</label>
								<? if(isset($sRitem[1]) && !is_null($sRitem[1])) { ?> <input type="text" name="it[<?php echo $keyRitem ?>][i]" class="form-control" value="<?=$itInputValue?>" id="field_id_i<?php echo $keyRitem ?>"<?=$itDisabled?> /> <? } ?>
							</div>
							<?php
						}
						?>
<!--						<div class="form-group">-->
<!--							<label class="checkbox c-checkbox">-->
<!--								<input name="name1" type="checkbox" value="5" class="form-control" id="field_id_4623" checked disabled>-->
<!--								<span class="text">Другой чекбокс</span>-->
<!--							</label>-->
<!--						</div>-->
<!--						<div class="form-group">-->
<!--							<label class="checkbox c-checkbox">-->
<!--								<input name="name12" type="checkbox" value="51" class="form-control" id="field_id_155523" checked disabled>-->
<!--								<span class="text">Создавать что-то выше чего-то</span>-->
<!--							</label>-->
<!--							<input type="text" name="name52text" class="form-control" value="300" id="field_id_46644" disabled>-->
<!--						</div>-->
					</div>
				</div>
			</div>
		<?php
		$leftArea = ob_get_clean();
		ob_start();
		?>
			<div>
				<div class="c-block">
<!-- айдишники id="tree12" id="tree1" как пример заменяешь на те что тебе нужны, они как раз и будут тебе массивом уходить -->
<!-- https://github.com/vakata/jstree вот тут если что доки :) -->
					<div class="form-group">
						<span class="caption">Поиск</span>
						<input id="tree-search" type="text" class="form-control">
					</div>
					<div id="c-tree">
<!--						<ul>-->
<!--							<li id="tree1">Товарные группы-->
<!--								<ul>-->
									?>
<!--									<li id="tree12">Запчасти кузова</li>-->
<!--									<li id="tree13">Оптика и комплектующие-->
<!--										<ul>-->
<!--											<li id="tree131">Фары передние-->
<!--												<ul>-->
<!--													<li id="tree1311">Фара передняя левая</li>-->
<!--													<li id="tree1312" data-jstree='{ "selected":true }'>Фара передняя праввая</li>-->
<!--													<li id="tree1313" >Комплект передних фар</li>-->
<!--												</ul>-->
<!--											</li>-->
<!--										</ul>-->
<!--									</li>-->
<!--								</ul>-->
<!--							</li>-->
<!--							<li id="tree2">Автомобили-->
<!--								<ul>-->
<!--									<li id="tree21">ACURA</li>-->
<!--									<li id="tree22">AUDI</li>-->
<!--									<li id="tree23">BMW-->
<!--										<ul>-->
<!--											<li id="tree231">1 series</li>-->
<!--											<li id="tree232">X5-->
<!--												<ul>-->
<!--													<li id="tree2321" data-jstree='{ "selected":true }'>E53 (1999 - 2004)</li>-->
<!--													<li id="tree2322" data-jstree='{ "selected":true }'>E70 (2007 - н.в.)</li>-->
<!--												</ul>-->
<!--											</li>-->
<!--											<li id="tree233">X6</li>-->
<!--										</ul>-->
<!--									</li>-->
<!--								</ul>-->
<!--							</li>-->
<!--						</ul>-->
					</div>
					<input id="c-tree-array" type="hidden" name="linked_objects" value="">
				</div>
				<script>
					$('#c-tree').jstree({
						"core": {
							"data": {
//								"url" : "/vendor/mikeborisov/hostcms-modules/src/direct/admin/campaign/treeTestLazy.json?action=lazy",
								"url" : "/admin/direct/shop/shops.php?action=lazy&site_id=<?=CURRENT_SITE?>&campaign_id=<?=$this->_object->id?>",
								"data" : function (node) {
									return {
										"id" : node.id,
										"adv_type": $('.js-create_cam_radio:checked').val()
									};
								}
							}
						},
//						"checkbox": {
//							"three_state": false
//							"whole_node" : false
//							"tie_selection" : false
//						},
						"plugins" : ["checkbox", "search"]
					});
					$('#c-tree').on("select_node.jstree", function(event, data){
						var currentElement = data.node,
							isData = currentElement.data;
						if(isData){
							var isControl = currentElement.data.control;
							if(isControl){
								var idChild = currentElement.children[0];
								$('#c-tree').jstree(true).deselect_node(idChild);
							}
						}
					});
					$('#c-tree').on("changed.jstree", function(){
						var arrayObj = $('#c-tree').jstree("get_selected", true),
							arrayId = [];
						for(var i=0; i<arrayObj.length; i++){
							arrayId.push(arrayObj[i].id);
						}
						console.log(arrayId);
						$('#c-tree-array').val(arrayId);
					});
				</script>
			</div>
		<?php
		$rightArea = ob_get_clean();
*/
//		$oMainColLeft->add(Admin_Form_Entity::factory('Code')->html($leftArea));
//		$oMainColRight->add(Admin_Form_Entity::factory('Code')->html($rightArea));

		return $this;
	}

	/**
	 * Processing of the form. Apply object fields.
	 * @hostcms-event Advertisement_Controller_Edit.onAfterRedeclaredApplyObjectProperty
	 */
	protected function _applyObjectProperty()
	{
		parent::_applyObjectProperty();

//		switch (isset($_REQUEST['hostcms']['operation'])) {
//			case $_REQUEST['hostcms']['operation']=='save' || $_REQUEST['hostcms']['operation']=='apply':
//				$jsonSettings = array(
//					'adv_type' => $_REQUEST['adv_type'],
//					'typeSettings' => $_REQUEST['adv_type']=='rGroups' ? (isset($_REQUEST['gr']) ? $_REQUEST['gr'] : array()) : (isset($_REQUEST['it']) ? $_REQUEST['it'] : array()),
//				);
//				$this->_object->type_settings = json_encode($jsonSettings);
//				$this->_object->save();
//
//				if( ($lo=Core_Array::getPost('linked_objects', ''))!='') {
//					$linkedObjects = explode(',', $lo);
//					$oCurrentLinkedGroups = Core_Entity::factory('Direct_Campaign_Link')->getCurrentLinkedGroups($this->_object->id);
//
//					foreach($linkedObjects as $linkedObject) {
//						$matches = array();
//						switch (true) {
//							case preg_match('/^gr?([0-9]*)$/', $linkedObject, $matches):
//								$linkedGroupID = $matches[1]*1;
//								if( $linkedGroupID>0 && !$this->checkOnValidate($oCurrentLinkedGroups, 'shop_group_id', $linkedGroupID) ) {
//									$mNewLink = Core_Entity::factory('Direct_Campaign_Link');
//									$mNewLink->campaign_id = $this->_object->id;
//									$mNewLink->type = Direct_Campaign_Link_Model::TL_GROUP;
//									$mNewLink->shop_group_id=$linkedGroupID;
//									$mNewLink->save();
//								}
//								break;
//						}
//					}
//					foreach($oCurrentLinkedGroups as $oCurrentLinkedGroup) {
//						$oCurrentLinkedGroup->delete();
//					}
//				}
//				break;
//		}
	}

	protected function checkOnValidate(&$objects, $field_name, $compareValue) {
		foreach ($objects as $oCurrentObjectKey => $oCurrentObject) {
			if($oCurrentObject->$field_name==$compareValue) {
				unset($objects[$oCurrentObjectKey]);
				return true;
			}
		}
		return false;
	}

//	protected function getCurrentLinkedGroups() {
//		$mCurrentLinkedGroups = Core_Entity::factory('Direct_Campaign_Link');
//		$mCurrentLinkedGroups
//			->queryBuilder()
//			->where('type', '=', 'group')
//		;
//		$oCurrentLinkedGroups = $mCurrentLinkedGroups->getAllByCampaign_Id($this->_object->id, FALSE);
//		return $oCurrentLinkedGroups;
//	}
}