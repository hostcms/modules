<?php
use Utils_Utl as utl;
use Unirest\Request as rest;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Directs.
 *
 * @package HostCMS 6\Direct
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Campaign_Group_Model extends Core_Entity
{
	/**
	 * Backend callback method
	 * @return string
	 */
	public function getgroups($yaCampaignIDs, $yaGroupIDs=array())
	{
		$yaCampaignIDsChunked = array_chunk($yaCampaignIDs, 10);
		$allGroups = array();

		foreach ($yaCampaignIDsChunked as $yaCampaignIDs) {
			$yaTokens = array();
			foreach ($yaCampaignIDs as $yaCampaignID) {
				$campaign = Core_Entity::factory('Direct_Campaign')->getByYa_Campaign_ID($yaCampaignID, FALSE);
//				utl::p($campaign->toArray());
				$profileModel = Core_Entity::factory('Direct_Profile')->getById($campaign->direct_profile_id);
				$yaTokens[$profileModel->id][] = $yaCampaignID;
			}

			foreach ($yaTokens as $profileID=>$yaCampaignIds) {
				$profileModel = Core_Entity::factory('Direct_Profile')->getById($profileID);
				$token = $profileModel->ya_token;
				$client = $profileModel->ya_client_login;
				$sandboxMode = ($profileModel->ya_sandbox_mode==1);
				$existingCampaignsModel = Core_Entity::factory('Direct_Campaign');
				$existingCampaignsModel
					->queryBuilder()
					->where('ya_campaign_id', 'IN', $yaCampaignIds);
				$existingCampaigns = $existingCampaignsModel->findAll(FALSE);
				$existingCampaigns = utl::getArrayKeyValuesFromArrays($existingCampaigns, 'ya_campaign_id');

				$directDriver = Direct_Drivers_Yandex_Driver::createInstance($token, $client, $sandboxMode);
				$adGroups = $directDriver->getAdGroups(array(), array(
					"CampaignIds" => $yaCampaignIds
				));
				$yaGroups = array();
				foreach ($adGroups as $adGroup) {
					$yaGroups[$adGroup->Id] = $adGroup;
				}
				$existingGroupsModel = Core_Entity::factory('Direct_Campaign_Group');
				$existingGroupsModel
					->queryBuilder()
					->where('ya_group_id', 'IN', array_keys($yaGroups));

				$existingGroups = $existingGroupsModel->findAll(FALSE);
				foreach ($existingGroups as $existingGroup) {
					/* $allGroups[] = */$existingGroup->updateYaGroup($yaGroups[$existingGroup->ya_group_id]);
					unset($yaGroups[$existingGroup->ya_group_id]);
				}
				foreach ($yaGroups as $yaGroup) {
					$directCampaign = $existingCampaigns[$yaGroup->CampaignId][0];
					$groupModel = Core_Entity::factory('Direct_Campaign_Group');
					$groupModel->ya_campaign_id = $yaGroup->CampaignId;
					$groupModel->ya_group_id = $yaGroup->Id;
					$groupModel->direct_campaign_id = $directCampaign->id;
					/* $allGroups[] = */$groupModel->updateYaGroup($yaGroup);
					unset($groupModel);
				}
			}
		}

		return $allGroups;
	}

	public function updateYaGroup($yaGroup, $save=TRUE) {
		$this->name = $yaGroup->Name;
		$this->ya_status = $yaGroup->Status;
		$this->ya_type = $yaGroup->Type;
		$this->ya_negative_keywords = isset($yaGroup->NegativeKeywords->Items) ? implode(' ', $yaGroup->NegativeKeywords->Items) : '';
		$this->ya_region_ids = implode(',', $yaGroup->RegionIds);
		($save) && $this->save();
		return $this;
	}

}