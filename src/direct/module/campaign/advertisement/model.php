<?php
use Utils_Utl as utl;
use HCMS\Shop\Groups as gr;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Directs.
 *
 * @package HostCMS 6\Direct
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Campaign_Advertisement_Model extends Core_Entity {

	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
		'direct_campaign' => array(),
		'direct_campaign_group' => array(),
	);

	/**
	 * Constructor.
	 * @param int $id entity ID
	 */
	public function __construct($id = NULL)
	{
		parent::__construct($id);

		if (is_null($id))
		{
			$this->_preloadValues['datetime'] = Core_Date::timestamp2sql(time());
		}
	}

	/**
	 * Backend callback method
	 * @param mixed $value value
	 * @return string
	 */
	public function campaignName($value = NULL)
	{
		// Get value
		if (is_null($value) || is_object($value))
		{
			return $this->direct_campaign->name;
		}
	}

	/**
	 * Backend callback method
	 * @param mixed $value value
	 * @return string
	 */
	public function groupName($value = NULL)
	{
		// Get value
		if (is_null($value) || is_object($value))
		{
			return $this->direct_campaign_group->name;
		}
	}

	/**
	 * Backend callback method
	 * @return string
	 */
	public function getads($yaCampaignIDs, $yaAdIDs=array())
	{
		$yaCampaignIDsChunked = array_chunk($yaCampaignIDs, 10);
		$allGroups = array();

		foreach ($yaCampaignIDsChunked as $yaCampaignIDs) {
			$yaTokens = array();
			$allAds = array();

			foreach ($yaCampaignIDs as $yaCampaignID) {
				$campaign = Core_Entity::factory('Direct_Campaign')->getByYa_Campaign_ID($yaCampaignID);
				$profileModel = Core_Entity::factory('Direct_Profile')->getById($campaign->direct_profile_id);
				$yaTokens[$profileModel->id][] = $yaCampaignID;
			}
			foreach ($yaTokens as $profileID => $yaCampaignIds) {
				$profileModel = Core_Entity::factory('Direct_Profile')->getById($profileID);
				$token = $profileModel->ya_token;
				$client = $profileModel->ya_client_login;
				$sandboxMode = ($profileModel->ya_sandbox_mode==1);
				$existingCampaignsModel = Core_Entity::factory('Direct_Campaign');
				$existingCampaignsModel
					->queryBuilder()
					->where('ya_campaign_id', 'IN', $yaCampaignIds);
				$existingCampaigns = $existingCampaignsModel->findAll(FALSE);
				$existingCampaigns = utl::getArrayKeyValuesFromArrays($existingCampaigns, 'ya_campaign_id');

				$existingGroupsModel = Core_Entity::factory('Direct_Campaign_Group');
				$existingGroupsModel
					->queryBuilder()
					->where('ya_campaign_id', 'IN', $yaCampaignIds);
				$existingGroups = $existingGroupsModel->findAll(FALSE);
				$existingGroups = utl::getArrayKeyValuesFromArrays($existingGroups, 'ya_group_id');

				$directDriver = Direct_Drivers_Yandex_Driver::createInstance($token, $client, $sandboxMode);
				$ads = $directDriver->getAdsByGroup(array(), array(
					"CampaignIds" => $yaCampaignIds
				));
				$yaAds = array();
				foreach ($ads as $ad) {
					$yaAds[$ad->Id] = $ad;
				}
				$existingAdsModel = Core_Entity::factory('Direct_Campaign_Advertisement');
				$existingAdsModel
					->queryBuilder()
					->where('ya_ad_id', 'IN', array_keys($yaAds));
				$existingAds = $existingAdsModel->findAll(FALSE);
				foreach ($existingAds as $existingAd) {
					/* $allAds[] = */$existingAd->updateYaAd($yaAds[$existingAd->ya_ad_id]);
					unset($yaAds[$existingAd->ya_ad_id]);
				}

				foreach ($yaAds as $yaAd) {
					$group = $existingGroups[$yaAd->AdGroupId][0];
					$campaign = $existingCampaigns[$yaAd->CampaignId][0];
					$adModel = Core_Entity::factory('Direct_Campaign_Advertisement');
					$adModel->direct_campaign_id = $campaign->id;
					$adModel->direct_campaign_group_id = $group->id;
					$adModel->ya_campaign_id = $yaAd->CampaignId;
					$adModel->ya_group_id = $yaAd->AdGroupId;
					$adModel->ya_ad_id = $yaAd->Id;
					/*$allAds[] = */ $adModel->updateYaAd($yaAd);
					unset($adModel);
				}
			}
		}
		return $allAds;
	}

	public function updateYaAd($yaAd, $save=TRUE) {
		$this->name = $yaAd->TextAd->Title;
		$this->ya_text = $yaAd->TextAd->Text;
		$matches = array();
		preg_match('/^(https?:\/\/)(?:w{3}\.)?([\da-zA-Z\.-]+\.[a-z\.]{2,6})([^?]*)(\??.*)$/', $yaAd->TextAd->Href, $matches);
		(isset($matches[3]) && substr($matches[3], -1) != '/') && $matches[3] .= '/';
		$this->ya_href_domain = $matches[1] . $matches[2];
		$this->ya_href = $matches[3];
		$this->ya_href_crc32 = crc32($this->ya_href);
		$this->ya_state = $yaAd->State;
		$this->ya_status = $yaAd->Status;
		$this->ya_status_show = $yaAd->StatusClarification;
		$this->ya_type = $yaAd->Type;
		isset($yaAd->TextAd) && $this->ya_text_ad = serialize($yaAd->TextAd);

		($save) && $this->save();
		return $this;
	}

	public function suspend($ids=NULL) {
		if(is_null($ids) && isset($this->ya_ad_id) && $this->ya_ad_id>0) {
			$ids = array($this->ya_ad_id);
		}
		if(is_null($ids) && !is_array($ids)) {
			$ids = array($ids);
		}
		if( count($ids)==0 ) {
			return false;
		}
		$returnes = array();
		$yaAdvIDsChunkeds = array_chunk($ids, 10000);
		$existingAd = Core_Entity::factory('Direct_Campaign_Advertisement')->getByYa_ad_id($ids[0], FALSE);
		$profileModel = $existingAd->direct_campaign->direct_profile;
		$token = $profileModel->ya_token;
		$client = $profileModel->ya_client_login;
		$sandboxMode = ($profileModel->ya_sandbox_mode==1);
		foreach ($yaAdvIDsChunkeds as $yaIDs) {
			$directDriver = Direct_Drivers_Yandex_Driver::createInstance($token, $client, $sandboxMode);
			$suspended = $directDriver->suspendAds($yaIDs);

			if(isset($suspended->SuspendResults) && is_array($suspended->SuspendResults)) {
				foreach ($suspended->SuspendResults as $susResult){
					if(isset($susResult->Id) && $susResult->Id>0) {
						$existingAd = Core_Entity::factory('Direct_Campaign_Advertisement')->getByYa_ad_id($susResult->Id, FALSE);
						$existingAd->ya_state = 'SUSPENDED';
						$existingAd->save();
					}
				}
			}
			$returnes[] = $suspended;
		}
		return $returnes;
	}

	public function resume($ids=NULL) {
		if(is_null($ids) && isset($this->ya_ad_id) && $this->ya_ad_id>0) {
			$ids = array($this->ya_ad_id);
		}
		if(is_null($ids) && !is_array($ids)) {
			$ids = array($ids);
		}
		if( count($ids)==0 ) {
			return false;
		}
		$returnes = array();
		$yaAdvIDsChunkeds = array_chunk($ids, 10000);
		$existingAd = Core_Entity::factory('Direct_Campaign_Advertisement')->getByYa_ad_id($ids[0], FALSE);
		$profileModel = $existingAd->direct_campaign->direct_profile;
		$token = $profileModel->ya_token;
		$client = $profileModel->ya_client_login;
		$sandboxMode = ($profileModel->ya_sandbox_mode==1);
		foreach ($yaAdvIDsChunkeds as $yaIDs) {
			$directDriver = Direct_Drivers_Yandex_Driver::createInstance($token, $client, $sandboxMode);
			$resumed = $directDriver->resumeAds($yaIDs);

			if(isset($resumed->ResumeResults) && is_array($resumed->ResumeResults)) {
				foreach ($resumed->ResumeResults as $susResult){
					if(isset($susResult->Id) && $susResult->Id>0) {
						$existingAd = Core_Entity::factory('Direct_Campaign_Advertisement')->getByYa_ad_id($susResult->Id, FALSE);
						$existingAd->ya_state = 'ON';
						$existingAd->save();
					}
				}
			}
			$returnes[] = $resumed;
		}
		return $returnes;
	}
}
