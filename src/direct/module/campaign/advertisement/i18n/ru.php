<?php
/**
 * SEO администрирование.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	//-- Профили --
	'model_name' => 'Директ.Объявления кампании',
	'actions' => 'Действия',
	'advertisements' => 'Объявления',

	'add_advertisement' => 'Добавить объявление',
	'edit_advertisement' => 'Редактировать объявление',
	'edit_success' => 'Редактирование выполнено успешно',

	'id' => 'Код',
	'header_text' => 'Заголовок объявления',
	'group_text' => 'Текст группы',
	'adv_text' => 'Текст объявления',
	'key_text' => 'Ключи',
	'datetime' => 'Дата создания',

	'direct_campaign_id' => 'Идентификатор кампании',
);