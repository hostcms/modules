<?php
use Utils_Utl as utl;
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Advertisement.
 *
 * @package HostCMS 6\Advertisement
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Campaign_Advertisement_Controller_Edit extends Utils_Admin_Form_Action_Controller_Type_Edit
{
	/**
	 * Set object
	 * @param object $object object
	 * @return self
	 */
	public function setObject($object)
	{
		parent::setObject($object);

		return $this;
	}

	/**
	 * Processing of the form. Apply object fields.
	 * @hostcms-event Advertisement_Controller_Edit.onAfterRedeclaredApplyObjectProperty
	 */
	protected function _applyObjectProperty()
	{
		parent::_applyObjectProperty();
	}
}