<?php
/**
 * SEO администрирование.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	//-- Профили --
	'model_name' => 'Директ.Кампании',
	'actions' => 'Действия',

	'add_campaign' => 'Добавить кампанию',
	'edit_campaign' => 'Редактировать кампанию',
	'edit_success' => 'Редактирование выполнено успешно',
	'get_campaigns' => 'Синхронизировать кампании',

	'advertisements' => 'Объявления',

	'id' => 'Код',
	'site_id' => 'Cайт',
	'name' => 'Наименование шаблона',
	'direct_profile_id' => 'Профиль',

	'tmpl_group_id' => 'Шаблон группы',
	'tmpl_head_id' => 'Шаблон заголовка',
	'tmpl_text_id' => 'Шаблон текста',
	'tmpl_key_id' => 'Шаблон ключей',

	'ya_campaign_id' => 'ID кампании Яндекс',
	'ya_status' => 'Статус',
	'ya_status_show' => 'Статус кампании',
	'ya_is_active' => 'Активность кампании',
	'ya_start_date' => 'Начало кампании',
	'ya_clicks' => 'Количество кликов кампании',

);