<?php
use Utils_Utl as utl;
use Unirest\Request as rest;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Directs.
 *
 * @package HostCMS 6\Direct
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Campaign_Model extends Core_Entity
{
	/**
	 * Belongs to relations
	 * @var array
	 */
	protected $_belongsTo = array(
		'direct_profile' => array(),
		'template_group' => array('model' => 'Direct_Template', 'foreign_key' => 'tmpl_group_id'),
		'template_head' => array('model' => 'Direct_Template', 'foreign_key' => 'tmpl_head_id'),
		'template_text' => array('model' => 'Direct_Template', 'foreign_key' => 'tmpl_text_id'),
		'template_key' => array('model' => 'Direct_Template', 'foreign_key' => 'tmpl_key_id'),
	);

	/**
	 * @return array
	 */
	public function getBelongsTo()
	{
		return $this->_belongsTo;
	}

	/**
	 * List of preloaded values
	 * @var array
	 */
	protected $_preloadValues = array(
		'ya_status' => '',
	);

	/**
	 * Constructor.
	 * @param int $id entity ID
	 */
	public function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	/**
	 * Backend callback method
	 * @param mixed $value value
	 * @return string
	 */
	public function templateProfile($value = NULL)
	{
		// Get value
		if (is_null($value) || is_object($value))
		{
			return $this->direct_profile->name;
		}
	}

	/**
	 * Backend callback method
	 * @param mixed $value value
	 * @return string
	 */
	public function campaignType($value = NULL)
	{
		// Get value
		if (is_null($value) || is_object($value))
		{
			return '-';
		}
	}

	/**
	 * Backend callback method
	 * @param mixed $value value
	 * @return strint
	 */
	public function campaignStage($value = NULL)
	{
		// Get value
		if (is_null($value) || is_object($value))
		{
			return '-';
		}
	}

	/**
	 * Delete object from database
	 * @param mixed $primaryKey primary key for deleting object
	 * @return Core_Entity
	 */
	public function delete($primaryKey = NULL)
	{
		if (is_null($primaryKey))
		{
			$primaryKey = $this->getPrimaryKey();
		}

		$this->id = $primaryKey;

		return parent::delete($primaryKey);
	}

	/**
	 * Backend callback method
	 * @param Admin_Form_Field $oAdmin_Form_Field
	 * @param Admin_Form_Controller $oAdmin_Form_Controller
	 * @return string
	 */
	public function name($oAdmin_Form_Field, $oAdmin_Form_Controller)
	{
		$link = $oAdmin_Form_Field->link;
		$onclick = $oAdmin_Form_Field->onclick;

		$link = $oAdmin_Form_Controller->doReplaces($oAdmin_Form_Field, $this, $link);
		$onclick = $oAdmin_Form_Controller->doReplaces($oAdmin_Form_Field, $this, $onclick);

		$oCore_Html_Entity_Div = Core::factory('Core_Html_Entity_Div');

		$oCore_Html_Entity_Div
			->add(
				Core::factory('Core_Html_Entity_A')
					->href($link)
					->onclick($onclick)
					->value(htmlspecialchars($this->name))
			);

		$oCore_Html_Entity_Div->execute();
	}

	public function getprofile($campaignID) {
		$profileID = Core_Array::getGet('profile_id', 0);
		$profileModel = Core_Entity::factory('Direct_Profile')->getById($profileID);
		return $profileModel;
	}

	/**
	 * Backend callback method
	 * @return string
	 */
	public function syncampaigns()
	{
		set_time_limit(5000);
		$profileID = Core_Array::getGet('profile_id', 0);
		$campaigns = $this->getcampaigns($profileID);
		$yaCampaignIDs = array_keys(utl::getArrayKeyValuesFromArrays($campaigns, 'ya_campaign_id'));
//		utl::p($yaCampaignIDs);

		$mGroups = Core_Entity::factory('Direct_Campaign_Group');
//		$groups = $mGroups->getgroups(array(19729267) /*$yaCampaignIDs*/);
		$groups = $mGroups->getgroups($yaCampaignIDs);
//		unset($groups);
//		utl::p(utl::setArrayOfObjectsToArray($groups));

		$mAds = Core_Entity::factory('Direct_Campaign_Advertisement');
//		$ads = $mAds->getads(array(19729267) /*$yaCampaignIDs*/);
		$ads = $mAds->getads($yaCampaignIDs);
//		unset($ads);
//		utl::p(utl::setArrayOfObjectsToArray($ads));
	}

	/**
	 * Backend callback method
	 * @return string
	 */
	public function getcampaigns($profileID, $yaCampaignIDs=array()) {
		$profileModel = Core_Entity::factory('Direct_Profile')->getById($profileID);
		$token = $profileModel->ya_token;
		$client = $profileModel->ya_client_login;
		$sandboxMode = (($profileModel->ya_sandbox_mode*1)==1);

		$directDriver = Direct_Drivers_Yandex_Driver::createInstance($token, $client, $sandboxMode);
		$campSectionCriteria = array(
			"Types" => array("TEXT_CAMPAIGN")
		);
		(count($yaCampaignIDs)>0) && $campSectionCriteria['Ids'] = $yaCampaignIDs;
		$campaigns = $directDriver->getCampaigns(array(), $campSectionCriteria);
		$allCampaigns = array();
		$yaCampaigns = array();
		foreach ($campaigns as $campaign) {
			$yaCampaigns[$campaign->Id] = $campaign;
		}
		$existingCampaignsModel = Core_Entity::factory('Direct_Campaign');
		$existingCampaignsModel
			->queryBuilder()
			->where('ya_campaign_id', 'IN', array_keys($yaCampaigns));
		$existingCampaigns = $existingCampaignsModel->findAll(FALSE);

		foreach ($existingCampaigns as $existingCampaign) {
			$allCampaigns[] = $existingCampaign->updateYaCampaign($yaCampaigns[$existingCampaign->ya_campaign_id]);
			unset($yaCampaigns[$existingCampaign->ya_campaign_id]);
		}
		foreach ($yaCampaigns as $yaCampaign) {
			$campaignModel = Core_Entity::factory('Direct_Campaign');
			$campaignModel->ya_campaign_id = $yaCampaign->Id;
			$campaignModel->site_id = CURRENT_SITE;
			$campaignModel->direct_profile_id = $profileID;
			$allCampaigns[] = $campaignModel->updateYaCampaign($yaCampaign);
		}
		return $allCampaigns;
	}

	public function updateYaCampaign($yaCampaign, $save=TRUE) {
		$this->name = $yaCampaign->Name;
		$this->ya_status = $yaCampaign->Status;
		$this->ya_is_active = $yaCampaign->State;
		$this->ya_start_date =  Core_Date::date2sql($yaCampaign->StartDate);
		$this->ya_clicks =  $yaCampaign->Statistics->Clicks;
		$this->ya_status_show = $yaCampaign->StatusClarification;
		($save) && $this->save();
		return $this;
	}
}