<?php
use Utils_Utl as utl;
/**
 * Утилиты администрирования.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Direct
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Direct_Observers_Core {

	static public function onBeforeShowCss($object, $args) {
		$instance = Core_Page::instance();
		$instance->css('/admin/direct/css/direct.css');
	}

	static public function onBeforeShowJs($object, $args) {
		$instance = Core_Page::instance();
		$instance->js('/admin/direct/js/direct.js');
	}
}