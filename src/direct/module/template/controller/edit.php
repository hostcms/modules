<?php
use Utils_Utl as utl;
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Advertisement.
 *
 * @package HostCMS 6\Advertisement
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Template_Controller_Edit extends Utils_Admin_Form_Action_Controller_Type_Edit
{
//	public static $temlateTypes = array(
//		1 => 'Группа',
//		2 => 'Заголовок',
//		3 => 'Текст',
//		4 => 'Ключ',
//	);
	/**
	 * Set object
	 * @param object $object object
	 * @return self
	 */
	public function setObject($object)
	{
		parent::setObject($object);

		$this->title(
			$this->_object->id
				? Core::_('Direct_Template.edit_template')
				: Core::_('Direct_Template.add_template')
		);


		$oMainTab = $this->getTab('main');
		$oAdditionalTab = $this->getTab('additional');
		$windowId = $this->_Admin_Form_Controller->getWindowId();

		$oMainTab
			->add($oMainRow1 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow2 = Admin_Form_Entity::factory('Div')->class('row'))
		;
		$oMainTab
			->move($this->getField('site_id')->readonly('readonly')->caption('')->type('hidden')->value($this->_object->site_id==0 ? CURRENT_SITE : $this->_object->site_id), $oAdditionalTab)
		;
//		if(core_)
//		utl::p($_REQUEST['hostcms']['action']);
		if(isset($_REQUEST['hostcms']['action']) && (!isset($_REQUEST['hostcms']['operation']) || $_REQUEST['hostcms']['operation']!='apply')) {
			?>
			<div class="row">
				<div class="col-md-12">
					<div class="widget">
						<div class="widget-body">
							{vashaperemennya} - Слово как есть<br/>
							{Vashaperemennya} - Первое слово с большой буквы<br/>
							{VashaPeremennya} - Каждое Слово С Большой Буквы<br/>
							<p>
								<b>Принцип использования переменных в шаблоне:</b><br/>
								<i>В продаже {name-grup-finishstep} {marka-auto} {model-auto} {gody} - {cena-tovara}
									руб. Оригинальный номер: {orig-nomer}. Быстрая
									доставка. Отличная цена.</i><br/>
								<b>Результат</b><br/>
								В продаже фара передняя левая BMW X5 2011-2014 - 12 380 руб. Оригинальный номер:
								893953050. Быстрая доставка. Отличная цена.<br/>
							</p>
						</div>
					</div>
				</div>
			</div>
			<?
		}

		$oMainTab->move($this->getField('name')->divAttr(array('class' => 'form-group col-lg-9')), $oMainRow1);
		$oMainTab->move($this->getField('tmpl_text'), $oMainRow2);
		$oMainTab->delete($this->getField('tmpl_type'));
		$oMainRow1
			->add($oTemplateType = Admin_Form_Entity::factory('Div')->class('row'), $this->getField('name'));

		$tmpType = $this->createSelect(Direct_Template_Model::$temlateTypes, 'Direct_template', 'tmpl_type', $this->_object->tmpl_type, array('class' => 'form-group col-lg-3'));
		$oTemplateType->add(
			$tmpType
		);

		return $this;
	}

	/**
	 * Show edit form
	 * @return boolean
	 */
	protected function _showEditForm()
	{
		// Контроллер показа формы редактирования с учетом скина
		$oAdmin_Form_Action_Controller_Type_Edit_Show = Admin_Form_Action_Controller_Type_Edit_Show::create();

		$oAdmin_Form_Action_Controller_Type_Edit_Show
			->title($this->title)
			->children($this->_children)
			->Admin_Form_Controller($this->_Admin_Form_Controller)
			->formId($this->_formId)
			->tabs($this->_tabs)
			->buttons($this->_addButtons())
		;

		$this->addContent(
			$oAdmin_Form_Action_Controller_Type_Edit_Show->showEditForm()
		);

		return TRUE;
	}
}