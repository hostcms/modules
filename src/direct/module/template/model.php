<?php

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Directs.
 *
 * @package HostCMS 6\Direct
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Template_Model extends Core_Entity
{
	public static $temlateTypes = array(
		1 => 'Группа',
		2 => 'Заголовок',
		3 => 'Текст',
		4 => 'Ключ',
	);

	/**
	 * Constructor.
	 * @param int $id entity ID
	 */
	public function __construct($id = NULL)
	{
		parent::__construct($id);
	}

	/**
	 * Backend callback method
	 * @param mixed $value value
	 * @return string
	 */
	public function templateName($value = NULL)
	{
		// Get value
		if (is_null($value) || is_object($value))
		{
			if(isset(self::$temlateTypes[$this->tmpl_type])) {
				return self::$temlateTypes[$this->tmpl_type];
			}
			return Core::_('Direct.undefined');
		}
	}

	/**
	 * Delete object from database
	 * @param mixed $primaryKey primary key for deleting object
	 * @return Core_Entity
	 */
	public function delete($primaryKey = NULL)
	{
		if (is_null($primaryKey))
		{
			$primaryKey = $this->getPrimaryKey();
		}

		$this->id = $primaryKey;

		return parent::delete($primaryKey);
	}
}