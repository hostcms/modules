<?php
/**
 * SEO администрирование.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	//-- Профили --
	'model_name' => 'Директ.Шаблоны',
	'actions' => 'Действия',

	'add_template' => 'Добавить шаблон',
	'edit_template' => 'Редактировать шаблон',
	'edit_success' => 'Редактирование выполнено успешно',

	'id' => 'Код',
	'site_id' => 'Cайт',
	'name' => 'Наименование шаблона',
	'tmpl_text' => 'Шаблон',
	'tmpl_type' => 'Тип шаблона',
);