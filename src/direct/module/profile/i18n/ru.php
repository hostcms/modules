<?php
/**
 * SEO администрирование.
 *
 * @package HostCMS 6\Seos
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	//-- Профили --
	'model_name' => 'Директ.Профили',
	'actions' => 'Действия',

	'add_profile' => 'Добавить профиль',
	'edit_profile' => 'Редактировать профиль',
	'edit_success' => 'Редактирование выполнено успешно',
	'worktime_header' => 'Время работы',
	'date_begin' => 'Начало',
	'date_end' => 'Завершение',
	'time_begin' => 'Время начала',
	'time_end' => 'Время завершения',

	'id' => 'Код',
	'site_id' => 'Cайт',
	'shop_country_id' => 'Страна',
	'shop_country_location_city_id' => 'Город',
	'phone' => 'Телефон',
	'email' => 'E-mail',
	'name' => 'Название компании / ФИО',
	'ogrn' => 'ОГРН / ОГРНИП',
	'contacts' => 'Контактное лицо',
	'postaddr' => 'Почтовый адрес',
	'description' => 'Описание',
);