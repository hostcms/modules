<?php
use Utils_Utl as utl;
defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Advertisement.
 *
 * @package HostCMS 6\Advertisement
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2005-2015 ООО "Хостмэйк" (Hostmake LLC), http://www.hostcms.ru
 */
class Direct_Profile_Controller_Edit extends Utils_Admin_Form_Action_Controller_Type_Edit
{
	/**
	 * Set object
	 * @param object $object object
	 * @return self
	 */
	public function setObject($object)
	{
		parent::setObject($object);

		$this->title(
			$this->_object->id
				? Core::_('Direct_Profile.edit_profile')
				: Core::_('Direct_Profile.add_profile')
		);


		$oMainTab = $this->getTab('main');
		$oAdditionalTab = $this->getTab('additional');
		$windowId = $this->_Admin_Form_Controller->getWindowId();

		// ---------------------------------------------------------------------
		$oMainTab
			->add($oMainRow1 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow2 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow3 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow4 = Admin_Form_Entity::factory('Div')->class('row'))
			->add($oMainRow5 = Admin_Form_Entity::factory('Div')->class('row'))
		;

		$oMainTab->delete($this->getField('shop_country_id'));
		$oMainTab->delete($this->getField('shop_country_location_city_id'));

		$namesCountries = array('Россия');
		$countriesModel = Core_Entity::factory('Shop_Country');
		$countriesModel
			->queryBuilder()
			->where('name', 'IN', $namesCountries)
		;
		$selectCountries = $this->createSelect($countriesModel, 'Direct_profile', 'shop_country_id', $this->_object->shop_country_id, array('class' => 'form-group col-lg-3'));

		$citiesModel = Core_Entity::factory('Shop_Country_Location_City');
		$citiesModel
			->queryBuilder()
			->join('shop_country_locations', 'shop_country_locations.id', '=', 'shop_country_location_cities.shop_country_location_id')
			->join('shop_countries', 'shop_countries.id', '=', 'shop_country_locations.shop_country_id')
			->where('shop_countries.name', 'IN', $namesCountries)
		;
		$selectCities = $this->createSelect($citiesModel, 'Direct_profile', 'shop_country_location_city_id', $this->_object->shop_country_location_city_id, array('class' => 'form-group col-lg-3'));
		$oMainRow1
			->add($selectCountries)
			->add($selectCities)
		;
		$oMainTab
			->move(
				$this
					->getField('phone')
					->divAttr(array('class' => 'form-group col-lg-3'))
					->format(array('minlen' => array('value' => 1), 'reg' => array('value' => '/(?:(?:\+?1\s*(?:[.-]\s*)?)?(?:(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]‌​)\s*)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)([2-9]1[02-9]‌​|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})\s*(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+)\s*)?$/'))
				), $oMainRow1)
			->move(
				$this
					->getField('email')
					->divAttr(array('class' => 'form-group col-lg-3'))
					->format(array('minlen' => array('value' => 1), 'lib' => array('value' => 'email'))
				), $oMainRow1)
		;

		$oMainTab
			->move(
				$this
					->getField('name')
					->divAttr(array('class' => 'form-group col-lg-9'))
					->format(array('minlen' => array('value' => 1))
				), $oMainRow2)
			->move(
				$this
					->getField('ogrn')
					->divAttr(array('class' => 'form-group col-lg-3'))
					->format(array('reg' => array('value' => '/^([0-9]{13}|[0-9]{15})$/'))
				), $oMainRow2)
		;

		$oMainTab
			->move($this->getField('contacts')->divAttr(array('class' => 'form-group col-lg-6')), $oMainRow3)
			->move($this->getField('postaddr')->divAttr(array('class' => 'form-group col-lg-6')), $oMainRow3)
		;

		$oMainTab
			->move($this->getField('description'), $oMainRow4)
		;

		$oMainTab
			->add($oWorktimesBlock = Admin_Form_Entity::factory('Div')->class('well with-header'));

		$oWorktimesBlock
			->add(Admin_Form_Entity::factory('Div')
				->class('header bordered-pink')
				->value(Core::_("Direct_Profile.worktime_header"))
			)
			->add($oWorktimeCurrentRow = Admin_Form_Entity::factory('Div')->class('row'));

//		$oMainTab->delete($this->getField('direct_profile_worktime'));

		$days = array('Пн'=>'Пн','Вт'=>'Вт','Ср'=>'Ср','Чт'=>'Чт','Пт'=>'Пт','Сб'=>'Сб','Вс'=>'Вс');
		$times = array('00'=>'00','01'=>'01','02'=>'02','03'=>'03','04'=>'04','05'=>'05','06'=>'06','07'=>'07','08'=>'08','09'=>'09','10'=>'10','11'=>'11','12'=>'12','13'=>'13','14'=>'14','15'=>'15','16'=>'16','17'=>'17','18'=>'18','19'=>'19','20'=>'20','21'=>'21','22'=>'22','23'=>'23');
		$aDirectProfileWorktimes = $this->_object->direct_profile_worktimes->findAll();
		foreach ($aDirectProfileWorktimes as $oDirectProfileWorktime) {
			$wtDayBegin = $this->createSelect($days, 'Direct_profile', 'date_begin', $oDirectProfileWorktime->day_from, array('class' => 'form-group col-lg-3'));
			$wtDayEnd = $this->createSelect($days, 'Direct_profile', 'date_end', $oDirectProfileWorktime->day_to, array('class' => 'form-group col-lg-3'));
			$wtTimeBegin = $this->createSelect($times, 'Direct_profile', 'time_begin', $oDirectProfileWorktime->time_from, array('class' => 'form-group col-lg-3'));
			$wtTimeEnd = $this->createSelect($times, 'Direct_profile', 'time_end', $oDirectProfileWorktime->time_to, array('class' => 'form-group col-lg-3'));
			$oWorktimeCurrentRow
				->add($wtDayBegin)
				->add($wtDayEnd)
				->add($wtTimeBegin)
				->add($wtTimeEnd)
			;
		}
//		foreach($aDirectProfileWorktimes as $oDirectProfileWorktime) {
//			utl::p($oDirectProfileWorktime->toArray());
//		}
//			->add(
//				Admin_Form_Entity::factory('Div')
//					//->caption(Core::_("Shop_Item.warehouse_item_count", $oWarehouse->name))
//					->value(1)
//					->class('form-group margin-top-10 col-lg-4 col-md-4 col-sm-6 col-xs-9')
//			)
//			->add(
//				Admin_Form_Entity::factory('Input')
//					//->caption(Core::_("Shop_Item.warehouse_item_count", $oWarehouse->name))
//					->value(2)
//					->name("warehouse_1")
//					->divAttr(array('class' => 'form-group col-lg-2 col-md-4 col-sm-4 col-xs-3'))
//			//->divAttr(array('class' => 'form-group col-lg-2 col-md-2 col-sm-2 col-xs-3'))
//			);

		$oMainTab
			->move($this->getField('site_id')->readonly('readonly')->caption('')->type('hidden')->value($this->_object->site_id==0 ? CURRENT_SITE : $this->_object->site_id), $oAdditionalTab)
		;

		return $this;
	}
}