<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Entity.
 *
 * @package HostCMS 6\Direct
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
class Direct_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '0.1';

	/**
	 * Module date
	 * @var date
	 */
	public $date = '2016-04-14';

	/**
	 * Module form settings
		array(
			'form1' => array(										//-- Форма
				'guid' => 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX',		//-- guid формы
				'key_field' => 'id',									//-- Наименование ключевого поля из БД
				'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
				'on_page' => 30,										//-- Количество строк на странице
				'show_operations' => 0,									//-- Показывать операции
				'show_group_operations' => 0,							//-- Показывать групповые операции
				'default_order_direction' => 0,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
				'name' => array(										//-- название формы
					1 => 'Наименование модуля',							//-- по-русски - 1=идентификатор языка
					2 => 'Module name'									//-- по-английски - 2=идентификатор языка
				),
				'fields' => array(										//-- поля на отображаемой форме
					'id' => array(										//-- наименование поля из сущности БД(название столбца)
						'name' => array(								//-- название поля в админке
							1 => 'Код',									//-- по-русски - 1=идентификатор языка
							2 => 'ID'									//-- по-английски - 2=идентификатор языка
						),
						'sorting' => 10,								//-- поле сортировки
						'ico' => '',									//-- иконка поля
						'type' => 1,									//-- тип поля
						'format' => '',									//-- формат поля
						'allow_sorting' => 1,							//-- разрешить сортировку по полю 0-нет, 1-да
						'allow_filter' => 1,							//-- разрешить фильтрацию по полю 0-нет, 1-да
						'editable' => 1,								//-- разрешить inline-редактирование по полю 0-нет, 1-да
						'filter_type' => 0,								//-- тип фильтрации
						'class' => '',									//-- класс для поля
						'width' => '',									//-- ширина поля, например '55px'
						'image' => '',									//-- картинка для поля
						'link' => '',									//-- ссылка для поля
						'onclick' => '',								//-- событие нажатия на поле
						'list' => '',									//--
					),
					'field2' =>  array(
						'name' => array(								//-- название поля в админке
							1 => 'Наименование кампании',				//-- по-русски - 1=идентификатор языка
							2 => 'Campaign name'						//-- по-английски - 2=идентификатор языка
						),
						'sorting' => 20,
						.....
					),
				),
				'actions' => array (
					'apply' => array (									//-- ключевое наименование действия для формы
						'name' => array (								//-- название действия в админке
							1 => 'Код',									//-- по-русски - 1=идентификатор языка
							2 => 'ID'									//-- по-английски - 2=идентификатор языка
						),
						'sorting' => 1000,								//-- сортировка для действий
						'picture' => '/admin/images/apply.gif',			//--
						'icon' => 'fa fa-check',
						'color' => 'palegreen',
						'single' => 0,
						'group' => 0,
						'dataset' => '-1',
						'confirm' => 0,
					),
					'action2' => array ( ... )
				)
			),
	 		'form2' => array( ... )
		)
	 * @var afSettings
	 */
	protected $_afSettings = array(
		'direct' => array(											//-- Форма
			'guid' => '16CD2C5D-9BC1-937E-4FC7-AFCA22EA0FA4',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Директ.Реклама',								//-- по-русски - 1=идентификатор языка
				2 => 'Direct.Advertisment'							//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
//				'name' =>  array(
//					'name' => array(								//-- название поля в админке
//						1 => 'Наименование профиля',				//-- по-русски - 1=идентификатор языка
//						2 => 'Profile name'							//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 20,
//				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование профиля',				//-- по-русски - 1=идентификатор языка
						2 => 'Profile name'							//-- по-английски - 2=идентификатор языка
					),
					'type' => 10,									//-- тип профиля, вычисляемое поле
					'allow_sorting' => 0,							//-- Сортировать
					'link' => '/admin/direct/campaign/index.php?site_id={site_id}&profile_id={id}',
					'onclick' => "$.adminLoad({path: '/admin/direct/campaign/index.php', additionalParams: 'site_id={site_id}&profile_id={id}', windowId: '{windowId}'}); return false",
					'sorting' => 20,								//-- поле сортировки
				),

//				'owner' =>  array(
//					'name' => array(
//						1 => 'Владелец',
//						2 => 'Owner'
//					),
//					'sorting' => 30,
//					'width' => '300px',
//				),
//				'start_datetime' =>  array(
//					'name' => array(								//-- название поля в админке
//						1 => 'Начало',				//-- по-русски - 1=идентификатор языка
//						2 => 'Start date'						//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 40,
//					'width' => '100px',								//-- ширина поля, например '55px'
//					'type' => 5,
//				),
//				'showed' =>  array(
//					'name' => array(
//						1 => 'Показов',
//						2 => 'Showed'
//					),
//					'sorting' => 50,
//					'width' => '80px',
//					'allow_sorting' => 0,
//					'allow_filter' => 0,
//					'editable' => 0,
//				),
//				'clicks' =>  array(
//					'name' => array(
//						1 => 'Кликов',
//						2 => 'Clicks'
//					),
//					'sorting' => 60,
//					'width' => '80px',
//					'allow_sorting' => 0,
//					'allow_filter' => 0,
//					'editable' => 0,
//				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '/admin/images/edit.gif',			//--
					'icon' => 'fa fa-check',
					'color' => 'palegreen',
					'single' => 0,
					'group' => 0,
					'dataset' => '0',
					'confirm' => 0,
				),
				'apply' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Применить',							//-- по-русски - 1=идентификатор языка
						2 => 'Apply'								//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- сортировка для действий
					'picture' => '/admin/images/apply.gif',			//--
					'icon' => 'fa fa-check',
					'color' => 'palegreen',
					'single' => 0,
					'group' => 0,
					'dataset' => '-1',
					'confirm' => 0,
				),
			)
		),
		'direct_profiles' => array(									//-- Форма
			'guid' => '487F0FA0-9F87-4FCA-5D2B-63427DD82BE8',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Директ.Реклама.Профили',						//-- по-русски - 1=идентификатор языка
				2 => 'Direct.Advertisment.Profiles'					//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование',						//-- по-русски - 1=идентификатор языка
						2 => 'Name'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- поле сортировки
				),
				'ogrn' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'ОГРН',								//-- по-русски - 1=идентификатор языка
						2 => 'OGRN'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 30,								//-- поле сортировки
					'width' => '130px',								//-- ширина поля, например '55px'
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '/admin/images/edit.gif',			//--
					'icon' => 'fa fa-check',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => '0',
					'confirm' => 0,
				),
//				'apply' => array(									//-- ключевое наименование действия для формы
//					'name' => array(								//-- название действия в админке
//						1 => 'Применить',							//-- по-русски - 1=идентификатор языка
//						2 => 'Apply'								//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 20,								//-- сортировка для действий
//					'picture' => '/admin/images/apply.gif',			//--
//					'icon' => 'fa fa-check',
//					'color' => 'palegreen',
//					'single' => 0,
//					'group' => 0,
//					'dataset' => '0',
//					'confirm' => 0,
//				),
			)
		),
		'direct_templates' => array(								//-- Форма
			'guid' => '3A83219E-852B-44CD-ADF5-C921427C229A',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Директ.Реклама.Шаблоны',						//-- по-русски - 1=идентификатор языка
				2 => 'Direct.Advertisment.Templates'				//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
				'templateName' => array(							//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Тип шаблона',							//-- по-русски - 1=идентификатор языка
						2 => 'Template type'						//-- по-английски - 2=идентификатор языка
					),
					'type' => 10,									//-- вычисляемое поле
					'sorting' => 20,								//-- поле сортировки
					'width' => '120px',								//-- ширина поля, например '55px'
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование',						//-- по-русски - 1=идентификатор языка
						2 => 'Name'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 30,								//-- поле сортировки
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '/admin/images/edit.gif',			//--
					'icon' => 'fa fa-check',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => '0',
					'confirm' => 0,
				),
			)
		),
		'direct_campaigns' => array(								//-- Форма
			'guid' => '2388B1EC-ECB0-18B9-E605-FD59BB63CE30',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'ya_is_active',				//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 0 - по возрастанию, 1 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Директ.Реклама.Кампании',						//-- по-русски - 1=идентификатор языка
				2 => 'Direct.Advertisment.Campaigns'				//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
				'ya_campaign_id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'ID кампании Яндекс',		//-- по-русски - 1=идентификатор языка
						2 => 'Yandex campaign ID'					//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- поле сортировки
					'width' => '120px',								//-- ширина поля, например '55px'
				),
				'templateProfile' => array(							//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Профиль',								//-- по-русски - 1=идентификатор языка
						2 => 'Template type'						//-- по-английски - 2=идентификатор языка
					),
					'type' => 10,									//-- вычисляемое поле
					'allow_sorting' => 0,							//-- Сортировать
					'sorting' => 30,								//-- поле сортировки
					'width' => '120px',								//-- ширина поля, например '55px'
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Наименование кампании',				//-- по-русски - 1=идентификатор языка
						2 => 'Campaign name'						//-- по-английски - 2=идентификатор языка
					),
					'type' => 10,									//-- тип кампании, вычисляемое поле
					'allow_sorting' => 0,							//-- Сортировать
					'link' => '/admin/direct/campaign/advertisement/index.php?site_id={site_id}&campaign_id={id}&profile_id={direct_profile_id}',
					'onclick' => "$.adminLoad({path: '/admin/direct/campaign/advertisement/index.php', additionalParams: 'site_id={site_id}&campaign_id={id}&profile_id={direct_profile_id}', windowId: '{windowId}'}); return false",
					'sorting' => 40,								//-- поле сортировки
				),
				'ya_status' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Статус кампании',						//-- по-русски - 1=идентификатор языка
						2 => 'Number'								//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 50,								//-- поле сортировки
					'width' => '220px',								//-- ширина поля, например '55px'
				),
				'ya_is_active' => array(							//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Активность кампании',					//-- по-русски - 1=идентификатор языка
						2 => 'Campaign activity'					//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 60,								//-- поле сортировки
					'width' => '100px',								//-- ширина поля, например '55px'
				),
//				'campaignStage' => array(							//-- наименование поля из сущности БД(название столбца)
//					'name' => array(								//-- название поля в админке
//						1 => 'Состояние',							//-- по-русски - 1=идентификатор языка
//						2 => 'Status'								//-- по-английски - 2=идентификатор языка
//					),
//					'type' => 10,									//-- вычисляемое поле
//					'allow_sorting' => 0,							//-- Сортировать
//					'sorting' => 70,								//-- поле сортировки
//					'width' => '140px',								//-- ширина поля, например '55px'
//				),
//				'clicks' => array(									//-- наименование поля из сущности БД(название столбца)
//					'name' => array(								//-- название поля в админке
//						1 => 'Кликов',								//-- по-русски - 1=идентификатор языка
//						2 => 'Clicks'								//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 80,								//-- поле сортировки
//					'width' => '100px',								//-- ширина поля, например '55px'
//				),
//				'remains' => array(									//-- наименование поля из сущности БД(название столбца)
//					'name' => array(								//-- название поля в админке
//						1 => 'Осталось',							//-- по-русски - 1=идентификатор языка
//						2 => 'Remains'								//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 90,								//-- поле сортировки
//					'width' => '100px',								//-- ширина поля, например '55px'
//				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '/admin/images/edit.gif',			//--
					'icon' => 'fa fa-check',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => '0',
					'confirm' => 0,
				),
				'syncampaigns' => array(							//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Получить кампании Яндекс',			//-- по-русски - 1=идентификатор языка
						2 => 'Get Yandex Campaigns'					//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- сортировка для действий
					'picture' => '/admin/images/edit.gif',			//--
					'icon' => 'fa fa-check',
					'color' => 'red',
					'single' => 0,
					'group' => 0,
					'dataset' => '0',
					'confirm' => 0,
				),
			)
		),
		'direct_campaigns_advertisements' => array(					//-- Форма
			'guid' => '914320EC-8ED6-423C-6195-73DB8BED9B1C',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Директ.Кампании.Объявления',					//-- по-русски - 1=идентификатор языка
				2 => 'Direct.Campaigns.Advertisements'				//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
				'campaignName' => array(							//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Кампания',							//-- по-русски - 1=идентификатор языка
						2 => 'Campaign'								//-- по-английски - 2=идентификатор языка
					),
					'type' => 10,									//-- вычисляемое поле
					'sorting' => 20,								//-- поле сортировки
					'width' => '150px',								//-- ширина поля, например '55px'
				),
				'groupName' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Группа',								//-- по-русски - 1=идентификатор языка
						2 => 'Group'								//-- по-английски - 2=идентификатор языка
					),
					'type' => 10,									//-- вычисляемое поле
					'sorting' => 30,								//-- поле сортировки
					'width' => '200px',								//-- ширина поля, например '55px'
				),
				'name' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Заголовок объявления',				//-- по-русски - 1=идентификатор языка
						2 => 'Advertisement header',				//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 40,								//-- поле сортировки
				),
				'ya_href' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'URL',									//-- по-русски - 1=идентификатор языка
						2 => 'URL',									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 50,								//-- поле сортировки
				),
				'ya_state' => array(									//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Статус',									//-- по-русски - 1=идентификатор языка
						2 => 'State',									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 50,								//-- поле сортировки
					'width' => '100px',								//-- ширина поля, например '55px'
				),
			),
			'actions' => array(
				'edit' => array(									//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Редактировать',						//-- по-русски - 1=идентификатор языка
						2 => 'Edit'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
					'picture' => '/admin/images/edit.gif',			//--
					'icon' => 'fa fa-check',
					'color' => 'palegreen',
					'single' => 1,
					'group' => 0,
					'dataset' => '0',
					'confirm' => 0,
				),
			)
		),
	);


	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Core_Event::attach('Core_Page.onBeforeShowCss', array('Direct_Observers_Core', 'onBeforeShowCss'));
		Core_Event::attach('Core_Page.onBeforeShowJs', array('Direct_Observers_Core', 'onBeforeShowJs'));
		Core_Skin::instance()->addJs('/admin/direct/plugins/jstree/jstree.min.js');
		Core_Skin::instance()->addCss('/admin/direct/plugins/jstree/style.css');
		Core_Skin::instance()->addCss('/admin/direct/css/direct.css');
		Core_Skin::instance()->addJs('/admin/direct/js/direct.js');

		$this->menu = array(
			array(
				'sorting' => 270,
				'block' => 1,
				'ico' => 'fa fa-google',
				'name' => Core::_('Direct.model_name'),
				'href' => "/admin/direct/index.php",
				'onclick' => "$.adminLoad({path: '/admin/direct/index.php'});return false"
			)
		);
	}

	/**
	 * Install module.
	 */
	public function install() {
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			//-- Добавляем форму --
			$adminForm = $this->adminForm($afSetting['name'], $afSetting);
		}
	}

	/**
	 * Uninstall.
	 */
	public function uninstall()
	{
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (!is_null($oAdmin_Form)) {
				// Удаление формы
				$oAdmin_Form->delete();
			}
		}
	}

	protected function adminForm($afName, $afSetting) {
		$oAdmin_Form = NULL;
		if(isset($afSetting['guid'])) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (is_null($oAdmin_Form)) {
				$oAdmin_Word_Form = $this->adminWordForm($afName);
				$oAdmin_Form = Core_Entity::factory('Admin_Form');
				$oAdmin_Form->admin_word_id = $oAdmin_Word_Form->id;
				$oAdmin_Form->key_field = $afSetting['key_field'];
				$oAdmin_Form->default_order_field = Core_Array::get($afSetting, 'default_order_field', $afSetting['key_field']);

				$oAdmin_Form->on_page = Core_Array::get($afSetting, 'on_page', 20);
				$oAdmin_Form->show_operations = Core_Array::get($afSetting, 'show_operations', 1);
				$oAdmin_Form->show_group_operations = Core_Array::get($afSetting, 'show_group_operations', 0);
				$oAdmin_Form->default_order_direction = Core_Array::get($afSetting, 'default_order_direction', 0);
				$oAdmin_Form->guid = $afSetting['guid'];
				$oAdmin_Form->save();
			}
			if(isset($afSetting['fields']) && is_array($afSetting['fields'])) {
				//-- Добавляем поля в форму --
				foreach ($afSetting['fields'] as $dbFieldKey => $field) {
					$adminFormField = $this->adminFormField($dbFieldKey, $field);
					$oAdmin_Form->add($adminFormField);
				}
			}
			if(isset($afSetting['actions']) && is_array($afSetting['actions'])) {
				//-- Добавляем действия в форму --
				foreach ($afSetting['actions'] as $dbActionKey => $action) {
					$adminFormAction = $this->adminFormAction($dbActionKey, $action);
					$oAdmin_Form->add($adminFormAction);
				}
			}
		}

		return $oAdmin_Form;
	}

	protected function adminFormAction($fDbName, $action) {
		$oAdmin_Word_Form = $this->adminWordForm($action['name']);
		$oAdmin_Form_Action = Core_Entity::factory('Admin_Form_Action')->save();
		$oAdmin_Form_Action->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Action->name = $fDbName;
		$oAdmin_Form_Action->picture = Core_Array::get($action, 'picture', '');
		$oAdmin_Form_Action->icon = Core_Array::get($action, 'icon', '');
		$oAdmin_Form_Action->color = Core_Array::get($action, 'color', '');
		$oAdmin_Form_Action->single = Core_Array::get($action, 'single', 0);
		$oAdmin_Form_Action->group = Core_Array::get($action, 'group', 0);
		$oAdmin_Form_Action->sorting = Core_Array::get($action, 'sorting', 1000);
		$oAdmin_Form_Action->dataset = Core_Array::get($action, 'dataset', '-1');
		$oAdmin_Form_Action->confirm = Core_Array::get($action, 'confirm', 0);
//		$oAdmin_Form_Action->save();
		return $oAdmin_Form_Action;
	}

	protected function adminFormField($fDbName, $field) {
		$oAdmin_Word_Form = $this->adminWordForm($field['name']);
		$oAdmin_Form_Field = Core_Entity::factory('Admin_Form_Field')->save();
		$oAdmin_Form_Field->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Field->name = $fDbName;
		$oAdmin_Form_Field->sorting = Core_Array::get($field, 'sorting', 1000);
		$oAdmin_Form_Field->ico = Core_Array::get($field, 'ico', '');
		$oAdmin_Form_Field->type = Core_Array::get($field, 'type', 1);
		$oAdmin_Form_Field->format = Core_Array::get($field, 'format', '');
		$oAdmin_Form_Field->allow_sorting = Core_Array::get($field, 'allow_sorting', 1);
		$oAdmin_Form_Field->allow_filter = Core_Array::get($field, 'allow_filter', 1);
		$oAdmin_Form_Field->editable = Core_Array::get($field, 'editable', 0);
		$oAdmin_Form_Field->filter_type = Core_Array::get($field, 'filter_type', 0);
		$oAdmin_Form_Field->class = Core_Array::get($field, 'class', '');
		$oAdmin_Form_Field->width = Core_Array::get($field, 'width', '');
		$oAdmin_Form_Field->image = Core_Array::get($field, 'image', '');
		$oAdmin_Form_Field->link = Core_Array::get($field, 'link', '');
		$oAdmin_Form_Field->onclick = Core_Array::get($field, 'onclick', '');
		$oAdmin_Form_Field->list = Core_Array::get($field, 'list', '');
		return $oAdmin_Form_Field;
	}

	protected function adminWordForm($afName) {
		$oAdmin_Word_Form = Core_Entity::factory('Admin_Word')->save();
		foreach ($afName as $langID => $langName) {
			$oAdmin_Word_Value = $this->adminWordValue($langID, $langName);
			$oAdmin_Word_Form->add($oAdmin_Word_Value);
		}
		return $oAdmin_Word_Form;
	}

	protected function adminWordValue($language_id, $name) {
		$oAdmin_Word_Value = Core_Entity::factory('Admin_Word_Value')->save();
		$oAdmin_Word_Value->admin_language_id = $language_id;
		$oAdmin_Word_Value->name = $name;
		$oAdmin_Word_Value->save();
		return $oAdmin_Word_Value;
	}
}