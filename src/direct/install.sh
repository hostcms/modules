#!/bin/bash

_CURRENTPATH=`pwd`

echo $_CURRENTPATH

if [ ! -d ../../../../../modules/direct ]; then 
    ln -s ../vendor/mikeborisov/hostcms-modules/src/direct/module ../../../../../modules/direct;
fi

if [ ! -d ../../../../../admin/direct ]; then 
    ln -s ../vendor/mikeborisov/hostcms-modules/src/direct/admin ../../../../../admin/direct;
fi

if [ ! -d ../../../../../modules/skin/bootstrap/module/direct ]; then
    ln -s ../../../../vendor/mikeborisov/hostcms-modules/src/direct/skin ../../../../../modules/skin/bootstrap/module/direct;
fi