// var responseExampleJSON = {
// 	"response": [
// 		{
// 			"id": 123,
// 			"group": "группа 1",
// 			"description": "Описание 1"
// 		},
// 		{
// 			"id": 321,
// 			"group": "группа 2",
// 			"description": "Описание 2"
// 		}
// 	]
// };
$(function(){
	$(document).on('click', '.restrict_group.active .fa-minus', function(event) {
		var _data_restrict_sugroups_id = $(this).attr('data-restrict-sugroups-id'),
			_data_type = $(this).attr('data-type'),
			_data_group_id = $(this).attr('data-group-id'),
			_topElement = null,
			_data = {}
		;
		if(typeof(_data_restrict_sugroups_id)!='undefined') {
			switch (_data_type) {
				case 'user':
					_topElement = $(this).closest('.url_ip');
					break;
				case 'group':
					_topElement = $(".group-ip i[data-group-id='"+_data_group_id+"'][data-restrict-sugroups-id='"+_data_restrict_sugroups_id+"']").closest('.url_ip');
					break;
			}
			_data = {
				"action": 'removeSiteuserIpFromRestrict',
				'data_restrict_sugroups_id': _data_restrict_sugroups_id,
			};
		}
		$.ajax({
			url:'/admin/restrict/process.php',
			method: 'post',
			dataType: 'json',
			data: _data,
			success: function(data){
				_topElement.remove();
			},
			error: function(){
				alert('error');
			}
		});
	});

	function groupIpUrls(){
		const $groupIp = $('.group-ip');
		$groupIp.each(function(){
			const $urlIp = $(this).find('.url_ip:nth-child(4) ~ span');
			$urlIp.css('display', 'none');

			const $buttonMore = $('<button />', {type: 'button', html: 'Показать все ip', class: 'btn group-ip-more'});
			$buttonMore.one('click', function(){
				$urlIp.removeAttr('style');
				$(this).remove();
			});
			$(this).append($buttonMore);
		});
	}
	groupIpUrls();
});

function restrictUpdateSUstatus(current) {
	console.log(current.attr('data-ingroup-status'));
	switch (current.attr('data-ingroup-status')) {
		case 'off':
			sendAddUserToGroup(current.attr('data-siteuser'), current.attr('data-group'), current);
			break;
		case 'on':
			sendRemoveUserFromGroup(current.attr('data-ingroup-id'), current);
			break;
	}
}

function addGroupModal(element, group_id, action_get, type, siteuser_name, existig_groups, siteuser_id){
	var responseObj = null,
		$form = $('<form>', {class:'group_modal_form', onsubmit:'return false;', html:'<div class="group_modal_form__title">'+siteuser_name+'</div>'}),
		$bodyForm = $('<div>', {class: 'group_modal_form__body'})
	var $selectDiv = $('<div>', {class: 'form-group'});
		;
	// расскоментировать когда будет готова серверная часть
	$.ajax({
		url:'/admin/restrict/process.php',
		method: 'post',
		dataType: 'json',
		data: {
			"action": 'actionGet'
		},
		success: function(data){
			responseObj = data.response;
			switch (type){
				case "user":
					var $name = $('<div>', {class:'form-group', html:''/*'<span class="caption">'+siteuser_name+'</span>'*/});
					var $ul = $('<ul>', {class:"group_modal_form__list"});
					for(var i=0; i<responseObj.response.length; i++){
						var group = responseObj.response[i].group,
							description = responseObj.response[i].description,
							_id = responseObj.response[i].id;
							_siteuser_group_id = responseObj.response[i].siteuser_group_id;
						$(element).addClass('js-current_group');
						// console.log(existig_groups);
						// console.log('group_id='+_id+', '+_siteuser_group_id+', '+existig_groups.indexOf(_siteuser_group_id*1));
						//в данном случае group_id == siteuser_id
						if(existig_groups.indexOf(_siteuser_group_id*1)<0) {
							var $a = $('<a>', {class:'group_modal_form__label', text:group, onclick:"sendAddUserToGroup("+group_id+", "+_siteuser_group_id+", '"+group+"')"});
							$ul.append($('<li>',{class:'form-group', html:'<div class="group_modal_form__description">'+description+'</div>'}).prepend($a));
						}
					}
					$bodyForm.append($name, $ul);
					$form.append($bodyForm);
					break;

				case "ipgroup":
					// var $selectDiv = $('<div>', {class:'form-group'}).append($select);
					var $select = $('<select>', {class:'form-control', name:'group'});
					for(var j=0; j<responseObj.response.length; j++){
						$select.append($('<option>', {value:responseObj.response[j].id, text:responseObj.response[j].group}));
					}
					$selectDiv.append($select);
				case "ipuser":
					// var name = $(element).closest('.group-ip').find('.group-url').text();
					var $url = $('<div>', {class:'form-group', html:'<span class="caption">URL</span><input type="text" class="form-control" name="groupURL" value="'+group_id+'">'}),
						$ip  = $('<div>', {class:'form-group', html:"<span class='caption'>IP</span>"}).append($('<input>', {type:"text", class:"form-control", name:"ip", required:"required"}).attr('pattern', '[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}')),
						$type  = $('<div>', {class:'form-group', html:"<span class='caption'></span>"}).append($('<input>', {type:"hidden", class:"form-control", name:"usertype", value:type.replace('ip', '')})),
						$userid  = $('<div>', {class:'form-group', html:"<span class='caption'></span>"}).append($('<input>', {type:"hidden", class:"form-control", name:"siteuser", value:siteuser_id})),
						$saveButtonIp = $('<button>', {type:'submit', class:"btn btn-darkorange", text:"Отправить"});
					$form.on('submit', function(){
						sendAddIpToGroup(group_id, $ip.find('input').val(), $form);
					});
					$bodyForm.append($url, $ip, $selectDiv, $saveButtonIp, $type, $userid);
					$form.append($bodyForm);
					break;
			}
		},
		error: function(){
			alert('error');
		}
	});
	$.fancybox.open($form);
	return false;
}

function sendAddIpToGroup(group_id, ip, form){
	$.ajax({
		url: '/admin/restrict/process.php',
		method: 'post',
		data: form.serialize() + '&group_id='+group_id+'&action=addIpToGroup',
		dataType: 'html',
		success: function(){
//			var $elements = $('[data-group-id='+group_id+']').parent();
//			$elements.before(", <a>"+ip+"</a> ");
			$.fancybox.close(true);
		},
		error: function(){
			alert('error');
		}
	});
	return false;
}

function sendAddUserToGroup(siteuser_id, id, name){
	$.ajax({
		url: '/admin/restrict/process.php',
		method: 'post',
		data: {
			"action":"addUserToGroup",
			"siteuser_id": siteuser_id,
			"group": id
		},
		dataType: 'json',
		success: function(data){
			if(data.status=='OK') {
				switch (typeof(name)) {
					// case 'string':
					// 	$('.js-current_group').removeClass('js-current_group').before("<div><a href='#'>"+name+"</a></div>");
					// 	$.fancybox.close(true);
					// 	break;
					case 'object':
						var _img = name.find('img');
						switch (name.attr('data-ingroup-status')) {
							case 'off':
								_img.attr('src', name.attr('data-img-on'));
								name.attr('data-ingroup-status', 'on');
								name.attr('data-ingroup-id', data.sugrouplist.id);
								break;
						}
						break;
				}
			} else {
				console.log(data);
			}
		},
		error: function(){
			alert('error');
		}
	});
	return false;
}
function sendRemoveUserFromGroup(data_ingroup_id, name){
	$.ajax({
		url: '/admin/restrict/process.php',
		method: 'post',
		data: {
			"action":"removeSiteuserFromGroup",
			"data_group_list_id": data_ingroup_id
		},
		dataType: 'json',
		success: function(data){
			if(data.status=='OK') {
				switch (typeof(name)) {
					case 'object':
						var _img = name.find('img');
						switch (name.attr('data-ingroup-status')) {
							case 'on':
								_img.attr('src', name.attr('data-img-off'));
								name.attr('data-ingroup-status', 'off');
								name.attr('data-ingroup-id', 0);
								break;
						}
						break;
				}
			} else {
				console.log(data);
			}
		},
		error: function(error){
			console.log(error);
		}
	});
	return false;
}
