<?php
use Utils_Utl as utl;
require_once(__DIR__.'/../../../../../../bootstrap.php');

/**
 * Restrict.
 *
 * @package HostCMS
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */

Core_Auth::authorization($sModule = 'restrict');

// Код формы
$sAdminFormAction = '/admin/restrict/index.php';
$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGUID('16CD2C5D-9BC1-937E-4FC7-AFCA22EA0FA4');
$iAdmin_Form_Id = $oAdmin_Form->id;
$siteID = CURRENT_SITE;


//// Контроллер формы
$oAdmin_Form_Controller = Admin_Form_Controller::create($oAdmin_Form);
$oAdmin_Form_Controller
	->module(Core_Module::factory($sModule))
	->setUp()
	->path($sAdminFormAction)
	->title(Core::_('Restrict.model_name'))
	->pageTitle(Core::_('Restrict.model_name'));

//utl::p($siteID);

// Хлебные крошки
$oBreadcrumbs = Admin_Form_Entity::factory('Breadcrumbs');

// Первая крошка на список магазинов
$oBreadcrumbs->add(
	Admin_Form_Entity::factory('Breadcrumb')
		->name(Core::_('Restrict.menu'))
		->href($oAdmin_Form_Controller->getAdminLoadHref(
			'/admin/restrict/index.php', NULL, NULL, ''
		))
		->onclick($oAdmin_Form_Controller->getAdminLoadAjax(
			'/admin/restrict/index.php', NULL, NULL, ''
		))
);
// Добавляем все хлебные крошки контроллеру
$oAdmin_Form_Controller->addEntity($oBreadcrumbs);

$mRestricts = Core_Entity::factory('Siteuser_Group_List');
$mRestricts
	->queryBuilder()
	->join(array('restrict_siteuser_groups', 'rsg'), 'rsg.siteuser_group_id', '=', 'siteuser_group_lists.siteuser_group_id')
	->join(array('siteusers', 'su'), 'su.id', '=', 'siteuser_group_lists.siteuser_id')
	->where('rsg.site_id', '=', CURRENT_SITE)
	->where('su.deleted', '=', 0)
	->where('rsg.name', '=', 'RESTRICT_ALL')
;
$arRestricts = $mRestricts->findAll(TRUE);
$siteusers = utl::getArrayValuesFromArrays($arRestricts, 'siteuser_id');

$oAdmin_Form_Dataset = new Admin_Form_Dataset_Entity(Core_Entity::factory('Siteuser'));
// Ограничение источника 0 по ID сайта
$oAdmin_Form_Dataset->addCondition(
	array(
		'where' => array('id', 'IN', $siteusers),
	)
);

$oAdmin_Form_Controller->addDataset($oAdmin_Form_Dataset);


$oAdmin_Form_Controller->execute();
