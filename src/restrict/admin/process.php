<?php
use Utils_Utl as utl;
/**
 * Restricts.
 *
 * @package HostCMS
 * @subpackage Restrict
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
require_once(__DIR__.'/../../../../../../bootstrap.php');

Core_Auth::authorization($sModule = 'restrict');
Core::parseUrl();

header('Content-Type: application/json');

$result =  array(
	'status' => "ERROR",
	'message' => "Нопределенная ошибка",
);

switch (Core_Array::getPost('action', 'none')) {
	case 'actionGet':
		$response = array(
			"response" => array()
		);
		$allSubGroups = Core_Entity::factory('Restrict_Siteuser_Group')
			->findAll(FALSE);
		foreach ($allSubGroups as $allSubGroup) {
			$sGroup = array(
					"id" => $allSubGroup->id,
					"siteuser_group_id" => $allSubGroup->siteuser_group_id,
					"group" => $allSubGroup->description,
					"description" => $allSubGroup->name,
			);
			$response['response'][] = $sGroup;
		}
		$result =  array(
			'response' => $response,
			'status' => "OK",
			'message' => "OK",
		);
		break;
	case 'addUserToGroup':
		$siteuserID = Core_Array::getPost('siteuser_id', 0)*1;
		$siteuserGroupID = Core_Array::getPost('group', 0)*1;
		if($siteuserGroupID>0 && $siteuserID>0) {
			$mSuGroupList = Core_Entity::factory('Siteuser_Group_List');
			$mSuGroupList
				->queryBuilder()
				->where('siteuser_group_id', '=', $siteuserGroupID)
				->where('siteuser_id', '=', $siteuserID)
			;
			$aSuGroupList = $mSuGroupList->findAll(TRUE);
			if(count($aSuGroupList)==0) {
				$mSuGroupList = Core_Entity::factory('Siteuser_Group_List');
				$mSuGroupList->siteuser_group_id=$siteuserGroupID;
				$mSuGroupList->siteuser_id=$siteuserID;
				$mSuGroupList->save();
			}
		}
		$result =  array(
			'status' => "OK",
			'message' => "OK",
			'sugrouplist' => $mSuGroupList->toArray(),
		);
		break;
	case 'removeSiteuserFromGroup':
		if(($dataGroupListId=Core_Array::getPost('data_group_list_id', 0))>0) {
			$dataLink = Core_Entity::factory('Siteuser_Group_List')->getById($dataGroupListId);
			$dataLink->delete();
			$result =  array(
				'status' => "OK",
				'message' => "OK",
			);
		}
		break;
	case 'removeSiteuserIpFromRestrict':
		if(($dataRestrictSugroupsId=Core_Array::getPost('data_restrict_sugroups_id', 0))>0) {
			$dataLink = Core_Entity::factory('Restrict_Url_Sugroup')->getById($dataRestrictSugroupsId);
			$dataLink->delete();
			$result =  array(
				'status' => "OK",
				'message' => "OK",
			);
		}
		break;
	case 'addIpToGroup':
		$ip = trim(Core_Array::getPost('ip', ''));
		$restrictGroupID = Core_Array::getPost('group', 0)*1;
		$restrictURL = trim(Core_Array::getPost('groupURL', ''));
		$restrictType = trim(Core_Array::getPost('usertype', ''));
		$siteuserID = trim(Core_Array::getPost('siteuser', 0))*1;
		if($ip!='' && (($restrictType=='group' && $restrictGroupID>0) || $restrictType=='user') && $restrictURL!='' && $restrictType!='') {
			$iRestrictURL = Core_Entity::factory('Restrict_Url')->getByName($restrictURL);
			if(!isset($iRestrictURL->id)) {
				$iRestrictURL = Core_Entity::factory('Restrict_Url');
				$iRestrictURL->name = $restrictURL;
				$iRestrictURL->site_id = CURRENT_SITE;
				$iRestrictURL->save();
			}
			$mRestrictURLsuGroup = Core_Entity::factory('Restrict_Url_Sugroup');
			$mRestrictURLsuGroup
				->queryBuilder()
				->where('restrict_url_id', '=', $iRestrictURL->id)
				->where('ip', '=', $ip)
				->where('restrict_siteuser_group_id', '=', (($restrictType=='user')?$siteuserID:$restrictGroupID))
			;
			$iRestrictURLsuGroup = $mRestrictURLsuGroup->findAll(FALSE);
			if(count($iRestrictURLsuGroup)==0 || is_null($iRestrictURLsuGroup)) {
				$mRestrictURLsuGroup = Core_Entity::factory('Restrict_Url_Sugroup');
				$mRestrictURLsuGroup->restrict_url_id = $iRestrictURL->id;
				$mRestrictURLsuGroup->restrict_siteuser_group_id = (($restrictType=='user')?$siteuserID:$restrictGroupID);
				$mRestrictURLsuGroup->type = $restrictType;
				$mRestrictURLsuGroup->ip = $ip;
				$mRestrictURLsuGroup->save();
//				utl::p($mRestrictURLsuGroup->toArray());
			}
			$result =  array(
				'status' => "OK",
				'message' => "OK",
			);
		} else {
			$result['message'] = "Недостаточно параметров для выполнения операции";
		}
		break;
}

echo json_encode($result);
