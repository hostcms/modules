#!/bin/bash

_CURRENTPATH=`pwd`

echo $_CURRENTPATH

if [ ! -d ../../../../../modules/restrict ]; then
    ln -s ../vendor/mikeborisov/hostcms-modules/src/restrict/module ../../../../../modules/restrict;
fi

if [ ! -d ../../../../../admin/restrict ]; then
    ln -s ../vendor/mikeborisov/hostcms-modules/src/restrict/admin ../../../../../admin/restrict;
fi

if [ ! -d ../../../../../modules/skin/bootstrap/module/restrict ]; then
    ln -s ../../../../vendor/mikeborisov/hostcms-modules/src/restrict/skin ../../../../../modules/skin/bootstrap/module/restrict;
fi