<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Restricts.
 *
 * @package HostCMS 6\Restrict
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © Борисов Михаил Юрьевич
 */
class Restrict_Url_Sugroup_Model extends Core_Entity {
	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;
}
