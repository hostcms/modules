<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Restricts.
 *
 * @package HostCMS 6\Restrict
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © Борисов Михаил Юрьевич
 */
class Restrict_Url_Model extends Core_Entity {

	public function getUrlsByType($aSuGroups=FALSE, $type='group', $userID=FALSE, $addCheckRestrict=false) {
		$mIpGroups = Core_QueryBuilder::select('ru.id')
			->select('ru.name')
			->select('rus.ip')
			->select(array('rus.id', 'restrict_id'))
			->select(array(Core_QueryBuilder::expression('COALESCE(`sg`.`id`, -1)'), 'group_id'))
			->from(array('restrict_url_sugroups', 'rus'))
			->join(array('restrict_urls', 'ru'), 'rus.restrict_url_id', '=', 'ru.id')
			->leftJoin(array('restrict_siteuser_groups', 'rsg'), 'rsg.id', '=', 'rus.restrict_siteuser_group_id')
			->leftJoin(array('siteuser_groups', 'sg'), 'sg.id', '=', 'rsg.siteuser_group_id')
			->where('rus.type', '=', $type)
			->orderBy('ru.name')
		;
		if($userID!==FALSE && $userID>0) {
			$mIpGroups
				->where('rus.restrict_siteuser_group_id', '=', $userID*1)
			;
			if($addCheckRestrict) {
				$mIpGroups
					->select(array('rsg1.name', 'rsg_name'))
					->join(array('siteuser_group_lists', 'sgl'), 'sgl.siteuser_id', '=', 'rus.restrict_siteuser_group_id')
					->join(array('restrict_siteuser_groups', 'rsg1'), 'rsg1.siteuser_group_id', '=', 'sgl.siteuser_group_id')
				;
			}
		} else {
			if(is_array($aSuGroups) && count($aSuGroups)>0) {
				$mIpGroups
					->where('sg.id', 'IN', $aSuGroups)
				;
			}
		}
		$arIpGroups = $mIpGroups->asAssoc()->execute()->result();
		return $arIpGroups;
	}

	/**
	 * Find all objects
	 * @param bool $bCache use cache, default TRUE
	 * @return array
	 */
	public function findAll($bCache = TRUE)
	{
		if (defined('CURRENT_SITE'))
		{
			$this->queryBuilder()
				->where($this->_tableName . '.site_id', '=', CURRENT_SITE);
		}
		return parent::findAll($bCache);
	}
}
