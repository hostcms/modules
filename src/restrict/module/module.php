<?php
defined('HOSTCMS') || exit('HostCMS: access denied.');
/**
 * Module.
 *
 * @package HostCMS 6\Restrict
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
class Restrict_Module extends Core_Module
{
	/**
	 * Module version
	 * @var string
	 */
	public $version = '0.1';

	/**
	 * Module date
	 * @var date
	 */
	public $date = '2016-09-02';

	/**
	 * Module form settings
		array(
			'form1' => array(										//-- Форма
				'guid' => 'XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX',		//-- guid формы
				'key_field' => 'id',									//-- Наименование ключевого поля из БД
				'default_order_field' => 'id',							//-- Поле сортировки по умолчанию
				'on_page' => 30,										//-- Количество строк на странице
				'show_operations' => 0,									//-- Показывать операции
				'show_group_operations' => 0,							//-- Показывать групповые операции
				'default_order_direction' => 0,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
				'name' => array(										//-- название формы
					1 => 'Наименование модуля',							//-- по-русски - 1=идентификатор языка
					2 => 'Module name'									//-- по-английски - 2=идентификатор языка
				),
				'fields' => array(										//-- поля на отображаемой форме
					'id' => array(										//-- наименование поля из сущности БД(название столбца)
						'name' => array(								//-- название поля в админке
							1 => 'Код',									//-- по-русски - 1=идентификатор языка
							2 => 'ID'									//-- по-английски - 2=идентификатор языка
						),
						'sorting' => 10,								//-- поле сортировки
						'ico' => '',									//-- иконка поля
						'type' => 1,									//-- тип поля
						'format' => '',									//-- формат поля
						'allow_sorting' => 1,							//-- разрешить сортировку по полю 0-нет, 1-да
						'allow_filter' => 1,							//-- разрешить фильтрацию по полю 0-нет, 1-да
						'editable' => 1,								//-- разрешить inline-редактирование по полю 0-нет, 1-да
						'filter_type' => 0,								//-- тип фильтрации
						'class' => '',									//-- класс для поля
						'width' => '',									//-- ширина поля, например '55px'
						'image' => '',									//-- картинка для поля
						'link' => '',									//-- ссылка для поля
						'onclick' => '',								//-- событие нажатия на поле
						'list' => '',									//--
					),
					'field2' =>  array(
						'name' => array(								//-- название поля в админке
							1 => 'Наименование кампании',				//-- по-русски - 1=идентификатор языка
							2 => 'Campaign name'						//-- по-английски - 2=идентификатор языка
						),
						'sorting' => 20,
						.....
					),
				),
				'actions' => array (
					'apply' => array (									//-- ключевое наименование действия для формы
						'name' => array (								//-- название действия в админке
							1 => 'Код',									//-- по-русски - 1=идентификатор языка
							2 => 'ID'									//-- по-английски - 2=идентификатор языка
						),
						'sorting' => 1000,								//-- сортировка для действий
						'picture' => '/admin/images/apply.gif',			//--
						'icon' => 'fa fa-check',
						'color' => 'palegreen',
						'single' => 0,
						'group' => 0,
						'dataset' => '-1',
						'confirm' => 0,
					),
					'action2' => array ( ... )
				)
			),
	 		'form2' => array( ... )
		)
	 * @var afSettings
	 */
	protected $_afSettings = array(
		'restrict' => array(											//-- Форма
			'guid' => '16CD2C5D-9BC1-937E-4FC7-AFCA22EA0FA4',		//-- guid формы
			'key_field' => 'id',									//-- Наименование ключевого поля из БД
			'default_order_field' => 'active',							//-- Поле сортировки по умолчанию
			'on_page' => 30,										//-- Количество строк на странице
			'show_operations' => 1,									//-- Показывать операции
			'show_group_operations' => 1,							//-- Показывать групповые операции
			'default_order_direction' => 1,							//-- Направление сортировки: 1 - по возрастанию, 0 - по убыванию
			'name' => array(										//-- название формы
				1 => 'Ограничения пользователя',					//-- по-русски - 1=идентификатор языка
				2 => 'Siteuser restrictions'						//-- по-английски - 2=идентификатор языка
			),
			'fields' => array(										//-- поля на отображаемой форме
				'id' => array(										//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Код',									//-- по-русски - 1=идентификатор языка
						2 => 'ID'									//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- поле сортировки
					'width' => '60px',								//-- ширина поля, например '55px'
				),
				'company' =>  array(
					'name' => array(								//-- название поля в админке
						1 => 'Компания, должность',					//-- по-русски - 1=идентификатор языка
						2 => 'Company'								//-- по-английски - 2=идентификатор языка
					),
					'allow_sorting' => 0,							//-- Сортировать
					'allow_filter' => 1,							//-- разрешить фильтрацию по полю 0-нет, 1-да
					'sorting' => 20,
					'width' => '10%',								//-- ширина поля, например '55px'
				),
				'login' =>  array(
					'name' => array(								//-- название поля в админке
						1 => 'Логин',					//-- по-русски - 1=идентификатор языка
						2 => 'Company'								//-- по-английски - 2=идентификатор языка
					),
					'allow_sorting' => 0,							//-- Сортировать
					'allow_filter' => 1,							//-- разрешить фильтрацию по полю 0-нет, 1-да
					'sorting' => 30,
					'width' => '7%',								//-- ширина поля, например '55px'
				),
				'userInfo' => array(								//-- наименование поля из сущности БД(название столбца)
					'name' => array(								//-- название поля в админке
						1 => 'Пользователь',						//-- по-русски - 1=идентификатор языка
						2 => 'User'									//-- по-английски - 2=идентификатор языка
					),
					'type' => 10,									//-- тип профиля, вычисляемое поле
					'allow_sorting' => 0,							//-- Сортировать
//					'link' => '/admin/direct/campaign/index.php?site_id={site_id}&profile_id={id}',
//					'onclick' => "$.adminLoad({path: '/admin/direct/campaign/index.php', additionalParams: 'site_id={site_id}&profile_id={id}', windowId: '{windowId}'}); return false",
					'sorting' => 40,								//-- поле сортировки
				),
				'active' =>  array(
					'name' => array(								//-- название поля в админке
						1 => '-',							//-- по-русски - 1=идентификатор языка
						2 => '-'								//-- по-английски - 2=идентификатор языка
					),
					'type' => 7,									//-- тип профиля, картинка-ссылка
					'image' => '1=/admin/images/check.gif=Enabled=fa fa-lightbulb-o fa-active
0=/admin/images/not_check.gif=Disabled=fa fa-lightbulb-o fa-inactive',
					'link' => '/admin/restrict/index.php?hostcms[action]=changeActive&hostcms[checked][{dataset_key}][{id}]=1',
					'onclick' => "$.adminLoad({path: '/admin/restrict/index.php',additionalParams: 'hostcms[checked][{dataset_key}][{id}]=1', action: 'changeActive', windowId: '{windowId}'}); return false",
					'allow_sorting' => 1,							//-- Сортировать
					'allow_filter' => 1,							//-- разрешить фильтрацию по полю 0-нет, 1-да
					'sorting' => 50,
					'width' => '25px',								//-- ширина поля, например '55px'
				),
			),
			'actions' => array(
				'changeActive' => array(							//-- ключевое наименование действия для формы
					'name' => array(								//-- название действия в админке
						1 => 'Вкл/выкл пользователя',				//-- по-русски - 1=идентификатор языка
						2 => 'Change active'						//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 10,								//-- сортировка для действий
//					'picture' => '/admin/images/edit.gif',			//--
//					'icon' => 'fa fa-check',
//					'color' => 'palegreen',
//					'single' => 0,
//					'group' => 0,
					'dataset' => -1,
//					'confirm' => 0,
				),
				'markDeleted' => array(							//-- ключевое наименование действия для формы
					'name' => array(							//-- название действия в админке
						1 => 'Удалить пользователя',			//-- по-русски - 1=идентификатор языка
						2 => 'Delete user'						//-- по-английски - 2=идентификатор языка
					),
					'sorting' => 20,								//-- сортировка для действий
					'picture' => '/admin/images/delete.gif',			//--
					'icon' => 'fa fa-trash-o',
					'color' => 'darkorange',
					'single' => 1,
					'group' => 0,
					'dataset' => -1,
					'confirm' => 1,
				),
//				'apply' => array(									//-- ключевое наименование действия для формы
//					'name' => array(								//-- название действия в админке
//						1 => 'Применить',							//-- по-русски - 1=идентификатор языка
//						2 => 'Apply'								//-- по-английски - 2=идентификатор языка
//					),
//					'sorting' => 20,								//-- сортировка для действий
//					'picture' => '/admin/images/apply.gif',			//--
//					'icon' => 'fa fa-check',
//					'color' => 'palegreen',
//					'single' => 0,
//					'group' => 0,
//					'dataset' => '-1',
//					'confirm' => 0,
//				),
			),
		),
	);

	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Core_Event::attach('siteuser.onCalluserInfo', array('Restrict_Observers_Siteuser', 'onCalluserInfo'));
		Core_Event::attach('Core.onAfterInitConstants', array('Restrict_Observers_Core', 'onAfterInitConstants'));
		Core_Event::attach('Restrict_Observers_Core.onAfterInitConstants', array('Restrict_Observers_Core', 'onAfterInitConstants'));
		Core_Skin::instance()->addCss('/admin/restrict/css/restrict.css');
		Core_Skin::instance()->addJs('/admin/restrict/js/restrict.js');

		Core_Page::instance()->addAllowedProperty('restrictByIP');
		Core_Page::instance()->restrictByIP = FALSE;

		$this->menu = array(
			array(
				'sorting' => 270,
				'block' => 1,
				'ico' => 'fa fa-user-times',
				'name' => Core::_('Restrict.model_name'),
				'href' => "/admin/restrict/index.php",
				'onclick' => "$.adminLoad({path: '/admin/restrict/index.php'});return false"
			)
		);
	}

	/**
	 * Install module.
	 */
	public function install() {
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			//-- Добавляем форму --
			$adminForm = $this->adminForm($afSetting['name'], $afSetting);
		}
	}

	/**
	 * Uninstall.
	 */
	public function uninstall()
	{
		foreach ($this->_afSettings as $afSettingKey => $afSetting) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (!is_null($oAdmin_Form)) {
				// Удаление формы
				$oAdmin_Form->delete();
			}
		}
	}

	protected function adminForm($afName, $afSetting) {
		$oAdmin_Form = NULL;
		if(isset($afSetting['guid'])) {
			$oAdmin_Form = Core_Entity::factory('Admin_Form')->getByGuid($afSetting['guid']);
			if (is_null($oAdmin_Form)) {
				$oAdmin_Word_Form = $this->adminWordForm($afName);
				$oAdmin_Form = Core_Entity::factory('Admin_Form');
				$oAdmin_Form->admin_word_id = $oAdmin_Word_Form->id;
				$oAdmin_Form->key_field = $afSetting['key_field'];
				$oAdmin_Form->default_order_field = Core_Array::get($afSetting, 'default_order_field', $afSetting['key_field']);

				$oAdmin_Form->on_page = Core_Array::get($afSetting, 'on_page', 20);
				$oAdmin_Form->show_operations = Core_Array::get($afSetting, 'show_operations', 1);
				$oAdmin_Form->show_group_operations = Core_Array::get($afSetting, 'show_group_operations', 0);
				$oAdmin_Form->default_order_direction = Core_Array::get($afSetting, 'default_order_direction', 0);
				$oAdmin_Form->guid = $afSetting['guid'];
				$oAdmin_Form->save();
			}
			if(isset($afSetting['fields']) && is_array($afSetting['fields'])) {
				//-- Добавляем поля в форму --
				foreach ($afSetting['fields'] as $dbFieldKey => $field) {
					$adminFormField = $this->adminFormField($dbFieldKey, $field);
					$oAdmin_Form->add($adminFormField);
				}
			}
			if(isset($afSetting['actions']) && is_array($afSetting['actions'])) {
				//-- Добавляем действия в форму --
				foreach ($afSetting['actions'] as $dbActionKey => $action) {
					$adminFormAction = $this->adminFormAction($dbActionKey, $action);
					$oAdmin_Form->add($adminFormAction);
				}
			}
		}

		return $oAdmin_Form;
	}

	protected function adminFormAction($fDbName, $action) {
		$oAdmin_Word_Form = $this->adminWordForm($action['name']);
		$oAdmin_Form_Action = Core_Entity::factory('Admin_Form_Action')->save();
		$oAdmin_Form_Action->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Action->name = $fDbName;
		$oAdmin_Form_Action->picture = Core_Array::get($action, 'picture', '');
		$oAdmin_Form_Action->icon = Core_Array::get($action, 'icon', '');
		$oAdmin_Form_Action->color = Core_Array::get($action, 'color', '');
		$oAdmin_Form_Action->single = Core_Array::get($action, 'single', 0);
		$oAdmin_Form_Action->group = Core_Array::get($action, 'group', 0);
		$oAdmin_Form_Action->sorting = Core_Array::get($action, 'sorting', 1000);
		$oAdmin_Form_Action->dataset = Core_Array::get($action, 'dataset', '-1');
		$oAdmin_Form_Action->confirm = Core_Array::get($action, 'confirm', 0);
//		$oAdmin_Form_Action->save();
		return $oAdmin_Form_Action;
	}

	protected function adminFormField($fDbName, $field) {
		$oAdmin_Word_Form = $this->adminWordForm($field['name']);
		$oAdmin_Form_Field = Core_Entity::factory('Admin_Form_Field')->save();
		$oAdmin_Form_Field->admin_word_id = $oAdmin_Word_Form->id;
		$oAdmin_Form_Field->name = $fDbName;
		$oAdmin_Form_Field->sorting = Core_Array::get($field, 'sorting', 1000);
		$oAdmin_Form_Field->ico = Core_Array::get($field, 'ico', '');
		$oAdmin_Form_Field->type = Core_Array::get($field, 'type', 1);
		$oAdmin_Form_Field->format = Core_Array::get($field, 'format', '');
		$oAdmin_Form_Field->allow_sorting = Core_Array::get($field, 'allow_sorting', 1);
		$oAdmin_Form_Field->allow_filter = Core_Array::get($field, 'allow_filter', 1);
		$oAdmin_Form_Field->editable = Core_Array::get($field, 'editable', 0);
		$oAdmin_Form_Field->filter_type = Core_Array::get($field, 'filter_type', 0);
		$oAdmin_Form_Field->class = Core_Array::get($field, 'class', '');
		$oAdmin_Form_Field->width = Core_Array::get($field, 'width', '');
		$oAdmin_Form_Field->image = Core_Array::get($field, 'image', '');
		$oAdmin_Form_Field->link = Core_Array::get($field, 'link', '');
		$oAdmin_Form_Field->onclick = Core_Array::get($field, 'onclick', '');
		$oAdmin_Form_Field->list = Core_Array::get($field, 'list', '');
		return $oAdmin_Form_Field;
	}

	protected function adminWordForm($afName) {
		$oAdmin_Word_Form = Core_Entity::factory('Admin_Word')->save();
		foreach ($afName as $langID => $langName) {
			$oAdmin_Word_Value = $this->adminWordValue($langID, $langName);
			$oAdmin_Word_Form->add($oAdmin_Word_Value);
		}
		return $oAdmin_Word_Form;
	}

	protected function adminWordValue($language_id, $name) {
		$oAdmin_Word_Value = Core_Entity::factory('Admin_Word_Value')->save();
		$oAdmin_Word_Value->admin_language_id = $language_id;
		$oAdmin_Word_Value->name = $name;
		$oAdmin_Word_Value->save();
		return $oAdmin_Word_Value;
	}
}