<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Restricts.
 *
 * @package HostCMS 6\Restrict
 * @version 6.x
 * @author Борисов Михаил Юрьевич
 * @copyright © Борисов Михаил Юрьевич
 */
class Restrict_Siteuser_Group_Model extends Core_Entity {
	/**
	 * Disable markDeleted()
	 * @var mixed
	 */
	protected $_marksDeleted = NULL;

	public function getSuGroupsBySiteuser($siteuser_id) {
//		$mSuGroups = Core_Entity::factory('Siteuser_Group');
//		$mSuGroups
//			->queryBuilder()
//			->join(array('siteuser_group_lists', 'sgl'), 'siteuser_groups.id', '=', 'sgl.siteuser_group_id')
//			->join(array('restrict_siteuser_groups', 'rsg'), 'rsg.siteuser_group_id', '=', 'siteuser_groups.id')
//			->where('sgl.siteuser_id', '=', $siteuser_id)
//			->orderBy('siteuser_groups.name')
//		;
//		$mSuGroups = Core_Entity::factory('Restrict_Siteuser_Group');
//		$mSuGroups
//			->queryBuilder()
//			->join(array('siteuser_groups', 'sg'), 'sg.id', '=', 'restrict_siteuser_groups.siteuser_group_id')
//			->join(array('siteuser_group_lists', 'sgl'), 'sg.id', '=', 'sgl.siteuser_group_id')
//			->where('sgl.siteuser_id', '=', $siteuser_id)
//			->orderBy('sg.name')
//		;
		$mSuGroups = Core_QueryBuilder::select('sg.*')
			->select(array('rsg.description', 'sgdescription'))
			->select(array('sgl.id', 'group_list_id'))
			->select(array('rsg.name', 'restrict_name'))
			->select(array('rsg.img_on', 'restrict_img_on'))
			->select(array('rsg.img_off', 'restrict_img_off'))
			->from(array('siteuser_groups', 'sg'))
			->join(array('siteuser_group_lists', 'sgl'), 'sg.id', '=', 'sgl.siteuser_group_id')
			->join(array('restrict_siteuser_groups', 'rsg'), 'rsg.siteuser_group_id', '=', 'sg.id')
			->where('sgl.siteuser_id', '=', $siteuser_id)
			->orderBy('sg.name')
		;

		$arSuGroups = $mSuGroups->asAssoc()->execute()->result();
		return $arSuGroups;
	}

	public function checkUserRestrictions() {
		$instance = Core_Page::instance();
		$currentSiteUser = $instance->siteuser;
		$currentUser = Core_Entity::factory("User")->getCurrent();
		if(!is_null($currentUser) && $currentUser->superuser==1) {
			$instance->restrictByIP = true;
			return true;
		}
		if(isset($currentSiteUser->id) && $currentSiteUser->id>0 && isset($instance->srcrequest->remoteip)) {
			$qCheckUser = Core_QueryBuilder::select('rus.ip')
				->from(array('restrict_url_sugroups', 'rus'))
				->join(array('siteuser_group_lists', 'sgl'), 'sgl.siteuser_id', '=', 'rus.restrict_siteuser_group_id')
				->join(array('restrict_siteuser_groups', 'rsg'), 'rsg.siteuser_group_id', '=', 'sgl.siteuser_group_id')
				->where('rus.restrict_siteuser_group_id', '=', $currentSiteUser->id*1)
				->open()
				->where('rus.ip', 'IN', array($instance->srcrequest->remoteip, '0.0.0.0'))
					->setOr()
					->where('rsg.name', '=', 'RESTRICT_SUPERADMIN')
				->close()
				->where('rus.type', 'IN', array('user'))
				->groupBy('rus.ip')
			;
			$arCheckUser = $qCheckUser->asAssoc()->execute()->result();
			$instance->restrictByIP = isset($arCheckUser[0]);

			if(!$instance->restrictByIP) {
				$qCheckGroup = Core_QueryBuilder::select('rus.ip')
					->from(array('restrict_url_sugroups', 'rus'))
					->join(array('restrict_siteuser_groups', 'rsg'), 'rsg.id', '=', 'rus.restrict_siteuser_group_id')
					->join(array('siteuser_group_lists', 'sgl'), 'sgl.siteuser_group_id', '=', 'rsg.siteuser_group_id')
					->join(array('siteusers', 's'), 's.id', '=', 'sgl.siteuser_id')
					->where('s.id', '=', $currentSiteUser->id*1)
					->where('rus.ip', '=', $instance->srcrequest->remoteip)
					->where('rus.type', 'IN', array('group'))
					->groupBy('rus.ip')
				;
				$arCheckGroup = $qCheckGroup->asAssoc()->execute()->result();
				$instance->restrictByIP = isset($arCheckGroup[0]);
			}
		}

		return $instance->restrictByIP;
	}

	/**
	 * Find all objects
	 * @param bool $bCache use cache, default TRUE
	 * @return array
	 */
	public function findAll($bCache = TRUE)
	{
		if (defined('CURRENT_SITE'))
		{
			$this->queryBuilder()
				->where($this->_tableName . '.site_id', '=', CURRENT_SITE);
		}
		return parent::findAll($bCache);
	}
}
