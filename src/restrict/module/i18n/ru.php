<?php
/**
 * Ограничения пользователя.
 *
 * @package HostCMS 6\Restrict
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	'undefined' => 'Неопределено',
	'model_name' => 'Ограничения пользователя',
	'menu' => 'Ограничения',
);