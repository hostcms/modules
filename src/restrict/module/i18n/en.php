<?php
/**
 * Restricts.
 *
 * @package HostCMS 6\Restrict
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © 2015 Борисов Михаил Юрьевич
 */
return array(
	'undefined' => 'Undefined',
	'model_name' => 'Siteuser restrictions',
	'menu' => 'Restrictions',
);