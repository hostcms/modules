<?php
use Utils_Utl as utl;
/**
 * Restrict.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Restrict
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Restrict_Observers_Core {

	static public function onAfterInitConstants($object, $args) {
		$instance = Core_Page::instance();

		$arIpGroups = Core_Entity::factory('Restrict_Url')->getUrlsByType();
		$arGroupIPs = utl::getArrayKeyValuesFromArrays($arIpGroups, 'name');
		$aGroupIPs = array();
		foreach ($arGroupIPs as $keyGroupIP => $arGroupIP) {
			$aGroupIPMatch = array();
			preg_match('/^\/(.*)\/$/', $keyGroupIP, $aGroupIPMatch);
			$aGroupIPs[] = str_replace('/', '\/', $aGroupIPMatch[1]);
		}
		$urls=implode('|', $aGroupIPs);
		$regexPattern = "/\/({$urls})\//i";
		$matches = array();
		$matchResult = preg_match($regexPattern, Core::$url['path'], $matches);
		$allowThisPage = TRUE;
		$mSuGroups = Core_Entity::factory('Restrict_Siteuser_Group');
		$mSuGroups->checkUserRestrictions();

		if($matchResult==1) {
			if(isset($instance->siteuser->id) && ($suID=$instance->siteuser->id)>0) {
				$arSuGroups = $mSuGroups->getSuGroupsBySiteuser($suID);
				$aSuGroups = utl::getArrayValuesFromArrays($arSuGroups);
				$arIpGroups = Core_Entity::factory('Restrict_Url')->getUrlsByType($aSuGroups);
				$arGroupIPs = utl::getArrayKeyValuesFromArrays($arIpGroups, 'ip');
				$arIpUsers = Core_Entity::factory('Restrict_Url')->getUrlsByType($aSuGroups, 'user', $instance->siteuser->id, true);
				$arIpUsersSA = utl::getArrayKeyValuesFromArrays($arIpUsers, 'rsg_name');
//				utl::p($arIpUsersSA);
				$arGroupIPs = array_merge($arGroupIPs, utl::getArrayKeyValuesFromArrays($arIpUsers, 'ip'));
//				utl::p($arGroupIPs);
				$allowThisPage = (isset($arGroupIPs[$instance->srcrequest->remoteip]))
					|| isset($arGroupIPs['0.0.0.0'])
					|| isset($arIpUsersSA['RESTRICT_SUPERADMIN']);
			} else {
				$regexPatternPart = "/^\/({$urls})\/$/i";
				$matchResultPart = preg_match($regexPatternPart, Core::$url['path']);
				$allowThisPage = ($matchResultPart==1);
			}
		}
		Core_Array::getRequest('action', 'none')=='exit' && $allowThisPage=TRUE;

		if(isset($instance->siteuser->id) && !$allowThisPage) {
			Core::$url['path'] = '/404/';
		}
	}
}