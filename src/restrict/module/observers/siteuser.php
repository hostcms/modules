<?php
use Utils_Utl as utl;
/**
 * Restrict.
 * Обработчики событий
 *
 * @package HostCMS
 * @subpackage Direct
 * @version 6.5.x
 * @author MikeBorisov
 * @copyright © 2005-2016 Михаил Борисов
 */
defined('HOSTCMS') || exit('HostCMS: access denied.');

class Restrict_Observers_Siteuser {

	static public function onCalluserInfo($object, $args)
	{
		ob_start();
		$row = Admin_Form_Entity::factory('Div')
			->class("restrict_group " . ($object->active == 0 ? 'disabled' : 'active'))
			->add($colTitle = Admin_Form_Entity::factory('Div')->class('col-lg-12 col-md-12 col-sm-12 col-xs-12'))
			->add($col1 = Admin_Form_Entity::factory('Div')->class('col-lg-4 col-md-4 col-sm-6 col-xs-12'))
			->add($col2 = Admin_Form_Entity::factory('Div')->class('col-lg-4 col-md-4 col-sm-6 col-xs-12'))
			->add($col3 = Admin_Form_Entity::factory('Div')->class('col-lg-4 col-md-4 col-sm-6 col-xs-12'))//			->add($col4 = Admin_Form_Entity::factory('Div')->class('col-lg-3 col-md-3 col-sm-6 col-xs-12'))
		;
		$siteUserName = trim(trim($object->name . ' ') . ' ' . trim($object->surname . ' ')) . " ($object->login)";
		$colTitle
			->class('header')
			->value($siteUserName);

		$arSuGroups = Core_Entity::factory('Restrict_Siteuser_Group')->getSuGroupsBySiteuser($object->id);
		$aSuGroups = utl::getArrayValuesFromArrays($arSuGroups);

		$allSuSubGroups = Core_Entity::factory('Restrict_Siteuser_Group');
		$allSuSubGroups
			->queryBuilder()
			->where('name', '<>', 'RESTRICT_ALL')
			->orderBy('sorting')
		;
		$allSuSubGroups = $allSuSubGroups
			->findAll(FALSE);
		$existedGroups = utl::getArrayKeyValuesFromArrays($arSuGroups, 'id');
		$allSuGroups = utl::getArrayKeyValuesFromArrays(utl::setArrayOfObjectsToArray($allSuSubGroups), 'groupby');
		foreach ($allSuGroups as $allSuGroupKey => $allSuGroup) {
			$allSuSubGroups = $allSuGroup;
			$col1->add(
				$userGroup = Admin_Form_Entity::factory('Div')
			);
			foreach ($allSuSubGroups as $allSuSubGroup) {
				trim($allSuSubGroup['img_on'])=='' &&  $allSuSubGroup['img_on'] = '/admin/restrict/images/check.gif';
				trim($allSuSubGroup['img_off'])=='' &&  $allSuSubGroup['img_off'] = '/admin/restrict/images/check_disabled.gif';
				$existInCurrentGroup = isset($existedGroups[$allSuSubGroup['siteuser_group_id']]);
				$existInCurrentGroupID = isset($existedGroups[$allSuSubGroup['siteuser_group_id']]) ? $existedGroups[$allSuSubGroup['siteuser_group_id']][0]['group_list_id'] : 0;
				$iconImagePath = $existInCurrentGroup ? $allSuSubGroup['img_on'] : $allSuSubGroup['img_off'];
				$image = Core::factory('Core_Html_Entity_A')
					->title($allSuSubGroup['description'])
					->add(
						Core::factory('Core_Html_Entity_Img')
							->src($iconImagePath)
					);
				if( $object->active==1 && $allSuSubGroup['name']!=='RESTRICT_ALL') {
					utl::divAttr($image, array(
						'ingroup-status' => $existInCurrentGroup ? 'on' : 'off'
					, 'ingroup-id' => $existInCurrentGroupID
					, 'siteuser' => $object->id
					, 'group' => $allSuSubGroup['siteuser_group_id']
					, 'img-on' => $allSuSubGroup['img_on']
					, 'img-off' => $allSuSubGroup['img_off']
					));
					$image
						->href('javascript:void(0);')
						->onclick('restrictUpdateSUstatus($(this)); return false;');
				}
				$userGroup->add($image);
			}
		}
		//-- IP адреса групп -------------------------------------------------------------------------------------------
		self::makeIpList("IP-адреса групп", $col2, $aSuGroups, 'group');
		self::makeIpList("IP-адреса пользователя", $col3, $aSuGroups, 'user', $object->id);

		$row->execute();
		$result = ob_get_clean();
		echo $result;
	}

	static public function makeIpList($title, $col, $aSuGroups, $type, $userID=0) {
		$col->add(
			$colTitle = Admin_Form_Entity::factory('Div')
				->value($title)
				->class("title")
		)->add(
			$userGroupURLs = Admin_Form_Entity::factory('Div')
		);
		$arIpGroups = Core_Entity::factory('Restrict_Url')->getUrlsByType($aSuGroups, $type, $userID);
		$arGroupIPs = utl::getArrayKeyValuesFromArrays($arIpGroups, 'name');
		$ipFor = '';
		foreach ($arGroupIPs as $arGroupURLkey => $arGroupUrl) {
			$userGroupURLs
				->add(
					Admin_Form_Entity::factory('Div')
						->class($type.'-ip')
						->add(
							Admin_Form_Entity::factory('Span')
								->class($type.'-url')
								->value($arGroupURLkey)
						)->add(
							$userGroupUrlIPs = Admin_Form_Entity::factory('Span')
						)
				);
			foreach ($arGroupUrl as $urlIPkey => $urlIP) {
				$userGroupUrlIPs->add(
					$userGroupUrlIPsInt = Core::factory('Core_Html_Entity_Span')->class('url_ip') // Admin_Form_Entity::factory('Span')
				);
				if($urlIPkey>0) {
					$userGroupUrlIPsInt->add(
						Admin_Form_Entity::factory('Code')->html(', ')
					);
				}
				$userGroupUrlIPsInt
					->add(
						Admin_Form_Entity::factory('Code')
							->html('<i class="fa fa-minus icon-separator" data-type="'.$type.'" data-group-id="'.$arGroupUrl[0]['id'].'" data-restrict-sugroups-id="'.$urlIP['restrict_id'].'"></i>')
					)->add(
						Admin_Form_Entity::factory('A')
							->value($urlIP['ip'])
					)
				;
			}
		}
		switch ($type) {
			case 'group':
				$ipFor = " для группы";
				break;
			case 'user':
				$ipFor = " для пользователя";
				break;
		}
		$colTitle->add(Admin_Form_Entity::factory('A')
			->onclick('addGroupModal(this, "'.(isset(array_keys($arGroupIPs)[0]) && isset($arGroupIPs[array_keys($arGroupIPs)[0]][0]['name'])?$arGroupIPs[array_keys($arGroupIPs)[0]][0]['name']:'').'", "getUserGroup", "ip'.$type.'", "Добавить IP на URL '.$ipFor.'", [], '.$userID.')')
			->add(
				Admin_Form_Entity::factory('Code')
					->html('<i class="fa fa-plus icon-separator"></i>')
			)
		);
	}
}