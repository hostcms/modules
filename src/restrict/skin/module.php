<?php
use Utils_Utl as utl;

defined('HOSTCMS') || exit('HostCMS: access denied.');

/**
 * Direct.
 *
 * @package HostCMS 6\Skin
 * @version 6.x
 * @author Hostmake LLC
 * @copyright © Михаил Борисов
 */
class Skin_Bootstrap_Module_Restrict_Module extends Restrict_Module
{
	/**
	 * Constructor.
	 */
	public function __construct()
	{
		parent::__construct();

		Core_Event::attach('Skin_Bootstrap.onLoadSkinConfig', array('Skin_Bootstrap_Module_Restrict_Module', 'onLoadSkinConfig'));
	}

	static public function onLoadSkinConfig($object, $args)
	{
		// Skin_Bootstrap_Admin_Form_Controller
		// Load config
		$aConfig = $object->getConfig();
		isset($aConfig['adminMenu']['users']) && $aConfig['adminMenu']['users']['modules'][] = 'restrict';
		isset($aConfig['adminMenu']['crm']) && $aConfig['adminMenu']['crm']['modules'][] = 'restrict';
		// Set new config
		$object->setConfig($aConfig);
	}
}